import React, { Component } from 'react';
import { FormLabel, Input } from 'react-native-elements'
import { StyleSheet,Text,View,FlatList, Alert, Dimensions, BackHandler } from 'react-native';
import Toast from 'react-native-easy-toast'

import { Button, Card, CardSection, ButtonGroupSection, InputMasked  } from './components/common';
import { Navigation } from 'react-native-navigation';
import { closeSidebar } from './navigation';
import { database }  from "./repositories/DatabaseManager";
import assessmentOptionsRepository from './repositories/assessmentOptionsRepository';
import assessmentsFormsRepository from './repositories/assessmentsFormsRepository';
import AssessmentScheduleRepository from './repositories/AssessmentScheduleRepository';
import assessmentResultRepository from './repositories/assessmentResultRepository';
import assessmentAnswerInfoRepository from './repositories/assessmentAnswerInfoRepository';

import { Loader } from './components/common/Loader';
import { DateTimeInputMask }  from './components/common/DateTimeInputMask'
import { getCurrentTimeToDatabase } from './repositories/BaseRepository';
import AssessmentRules from './Rules/AssessmentRules';
import AdmissionRepository from './repositories/AdmissionRepository';
import admissionDiagnosisRepository from './repositories/admissionDiagnosisRepository';
import WomanRepository from './repositories/WomanRepository';
import MeowsRules from './Rules/MeowsRules';
import PregnancyRepository from './repositories/PregnancyRepository';
import RulesControl from './Rules/RulesControl';
import SettingsRepository from './repositories/SettingsRepository';
import { calculateDilatationProgress } from './decision_models/Functions';


class CustomForm extends Component {

	constructor(props) {
		super(props)
		console.log(props);

		this.state = {
			questionList: [], formLabel: '', admissionId: null,
			assessmentAnswerInfo: { 
				id: undefined, begin_fill_form: "", 
				end_fill_form: "", assessment_form_id: "" 
			},
			admission:{}
		}

		//Navigation.events().bindComponent(this);
		
		this.navigationEventListener = Navigation.events().bindComponent(this);
		this.updateIndex = this.updateIndex.bind(this);
		this.leaveAndEvaluateAssessment = this.leaveAndEvaluateAssessment.bind(this);

		AdmissionRepository.getAdmissionById(this.props.adm_id).then((admission) => {
			this.state.admission = admission;
			console.log(this.state);
		});
	}
	
	showLoading = () => {    
		this.setState({animating: true});
	}

	hideLoading = () => {
		this.setState({animating: false});
	} 

	/************************* COMPONENTS LIVE CYCLE *****************************/
	componentDidMount()
	{
		console.log("PROPS CUSTOM " + JSON.stringify(this.props))
		this.showLoading();
		if(this.props.adm_id != undefined && this.props.adm_id > 0 && (this.props.form_name == undefined || this.props.form_name == '') && this.props.scheduled_form_id == undefined) {
			AssessmentScheduleRepository.getLastAssessmentScheduleByAdmissionId(this.props.adm_id)
			.then((scheduled) => {
				if(scheduled != undefined && scheduled.assessments_forms_id > 0) {
					this.setState({"scheduled_id": scheduled.id});
					console.log("SCHEDULED ID: " + this.state.scheduled_id)
					assessmentsFormsRepository.getassessmentsFormsById(scheduled.assessments_forms_id)
					.then((data) => {
						this.setQuestionList(data);
						this.hideLoading();
					});
				}
				else {
					assessmentsFormsRepository.getAssessmentFormByName("first_assessment")
					.then((data) => {
						this.setQuestionList(data);
						this.hideLoading();
					});
				}	
			});
		}
		else if (this.props.adm_id != undefined && this.props.adm_id > 0 && this.props.form_name != undefined && this.props.form_name != '' && 
			this.props.scheduled_form_id != undefined && this.props.scheduled_form_id > 0) {
				AssessmentScheduleRepository.getAssessmentScheduleById(this.props.scheduled_form_id)
				.then((scheduled) => {
					if(scheduled != undefined && scheduled.assessments_forms_id > 0) {
						this.setState({"scheduled_id": scheduled.id});
						assessmentsFormsRepository.getassessmentsFormsById(scheduled.assessments_forms_id)
						.then((data) => {
							this.setQuestionList(data);
							this.hideLoading();
						});
					}
					else {
						assessmentsFormsRepository.getAssessmentFormByName("first_assessment")
						.then((data) => {
							this.setQuestionList(data);
							this.hideLoading();
						});
					}	
				});
		}
		else if(this.props.form_name != undefined)
		{	
			assessmentsFormsRepository.getAssessmentFormByName(this.props.form_name)
			.then((data) => {
				this.setQuestionList(data);
				this.hideLoading();
			});	
		}
		else {
			assessmentsFormsRepository.getAssessmentFormByName("first_assessment")
			.then((data) => {
				this.setQuestionList(data);
				this.hideLoading();
			});
		}	
		
		if(this.props.adm_id != undefined && this.props.adm_id > 0){
			admissionDiagnosisRepository.getadmissionDiagnosisByAdmissionIdForRules(this.props.adm_id)
			.then((diagnosis) => {
				////console.log("DIAGNOSIS " + JSON.stringify(diagnosis))
				this.state.diagnosis = diagnosis;
			});
			WomanRepository.getWomanByAdmissionId(this.props.adm_id).then((woman) => {
				this.state.woman = woman;
				PregnancyRepository.getPregnancyByWomanId(woman.id).then((pregnancy) => {
					this.state.pregnancy = pregnancy;
				})
			});
		}
		
		//console.log("************************ dta ***************************");
		SettingsRepository.getAllSettingsForRules().then((settings) => {
			this.state["settings"] = settings;
		});
		BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
		
		////console.log("NAVIGATION UNMOUNTED;")
		if (this.navigationEventListener) {
			this.navigationEventListener.remove();
		}
		return false;
	}
			
	
	/************************* NAVIGATION HANDLERS *****************************/
		
	navigationButtonPressed({buttonId}) {
		console.log(buttonId);
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId === 'sidebarButton') {
			closeSidebar();
		}else if(buttonId === 'backButton'){
			if(this.state.questionList == undefined){
				console.log("TESTE 2");
				console.log("TESTE 2");
				console.log("TESTE 2");
				console.log("TESTE 2");
				console.log("TESTE 2");
				console.log("TESTE 2");
				console.log("TESTE 2");
				Navigation.pop(this.props.componentId);
			} 
			else if(!this.checkIfFormIsCompleted()){	
				//this.leaveAndEvaluateAssessment();
			}
		}
	}


	onBackPress = () => {
		return this.checkIfFormIsCompleted();
	}

	onNavigatorEvent(event) {
		//console.log("NAVIGATION BUTTON PRESSED 1;")
		if (event.id === 'backPress') {
			//console.log("NAVIGATION BUTTON PRESSED 2;")
		}
	}


	/************************* CUSTOM FUNCTIONS *****************************/
	setQuestionList(data){
		this.setState({	questionList: data.questionList });
		if(data.questionList != undefined && data.questionList.length > 0)
		{
			this.setState({ formId : data.questionList[0].formId });
		}
		this.hideLoading();
	}
	
    leaveAndEvaluateAssessment(){     
		assessment = {}
		for (var i=0; i<this.state.questionList.length; i++){
			if(this.state.questionList[i].answer != undefined)
			{
				if(this.state.questionList[i].selectedIndex != undefined)
					assessment[this.state.questionList[i].questionName.toString()] = this.state.questionList[i].selectedIndex;
				else
					assessment[this.state.questionList[i].questionName.toString()] = this.state.questionList[i].answer;
			}
		}	
		
        var fact = { 
			assessment: assessment, 
			diagnosis: this.state.diagnosis, 
			pregnancy: this.state.pregnancy,
			settings: this.state.settings,
			admission: {	id: this.props.adm_id, 
							multiple_pregnancy: this.state.admission.multiple_pregnancy, 
							mode_onset_labour: this.state.admission.mode_onset_labour }, 
			woman: {name: this.state.woman.name}
		};
		
        RulesControl.control_rules(fact);
		//console.log(fact)   
		
		Navigation.pop(this.props.componentId);
	}

	
	//Confirme if all questions was answered
		//If completed, update time of end form
		//Else confirm is want to leave the form
	checkIfFormIsCompleted()
	{
		//console.log("QUESTION LIST " + this.state.questionList);
		if(this.state.questionList == undefined){
			return false;
		}
		// console.log("SCHEDULED ID: ")
		// console.log(this.state)

		var completedForm = this.state.questionList.every((element, index, array) => element['answer'] != undefined);
		
		if(!completedForm)
		{
			Alert.alert(
				'Incompleted assessment',
				"There are questions not answered. This can affect the evaluations and support decision. Are you sure you want to finish?",
				[
					{					
						text: 'No', onPress: () => { console.log('Ask me later pressed'); }
					},
					{
						text: 'Yes', 
						onPress: () => { 
							this.showLoading();
							
							if(this.state.assessmentAnswerInfo.id != undefined) 
							{
								assessmentAnswerInfoRepository.updateAssessmentTimeEndFillAnswerInfo(this.state.assessmentAnswerInfo.id);
							}
							AssessmentScheduleRepository.setAssessmentScheduleAsFilledIn(this.state.scheduled_id)
							.then(()=> {
								this.leaveAndEvaluateAssessment();
							})
						}
					}
				],
				{ cancelable: false }
			);
			return true;
		}
		else
		{
			if(this.state.assessmentAnswerInfo.id != undefined) 
			{
				assessmentAnswerInfoRepository.updateAssessmentTimeEndFillAnswerInfo(this.state.assessmentAnswerInfo.id);
			}
			//assessmentAnswerInfoRepository.updateAssessmentTimeEndFillAnswerInfo(this.state.assessmentAnswerInfo.id);
			AssessmentScheduleRepository.setAssessmentScheduleAsFilledIn(this.state.scheduled_id)
			.then(()=> {
				this.leaveAndEvaluateAssessment();
			})
			return false;		
		}
	}

	//Update the active button on custom form
	updateIndex(selectedIndex, questionId) {
		//adjustment to copy json
		var indexQuestion = this.state.questionList.map(function (e) { return e.questionId; }).indexOf(questionId);
		var itens = Object.assign([], this.state.questionList, []);

		itens[indexQuestion].selectedOptionId = itens[indexQuestion].options[selectedIndex].optionId;
		itens[indexQuestion].answer           = itens[indexQuestion].options[selectedIndex].label_reply;
		itens[indexQuestion].selectedIndex    = selectedIndex;
		
		this.setState({ questionList: itens });
		this.saveAssessmentResult(itens[indexQuestion].answer, itens[indexQuestion].questionId, itens[indexQuestion].selectedOptionId);
		
	}

	onChangeTextWithValidation(value, min, max)
	{
		if(value>max){

		}
		else if (value<min){
		}

	}

	onEndEditing(answer, question_id)
	{
		if(answer == undefined || answer == "") {
			return;
		}
		var indexQuestion = this.state.questionList.map(function (e) { return e.questionId; }).indexOf(question_id);
		
		var itens = Object.assign([], this.state.questionList, []);

		itens[indexQuestion].selectedOptionId = null;
		itens[indexQuestion].answer           = answer;
		itens[indexQuestion].selectedIndex    = null;
		
		this.setState({ questionList: itens });

		this.saveAssessmentResult(answer, question_id, null);
	}
	
	saveAssessmentResult(answer, question_id, assessment_options_id)
	{	      
		var assessmentResult = {
			answer: answer,                 
			admission_id: this.props.adm_id,            
			assessment_questions_id: question_id, 
			assessment_options_id: assessment_options_id,         
			forms_id: this.state.formId,                
			userBoldId: this.props.user_id,
			assessment_answer_info_id: this.assessment_answer_info_id 
		} 
		 
		if(this.state.assessmentAnswerInfo.id == undefined) 
		{
			assessmentAnswerInfo = {  
				begin_fill_form: getCurrentTimeToDatabase(), 
				end_fill_form: "", 
				assessment_form_id: this.state.formId,
				admission_id: this.props.adm_id
			}

			assessmentAnswerInfoRepository.insertassessmentAnswerInfo(assessmentAnswerInfo)
			.then((data) => {
				console.log("APOS SALVAR O INFO " + JSON.stringify(data))
				this.setState({assessmentAnswerInfo: data});
				assessmentResult.assessment_answer_info_id = data.id;
				this.callInsertAssessmentResult(assessmentResult);
			});
		}
		else{
			assessmentResult.assessment_answer_info_id = this.state.assessmentAnswerInfo.id;
			////console.log("ASSESSMENT RESULT " + JSON.stringify(assessmentResult))
			this.callInsertAssessmentResult(assessmentResult);
		}
	}

	callInsertAssessmentResult(assessmentResult){
		//console.log("SALVA ASSESSMENT " + JSON.stringify(assessmentResult));
		assessmentResultRepository.insertassessmentResult(assessmentResult)
		.then((data) => {			
			this.refs.toast.show('Saved', 500);
		});
	}



	render() {
		const { questionList } = this.state
		return (
			<View style={styles.container}>
		        <Toast ref="toast" position='top'/>			
				<Card>
					<FlatList
						style = {{width: dimensions.width}}
						data={questionList}
						keyExtractor={item => item.questionId.toString()}
						renderItem={({ item }) => {
							////console.log("ITEM " + (item.minimum_value) + " " + item.maximum_value)
							////console.log("ITEM " + JSON.stringify(item))
							return (							
								<CardSection style={{flex:1}}>
									{
										(item.options != null && item.options[0].optionId != null) ?
											<ButtonGroupSection
												label={item.label_question}
												id={item.questionId}
												onPress={(selectedIndex) => this.updateIndex(selectedIndex, item.questionId)}
												selectedIndex={item.selectedIndex}
												buttons={item.options.map((option) => { return (<Text> {option.label_reply} </Text>) })}
												containerStyle={{ height: null }}
												vertical={ (item.options.length <= 4 ? false : true) } 
											/>
											: (parseInt(item.type) <= 2 ) ?
											<InputMasked
												keyboardType="numeric"
												label={item.label_question}
												id={item.questionId} 
												onChangeText={(id) => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												minValue={item.minimum_value}
												maxValue={item.maximum_value}
											/>
											: (parseInt(item.type) == 4) ?
											<DateTimeInputMask
												label={item.label_question}
												id={item.questionId} 
												options={{ format: 'DD/MM/YYYY HH:mm' }}	
												placeholder="dd-MM-yyyy hh:mm"
												value={ item.answer == undefined ? "" : `${item.answer}` }
												onChangeText={() => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												errorMessage={""}
												/>
											: (parseInt(item.type) == 5) ?
											<DateTimeInputMask
												label={item.label_question}
												id={item.questionId} 
												options={{ format: 'DD-MM-YYYY' }}		
												placeholder="dd-MM-yyyy"
												value={ item.answer == undefined ? "" : `${item.answer}` }
												onChangeText={() => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												errorMessage={""}
												/>
											: (parseInt(item.type) == 6) ? 
											<DateTimeInputMask
												label={item.label_question}
												id={item.questionId} 
												options={{ format: 'HH:mm' }}	
												placeholder="HH:mm"		
												value={ item.answer == undefined ? "" : `${item.answer}` }
												onChangeText={() => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												errorMessage={""}
												/>													
											:
											<InputMasked
												label={item.label_question}
												id={item.questionId} 
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												minValue={item.minimum_value}
												maxValue={item.maximum_value}
											/>

									}
								</CardSection>
							)
						}}
					/>							
					<Loader animating = { this.state.animating } />
				</Card>
			</View>
		);
	}
}


const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
		alignSelf: 'stretch'
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	datetimeStyle: {
		width: '80%', 
		fontSize: 16,
		marginLeft: 5
	}
});

export default CustomForm;
