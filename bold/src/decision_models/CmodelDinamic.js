import { diffTimeInMinutes, diffTimeNowInMinutes } from "../repositories/BaseRepository";

//calculate time since admission
export function cmodelDynamic(data) {
    new_data["uterine_height"]                              = data.admission.symphysis_fundal_height  ;
    new_data["dilation"]                                    = data.assessment.cervical_dilatation_cm  ;
    new_data["age"]                                         = data.woman.age  ;
    new_data["maternal_height"]                             = data.antenatal_care.height  ;

    bmi = data.antenatal_care.weight/(data.antenatal_care.height*data.antenatal_care.height);
    
    new_data["low_weight"] =  
            (bmi<=23.9 && data.pregnancy.estimate_gestional_age == 34) ||
            (bmi<=24.1 && data.pregnancy.estimate_gestional_age == 35) ||
            (bmi<=24.2 && data.pregnancy.estimate_gestional_age == 36) ||
            (bmi<=24.4 && data.pregnancy.estimate_gestional_age == 37) ||
            (bmi<=24.5 && data.pregnancy.estimate_gestional_age == 38) ||
            (bmi<=24.7 && data.pregnancy.estimate_gestional_age == 39) ||
            (bmi<=24.9 && data.pregnancy.estimate_gestional_age == 40) ||
            (bmi<=25   && data.pregnancy.estimate_gestional_age == 41) ||
            (bmi<=25   && data.pregnancy.estimate_gestional_age == 42) ? 1 : 0;

    new_data["overweight"] =
            (bmi>=28.4 && bmi<=32.5 && data.pregnancy.estimate_gestional_age ==34) ||
            (bmi>=28.5 && bmi<=32.6 && data.pregnancy.estimate_gestional_age ==35) ||
            (bmi>=28.6 && bmi<=32.7 && data.pregnancy.estimate_gestional_age ==36) ||
            (bmi>=28.8 && bmi<=32.8 && data.pregnancy.estimate_gestional_age ==37) ||
            (bmi>=28.9 && bmi<=32.9 && data.pregnancy.estimate_gestional_age ==38) ||
            (bmi>=29   && bmi<=33   && data.pregnancy.estimate_gestional_age ==39) ||
            (bmi>=29.2 && bmi<=33.1 && data.pregnancy.estimate_gestional_age ==40) ||
            (bmi>=29.3 && bmi<=33.2 && data.pregnancy.estimate_gestional_age ==41) ||
            (bmi>=29.3 && bmi<=33.2 && data.pregnancy.estimate_gestional_age ==42) ? 1 : 0;

    new_data["obese"] = 
            (bmi>=32.6 && data.pregnancy.estimate_gestional_age ==34) ||
            (bmi>=32.7 && data.pregnancy.estimate_gestional_age ==35) ||
            (bmi>=32.8 && data.pregnancy.estimate_gestional_age ==36) ||
            (bmi>=32.9 && data.pregnancy.estimate_gestional_age ==37) ||
            (bmi>=33   && data.pregnancy.estimate_gestional_age ==38) ||
            (bmi>=33.1 && data.pregnancy.estimate_gestional_age ==39) ||
            (bmi>=33.2 && data.pregnancy.estimate_gestional_age ==40) ||
            (bmi>=33.3 && data.pregnancy.estimate_gestional_age ==41) ||
            (bmi>=33.3 && data.pregnancy.estimate_gestional_age ==42) ? 1 : 0;
   
   
    new_data["mode_onset_labor_induced"]                    = data.admission.mode_onset_labour  ;

    if(data.pregnancy.parity == 0){
        new_data["parity_multi_without_CS"]                 = 0;
        new_data["parity_multi_with_CS"]                    = 0;
    }
    else if (data.pregnancy.parity > 0 ){
        new_data["parity_multi_without_CS"]                 = data.pregnancy.num_previous_c_section == 0 ? 0 : 1;
        new_data["parity_multi_with_CS"]                    = data.pregnancy.num_previous_c_section >= 1 ? 0 : 1;
    }    
    
    new_data["prominent_sciatic_spines"]                    = data.assessment.ischial_spines_prominent  ;
    
    new_data["position_cervix_central"]                     = data.assessment.cervix_position == 1 ? 1 : 0 ;
    new_data["position_cervix_posterior"]                   = data.assessment.cervix_position == 2 ? 1 : 0   ;

    new_data["gestational_age_preterm"]                     = data.pregnancy.estimate_gestional_age <= 36 ? 1 : 0  ;
    new_data["gestational_age_postterm"]                    = data.pregnancy.estimate_gestional_age >= 42 ? 1 : 0  ;

    new_data["fetal_presentation_transverse_cephalic"]      = data.assessment.fetal_presentation == 0 && data.assessment.position_fetal_head == 1 ? 1 : 0  ;
    new_data["fetal_presentation_posterior_cephalic"]       = data.assessment.fetal_presentation == 0 && data.assessment.position_fetal_head == 2 ? 1 : 0  ;
    new_data["fetal_presentation_other"]                    = data.assessment.fetal_presentation == 0 && data.assessment.position_fetal_head == 3 ? 1 : 0  ;

    new_data["mhr_40_49_or_101_110"]                        = (data.assessment.maternal_heart_rate >= 40 && data.assessment.maternal_heart_rate <= 49 ) || (data.assessment.maternal_heart_rate >= 101 && data.assessment.maternal_heart_rate <= 110) ? 1 : 0 ;
    new_data["mhr_under_40_or_over_110"]                    = data.assessment.maternal_heart_rate < 40 || data.assessment.maternal_heart_rate >= 110 ? 1 : 0 ;
    new_data["systolic_bp_80_89_or_140_149"]                = (data.assessment.systolic_blood_pressure >= 80 && data.assessment.systolic_blood_pressure <= 89) || (data.assessment.systolic_blood_pressure >= 140 && data.assessment.systolic_blood_pressure <= 149) ? 1 : 0 ;
    new_data["systolic_bp_71_79_or_150_159"]                = (data.assessment.systolic_blood_pressure >= 71 && data.assessment.systolic_blood_pressure <= 79) || (data.assessment.systolic_blood_pressure >= 150 && data.assessment.systolic_blood_pressure <= 159) ? 1 : 0 ;
    new_data["systolic_bp_under_70_over_160"]               = (data.assessment.systolic_blood_pressure < 70 || data.assessment.systolic_blood_pressure > 160) ? 1 : 0 ;
    new_data["number_of_contractions_under_3"]              = data.assessment.number_uterine_contractions < 3 ? 1 : 0 ;
    new_data["suspicion_fetal_distress_yes"]                = (data.assessment.fetal_heart_rate <= 120 || data.assessment.fetal_heart_rate >= 160 ) ? 1 : 0 ;
    new_data["fetal_movements"]                             = data.assessment.fetal_movements_last_2h  ;
    new_data["fetal_descent_at_below_ischial_spines"]       = data.assessment.fetal_station < 2 ? 1 : 0 ;
    new_data["amniotic_membrane_ruptured_with_meconium"]    = data.amniotic_membranes_status >= 2 ? 1 : 0 ;
    new_data["amniotic_membrane_ruptured_without_meconium"] = data.amniotic_membranes_status == 1 ? 1 : 0 ;
    new_data["pain_moderate_extreme"]                       = data.assessment.painful_distress >=2 ? 1 : 0 ;
    new_data["time_between_adm_active_over_8h"]             = diffTimeInMinutes(data.admission.createdAt, data.diagnosis.active_labour.createdAt)/60 > 8 ? 1 : 0 ;

    time_since_4_cm = diffTimeNowInMinutes(data.cervical_dilatation_4_createdAt)/60;


    var percentage = null;
    if(time_since_4_cm <= 2) {
        percentage = calculateCSection2Hours(new_data);

        if(percentage == null)
            percentage = calculateCSection2HoursReduced(new_data);

    }
    else if (time_since_4_cm <= 4){
        percentage = calculateCSection4Hours(new_data);

        if(percentage == null)
            percentage = calculateCSection4HoursReduced(new_data);

    }
    else if(time_since_4_cm <= 6){
        percentage = calculateCSection6Hours(new_data);

        if(percentage == null)
            percentage = calculateCSection6HoursReduced(new_data);

    }

}

///Admission model
calculateCSectionAtAdmission = function(data) 
{
    var beta                                             = -4.611;
    var coef_uterine_height                              =  0.176;
    var coef_dilation                                    = -0.158;
    var coef_age                                         =  0.002;
    var coef_maternal_height                             = -0.020;
    var coef_low_weight                                  = -0.150;
    var coef_overweight                                  =  0.239;
    var coef_obese                                       =  0.337;
    var coef_mode_onset_labor_induced                    =  0.039;
    var coef_parity_multi_without_CS                     = -1.275;
    var coef_parity_multi_with_CS                        =  0.868;
    var coef_prominent_sciatic_spines                    =  1.107;
    var coef_position_cervix_central                     = -0.082;
    var coef_position_cervix_osterior                    =  0.308;
    var coef_gestational_age_preterm                     = -0.214;
    var coef_gestational_age_postterm                    =  0.329;
    var coef_fetal_presentation_transverse_cephalic      =  1.139;
    var coef_fetal_presentation_posterior_cephalic       =  0.889;
    var coef_fetal_presentation_other                    =  0.688;
    var coef_mhr_40_49_or_101_110                        =  0.156;
    var coef_mhr_under_40_or_over_110                    =  0.560;
    var coef_systolic_bp_80_89_or_140_149                = -0.020;
    var coef_systolic_bp_71_79_or_150_159                =  0.356;
    var coef_systolic_bp_under_70_over_160               =  0.146;
    var coef_number_of_contractions_under_3              =  0.555;
    var coef_suspicion_fetal_distress_yes                =  0.657;
    var coef_fetal_movements                             = -0.405;
    var coef_fetal_descent_at_below_ischial_spines       = -0.535;
    var coef_amniotic_membrane_ruptured_with_meconium    =  0.758;
    var coef_amniotic_membrane_ruptured_without_meconium = -0.041;
    var coef_pain_moderate_extreme                       = -0.178;

    try {
        var Logit = beta + 
                (coef_uterine_height                              * data.uterine_height                             ) + 
                (coef_dilation                                    * data.dilation                                   ) + 
                (coef_age                                         * data.age                                        ) + 
                (coef_maternal_height                             * data.maternal_height                            ) + 
                (coef_low_weight                                  * data.low_weight                                 ) + 
                (coef_overweight                                  * data.overweight                                 ) + 
                (coef_obese                                       * data.obese                                      ) + 
                (coef_mode_onset_labor_induced                    * data.mode_onset_labor_induced                   ) + 
                (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                    ) + 
                (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                       ) + 
                (coef_prominent_sciatic_spines                    * data.prominent_sciatic_spines                   ) + 
                (coef_position_cervix_central                     * data.position_cervix_central                    ) + 
                (coef_position_cervix_osterior                    * data.position_cervix_osterior                   ) + 
                (coef_gestational_age_preterm                     * data.gestational_age_preterm                    ) + 
                (coef_gestational_age_postterm                    * data.gestational_age_postterm                   ) + 
                (coef_fetal_presentation_transverse_cephalic      * data.fetal_presentation_transverse_cephalic     ) + 
                (coef_fetal_presentation_posterior_cephalic       * data.fetal_presentation_posterior_cephalic      ) + 
                (coef_fetal_presentation_other                    * data.fetal_presentation_other                   ) + 
                (coef_mhr_40_49_or_101_110                        * data.mhr_40_49_or_101_110                       ) + 
                (coef_mhr_under_40_or_over_110                    * data.mhr_under_40_or_over_110                   ) + 
                (coef_systolic_bp_80_89_or_140_149                * data.systolic_bp_80_89_or_140_149               ) + 
                (coef_systolic_bp_71_79_or_150_159                * data.systolic_bp_71_79_or_150_159               ) + 
                (coef_systolic_bp_under_70_over_160               * data.systolic_bp_under_70_over_160              ) + 
                (coef_number_of_contractions_under_3              * data.number_of_contractions_under_3             ) + 
                (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes               ) + 
                (coef_fetal_movements                             * data.fetal_movements                            ) + 
                (coef_fetal_descent_at_below_ischial_spines       * data.fetal_descent_at_below_ischial_spines      ) + 
                (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium   ) + 
                (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium) + 
                (coef_pain_moderate_extreme                       * data.pain_moderate_extreme                      );
        
        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
        
    }
    catch(err) {
        return null;
    }
}

//2h model completed
calculateCSection2Hours = function(data) 
{
    beta                                            	= -5.894;
    coef_uterine_height                              	=  0.130;
    coef_dilation                                    	= -0.232;
    coef_age                                         	=  0.005;
    coef_maternal_height                             	=  0.001;
    coef_low_weight                                  	= -0.437;
    coef_overweight                                  	=  0.421;
    coef_obese                                       	=  0.302;
    coef_mode_onset_labor_induced                    	=  0.183;
    coef_parity_multi_without_CS                     	= -1.086;
    coef_parity_multi_with_CS                        	=  0.729;
    coef_prominent_sciatic_spines                    	=  1.762;
    coef_position_cervix_central                     	=  0.346;
    coef_position_cervix_osterior                    	=  0.602;
    coef_gestational_age_preterm                     	= -0.361;
    coef_gestational_age_postterm                    	=  0.161;
    coef_mhr_40_49_or_101_110                        	=  0.041;
    coef_mhr_under_40_or_over_110                    	=  0.728;
    coef_systolic_bp_80_89_or_140_149                	=  0.253;
    coef_systolic_bp_71_79_or_150_159                	=  0.321;
    coef_systolic_bp_under_70_over_160               	=  1.172;
    coef_number_of_contractions_under_3              	=  0.009;
    coef_suspicion_fetal_distress_yes                	=  1.207;
    coef_fetal_movements                             	= -0.431;
    coef_fetal_descent_at_below_ischial_spines       	= -0.716;
    coef_amniotic_membrane_ruptured_with_meconium    	=  0.513;
    coef_amniotic_membrane_ruptured_without_meconium 	=  1.589;
    coef_pain_moderate_extreme                       	=  0.118;
    coef_time_between_adm_active_over_8h                =  0.433;

    try{
        var Logit = beta + 
                    (coef_uterine_height                              * data.uterine_height                              	) + 	
                    (coef_dilation                                    * data.dilation                                    	) + 	
                    (coef_age                                         * data.age                                         	) + 	
                    (coef_maternal_height                             * data.maternal_height                             	) + 	
                    (coef_low_weight                                  * data.low_weight                                  	) + 	
                    (coef_overweight                                  * data.overweight                                  	) + 	
                    (coef_obese                                       * data.obese                                       	) + 	
                    (coef_mode_onset_labor_induced                    * data.mode_onset_labor_induced                    	) + 	
                    (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                     	) + 	
                    (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                        	) + 	
                    (coef_prominent_sciatic_spines                    * data.prominent_sciatic_spines                    	) + 	
                    (coef_position_cervix_central                     * data.position_cervix_central                     	) + 	
                    (coef_position_cervix_osterior                    * data.position_cervix_osterior                    	) + 	
                    (coef_gestational_age_preterm                     * data.gestational_age_preterm                     	) + 	
                    (coef_gestational_age_postterm                    * data.gestational_age_postterm                    	) + 	
                    (coef_mhr_40_49_or_101_110                        * data.mhr_40_49_or_101_110                        	) + 	
                    (coef_mhr_under_40_or_over_110                    * data.mhr_under_40_or_over_110                    	) + 	
                    (coef_systolic_bp_80_89_or_140_149                * data.systolic_bp_80_89_or_140_149                	) + 	
                    (coef_systolic_bp_71_79_or_150_159                * data.systolic_bp_71_79_or_150_159                	) + 	
                    (coef_systolic_bp_under_70_over_160               * data.systolic_bp_under_70_over_160               	) + 	
                    (coef_number_of_contractions_under_3              * data.number_of_contractions_under_3              	) + 	
                    (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes                	) + 	
                    (coef_fetal_movements                             * data.fetal_movements                             	) + 	
                    (coef_fetal_descent_at_below_ischial_spines       * data.fetal_descent_at_below_ischial_spines       	) + 	
                    (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium    	) + 	
                    (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium 	) + 	
                    (coef_pain_moderate_extreme                       * data.pain_moderate_extreme                       	) + 	
                    (coef_time_between_adm_active_over_8h             * data.time_between_adm_active_over_8h                 )  

        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
        
    }
    catch(err) {
        return null;
    }
}

//2h model reduced
calculateCSection2HoursReduced = function(data) 
{
    beta                                        	    = -5.326;
    coef_uterine_height                              	=  0.134;
    coef_dilation                                    	= -0.257;
    coef_low_weight                                  	= -0.469;
    coef_overweight                                  	=  0.421;
    coef_obese                                       	=  0.464;
    coef_parity_multi_without_CS                     	= -1.184;
    coef_parity_multi_with_CS                        	=  0.905;
    coef_prominent_sciatic_spines                    	=  2.150;
    coef_position_cervix_central                     	=  0.197;
    coef_position_cervix_osterior                    	=  0.442;
    coef_systolic_bp_80_89_or_140_149                	=  0.078;
    coef_systolic_bp_71_79_or_150_159                	=  0.603;
    coef_systolic_bp_under_70_over_160               	=  0.840;
    coef_suspicion_fetal_distress_yes                	=  0.790;
    coef_fetal_movements                             	= -0.388;
    coef_fetal_descent_at_below_ischial_spines       	= -0.441;
    coef_amniotic_membrane_ruptured_with_meconium    	=  0.500;
    coef_amniotic_membrane_ruptured_without_meconium 	=  1.537;
    coef_time_between_adm_active_over_8h	            =  0.410;

    try {
        var Logit = beta +                                     
            (coef_uterine_height                              * data.uterine_height                              )+
            (coef_dilation                                    * data.dilation                                    )+
            (coef_low_weight                                  * data.low_weight                                  )+
            (coef_overweight                                  * data.overweight                                  )+
            (coef_obese                                       * data.obese                                       )+
            (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                     )+
            (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                        )+
            (coef_prominent_sciatic_spines                    * data.prominent_sciatic_spines                    )+
            (coef_position_cervix_central                     * data.position_cervix_central                     )+
            (coef_position_cervix_osterior                    * data.position_cervix_osterior                    )+
            (coef_systolic_bp_80_89_or_140_149                * data.systolic_bp_80_89_or_140_149                )+
            (coef_systolic_bp_71_79_or_150_159                * data.systolic_bp_71_79_or_150_159                )+
            (coef_systolic_bp_under_70_over_160               * data.systolic_bp_under_70_over_160               )+
            (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes                )+
            (coef_fetal_movements                             * data.fetal_movements                             )+
            (coef_fetal_descent_at_below_ischial_spines       * data.fetal_descent_at_below_ischial_spines       )+
            (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium    )+
            (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium )+
            (coef_time_between_adm_active_over_8h	          * data.time_between_adm_active_over_8h	            );


        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
        
    }
    catch(err) {
        return null;
    }
}

//4h model completed
calculateCSection4Hours = function(data) 
{
    beta                                        	    = -0.113;
    coef_uterine_height                              	=  0.043;
    coef_dilation                                    	= -0.689;
    coef_age                                         	=  0.036;
    coef_maternal_height                             	= -0.005;
    coef_low_weight                                  	= -0.399;
    coef_overweight                                  	=  0.211;
    coef_obese                                       	=  0.458;
    coef_mode_onset_labor_induced                    	=  0.262;
    coef_parity_multi_without_CS                     	= -1.160;
    coef_parity_multi_with_CS                        	=  0.138;
    coef_prominent_sciatic_spines                    	=  1.519;
    coef_position_cervix_central                     	= -0.066;
    coef_position_cervix_osterior                    	=  0.025;
    coef_gestational_age_preterm                     	= -0.682;
    coef_gestational_age_postterm                    	=  1.008;
    coef_mhr_40_49_or_101_110                        	=  1.441;
    coef_mhr_under_40_or_over_110                    	=  0.248;
    coef_systolic_bp_80_89_or_140_149                	=  0.441;
    coef_systolic_bp_71_79_or_150_159                	= -0.340;
    coef_systolic_bp_under_70_over_160               	= -1.029;
    coef_number_of_contractions_under_3              	=  0.362;
    coef_suspicion_fetal_distress_yes                	=  2.509;
    coef_fetal_movements                             	=  0.046;
    coef_fetal_descent_at_below_ischial_spines       	= -0.160;
    coef_amniotic_membrane_ruptured_with_meconium    	=  0.617;
    coef_amniotic_membrane_ruptured_without_meconium 	=  1.336;
    coef_pain_moderate_extreme                       	=  0.583;
    coef_time_between_adm_active_over_8h	            = -0.321;

    try {
        var Logit = beta + 
                    (coef_uterine_height                              * data.uterine_height                              ) +
                    (coef_dilation                                    * data.dilation                                    ) +
                    (coef_age                                         * data.age                                         ) +
                    (coef_maternal_height                             * data.maternal_height                             ) +
                    (coef_low_weight                                  * data.low_weight                                  ) +
                    (coef_overweight                                  * data.overweight                                  ) +
                    (coef_obese                                       * data.obese                                       ) +
                    (coef_mode_onset_labor_induced                    * data.mode_onset_labor_induced                    ) +
                    (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                     ) +
                    (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                        ) +
                    (coef_prominent_sciatic_spines                    * data.prominent_sciatic_spines                    ) +
                    (coef_position_cervix_central                     * data.position_cervix_central                     ) +
                    (coef_position_cervix_osterior                    * data.position_cervix_osterior                    ) +
                    (coef_gestational_age_preterm                     * data.gestational_age_preterm                     ) +
                    (coef_gestational_age_postterm                    * data.gestational_age_postterm                    ) +
                    (coef_mhr_40_49_or_101_110                        * data.mhr_40_49_or_101_110                        ) +
                    (coef_mhr_under_40_or_over_110                    * data.mhr_under_40_or_over_110                    ) +
                    (coef_systolic_bp_80_89_or_140_149                * data.systolic_bp_80_89_or_140_149                ) +
                    (coef_systolic_bp_71_79_or_150_159                * data.systolic_bp_71_79_or_150_159                ) +
                    (coef_systolic_bp_under_70_over_160               * data.systolic_bp_under_70_over_160               ) +
                    (coef_number_of_contractions_under_3              * data.number_of_contractions_under_3              ) +
                    (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes                ) +
                    (coef_fetal_movements                             * data.fetal_movements                             ) +
                    (coef_fetal_descent_at_below_ischial_spines       * data.fetal_descent_at_below_ischial_spines       ) +
                    (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium    ) +
                    (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium ) +
                    (coef_pain_moderate_extreme                       * data.pain_moderate_extreme                       ) +
                    (coef_time_between_adm_active_over_8h	          * data.time_between_adm_active_over_8h	            );

        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
        
    }
    catch(err) {
        return null;
    }
}


//4h model reduced 

calculateCSection4HoursReduced = function(data) 
{	
    beta                                             	=  0,724;
    coef_dilation                                    	= -0,686;
    coef_age                                         	=  0,035;
    coef_low_weight                                  	= -0,683;
    coef_overweight                                  	=  0,249;
    coef_obese                                       	=  0,577;
    coef_parity_multi_without_CS                     	= -1,360;
    coef_parity_multi_with_CS                        	=  0,425;
    coef_prominent_sciatic_spines                    	=  1,057;
    coef_gestational_age_preterm                     	= -1,136;
    coef_gestational_age_postterm                    	=  0,660;
    coef_mhr_40_49_or_101_110                        	=  0,914;
    coef_mhr_under_40_or_over_110                    	=  0,378;
    coef_number_of_contractions_under_3              	=  0,414;
    coef_suspicion_fetal_distress_yes                	=  2,121;
    coef_amniotic_membrane_ruptured_with_meconium    	=  0,658;
    coef_amniotic_membrane_ruptured_without_meconium 	=  1,321;
    coef_pain_moderate_extreme                       	=  0,447;

    try {
        var Logit = beta + 
                    (coef_dilation                                    * data.dilation                                    ) + 
                    (coef_age                                         * data.age                                         ) + 
                    (coef_low_weight                                  * data.low_weight                                  ) + 
                    (coef_overweight                                  * data.overweight                                  ) + 
                    (coef_obese                                       * data.obese                                       ) + 
                    (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                     ) + 
                    (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                        ) + 
                    (coef_prominent_sciatic_spines                    * data.prominent_sciatic_spines                    ) + 
                    (coef_gestational_age_preterm                     * data.gestational_age_preterm                     ) + 
                    (coef_gestational_age_postterm                    * data.gestational_age_postterm                    ) + 
                    (coef_mhr_40_49_or_101_110                        * data.mhr_40_49_or_101_110                        ) + 
                    (coef_mhr_under_40_or_over_110                    * data.mhr_under_40_or_over_110                    ) + 
                    (coef_number_of_contractions_under_3              * data.number_of_contractions_under_3              ) + 
                    (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes                ) + 
                    (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium    ) + 
                    (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium ) + 
                    (coef_pain_moderate_extreme                       * data.pain_moderate_extreme                       );
            
        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
        
    }
    catch(err) {
        return null;
    }
}

//6h model completed

calculateCSection6HoursReduced = function(data) 
{
    beta                                        	    = -9.510;
    coef_uterine_height                              	=  0.145;
    coef_dilation                                    	= -0.403;
    coef_age                                         	=  0.060;
    coef_maternal_height                             	=  0.015;
    coef_low_weight                                  	= -0.263;
    coef_overweight                                  	=  0.365;
    coef_obese                                       	=  0.433;
    coef_mode_onset_labor_induced                    	= -0.436;
    coef_parity_multi_without_CS                     	= -1.270;
    coef_parity_multi_with_CS                        	=  1.077;
    coef_position_cervix_central                     	= -0.207;
    coef_position_cervix_osterior                    	=  0.134;
    coef_gestational_age_preterm                     	=  0.048;
    coef_gestational_age_postterm                    	= -0.007;
    coef_mhr_40_49_or_101_110                        	=  1.377;
    coef_mhr_under_40_or_over_110                    	=  1.377;
    coef_systolic_bp_80_89_or_140_149                	= -1.190;
    coef_systolic_bp_71_79_or_150_159                	=  1.928;
    coef_systolic_bp_under_70_over_160               	=  1.157;
    coef_number_of_contractions_under_3              	=  0.839;
    coef_suspicion_fetal_distress_yes                	=  1.683;
    coef_fetal_movements                             	=  0.233;
    coef_fetal_descent_at_below_ischial_spines       	= -0.420;
    coef_amniotic_membrane_ruptured_with_meconium    	=  1.189;
    coef_amniotic_membrane_ruptured_without_meconium 	=  1.978;
    coef_pain_moderate_extreme                       	=  0.346;
    coef_time_between_adm_active_over_8h	            =  0.668;

    try {
        var Logit = beta + 
                    (coef_uterine_height                              * data.uterine_height                             ) +
                    (coef_dilation                                    * data.dilation                                   ) +
                    (coef_age                                         * data.age                                        ) +
                    (coef_maternal_height                             * data.maternal_height                            ) +
                    (coef_low_weight                                  * data.low_weight                                 ) +
                    (coef_overweight                                  * data.overweight                                 ) +
                    (coef_obese                                       * data.obese                                      ) +
                    (coef_mode_onset_labor_induced                    * data.mode_onset_labor_induced                   ) +
                    (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                    ) +
                    (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                       ) +
                    (coef_position_cervix_central                     * data.position_cervix_central                    ) +
                    (coef_position_cervix_osterior                    * data.position_cervix_osterior                   ) +
                    (coef_gestational_age_preterm                     * data.gestational_age_preterm                    ) +
                    (coef_gestational_age_postterm                    * data.gestational_age_postterm                   ) +
                    (coef_mhr_40_49_or_101_110                        * data.mhr_40_49_or_101_110                       ) +
                    (coef_mhr_under_40_or_over_110                    * data.mhr_under_40_or_over_110                   ) +
                    (coef_systolic_bp_80_89_or_140_149                * data.systolic_bp_80_89_or_140_149               ) +
                    (coef_systolic_bp_71_79_or_150_159                * data.systolic_bp_71_79_or_150_159               ) +
                    (coef_systolic_bp_under_70_over_160               * data.systolic_bp_under_70_over_160              ) +
                    (coef_number_of_contractions_under_3              * data.number_of_contractions_under_3             ) +
                    (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes               ) +
                    (coef_fetal_movements                             * data.fetal_movements                            ) +
                    (coef_fetal_descent_at_below_ischial_spines       * data.fetal_descent_at_below_ischial_spines      ) +
                    (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium   ) +
                    (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium) +
                    (coef_pain_moderate_extreme                       * data.pain_moderate_extreme                      ) +
                    (coef_time_between_adm_active_over_8h	          * data.time_between_adm_active_over_8h	        );

        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
         
    }
    catch(err) {
        return null;
    }
}

//6h model reduced 
	
calculateCSection6HoursReduced = function(data) 
{
    beta                                               	= -4.637;
    coef_uterine_height                              	=  0.123;
    coef_dilation                                    	= -0.494;
    coef_age                                         	=  0.020;
    coef_parity_multi_without_CS                     	= -0.832;
    coef_parity_multi_with_CS                        	=  1.637;
    coef_mhr_40_49_or_101_110                        	=  1.427;
    coef_mhr_under_40_or_over_110                    	=  1.427;
    coef_systolic_bp_80_89_or_140_149                	= -0.106;
    coef_systolic_bp_71_79_or_150_159                	=  1.241;
    coef_systolic_bp_under_70_over_160               	=  0.835;
    coef_number_of_contractions_under_3              	=  0.456;
    coef_suspicion_fetal_distress_yes                	=  1.187;
    coef_amniotic_membrane_ruptured_with_meconium    	=  1.492;
    coef_amniotic_membrane_ruptured_without_meconium 	=  2.413;
    coef_time_between_adm_active_over_8h	            =  0.529;

    try {
        var Logit = beta + 
                    (coef_uterine_height                              * data.uterine_height                              ) + 
                    (coef_dilation                                    * data.dilation                                    ) + 
                    (coef_age                                         * data.age                                         ) + 
                    (coef_parity_multi_without_CS                     * data.parity_multi_without_CS                     ) + 
                    (coef_parity_multi_with_CS                        * data.parity_multi_with_CS                        ) + 
                    (coef_mhr_40_49_or_101_110                        * data.mhr_40_49_or_101_110                        ) + 
                    (coef_mhr_under_40_or_over_110                    * data.mhr_under_40_or_over_110                    ) + 
                    (coef_systolic_bp_80_89_or_140_149                * data.systolic_bp_80_89_or_140_149                ) + 
                    (coef_systolic_bp_71_79_or_150_159                * data.systolic_bp_71_79_or_150_159                ) + 
                    (coef_systolic_bp_under_70_over_160               * data.systolic_bp_under_70_over_160               ) + 
                    (coef_number_of_contractions_under_3              * data.number_of_contractions_under_3              ) + 
                    (coef_suspicion_fetal_distress_yes                * data.suspicion_fetal_distress_yes                ) + 
                    (coef_amniotic_membrane_ruptured_with_meconium    * data.amniotic_membrane_ruptured_with_meconium    ) + 
                    (coef_amniotic_membrane_ruptured_without_meconium * data.amniotic_membrane_ruptured_without_meconium ) + 
                    (coef_time_between_adm_active_over_8h	          * data.time_between_adm_active_over_8h	         );
        
        
        CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
        return(CSprobability);
        
    }
    catch(err) {
        return null;
    }
}