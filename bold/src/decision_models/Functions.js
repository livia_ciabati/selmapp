import AdmissionRepository from "../repositories/AdmissionRepository";
import assessmentResultRepository from "../repositories/assessmentResultRepository";
import { diffTimeInMinutes } from "../repositories/BaseRepository";

export function calculateDilatationProgress(adm_id){
    return new Promise((resolve, reject) => {            
        AdmissionRepository.getAdmissionById(adm_id).then(() => {
            //get all dilation values
            return assessmentResultRepository.getAllAssessmentResultByNameAndAdmissionId("cervical_dilatation_cm", adm_id)
        }).then((graph_data) => {  
            console.log(graph_data);   
            dilatation_speed = undefined; 
            if(graph_data.length > 1){
                head = graph_data[0];
                tail = graph_data[graph_data.length-1];

                if((head.createdAt).length > 19){
                    head.createdAt = head.createdAt.substr(0, 19);
                }
                if((tail.createdAt).length > 19){
                    tail.createdAt = tail.createdAt.substr(0, 19);
                }

                head.value = parseInt(head.value.replace(/ cm/i, ''));
                tail.value = parseInt(tail.value.replace(/ cm/i, ''));

                diffHours = diffTimeInMinutes(tail.createdAt, head.createdAt)/60;
                diffDilatation = tail.value - head.value;
                dilatation_speed = diffDilatation/diffHours;
            }
            resolve(dilatation_speed);
        });
    });
}

export function calculateUrineVolume(adm_id) {
    return new Promise((resolve, reject) => {            
        AdmissionRepository.getAdmissionById(adm_id).then(() => {
            //get all dilation values
            return assessmentResultRepository.getAllAssessmentResultByNameAndAdmissionId("urine_output", adm_id)
        }).then((graph_data) => {  
            console.log(graph_data);  
            urine_volume_per_hour = undefined;   
            if(graph_data.length > 1){
                head = graph_data[0];
                tail = graph_data[graph_data.length-1];

                if((head.createdAt).length > 19){
                    head.createdAt = head.createdAt.substr(0, 19);
                }
                if((tail.createdAt).length > 19){
                    tail.createdAt = tail.createdAt.substr(0, 19);
                }

                diffHours = diffTimeInMinutes(tail.createdAt, head.createdAt)/60;

                volumeTotal = 0;
                graph_data.forEach(element => {
                    volumeTotal += parseInt(element.value)
                });
                urine_volume_per_hour = volumeTotal/diffHours;
                //resolve(urine_volume_per_hour);
            }
            resolve(urine_volume_per_hour);
        });
    });
}
