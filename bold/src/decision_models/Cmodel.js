export function cmodel(data) 
{
    //console.log("CMODEL");
    //console.log(data);
    if(
        data.parity != undefined
        && data.num_previous_c_section != undefined
        && data.multiple_pregnancy != undefined
        && data.mode_onset_labour != undefined
        && data.fetal_presentation != undefined
        && data.estimate_gestional_age != undefined
    )
    {
        Parity = (data.parity == 0 ? 0 : data.parity <= 2 ? 1 : 2);
        Prev_cs = (data.num_previous_c_section == 0 ? 0 : data.num_previous_c_section == 1 ? 1 : 2);
        Multiples = data.multiple_pregnancy;
        PIC = data.mode_onset_labour;
        FetPres = data.fetal_presentation;
        PreTerm = (data.estimate_gestional_age >= 37 ? 0 : 1);

        return this.calculateCSection_1(Parity, Prev_cs, Multiples, PIC, FetPres, PreTerm)
    }
    else if(
        data.parity != undefined
        && data.num_previous_c_section != undefined
        && data.multiple_pregnancy != undefined
        && data.mode_onset_labour != undefined
        && data.fetal_presentation != undefined
        && data.estimate_gestional_age != undefined
        && data.woman.age != undefined)
    {
        Parity = (data.parity == 0 ? 0 : data.parity <= 2 ? 1 : 2);
        Prev_cs = (data.num_previous_c_section == 0 ? 0 : data.num_previous_c_section == 1 ? 1 : 2);
        Multiples = data.multiple_pregnancy;
        PIC = data.mode_onset_labour;
        FetPres = data.fetal_presentation;
        PreTerm = (data.estimate_gestional_age >= 37 ? 0 : 1);
        Mat_Age = (data.woman.age < 20? 0 : data.woman.age <= 34? 1: 0 );
        
        return this.calculateCSection_1_1(Parity, Prev_cs, Multiples, PIC, FetPres, PreTerm, Mat_Age)
    }
    return undefined;
} 

calculateCSection_1 = function(Parity, Prev_cs, Multiples, PIC, FetPres, PreTerm) {
    Beta = -3.392134;
    
    x1 = -0.559968;
    x2 = 2.842534;
    x3 = 1.694844;
    x4 = 2.747953;
    x5 = 2.922391;
    x6 = 0.368073;
    
    Logit = Beta +  (x1 * Parity) + 
                    (x2 * Prev_cs) +  
                    (x3 * Multiples) + 
                    (x4 * PIC) + 
                    (x5 * FetPres) + 
                    (x6 * PreTerm);
    
    CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
    return(CSprobability);
  }  
  
  calculateCSection_1_1 = function(Parity, Prev_cs, Multiples, PIC, FetPres, PreTerm, Mat_Age){
      Beta = -3.992549;
      
      x1 = -0.760441;
      x2 = 2.873179;
      x3 = 1.722743;
      x4 = 2.708164;
      x5 = 2.911992;
      x6 = 0.364223;
      x7 = 0.734265;
      
      Logit = Beta + (x1 * Parity) + (x2 * Prev_cs) + (x3 * Multiples) + (x4 * PIC) + (x5 * FetPres) + (x6 * PreTerm) + (x7 * Mat_Age);	
      
      CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
      return(CSprobability);
      
  }
  
  calculateCSection_1_2 = function(Parity, Prev_cs, Multiples, PIC, FetPres, PreTerm, Mat_Age, Severity){
      Beta = -3.989357;
      
      x1 = -0.76173;
      x2 = 2.87813;
      x3 = 1.721366;
      x4 = 2.686502;
      x5 = 2.9241;
      x6 = 0.285275;
      x7 = 0.728236;
      x8 = 1.499462;
      
      Logit = Beta + (x1 * Parity) + (x2 * Prev_cs) + (x3 * Multiples) + (x4 * PIC) + (x5 * FetPres) + (x6 * PreTerm) + (x7 * Mat_Age) + (x8 * Severity);	
      
      CSprobability = Math.Math.exp(Logit) / (1 + Math.exp(Logit))
      return(CSprobability);
      
  }
  
  calculateCSection_1_3 = function(Parity, Prev_cs, Multiples, PIC, FetPres, Mat_Age, Severity, PlaPrev, AbrPLac, ChrHyp, PreECLAM, RenalD, HIV){
      Beta = -4.015252;
      
      x1 = -0.77531;
      x2 = 2.922222;
      x3 =1.834027;
      x4 =2.634921;
      x5 = 2.985162;
      x7 = 0.71104;
      x8 = 0.661417;
      x9 = 3.796513;
      x10 = 2.741255;
      x11 = 0.561991;
      x12 = 0.98718;
      x13 = 1.301346;
      x14 =	1.310211;
      
      
      Logit = Beta + (x1 * Parity) + (x2 * Prev_cs) + (x3 * Multiples) + (x4 * PIC) + (x5 * FetPres) + (x7 * Mat_Age)
      + (x8 * Severity) + (x9 * PlaPrev) + (x10 * AbrPLac) + (x11 * ChrHyp) + (x12 * PreECLAM) + (x13 * RenalD) + (x14 * HIV);
      
      CSprobability = Math.exp(Logit) / (1 + Math.exp(Logit))
      return(CSprobability);
  }