import { TensorFlow } from 'react-native-tensorflow';
 
import WomanRepository from '../repositories/WomanRepository';
import AdmissionRepository from '../repositories/AdmissionRepository';
import PregnancyRepository from '../repositories/PregnancyRepository';
import diseaseRepository from '../repositories/diseaseRepository';
import antenatalCareRepository from '../repositories/antenatalCareRepository';
import DashboardRepository from '../repositories/DashboardRepository';

export default class MachineLearning {
 
  

  static load = async(vector_for_ml) =>
  { 
    // const tf = new TensorFlow({
    //   model: require('../../assets/tf_model_final.pb'),
    //   labels: require('../../assets/outcome.txt')
    // });

    //console.log("entrou load");
    const tf = new TensorFlow("tf_model_final.pb");
    console.log("1");
    test = await tf.feed({name: "dense_3_input", data: vector_for_ml, /*shape:[96],*/ dtype: "float"})
    console.log("2");
    //test2 = await tf.run(['outputNames']);
    //console.log("3");
    //const output = await tf.fetch('outputName');
    //console.log("4");
    //console.log(output)    
  }


  static prepCaseForMLEvaluation = async (woman_id, admission_id) => {

    objeto = {};
    woman = WomanRepository.getWomanById(woman_id)
      .then((data) => {
        objeto["woman"] = data;
    });

    admission = AdmissionRepository.getAdmissionById(admission_id)
      .then((data) => {      
        objeto["admission"] = data;
    })

    pregnancy = PregnancyRepository.getPregnancyByWomanId(woman_id)
      .then((data) => {
        objeto["pregnancy"] = data;
    });

    disease = diseaseRepository.getDiseasesByWomanId(woman_id)
      .then((data) => {
        objeto["disease"] = data;
      });

    antenatal = antenatalCareRepository.getAntenatalCareByWomanId(woman_id)
      .then((data) => {
        objeto["antenatal_care"] = data;
      })

    first_assessment = DashboardRepository.getAssessmentResultsByAdmId(admission_id, first=true)
      .then(data => {
        objeto["first_assessment"] = data;
      });
    

    assessment = DashboardRepository.getAssessmentResultsByAdmId(admission_id)
      .then(data => {
        objeto["assessment"] = data;
      });

    promiseArray = [woman, admission, pregnancy, disease, antenatal, first_assessment, assessment];
    const bar = await Promise.all(promiseArray);
    console.log(objeto);

    vector_for_ml = {
      'marital_status'                                : objeto.woman.marital_status                                         ,                                              
      'education_level'                               : objeto.woman.education_level                                        ,
      'gainful_occupation'                            : objeto.woman.gainful_occupation                                     ,
      'age'                                           : objeto.woman.age                                                    ,
      'outcome_of_last_pregNAncy'                     : objeto.pregnancy.outcome_last_childbirth                            ,
      'method_of_gestatioNAl_age'                     : objeto.pregnancy.estimate_gestional_age_method                      ,
      'gravidity'                                     : objeto.pregnancy.gravidity                                          ,
      'parity'                                        : objeto.pregnancy.parity                                             ,
      'previous_abortions'                            : objeto.pregnancy.num_previous_abort                                 ,
      'number_induced_abortion'                       : objeto.pregnancy.n_previous_induced_abort                           ,
      'num_spontaneous_abortions'                     : objeto.pregnancy.n_previous_spontaneous_abort                       ,
      'num_previous_stillbirth'                       : objeto.pregnancy.n_previous_stillbirths                             ,
      'previous_uterine_surgery'                      : objeto.pregnancy.previous_uterine_surgery                           ,
      'mode_of_onset_of_labour'                       : objeto.admission.mode_onset_labour                                  ,
      'duration_of_labour'                            : objeto.admission.hours_labour_before_admission                      ,
      'chronic_hypertension'                          : objeto.disease.chronic_hypertension                                 ,
      'diabetes'                                      : objeto.disease.diabetes_mellitus                                    ,
      'hiv'                                           : objeto.disease.hiv                                                  ,
      'aids_hiv_syndrome'                             : objeto.disease.aids_hiv                                             ,
      'chronic_aNAemia'                               : objeto.disease.chronic_anaemia                                      ,
      'obesity'                                       : objeto.disease.obesity                                              ,
      'heart_disease'                                 : objeto.disease.heart_disease                                        ,
      'lung_disease'                                  : objeto.disease.lung_disease                                         ,
      'reNAl_disease'                                 : objeto.disease.renal_disease                                        ,
      'other_chronic_disease'                         : objeto.disease.other_chronic_disease                                ,
      'current_weight_kg'                             : objeto.antenatal_care.weight                                        ,
      'height_cm'                                     : objeto.antenatal_care.height                                        ,
      'placenta_praevia'                              : objeto.antenatal_care.placenta_praevia                              ,
      'accreta_increta_percreta'                      : objeto.antenatal_care.accreta_increta_percreta                      ,
      'abruptio_placentae'                            : objeto.antenatal_care.abruptio_placentae                            ,
      'other_obstetric_haemorrhag'                    : objeto.antenatal_care.other_obstetric_haemorrhage                   ,
      'pre_eclampsia'                                 : objeto.antenatal_care.pre_eclampsia                                 ,
      'eclampsia'                                     : objeto.antenatal_care.eclampsia                                     ,
      'pyelonephritis'                                : objeto.antenatal_care.pyelonephritis                                ,
      'malaria'                                       : objeto.antenatal_care.malaria                                       ,
      'preterm_rupture_membrane'                      : objeto.antenatal_care.preterm_rupture_membranes                     ,
      'aNAemia'                                       : objeto.antenatal_care.anaemia                                       ,
      'gestatioNAl_diabetes'                          : objeto.antenatal_care.gestational_diabetes                          ,
      'other_pregNAncy_complications'                 : objeto.antenatal_care.other_pregnancy_complications                 ,
      'fetal_movements_last_2h'                       : objeto.first_assessment.fetal_movements_last_2h                     ,
      'cervix_effacement'                             : objeto.first_assessment.cervix_effacement                           ,
      'fetal_presentation'                            : objeto.first_assessment.fetal_presentation                          ,
      'fetal_station'                                 : objeto.first_assessment.fetal_station                               ,
      'position_fetal_head'                           : objeto.first_assessment.position_fetal_head                         ,
      'caput_succedaneum'                             : objeto.first_assessment.caput_succedaneum                           ,
      'moulding'                                      : objeto.first_assessment.moulding                                    ,
      'cervix_position'                               : objeto.first_assessment.cervix_position                             ,
      'cervix_consistency'                            : objeto.first_assessment.cervix_consistency                          ,
      'materNAl_heart_rate'                           : objeto.first_assessment.maternal_heart_rate                         ,
      'systolic_blood_pressure'                       : objeto.first_assessment.systolic_blood_pressure                     ,
      'diastolic_blood_pressure'                      : objeto.first_assessment.diastolic_blood_pressure                    ,
      'axillary_temperature'                          : objeto.first_assessment.axillary_temperature                        ,
      'symphysis_fundal_height_cm'                    : objeto.first_assessment.symphysis_fundal_height_cm                  ,
      'abdomiNAl_circumference_cm'                    : objeto.first_assessment.abdomiNAl_circumference_cm                  ,
      'number_uterine_contractions'                   : objeto.first_assessment.number_uterine_contractions                 ,
      'duration_uterine_contractions'                 : objeto.first_assessment.duration_uterine_contractions               ,
      'fetal_heart_rate_bpm'                          : objeto.first_assessment.fetal_heart_rate                            ,
      'cervical_dilatation_cm'                        : objeto.first_assessment.cervical_dilatation_cm                      ,
      'history_prolonged_labour'                      : objeto.first_assessment.history_prolonged_labour                    ,
      'c1_fetal_movements'                            : objeto.assessment.fetal_movements_last_2h                           ,
      'c1_fetal_presentation'                         : objeto.assessment.fetal_presentation                                ,
      'c1_fetal_station'                              : objeto.assessment.fetal_station                                     ,
      'c1_moulding'                                   : objeto.assessment.moulding                                          ,
      'c1_caput_succedaneum'                          : objeto.assessment.caput_succedaneum                                 ,
      'c1_position_of_fetal_head'                     : objeto.assessment.position_fetal_head                               ,
      'c1_amniotic_membranes_status'                  : objeto.assessment.amniotic_membranes_status                         ,
      'c1_dyas_blood_pressure'                        : objeto.assessment.diastolic_blood_pressure                          ,
      'c1_temperature_c'                              : objeto.assessment.axillary_temperature                              ,
      'c1_woman_mental_status'                        : objeto.assessment.woman_mental_status                               ,
      'c1_painful_distress'                           : objeto.assessment.painful_distress                                  ,
      'c1_aNAlgesia'                                  : objeto.assessment.analgesia                                         ,
      'c1_companionship'                              : objeto.assessment.labour_companionship                              ,
      'c1_materNAl_position'                          : objeto.assessment.maternal_position                                 ,
      'c1_oral_fluid_intake'                          : objeto.assessment.oral_fluid_intake                                 ,
      'c1_oral_food_intake'                           : objeto.assessment.oral_food_intake                                  ,
      'c1_iv_fluids'                                  : objeto.assessment.iv_fluids                                         ,
      'c1_number_uterine_contraction'                 : objeto.assessment.number_uterine_contractions                       ,
      'c1_duration_uterine_contraction'               : objeto.assessment.duration_uterine_contractions                     ,
      'c1_fetal_heart_rate'                           : objeto.assessment.fetal_heart_rate                                  ,
      'c1_cervical_dilatation'                        : objeto.assessment.cervical_dilatation_cm                            ,
      'c1_syst_blood_pressure'                        : objeto.assessment.systolic_blood_pressure                           ,
      'c1_materNAl_heart_rate'                        : objeto.assessment.maternal_heart_rate                               ,
      'c1_oxytocin_infusion'                          : objeto.assessment.oxytocin_infusion                                 ,
      'c1_oxytocin_dilution'                          : objeto.assessment.oxytocin_dilution                                 ,
      'c1_oxytocin_rate_drops'                        : objeto.assessment.oxytocin_rate_drops                               ,
      'foot_length_cm'                                : "?"                                                              ,
      'fetal_movements'                               : "?"                                                              , //fetal_movements at admission
    }

    vector_for_ml = {
      'marital_status'                                : 1,                                              
      'education_level'                               : 1,
      'gainful_occupation'                            : 1,
      'age'                                           : 1,
      'outcome_of_last_pregNAncy'                     : 1,
      'method_of_gestatioNAl_age'                     : 1,
      'gravidity'                                     : 1,
      'parity'                                        : 1,
      'previous_abortions'                            : 1,
      'number_induced_abortion'                       : 1,
      'num_spontaneous_abortions'                     : 1,
      'num_previous_stillbirth'                       : 1,
      'previous_uterine_surgery'                      : 1,
      'mode_of_onset_of_labour'                       : 1,
      'duration_of_labour'                            : 1,
      'chronic_hypertension'                          : 1,
      'diabetes'                                      : 1,
      'hiv'                                           : 1,
      'aids_hiv_syndrome'                             : 1,
      'chronic_aNAemia'                               : 1,
      'obesity'                                       : 1,
      'heart_disease'                                 : 1,
      'lung_disease'                                  : 1,
      'reNAl_disease'                                 : 1,
      'other_chronic_disease'                         : 1,
      'current_weight_kg'                             : 1,
      'height_cm'                                     : 1,
      'placenta_praevia'                              : 1,
      'accreta_increta_percreta'                      : 1,
      'abruptio_placentae'                            : 1,
      'other_obstetric_haemorrhag'                    : 1,
      'pre_eclampsia'                                 : 1,
      'eclampsia'                                     : 1,
      'pyelonephritis'                                : 1,
      'malaria'                                       : 1,
      'preterm_rupture_membrane'                      : 1,
      'aNAemia'                                       : 1,
      'gestatioNAl_diabetes'                          : 1,
      'other_pregNAncy_complications'                 : 1,
      'fetal_movements_last_2h'                       : 1,
      'cervix_effacement'                             : 1,
      'fetal_presentation'                            : 1,
      'fetal_station'                                 : 1,
      'position_fetal_head'                           : 1,
      'caput_succedaneum'                             : 1,
      'moulding'                                      : 1,
      'cervix_position'                               : 1,
      'cervix_consistency'                            : 1,
      'materNAl_heart_rate'                           : 1,
      'systolic_blood_pressure'                       : 1,
      'diastolic_blood_pressure'                      : 1,
      'axillary_temperature'                          : 1,
      'symphysis_fundal_height_cm'                    : 1,
      'abdomiNAl_circumference_cm'                    : 1,
      'number_uterine_contractions'                   : 1,
      'duration_uterine_contractions'                 : 1,
      'fetal_heart_rate_bpm'                          : 1,
      'cervical_dilatation_cm'                        : 1,
      'history_prolonged_labour'                      : 1,
      'c1_fetal_movements'                            : 1,
      'c1_fetal_presentation'                         : 1,
      'c1_fetal_station'                              : 1,
      'c1_moulding'                                   : 1,
      'c1_caput_succedaneum'                          : 1,
      'c1_position_of_fetal_head'                     : 1,
      'c1_amniotic_membranes_status'                  : 1,
      'c1_dyas_blood_pressure'                        : 1,
      'c1_temperature_c'                              : 1,
      'c1_woman_mental_status'                        : 1,
      'c1_painful_distress'                           : 1,
      'c1_aNAlgesia'                                  : 1,
      'c1_companionship'                              : 1,
      'c1_materNAl_position'                          : 1,
      'c1_oral_fluid_intake'                          : 1,
      'c1_oral_food_intake'                           : 1,
      'c1_iv_fluids'                                  : 1,
      'c1_number_uterine_contraction'                 : 1,
      'c1_duration_uterine_contraction'               : 1,
      'c1_fetal_heart_rate'                           : 1,
      'c1_cervical_dilatation'                        : 1,
      'c1_syst_blood_pressure'                        : 1,
      'c1_materNAl_heart_rate'                        : 1,
      'c1_oxytocin_infusion'                          : 1,
      'c1_oxytocin_dilution'                          : 1,
      'c1_oxytocin_rate_drops'                        : 1,
      'foot_length_cm'                                : 1,
      'fetal_movements'                               : 1, //fetal_movements at admission
    }
    console.log(vector_for_ml);
    resp = await MachineLearning.load(vector_for_ml);
    console.log(resp);
  }
}