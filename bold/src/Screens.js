import { Navigation } from 'react-native-navigation';

export function registerScreens() {
  Navigation.registerComponent('Login'                 , () => require('./components/LoginForm').default);
  Navigation.registerComponent('WomanList'             , () => require('./WomanList').default);
  Navigation.registerComponent('WomanForm'             , () => require('./WomanForm').default);
  Navigation.registerComponent('CustomForm'            , () => require('./CustomForm').default);
  Navigation.registerComponent('Simple'                , () => require('./Simple').default);
  Navigation.registerComponent('Tutorial'              , () => require('./Tutorial').default);
  Navigation.registerComponent('SideBar'               , () => require('./SideBar').default);
  Navigation.registerComponent('Dashboard'             , () => require('./components/Dashboard').default);
  Navigation.registerComponent('Intervention'          , () => require('./components/Intervention').default);
  Navigation.registerComponent('TimelineAssessment'    , () => require('./components/TimelineAssessment').default);
  Navigation.registerComponent('DashboardHOC'          , () => require('./DashboardHOC').default);
  Navigation.registerComponent('LabourOutcome'         , () => require('./components/LabourOutcome').default);
  Navigation.registerComponent('OutcomeForm'           , () => require('./OutcomeForm').default);
  Navigation.registerComponent('Alerts'                , () => require('./Alerts').default);
  Navigation.registerComponent('Assessment'            , () => require('./Assessment').default);
  Navigation.registerComponent('AssessmentQuestion'    , () => require('./components/AssessmentQuestion').default);
  Navigation.registerComponent('Settings'              , () => require('./components/Settings').default);
  Navigation.registerComponent('SimulateTime'          , () => require('./components/SimulateTime').default);
}
