import React, { Component } from 'react';
//import { AsyncStorage, Linking } from "react-native";

import { Navigation } from 'react-native-navigation'
import {
	StyleSheet,
	View,
	FlatList,
	TouchableOpacity,
	Text
} from 'react-native';
import {Avatar, ListItem } from 'react-native-elements'
import PushNotification from 'react-native-push-notification';

import { settingsLayout, openModalSimulateTime, closeSidebar } from './navigation';
import { DropboxAuthorize } from "./sync/dropbox/DropboxAuthorize";
import { DropboxDatabaseSync } from "./sync/dropbox/DropboxDatabaseSync";
import PushController from './PushController.js'; 
import UserBoldRepository from './repositories/UserBoldRepository';

class SideBar extends Component {
	constructor(props) {
		super(props)
    //console.log("++++++++++++++++++++++++++++++++++ SIDEBAR FORM PROPS: " + JSON.stringify(props));
		Navigation.events().bindComponent(this);
		
		this.dropboxAuth = new DropboxAuthorize();
		this.dropboxSync = new DropboxDatabaseSync();
		this.state = {
		  isDropboxStatusKnown: false,
		  hasAuthorizedWithDropbox: false,
			downloading: false,
			user: {username:''}
		};
		this.startSync = this.startSync.bind(this);
	}
	
	componentDidMount(){
		if(this.props.user_id != undefined){
			UserBoldRepository.getUserBoldById(this.props.user_id).then((user) =>
			{
				if(user != undefined)
				{
					//console.log(user);
					this.setState({user: user})
				}
			});
		}	
	}

	startSync(){
		PushNotification.localNotification({
			message: 'Sync database started!'
		});
		this.dropboxSync.upload(this.props.user_id); 
	}

  list = [
	{
	  title: 'Profile',
	  icon: 'person',
	  onPress: () => { console.log("teste")}
	},
	{
	  title: 'Sync Data',
	  icon: 'sync',
	  onPress: () => { this.startSync(); }
	},
	{
	  title: 'Settings',
	  icon: 'settings',
	  onPress: () => { settingsLayout(this.props.componentId, this.props.user_id) }
	},
	{
	  title: 'Simulate Time',
	  icon: 'add-alarm',
	  onPress: () => { openModalSimulateTime() }
	}
  ]

	render() {
		return (
			<View style={styles.container} >
				<View style={styles.containerAvatar} >
				<Avatar
					large
					rounded
					//source={{uri: "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"}}
					onPress={() => console.log("Works!")}
					activeOpacity={0.7}
					title={this.state.user.username.toUpperCase().substring(0,2)}				
					/>
					<Text>{this.state.user.username.toUpperCase()}</Text>		
					<Text>Version α 0.1</Text>				
				</View>
				<View>
				{
					this.list.map((item) => (
					<ListItem
						key={item.title}
						title={item.title}
						leftIcon={{name: item.icon}}
						style={styles.containerList}
						onPress={item.onPress}
					/>
					))
				}
				<PushController />
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
	},	
	containerAvatar: {
		flex: 3,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF',
		alignItems:'center'
	},	
	containerList: {
		height: 60,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF',
		alignItems:'center'
	}
});


export default SideBar;