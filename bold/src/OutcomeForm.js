import React, { Component } from 'react';
import { StyleSheet,Text, ScrollView, View, TouchableOpacity, Animated, Alert, BackHandler  } from "react-native";
import { Navigation } from 'react-native-navigation';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
//import { Button, Card, CardSection, InputMasked } from './components/common';
import NeonatalOutcome from './components/NeonatalOutcome'
import MaternalOutcome from './components/MaternalOutcome'
//import { FormLabel, FormInput, ButtonGroup } from 'react-native-elements'
import { womanListLayout, closeSidebar } from './navigation'
//import Icon from 'react-native-vector-icons/FontAwesome';

import WomanRepository from './repositories/WomanRepository';
//import PrenancyRepository from './repositories/PregnancyRepository';
import AdmissionRepository from './repositories/AdmissionRepository';

//let SQLite = require('react-native-sqlite-storage')

class OutcomeForm extends Component {

	constructor(props) {
	  super(props)
    //console.log("OUTCOME FORM PROPS: " + JSON.stringify(props));
   

    // this.state.woman_id = this.props.woman_id;
    // this.state.userId = this.props.user_id;
    // this.state.admissionId = this.props.admission_id;

    this.updateIndex = this.updateIndex.bind(this);
    this.confirmCloseCase = this.confirmCloseCase.bind(this);

		Navigation.events().bindComponent(this);
  }
 
  state = {
    id: '', name: '', enabled: '', modified_by_user_id: '', birthday: '',
    age: '', status: '', education_level: '', gainful_occupation: '', createdAt: '',
    updatedAt: '', userBoldId: '', admission_id: undefined, pregnancy_id: undefined, woman_id: undefined,
    final_mode_delivery: 0,
    index: 0,
    routes: [
      { key: 'child', title: 'Childbirth', user_id: this.props.user_id, woman_id: this.props.woman_id, admission_id: this.props.admission_id},
      { key: 'maternal', title: 'Maternal', user_id: this.props.user_id, woman_id: this.props.woman_id, admission_id: this.props.admission_id},
    ],
    updateIndex: this.updateIndex
  };

	/************************* COMPONENTS LIVE CYCLE *****************************/
  componentDidMount(route, navigator) {
      //console.log("Woman Id: "+this.props.woman_id)
      //console.log("User Id: "+this.props.userId);
      WomanRepository.getWomanById(this.props.woman_id)
      .then((data) => {
          this.setState(data);
          //console.log("getWomanById " + JSON.stringify(data));
      });
      BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
		
		////console.log("NAVIGATION UNMOUNTED;")
		if (this.navigationEventListener) {
			this.navigationEventListener.remove();
		}
		return false;
	}
		
  
	/************************* NAVIGATION HANDLERS *****************************/  

	// navigationButtonPressed({ returnButton }) {
  // }
  
	navigationButtonPressed({buttonId}) {
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId === 'sidebarButton') {
			closeSidebar();
		}else if(buttonId === 'backButton'){
      if(!this.confirmCloseCase()){	
        Navigation.pop(this.props.componentId);		
      }
		}
	}

  onBackPress = () => {
		return this.confirmCloseCase();
	}

	onNavigatorEvent(event) {
		if (event.id === 'backPress') {
      return this.confirmCloseCase();
		}
  }

  

	/************************* CUSTOM FUNCTIONS *****************************/
  confirmCloseCase(){
    //console.log("ADMISSION " + JSON.stringify(this.props));
    Alert.alert(
      'Finalize case',
      "This will remove the woman from the list and the case will no longer be accessible",
      [
        {					
          text: 'Return, but not finalize it', onPress: () => { 
            Navigation.pop(this.props.componentId);		
			//Navigation.pop('WomanList');
           }
        },
        {
          text: 'Okey, finalize it', 
          onPress: () => { 
            //this.showLoading();
            
            AdmissionRepository.finalizeAdmission(this.props.admission_id)
              .then(()=> {
                //this.hideLoading();
                Navigation.pop(this.props.componentId);		
			//Navigation.pop('WomanList');
              })
          }
        }
      ],
      { cancelable: true }
    );
    return true;
  }


  
	/************************* TAB AREA *****************************/
  _handleIndexChange = index => this.setState({ index });

  _renderTabBar = props => {
      const inputRange = props.navigationState.routes.map((x, i) => i);

      return (
        <View style={styles.tabBar}>
          {
            props.navigationState.routes.map((route, i) => {
                  const color = props.position.interpolate({
                  inputRange,
                  outputRange: inputRange.map(
                      inputIndex => (inputIndex === i ? '#D6356C' : '#222')
                  ),
              });
              return (
                  
              <TouchableOpacity                    
              style={
                  route.key === props.navigationState.routes[this.state.index].key
                    ? styles.selectedTabStyle
                    : styles.tabStyle
                }
              onPress={() => this.setState({ index: i })}
              key={i}
              >
              <Animated.Text               
              style={
                  route.key === props.navigationState.routes[this.state.index].key
                    ? styles.selectedTabTextStyle
                    : styles.tabTextStyle
                }
                >{route.title}</Animated.Text>
          </TouchableOpacity>
              );
          })}
        </View>
      );
  };

  _renderScene = SceneMap({
		child: NeonatalOutcome,
		maternal: MaternalOutcome,
  });

	updateIndex (selectedIndex) {
   //console.warn(selectedIndex)
	  this.setState({ final_mode_delivery: selectedIndex })
  }

  render() {
      ////console.log(this.props.navigator.passProps);
      return (
          <TabView
              navigationState={this.state}
              renderScene={this._renderScene}
              renderTabBar={this._renderTabBar}
              onIndexChange={this._handleIndexChange}
          />
      );
  }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
    },
    tabBar: {
      flexDirection: 'row',
    },
    tabStyle: {
      flex: 3,
      alignItems: 'center',
      padding: 10, 
      paddingTop: 16,
      paddingBottom: 16,
      borderWidth: 1, 
      borderColor: "#FFFFFF"
    },
    selectedTabStyle: {  
        flex: 3, 
        alignItems: 'center',
        padding: 10, 
        paddingTop: 16,
        paddingBottom: 16,
		borderWidth: 1, 
		borderColor: "#86939e",   
        backgroundColor: '#2096F3',
        borderRadius:3
    },
    tabTextStyle:{
        color: "#86939e"
    },
    selectedTabTextStyle:{
        color: "#FFF"
    },
	tabIcon: {
      flex: 1,
      alignItems: 'flex-end',
      padding: 16,
    },
    buttonGroupContainer: {
        flexDirection: 'column',
        height: null
    },
    buttonGroupButton :{
        width: 'auto',
        height: 'auto',
        padding: 10,
        borderWidth: 1,
        borderColor: "#FFFFFF"
    }
});

export default OutcomeForm;
