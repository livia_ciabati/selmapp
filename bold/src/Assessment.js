import React, { Component } from 'react';
import { StyleSheet,Text, ScrollView, View, FlatList, TouchableOpacity } from "react-native";
import { Navigation } from 'react-native-navigation';
import { womanListLayout, closeSidebar, dashboardLayout, assessmentLayout } from './navigation'
import { Icon, Divider, List, ListItem } from 'react-native-elements'


let SQLite = require('react-native-sqlite-storage')

class Assessment extends Component {
  constructor(props) {
        super(props)
        ////console.log(JSON.stringify(props));
    let db = SQLite.openDatabase({ name: 'selma.db', createFromLocation: "~selma.db" }, this.openCB, this.errorCB);
    
    db.transaction(
      (tx) => {
        //react 
        //get answers of determinate question
        if(props.id == 221){  // systolic pressure

          tx.executeSql('SELECT aq.id, aq.name as name_question, aq.label_question, ar.answer as answer1,ar2.answer as answer2, ar.admissionId, ar.createdAt,ar2.createdAt ' +
            ' FROM assessmentQuestions aq  ' +
            ' INNER JOIN assessmentResult ar on ar.assessmentQuestionsId = aq.id ' +
            ' LEFT JOIN  assessmentResult ar2 on ar.createdAt = ar2.createdAt AND ar2.assessmentQuestionsId= 222 AND ar2.admissionId = 1517 ' +
            ' WHERE ar.admissionId = 1517 AND aq.id = 221 ' +
            ' ORDER BY ar.createdAt ASC ', 
            [], 
            (tx, results) => {
              var q = results.rows.item(0).label_question;
              var len = results.rows.length;
              var d = [];
              for (let i = 0; i < len; i++) {
                let row = results.rows.item(i);
                let questionFormated = "Blood Pressure (mmHg)";
                d.push({id: row.id,
                      admissionId: row.admissionId,
                      label_question: questionFormated,
                      name_question: row.name_question,
                      answer: row.answer1.replace("mmHg","") + "/" + row.answer2.replace("mmHg",""),
                      createdAt: row.createdAt});  
                  
              }
              this.setState({ answers: d });
              this.setState({ question: q });

              ////console.warn(this.state.answers);
            },
            (tx, err) => {
             //console.warn(tx);
            }
          ); // end tx.executeSql()
        } else {
          if(props.id == 226) { //number uterine contractions
            tx.executeSql('SELECT aq.id, aq.name as name_question, aq.label_question, ar.answer as answer1,ar2.answer as answer2, ar.admissionId, ar.createdAt,ar2.createdAt ' +
              ' FROM assessmentQuestions aq  ' +
              ' INNER JOIN assessmentResult ar on ar.assessmentQuestionsId = aq.id ' +
              ' LEFT JOIN  assessmentResult ar2 on ar.createdAt = ar2.createdAt AND ar2.assessmentQuestionsId= 227 AND ar2.admissionId = 1517 ' +
              ' WHERE ar.admissionId = 1517 AND aq.id = 226 ' +
              ' ORDER BY ar.createdAt ASC ', 
              [], 
              (tx, results) => {
                var q = results.rows.item(0).label_question;
                var len = results.rows.length;
                var d = [];
                for (let i = 0; i < len; i++) {
                  let row = results.rows.item(i);
                  let questionFormated = "Number/Duration of uterine contractions (10min)";
                  d.push({id: row.id,
                        admissionId: row.admissionId,
                        label_question: questionFormated,
                        name_question: row.name_question,
                        answer: row.answer1 + "/" + row.answer2,
                        createdAt: row.createdAt});  
                    
                }
                this.setState({ answers: d });
                this.setState({ question: q });

                ////console.warn(this.state.answers);
              },
              (tx, err) => {
               //console.warn(tx);
              }
            ); // end tx.executeSql()
          } else {
            tx.executeSql('SELECT aq.id, aq.name as name_question, aq.label_question, ar.answer, ar.admissionId, ar.createdAt ' +
              ' FROM assessmentQuestions aq ' +
              ' INNER JOIN assessmentResult ar on ar.assessmentQuestionsId = aq.id ' +
              ' WHERE ar.admissionId = 1517 AND aq.id = ? ' +
              ' ORDER BY name_question ASC , createdAt ASC ', 
              [props.id], 
              (tx, results) => {
                var q = results.rows.item(0).label_question;
                var len = results.rows.length;
                var d = [];
                for (let i = 0; i < len; i++) {
                  let row = results.rows.item(i);
                  d.push({id: row.id,
                        admissionId: row.admissionId,
                        label_question: row.label_question,
                        name_question: row.name_question,
                        answer: row.answer ,
                        createdAt: row.createdAt});  
                    
                }
                this.setState({ answers: d });
                this.setState({ question: q });

                ////console.warn(this.state.answers);
              },
              (tx, err) => {
               //console.warn(tx);
              }
            ); // end tx.executeSql()
          }
        }
            


        //add information about woman, pregnancy and admission
        let ad = {id_wom: 247,
                name: "Lindsay", 
                age: 28,
                estimate_gestacional_age: 37,
                id_admission: 1517,
                g: 0,
                p: 0,
                a: 0
                };
        this.setState({ admission: ad });
      }
    ); //end db.transaction
  }

  state = {
    question: "",
    answers: [{id: "", admissionId: "" ,label_question: "",name_question: "", answer: "", createdAt: ""}],
    admission: [{id_wom:"",name: "",age: "", estimate_gestacional_age: "", id_admission: "", g: "", p: "", a: ""}]
  }

 
  navigationButtonPressed({buttonId}) {
    if (buttonId === 'cancelBtn') {
      Navigation.dismissModal(this.props.componentId);
    } else if (buttonId === 'saveBtn') {
      alert('saveBtn');
    }else if(buttonId == 'sidebarButton') {
      closeSidebar();
    }
  }

  errorCB(err) {
    //console.log("SQL Error: " + err);
  }

  successCB() {
    //console.log("SQL executed fine");
  }

  openCB() {
    //console.log("Database OPENED");
  }
    
  componentDidMount(route, navigator) {
    //console.log("teste")
    //console.log(this.props.id)
  }


  render() {
    ////console.log(this.props.navigator.passProps);
    // const { attr } = this.state
    ////console.warn("Render");
    ////console.warn(this.state);
    return (
      <View style={styles.container}>
        <View style={styles.containerLabel}>
          <Text style={styles.informationText}>
            Name: {this.state.admission.name}  
          </Text>
          <Text style={styles.informationText}>
            Age: {this.state.admission.age} G{this.state.admission.g} P{this.state.admission.p} A{this.state.admission.a} Id: {this.state.admission.id_admission}
          </Text>
        </View>
        <View style={styles.containerInfo}>
          <ScrollView>
            <View style={styles.answersView}>
              <Icon 
                    name='table' 
                    type='font-awesome' 
                    size={18}
              /> 
              <Text style={styles.iconDashboard}>
                {this.state.question}
              </Text>
              <Divider style={{ backgroundColor: '#bfbfbf' }} />
              <FlatList
                data={this.state.answers}
                keyExtractor={item => item.id.toString()}
                numColumns={1}
                renderItem={
                  ({ item }) => {
                      return (
                        <View style={styles.item}>
                          <Text style={styles.answerFormat}>
                            {item.answer}
                          </Text>
                          <Text style={styles.dateAnswerFormat}>
                            {item.createdAt}
                          </Text>
                        </View> 
                      )  
                  }
                }
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
    },
  item: {
    // alignItems: "flex-start",
    backgroundColor: "#00CC00",
    flexGrow: 1,
    margin: 4,
    padding: 5,
    flexBasis: 0,
  },
  containerLabel: {
    backgroundColor: "#f2f2f2",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  informationText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  containerInfo: {
    backgroundColor: "#f2f2f2",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  answersView: {
    backgroundColor: "#e7e7e7",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  iconDashboard: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  answerFormat: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  dateAnswerFormat: {
    fontSize: 12,
    fontWeight: 'normal',
    textAlign: 'center',
  },
});


export default Assessment;
