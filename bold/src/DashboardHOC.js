import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Animated, BackHandler, ActivityIndicator } from "react-native";
import { Navigation } from 'react-native-navigation';
import { TabView, SceneMap, PagerExperimental  } from 'react-native-tab-view';
import Dashboard from "./components/Dashboard" ;
import Intervention from "./components/Intervention" ;
import TimelineAssessment from "./components/TimelineAssessment" ;
import { closeSidebar } from './navigation';

class DashboardHOC extends Component {
	constructor(props) {
        super(props) 
        //console.log("props DashboardHOC " + JSON.stringify(props))

		this.navigationEventListener = Navigation.events().bindComponent(this);
	}

	
	navigationButtonPressed({buttonId}) {
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId == 'sidebarButton') {
			closeSidebar();
		}else if(buttonId === 'backButton'){
            Navigation.pop(this.props.componentId);
		}
	}

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backHandler);
        //console.log("COMPONENT DID MOUNT " + JSON.stringify(this.props));
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
        //console.log("COMPONENT WILL MOUNT " + JSON.stringify(this.props));
    }
    

	componentDidAppear() {
        //console.log("COMPONENT DID APPEAR " + JSON.stringify(this.state));
	}

	componentDidDisappear() {        
        //console.log("COMPONENT DID DISAPEAR " + JSON.stringify(this.props));
	}

    backHandler = () => {
        return false;
    }

    state = {
        reload: true,
        index: 0, 
        routes: [
          { key: 'dashboard',          title: 'Dashboard',    adm_id: this.props.adm_id, user_id: this.props.user_id },
          { key: 'intervention',       title: 'Intervention', adm_id: this.props.adm_id, user_id: this.props.user_id },
          { key: 'timelineAssessment', title: 'Timeline',     adm_id: this.props.adm_id, user_id: this.props.user_id },
        ],
        updateIndex: this.updateIndex,
        saveButton: 0
    };

    _handleIndexChange = (index) =>
    {
        this.setState({ reload: false });
        return this.setState({ index })
    };

    _renderTabBar = props => {        
        //console.log("HANDLE INDEX CHANGE " + JSON.stringify(this.state.routes)) 
        const inputRange = props.navigationState.routes.map((x, i) => i);

        return (
            <View style={styles.tabBar}>
                {props.navigationState.routes.map((route, i) => {
                    const color = props.position.interpolate({
                    inputRange,
                    outputRange: inputRange.map(
                        inputIndex => (inputIndex === i ? '#D6356C' : '#222')
                    ),
                });
                return (
                    
                <TouchableOpacity                    
                style={
                    route.key === props.navigationState.routes[this.state.index].key
                      ? styles.selectedTabStyle
                      : styles.tabStyle
                  }
                onPress={() => this.setState({ index: i })}
                key={i}
                >
                <Animated.Text               
                style={
                    route.key === props.navigationState.routes[this.state.index].key
                      ? styles.selectedTabTextStyle
                      : styles.tabTextStyle
                  }
                  >{route.title}</Animated.Text>
            </TouchableOpacity>
                );
            })}
            </View>
        );
    };
      
    _renderScene = SceneMap({
        dashboard: Dashboard,
        intervention: Intervention,
        timelineAssessment: TimelineAssessment
    });

    renderScene = ({ route }) => {        
        if (route.key == 'dashboard' && this.state.index == 0) {
          var dash = <Dashboard route={{ adm_id: this.props.adm_id, user_id: this.props.user_id }} reload={ this.state.reload } />;
          this.state.reload = false;
          return dash;
        }
        if (route.key == 'intervention' && this.state.index == 1) {
          return <Intervention route={{ adm_id: this.props.adm_id, user_id: this.props.user_id }} />;
        }
        if (route.key == 'timelineAssessment' && this.state.index == 2) {
          return <TimelineAssessment route={{ adm_id: this.props.adm_id, user_id: this.props.user_id }} />;
        }
    }

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={this._renderScene}
                renderTabBar={this._renderTabBar}
                onIndexChange={ this._handleIndexChange}
            />
        );
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
    },
    tabBar: {
      flexDirection: 'row',
    },
    tabStyle: {
      flex: 3,
      alignItems: 'center',
      padding: 10, 
      paddingTop: 16,
      paddingBottom: 16,
      borderWidth: 1, 
      borderColor: "#FFFFFF"
    },
    selectedTabStyle: {  
        flex: 3, 
        alignItems: 'center',
        padding: 10, 
        paddingTop: 16,
        paddingBottom: 16,
		borderWidth: 1, 
		borderColor: "#86939e",   
        backgroundColor: '#2096F3',
        borderRadius:3
    },
    tabTextStyle:{
        color: "#86939e"
    },
    selectedTabTextStyle:{
        color: "#FFF"
    },
    tabIcon: {
      flex: 1,
      alignItems: 'flex-end',
      padding: 16
    },
    buttonGroupContainer: { 
        flexDirection: 'column', 
        flex:1,
        height: null 
    },
    buttonGroupButton :{ 
        width: 'auto', 
        height: 'auto', 
        padding: 10, 
        borderWidth: 1, 
        borderColor: "#FFFFFF"
    }
});

export default DashboardHOC;
