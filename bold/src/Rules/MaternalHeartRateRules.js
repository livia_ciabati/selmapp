import { diffTimeNowInMinutes, getCurrentTimeToDatabase } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import assessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var MaternalHeartRate = { 
    normalMaternalHeartRateRule: [{
        "name": "Normal maternal heart",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.assessment.maternal_heart_rate >= 60 && this.assessment.maternal_heart_rate < 100);
        },
        "consequence": function (R) {  
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_mhr', diagnosisValue = 1, admissionId= this.admission.id)
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_tachycardia', diagnosisValue = 0, admissionId= this.admission.id);

            R.next();
        }
    }],
    bradycardia: [{
        "name": "Bradycardia",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.assessment.maternal_heart_rate < 60 && this.assessment.maternal_heart_rate >= 50);
        },
        "consequence": function (R) {
            //console.log(this);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_mhr', diagnosisValue = 0, admissionId= this.admission.id)
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'bradycardia', diagnosisValue = 1, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            this.diagnosis["bradycardia"] = {value : 1, createdAt: getCurrentTimeToDatabase()};

            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Bradycardia', admissionId= this.admission.id, '', publish = false);
            assessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_vitals', admissionId = this.admission.id, intervalMinutes = 0);
            R.next();              
        }
    },
    {
        "name": "Severe bradycardia",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.assessment.maternal_heart_rate < 50);
        },
        "consequence": function (R) {
            this.result = true;   
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_mhr', diagnosisValue = 0, admissionId= this.admission.id)
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_bradycardia', diagnosisValue = 1, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            this.diagnosis["severe_bradycardia"] = {value : 1, createdAt: getCurrentTimeToDatabase()};

            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Call for high level', admissionId = this.admission.id, '', publish = false);
            R.next();
        }
    },
    {
        "name": "[Bradycardia] Bad MEOWS with continuous monitoring",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when((this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3) 
                    && this.settings.continuous_monitoring == 1);
        },
        "consequence": function (R) {
            this.result = true;
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous monitoring', admissionId = this.admission.id, '', publish = false);
            assessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_monitoring', admissionId = this.admission.id, intervalMinutes = 1);
            
            R.next();
        }
    },
    {
        "name": "[Bradycardia] Bad MEOWS without continuous monitoring",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when((this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3) 
                    && this.settings.continuous_monitoring == 0);
        },
        "consequence": function (R) {
            this.result = true;
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Intensified Monitoring', admissionId = this.admission.id, '', publish = false);
            assessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_vitals', admissionId = this.admission.id, intervalMinutes = 15);

            if(this.diagnosis.intensified_monitoring != undefined)
                this.diagnosis.intensified_monitoring.value = 1
            else  
                this.diagnosis["intensified_monitoring"] = { value: 1 };

            R.next();
        }
    },
    {
        "name": "[Bradycardia] Yellow MEOWS",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when((this.diagnosis.bradycardia != undefined && this.diagnosis.bradycardia.value == 1) && this.meows.yellowFlag >= 2);
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Moderate to High risk of illness or death from bradycardia', admissionId = this.admission.id, '', publish = false);
            R.next();
        }
    },
    {
        "name": "[Bradycardia] Red MEOWS",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.diagnosis.severe_bradycardia != undefined && this.diagnosis.severe_bradycardia.value == 1 && this.meows.redFlag >= 2);
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'High risk of illness or death from bradycardia', admissionId = this.admission.id, '', publish = false);            
            R.next();
        }
    },
],
    tachycardia: [{
        "name": "Tachycardia",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.assessment.maternal_heart_rate > 100 && this.assessment.maternal_heart_rate < 120);
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_mhr', diagnosisValue = 0, admissionId= this.admission.id)
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'tachycardia', diagnosisValue = 1, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            this.diagnosis["tachycardia"] = {value : 1, createdAt: getCurrentTimeToDatabase()};

            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Tachycardia', admissionId= this.admission.id, '', publish = false);
            assessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'causes_tachcardia_maternal_vitals', admissionId = this.admission.id, intervalMinutes = 0);
            
            this.result = true;
            R.next();
        }
    },
    {
        "name": "Severe tachycardia",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.assessment.maternal_heart_rate > 120);
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_mhr', diagnosisValue = 0, admissionId= this.admission.id)
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_bradycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'tachycardia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_tachycardia', diagnosisValue = 1, admissionId= this.admission.id);
            this.diagnosis["severe_tachycardia"] = {value : 1, createdAt: getCurrentTimeToDatabase()};

            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Call for high level', admissionId= this.admission.id, '', publish = false);
            
            this.result = true;
            R.next();
        }
    },
    {
        "name": "[Tachycardia] Meows Evaluation - Continuous monitoring",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when((this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3) 
                    && this.settings.continuous_monitoring == 1);
        },
        "consequence": function (R) {
            this.result = true;
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous monitoring', admissionId = this.admission.id, '', publish = false);
            assessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_monitoring', admissionId = this.admission.id, intervalMinutes = 1);
            
            R.next();
        }
    },
    {
        "name": "[Tachycardia] Meows Evaluation - without continuous monitoring monitoring",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when((this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3) 
                    && this.settings.continuous_monitoring == 0);
        },
        "consequence": function (R) {
            this.result = true;
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Intensified Monitoring', admissionId = this.admission.id, '', publish = false);
            assessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_vitals', admissionId = this.admission.id, intervalMinutes = 15);
            
            //console.log("6 INTENSIFIED MONITORING: " + JSON.stringify(this.diagnosis));
            if(this.diagnosis.intensified_monitoring != undefined)
                this.diagnosis.intensified_monitoring.value = 1
            else  
                this.diagnosis["intensified_monitoring"] = { value: 1 };

            R.next();
        }
    },
    {
        "name": "[Tachycardia] Bad MEOWS with CTG available",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when((this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3) 
                    && this.settings.ctg_available == 1);
        },
        "consequence": function (R) {
            this.result = true;
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_cardiotography', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous cardiotography', admissionId = this.admission.id, '', publish = false);
            
            R.next();
        }
    },
    {
        "name": "[Tachycardia] Yellow MEOWS",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.diagnosis.tachycardia != undefined && this.diagnosis.tachycardia.value == 1 && this.meows.yellowFlag >= 2);
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Moderate to High risk of illness or death from tachycardia', admissionId = this.admission.id, '', publish = false);
            R.next();
        }
    },
    {
        "name": "[Tachycardia] Red MEOWS",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(this.diagnosis.severe_tachycardia != undefined && this.diagnosis.severe_tachycardia.value == 1 && this.meows.redFlag >= 2);
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'High risk of illness or death from tachycardia', admissionId = this.admission.id, '', publish = false);            
            R.next();
        }
    }]
}
