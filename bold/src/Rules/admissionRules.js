import RuleEngine from "node-rules";
import AdmissionRepository from '../repositories/AdmissionRepository';
import admissionAlertsRepository from '../repositories/admissionAlertsRepository';
import AssessmentScheduleRepository from '../repositories/AssessmentScheduleRepository';
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";


export var AdmissionRules = {
    elegibility: [{
        "name": "Elegible",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.admission.first_stage_labour == 1 && this.admission.fetal_death == 0 && 
                this.admission.advanced_first_stage_labour == 0 && this.admission.multiple_pregnancy == 0 && this.pregnancy.estimate_gestional_age >= 37 && this.pregnancy.estimate_gestional_age < 42 &&
                this.admission.elective_c_section == 0 && this.admission.pre_labour_c_section == 0 && this.admission.emergency_c_section_or_laparotomy == 0 && 
                this.admission.attemped_labour_induction  == 0 && this.admission.false_labour == 0 && this.admission.non_emancipated_minors_without_guardian == 0 && 
                this.admission.unable_give_consent_or_health_problem == 0 && this.admission.elegible != 1);
        },
        "consequence": function (R) {
            AdmissionRepository.getAdmissionById(this.admission.id).then((admission) => {
                admission.elegible = 1;
                AdmissionRepository.updateAdmission(admission);
            });

            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName      = 'elegible', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository   .insertAdmissionAlertsByTitle  (admissionTitle     = 'Eligible for SELMA', admissionId= this.admission.id, this.woman.name, publish = true);
            AssessmentScheduleRepository.insertScheduledFormFromName   (assessmentFormName = 'first_assessment', admissionId = this.admission.id, intervalMinutes = 0);
            R.next();
        }
    }, 
    {
        "name": "Not elegible",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(!(this.admission.first_stage_labour == 1 && this.admission.fetal_death == 0 && 
                this.admission.advanced_first_stage_labour == 0 && this.admission.multiple_pregnancy == 0 && this.pregnancy.estimate_gestional_age >= 37 && this.pregnancy.estimate_gestional_age < 42 &&
                this.admission.elective_c_section == 0 && this.admission.pre_labour_c_section == 0 && this.admission.emergency_c_section_or_laparotomy == 0 && 
                this.admission.attemped_labour_induction  == 0 && this.admission.false_labour == 0 && this.admission.non_emancipated_minors_without_guardian == 0 && 
                this.admission.unable_give_consent_or_health_problem == 0) && 
                (this.admission.elegible == undefined || this.admission.elegible == null));
        },
        "consequence": function (R) {
            AdmissionRepository.getAdmissionById(this.admission.id).then((admission) => {
                admission.elegible = 0;
                AdmissionRepository.updateAdmission(admission);
            });
            
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'elegible', diagnosisValue = 0, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Not eligible for SELMA', admissionId= this.admission.id, this.woman.name, publish = true);
            
            R.stop();
        }
    }]
};
