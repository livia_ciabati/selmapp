import { diffTimeNowInMinutes } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var FirstStage = {         
    evaluate_active_phase: [{
        "name": "Active phase",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(
                this.diagnosis.elegible != undefined
                && parseInt(this.diagnosis.elegible.value) == 1 
                && (this.diagnosis.active_phase == undefined || this.diagnosis.active_phase.value != 1)
                && this.assessment.cervical_dilatation_cm != undefined 
                && this.assessment.cervical_dilatation_cm >= 5 
                && this.assessment.cervical_dilatation_cm < 10
            );
        },
        "consequence": function (R) {
            //console.log(this);
            
            //active phase confirmed, call dinamic evaluations     
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'active_phase', diagnosisValue = 1, admissionId = this.admission.id),
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Active phase', admissionId= this.admission.id, this.woman.name, publish = false)
            this.diagnosis["active_phase"] = { value: 1 }

            R.stop();

        }
    }, 
    //second stage
    {
        "name": "Second stage",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(
                this.diagnosis.elegible != undefined
                && parseInt(this.diagnosis.elegible.value) == 1 
                && this.assessment.cervical_dilatation_cm != undefined 
                && this.assessment.cervical_dilatation_cm == 10
            );
        },
        "consequence": function (R) {
                    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'second_stage', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Second stage', admissionId= this.admission.id, this.woman.name, publish = false);
            
            R.stop();
        }
    }, 
    //long latent phase
    {
        "name": "Long latent phase",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(parseInt(this.diagnosis.elegible.value) == 1 
                && diffTimeNowInMinutes(this.diagnosis.elegible.createdAt) >= (8 * 60)
                && (this.diagnosis.active_phase == undefined
                    || this.diagnosis.active_phase != undefined && this.diagnosis.active_phase.value != 1)
                && this.assessment.cervical_dilatation_cm != undefined 
                && this.assessment.cervical_dilatation_cm < 5);
        },
        "consequence": function (R) {
            //console.log(this);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'long_latent_phase', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Discharge - Long latent phase', admissionId= this.admission.id, this.woman.name, publish = false);
            
            R.stop();
        }
    },
    //latent phase
    {
        "name": "Latent phase",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            R.when(parseInt(this.diagnosis.elegible.value) == 1 
                && diffTimeNowInMinutes(this.diagnosis.elegible.createdAt) < (8 * 60)
                && (this.diagnosis.active_phase == undefined ||  this.diagnosis.active_phase.value != 1 )
                && this.assessment.cervical_dilatation_cm != undefined && this.assessment.cervical_dilatation_cm < 5)
        },
        "consequence": function (R) {
            
            //console.log("Latent phase 1")            
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'active_phase', diagnosisValue = 0, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Latent phase', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'revaluate_latent_phase', admissionId = this.admission.id, intervalMinutes = 240);

            R.stop();
        }
    }]
}