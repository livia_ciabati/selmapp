import RuleEngine from "node-rules";
import admissionAlertsRepository from '../repositories/admissionAlertsRepository';
import AssessmentScheduleRepository from '../repositories/AssessmentScheduleRepository';
import { diffTimeNowInMinutes } from '../repositories/BaseRepository';
import diagnosisRepository from "../repositories/diagnosisRepository";
import { FirstStage } from "./FirstStage";
import { Meows } from "./MeowsRules";
import { MaternalHeartRate } from "./MaternalHeartRateRules";
import AdmissionRules from "./AdmissionRules";
import { BloodPressureRules } from "./BloodPressureRules";
import { FetalDescentRules } from "./FetalDescentRules";
import { FetalHeartRate } from "./FetalHeartRateRules";
import { MembranesRules } from "./MembranesRules";
import { ProgressRules } from "./ProgressRules";
import { TemperatureRules } from "./TemperatureRules";
import { UrineRules } from "./UrineRules";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";

/* Check if the engine blocks it! */
export default class AssessmentRules {

    static assessment_rules(fact) {
        console.log(fact);
        R = new RuleEngine();
        R.ignoreFactChanges = true;  
                
        if(fact.meows == undefined){
            fact["meows"] = { greenFlag:0, yellowFlag:0, redFlag:0 }
        }

        R.register(Meows.meows_rules);
        R.register(MaternalHeartRate.normalMaternalHeartRateRule);
        R.register(MaternalHeartRate.bradycardia);
        R.register(MaternalHeartRate.tachycardia);
        R.register(ProgressRules.evaluate_progress);
        R.register(UrineRules.evaluate_urine);
        R.register(TemperatureRules.evaluate_temperature);
        R.register(MembranesRules.evaluate_membranes);
        R.register(FetalHeartRate.evaluate_fetal_heart_rate);
        R.register(FetalDescentRules.evaluate_fetal_descent);
        R.register(BloodPressureRules.evaluate_blood_pressure);

        R.execute(fact, function (data) {
            console.log(data.matchPath);
            //console.log(fact);
            admissionDiagnosisRepository.getadmissionDiagnosisByAdmissionIdForRules(fact.admission.id).then((diagnosis) => {
                console.log(diagnosis);
                for (var key of Object.keys(diagnosis)) {
                    console.log(key + " -> " + diagnosis[key])
                    if(key.indexOf('intensified_monitoring') < 0 && diagnosis[key].value == 1 && diagnosis[key].risk_level == 3){
                        fact.diagnosis.intensified_monitoring = {value: 1};
                    }
                }


                //Modifing time of schedule standard forms
                if(fact.diagnosis.intensified_monitoring != undefined && fact.diagnosis.intensified_monitoring.value == 1){
                    AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'intrapartum_data', admissionId = fact.admission.id, intervalMinutes = 60)
                    .then(() => { return AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'all_vitals', admissionId = fact.admission.id, intervalMinutes = 30) })
                    .then(() => { return AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_fhr_monitoring', admissionId = fact.admission.id, intervalMinutes = 15) })
                    .then(() => { return AssessmentScheduleRepository.cancelDuplicatedFormsByAdmissionId(fact.admission.id)})
                    .then(() => { 
                        console.log("INTENSIFIED MONITORING");
                        admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'standard_monitoring', diagnosisValue = 0, admissionId= fact.admission.id);
                        admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId= fact.admission.id);
                
                        return PubSub.publish('update-list-alert', {adm_id: fact.admission.id, name: fact.woman.name}) 
                    });
                }
                else{
                    AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'intrapartum_data', admissionId = fact.admission.id, intervalMinutes = 240)
                    .then(() => { return AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'all_vitals', admissionId = fact.admission.id, intervalMinutes = 60) })
                    .then(() => { return AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_fhr_monitoring', admissionId = fact.admission.id, intervalMinutes = 30) })
                    .then(() => AssessmentScheduleRepository.cancelDuplicatedFormsByAdmissionId(fact.admission.id))
                    .then(() => { 
                        console.log("STANDARD MONITORING");
                        
                        admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'standard_monitoring', diagnosisValue = 1, admissionId=fact.admission.id);
                        admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 0, admissionId=fact.admission.id);
                
                        return PubSub.publish('update-list-alert', {adm_id: fact.admission.id, name: fact.woman.name}) 
                    });
                }
            })
        });
    }

};
