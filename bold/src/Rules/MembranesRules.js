import { diffTimeNowInMinutes, getCurrentTimeToDatabase } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var MembranesRules = {
    evaluate_membranes: [{ 
        "name": "Intact membranes",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.amniotic_membranes_status != undefined
                && this.assessment.amniotic_membranes_status < 1);
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ruptured_membranes', diagnosisValue = 0, admissionId= this.admission.id);
            this.diagnosis.rupture_membranes = new Object();
            this.diagnosis.rupture_membranes = {value: 0, createdAt: getCurrentTimeToDatabase()};
            R.next();
        }
    }, { 
        "name": "Ruptured membranes",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.assessment.amniotic_membranes_status != undefined
                && this.assessment.amniotic_membranes_status == 1)
                ||
                (this.assessment.amniotomy != undefined
                    && this.assessment.amniotomy == 1));
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ruptured_membranes', diagnosisValue = 1, admissionId= this.admission.id);
            this.diagnosis.rupture_membranes = new Object();
            this.diagnosis.rupture_membranes = {value: 1, createdAt: getCurrentTimeToDatabase()};
            R.next();
        }
    },{ 
        "name": "Meconium",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.amniotic_membranes_status != undefined
                && this.assessment.amniotic_membranes_status >= 2 );
        },
        "consequence": function (R) {
            
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'meconium', diagnosisValue = 1, admissionId= this.admission.id);
            this.diagnosis["meconium"] = {value: 1,  createdAt: getCurrentTimeToDatabase()};
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ruptured_membranes', diagnosisValue = 1, admissionId= this.admission.id);
            this.diagnosis["rupture_membranes"] = new Object();
            this.diagnosis["rupture_membranes"] = {value: 1, createdAt: getCurrentTimeToDatabase()};
            R.next();
        }
    },
    {
        "name": "[Slow but normal] Intact membranes",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_but_normal != undefined
                && this.diagnosis.slow_but_normal.value == 1 
                && (this.diagnosis.ruptured_membranes == undefined || (this.diagnosis.ruptured_membranes != undefined && this.diagnosis.ruptured_membranes != 1))
                && (this.assessment.amniotic_membranes_status != undefined && this.assessment.amniotic_membranes_status == 0)
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ruptured_membranes', diagnosisValue = 0, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Offer amniotomy', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'amnio_perform', admissionId = this.admission.id, intervalMinutes = 0);

            R.next();
        }
    },
    { 
        "name": "[Slow but normal] Ruptured membranes less than 18h",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_but_normal != undefined
                && this.diagnosis.slow_but_normal == 1                 
                && this.diagnosis.rupture_membranes != undefined
                && this.diagnosis.rupture_membranes.value == 1
                && diffTimeNowInMinutes(this.diagnosis.rupture_membranes.createdAt) <= (18 * 60)
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ruptured_membranes', diagnosisValue = 1, admissionId= this.admission.id);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'full_assessment', admissionId = this.admission.id, intervalMinutes = 120);
    
            R.next();
        }
    },
    { 
        "name": "[Slow but normal] Ruptured membranes more than 18h",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_but_normal != undefined
                && this.diagnosis.slow_but_normal == 1                 
                && this.diagnosis.rupture_membranes != undefined
                && this.diagnosis.rupture_membranes.value == 1
                && diffTimeNowInMinutes(this.diagnosis.rupture_membranes.createdAt) > (18 * 60)
                );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'long_ruptured_membranes', diagnosisValue = 1, admissionId= this.admission.id);
    
                R.next();
        }
    },
    { 
        "name": "[Long ruptured membranes] Purulent AF and fever",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.rupture_membranes != undefined
                && (this.diagnosis.rupture_membranes.value == 1
                    || diffTimeNowInMinutes(this.diagnosis.rupture_membranes.createdAt) > (18 * 60) ) 
                && this.assessment.purulent == 1 
                && this.diagnosis.fever != undefined
                && this.diagnosis.fever == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'purulent_af_fever', diagnosisValue = 0, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Check penicillin alergy', admissionId= this.admission.id, this.woman.name, publish = false);

            R.next();
        }
    },
    // { 
    //     "name": "[Ruptured membranes] Green or black stained AF and abnormal FHR",
    //     "priority": 3,
    //     "on": true,
    //     "condition": function (R) {
    //         R.when(this.diagnosis.meconium != undefined
    //             && this.diagnosis.meconium.value == 1 
    //             && this.diagnosis.abnormal_fhr != undefined
    //             && this.diagnosis.abnormal_fhr.value == 1
    //         );
    //     },
    //     "consequence": function (R) {
    //         admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'meconium', diagnosisValue = 1, admissionId= this.admission.id);
    //         this.diagnosis["meconium"] = {value: 1}
    //         R.next();
    //     }
    // },
    { 
        "name": "[Ruptured membranes] Green or black stained AF and abnormal FHR with continuous monitoring",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.meconium != undefined
                && this.diagnosis.meconium.value == 1 
                && this.diagnosis.abnormal_fhr != undefined
                && this.diagnosis.abnormal_fhr.value == 1
                && this.settings.continuous_monitoring == 1
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous monitoring', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_monitoring', admissionId = this.admission.id, intervalMinutes = 1);

            R.next();
        }
    },
    { 
        "name": "[Ruptured membranes] Green or black stained AF and abnormal FHR without continuous monitoring",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.meconium != undefined
                && this.diagnosis.meconium.value == 1 
                && this.diagnosis.abnormal_fhr != undefined
                && this.diagnosis.abnormal_fhr.value == 1
                && this.settings.continuous_monitoring == 0
            );
        },
        "consequence": function (R) {
    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Intensified monitoring', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'all_vitals', admissionId = this.admission.id, intervalMinutes = 15);

            //console.log("8 INTENSIFIED MONITORING: " + JSON.stringify(this.diagnosis));
            if(this.diagnosis.intensified_monitoring != undefined)
                this.diagnosis.intensified_monitoring.value = 1
            else  
                this.diagnosis["intensified_monitoring"] = { value: 1 };

            R.next();
        }
    },
    { 
        "name": "[Rupture membranes] Blood",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.rupture_membranes != undefined
                && this.diagnosis.rupture_membranes.value >= 1
                && this.assessment.blood != undefined
                && this.assessment.blood == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'af_bleeding', diagnosisValue = 1, admissionId= this.admission.id);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'rupture_membranes_blood', admissionId = this.admission.id, intervalMinutes = 0);

            R.next();
        }
    },
    { 
        "name": "Vaginal bleeding with laboratory capacity",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.vaginal_bleeding != undefined
                && this.assessment.vaginal_bleeding == 1 
                && this.settings.laboratory_capacity == 1
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'vaginal_bleeding', diagnosisValue = 1, admissionId= this.admission.id);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_blood_loss_hemodinamic_conditions', admissionId = this.admission.id, intervalMinutes = 0);
    
            R.next();
        }
    },
    { 
        "name": "Vaginal bleeding without laboratory capacity",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.vaginal_bleeding != undefined
                && this.assessment.vaginal_bleeding == 1 
                && this.settings.laboratory_capacity == 0
        );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'vaginal_bleeding', diagnosisValue = 1, admissionId= this.admission.id);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_blood_loss', admissionId = this.admission.id, intervalMinutes = 0);
            R.next();
        }
    },
    { 
        "name": "[Vaginal bleeding] Anaemia",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.anaemia != undefined 
                && this.assessment.anaemia == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'anaemia', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Correct anaemia', admissionId= this.admission.id, this.woman.name, publish = false);
    
            R.next();
        }
    },
    { 
        "name": "[Vaginal bleeding] Coagulopathy",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.coagulopathy != undefined
                && this.assessment.coagulopathy == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'coagulopathy', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Correct coagulopathy', admissionId= this.admission.id, this.woman.name, publish = false);
            R.next();
        }
    },
    { 
        "name": "Vaginal bleeding",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.vaginal_bleeding != undefined
                && this.assessment.vaginal_bleeding == 1
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Expedite birth', admissionId= this.admission.id, this.woman.name, publish = false);
            R.next();
        }
    },
    { 
        "name": "Meconium and abnormal fhr",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.meconium != undefined 
                && this.diagnosis.meconium == 1
                && this.diagnosis.abnormal_fhr != undefined
                && this.diagnosis.abnormal_fhr == 1
            );
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fetal_distress_suspicion', diagnosisValue = 0, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Fetal distress suspicion', admissionId= this.admission.id, this.woman.name, publish = false);

            R.next();
        }
    }]
}