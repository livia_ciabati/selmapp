import { diffTimeNowInMinutes, diffTimeInMinutes } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var UrineRules = {
    evaluate_urine: [
        { 
            "name": "Evaluating urine",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.assessment.urine_output != undefined
                    && this.assessment.urine_volume_per_hour != undefined 
                    && this.assessment.urine_volume_per_hour >= 30) ;
            },
            "consequence": function (R) {    
                    AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'urine_diagnosis', admissionId = this.admission.id, intervalMinutes = 0);
                    
                    R.next();
            }
        },
        { 
            "name": "Glycosuria suspicion",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.assessment.glycosuria != undefined
                    && this.assessment.glycosuria == 1
                    );
            },
            "consequence": function (R) {    
                    admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'glycosuria_suspicion', diagnosisValue = 1, admissionId= this.admission.id);     
                    R.next();
            }
        },
        { 
        "name": "Glicosuria",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(
                // this.diagnosis.glycosuria_suspicion != undefined
                // && this.diagnosis.glycosuria_suspicion.value == 1
                this.assessment.glycosuria != undefined
                && this.assessment.glycosuria >= 2               
            );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'glycosuria', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Check venous plasma glucose', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'venous_plasma_glucose', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "Diabetes in labour",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.diabetes_labour_suspicion == undefined && (this.assessment.random_glucose != undefined
                && this.assessment.random_glucose >= 11.1)
                 || 
                (this.assessment.fasting_glucose != undefined
                && this.assessment.fasting_glucose > 7)
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'diabetes_labour_suspicion', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Undiagnosed diabetes in labour suspicion', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'capillary_glucose', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },    
    { 
        "name": "[Diabetes in labour] Hyperglycaemia",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.diabetes_labour_suspicion != undefined && this.diagnosis.diabetes_labour_suspicion == 1 
                && (this.assessment.random_glucose != undefined && this.assessment.random_glucose >= 11.1)
                 || 
                (this.assessment.fasting_glucose != undefined && this.assessment.fasting_glucose > 7))
        },
        "consequence": function (R) {    
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Consider DKI drip and test for ketonaemia', admissionId= this.admission.id, this.woman.name, publish = false);

                R.next();
        }
    },
    { 
        "name": "Ketones",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.ketones_present != undefined
                && this.assessment.ketones_present == 1
                );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ketones', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Ketones present', admissionId= this.admission.id, this.woman.name, publish = false);
                
                R.next();
        }
    },
    { 
        "name": "Ketones present and low risk",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.ketones_present != undefined
                && this.assessment.ketones_present == 1
                && this.diagnosis.standard_monitoring != undefined
                && this.diagnosis.standard_monitoring.value == 1
            );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ketones_present', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Encourage woman to increase oral intake', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'dehydration_form', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "Ketones present and high risk",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.ketones_present != undefined
                && this.assessment.ketones_present == 1
                && this.diagnosis.intensified_monitoring != undefined
                && this.diagnosis.intensified_monitoring.value == 1
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ketones_present', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Give IV fluids', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'dehydration_form', admissionId = this.admission.id, intervalMinutes = 0);
            
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'iv_fluids', diagnosisValue = 1, admissionId= this.admission.id);

            R.next();
        }
    },
    { 
        "name": "[Ketones] Vomiting",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.ketones != undefined
                && this.diagnosis.ketones.value == 1
                && this.assessment.vommiting != undefined
                && this.assessment.vommiting == 1
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ketones_vomiting', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Replace lost volume', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Ketones] Diarrhoea",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.ketones != undefined
                && this.diagnosis.ketones.value == 1
                && this.assessment.diarrhoea != undefined
                && this.assessment.diarrhoea == 1
            );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'ketones_diarrhoea', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Replace lost volume', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "Oliguria suspicion",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.urine_volume_per_hour != undefined 
                && this.assessment.urine_volume_per_hour < 30 
                && (this.diagnosis.oliguiria_suspicion == undefined
                    || this.assessment.oliguiria_suspicion != 1)            
           );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_suspicion', diagnosisValue = 1, admissionId= this.admission.id);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'oliguria_suspicion', admissionId = this.admission.id, intervalMinutes = 0);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Oliguria suspicion', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Oliguria] Able to pass urine",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.able_pass_urine != undefined 
                && this.assessment.able_pass_urine == 1 
                && this.assessment.urine_volume_per_hour != undefined
                && this.assessment.urine_volume_per_hour < 30
            );
        },
        "consequence": function (R) {    
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Reassess urine output in 2 hours', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'urine_form', admissionId = this.admission.id, intervalMinutes = 120);

                R.next();
        }
    },
    { 
        "name": "[Oliguria] Catetherization",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(
                ((this.assessment.bladder_papabble != undefined && this.assessment.bladder_papabble == 1)
                    || 
                 (this.assessment.catheter != undefined && this.assessment.catheter == 1)
                )
                && this.assessment.able_pass_urine != undefined 
                && this.assessment.able_pass_urine == 0  
                && this.assessment.urine_volume_per_hour != undefined
                && this.assessment.urine_volume_per_hour < 30
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_catetherization', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Catetherization', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Oliguria] Rales on lungs",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.rales_lung != undefined
                && this.assessment.rales_lung == 1
            );
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_rales_lungs', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Rales on lungs', admissionId= this.admission.id, this.woman.name, publish = false);

            this.diagnosis["oliguria_rales_lungs"] = {value:1};
            R.next();
        }
    },
    { 
        "name": "[Oliguria] Vomitting or diarrhoea",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.oliguria_rales_lungs == undefined
                    || this.diagnosis.oliguria_rales_lungs.value == 0 )
                && ((this.assessment.vommiting != undefined
                        && this.assessment.vommiting == 1 )
                    || (this.assessment.diarrhoea != undefined
                        && this.assessment.diarrhoea == 1))
            );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_vomitting_diarrhoea', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Anti-emetics', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Oliguria] Reduced intake or fluids",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.oliguria_rales_lungs == undefined
                    || this.diagnosis.oliguria_rales_lungs.value == 0 )
                && this.assessment.vommiting != undefined
                && this.assessment.vommiting == 0 
                && this.assessment.diarrhoea != undefined
                && this.assessment.diarrhoea == 0 
                && this.assessment.reduced_fluids_intake != undefined
                && this.assessment.reduced_fluids_intake == 1
              );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_reduced_intake', diagnosisValue = 1, admissionId= this.admission.id);    
                this.diagnosis["oliguria_reduced_intake"] = {value: 1}
                R.next();
        }
    },
    { 
        "name": "[Oliguria] Able to intake fluids",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.oliguria_reduced_intake != undefined 
                && this.diagnosis.oliguria_reduced_intake.value == 1 
                && this.assessment.able_intake != undefined
                && this.assessment.able_intake == 1
            );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_able_intake', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Encourage oral fluids intake', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Oliguria] Unable to intake fluids",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.oliguria_reduced_intake != undefined 
                && this.diagnosis.oliguria_reduced_intake.value == 1 
                && this.assessment.able_intake != undefined && this.assessment.able_intake == 0
            );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oliguria_able_intake', diagnosisValue = 0, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Give IV fluids', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Oliguria] Reassess urine",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.urine_volume_per_hour != undefined
                && this.assessment.urine_volume_per_hour < 30
                && this.diagnosis.oliguria_suspicion != undefined
                && diffTimeInMinutes(this.diagnosis.oliguria_suspicion.createdAt) >= 120
            );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'Assess for acute kidney injury', diagnosisValue = 0, admissionId= this.admission.id);
               
                R.next();
        }
    },
    { 
        "name": "Proteinura with dipstick available",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.settings.dipstick_available == 1
                && this.assessment.proteinura != undefined
                && this.assessment.proteinuria >= 2);
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'proteinura', diagnosisValue = 1, admissionId= this.admission.id);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'pre_eclapmsia_symptoms', admissionId = this.admission.id, intervalMinutes = 0);
    
                this.diagnosis["proteinura"] = {value: 1};
                R.next();
        }
    },
    { 
        "name": "Proteinura without dipstick available",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.settings.dipstick_available != undefined
                && this.settings.dipstick_available == 0
                && this.assessment.urine_cloudy == 1);
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'proteinura_suspicion', diagnosisValue = 1, admissionId= this.admission.id);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'pre_eclapmsia_symptoms', admissionId = this.admission.id, intervalMinutes = 0);
    
                this.diagnosis["proteinura_suspicion"] = {value: 1};
                R.next();
        }
    },
    { 
        "name": "[Proteinura] Laboratory capacity",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(((this.diagnosis.proteinura != undefined && this.diagnosis.proteinura == 1) 
                    || 
                (this.diagnosis.proteinura_suspicion != undefined && this.diagnosis.proteinura_suspicion.value == 1)) 
                && this.settings.laboratory_capacity == 1
            );
        },
        "consequence": function (R) {
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Sent PCR to confirm proteinura', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'pcr_proteinura_results', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "[Proteinura] [Laboratory capacity] PCR results",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.assessment.leucocyte_esterase != undefined && this.assessment.leucocyte_esterase == 1)
                    ||
                (this.assessment.nitrites != undefined && this.assessment.nitrites == 1)
            );
        },
        "consequence": function (R) {
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Sent PCR to confirm proteinura', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'pcr_proteinura_results', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "[Proteinura] Without laboratory capacity",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.assessment.flank_pain != undefined && this.assessment.flank_pain == 1)
                || (this.assessment.burning_micturarion != undefined && this.assessment.burning_micturarion == 1)
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Request medical review', admissionId= this.admission.id, this.woman.name, publish = false);
                
            R.next();
        }
    }
    ]
}