import RuleEngine from "node-rules";
import { AdmissionRules } from "./AdmissionRules";
import AssessmentRules from "./AssessmentRules";
import { FirstStage } from "./FirstStage";
import Markov from "../decision_models/Markov";
import { RobsonRules } from "./RobsonRules";
import { calculateDilatationProgress, calculateUrineVolume } from "../decision_models/Functions";

export default class RulesControl{

    static elegibility(fact) {
        console.log(fact)
        R = new RuleEngine();
        R.ignoreFactChanges = true;  
        R.register(AdmissionRules.elegibility);  
        R.execute(fact, function (data) {
            console.log(data.matchPath);
        });
    }

    static control_rules(fact){
        R = new RuleEngine();
        R.ignoreFactChanges = true;  
        
        //if elegibility was not evaluated yet <- this must never happen
        if(fact.diagnosis.elegible == undefined) {
        }
        //if elegible but not in active phase
        else if (fact.diagnosis.elegible != undefined 
            && fact.diagnosis.elegible.value == 1 
            && (fact.diagnosis.active_phase == undefined || fact.diagnosis.active_phase.value != 1))
        {
            R = new RuleEngine();
            R.ignoreFactChanges = true;
            R.register(FirstStage.evaluate_active_phase);

            R.execute(fact, function (data) {
                if(data.diagnosis.active_phase != undefined 
                    && data.diagnosis.active_phase.value == 1)
                {
                    //if entered in active phase start a new engine and evaluate assessment
                    AssessmentRules.assessment_rules(data);
                    //console.log(data.matchPath);
                }
                else {
                    return PubSub.publish('update-list-alert', {adm_id: fact.admission.id, name: fact.woman.name});
                }
            });            
        }
        //if elegible and in active phase - run all assessments evaluated
        else if (fact.diagnosis.elegible != undefined 
            && fact.diagnosis.elegible.value == 1 
            && fact.diagnosis.active_phase != undefined
            && fact.diagnosis.active_phase.value == 1
            )
        {            
            calculateUrineVolume(fact.admission.id).then((urine_volume_per_hour) => 
            {
                console.log("URINE VOLUME " + urine_volume_per_hour);
                //if(fact.assessment.urine_output != undefined){
                    fact.assessment.urine_volume_per_hour = urine_volume_per_hour;
                //}
                if(fact.assessment.cervical_dilatation_cm != undefined){
                    return calculateDilatationProgress(fact.admission.id);
                }
            }).then((cervical_dilatation_speed)  => 
            {
                if(cervical_dilatation_speed != undefined) {   
                    fact.assessment.cervical_dilatation_speed = cervical_dilatation_speed;             
                    return Markov.calculateSlowProgress(fact.admission.id);
                }
            }).then(() => 
            {
                console.log(fact);
                AssessmentRules.assessment_rules(fact);
            });

        }
        //if elegible and in active phase - run all assessments evaluated
        // else if (fact.diagnosis.elegible != undefined 
        //     && fact.diagnosis.elegible.value == 1 
        //     && fact.diagnosis.active_phase != undefined
        //     && fact.diagnosis.active_phase.value == 1)
        // {
        //     AssessmentRules.assessment_rules(fact);
        // }
        

        //check robson        
        var containRobson = false;
        //check if robson is not evaluated or is missing
        Object.keys(fact.diagnosis).forEach(element => {
            if(element.indexOf("robson") >= 0 && element != "robson99")
                containRobson = true;        
        });

        if(!containRobson){
            RobRules = new RuleEngine();
            RobRules.ignoreFactChanges = true;
            RobRules.register(RobsonRules.robsons);
            RobRules.execute(fact, function(data){
                //console.log(data.matchPath);
            });
        }

    }

}

