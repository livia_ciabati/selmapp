import RuleEngine from "node-rules";

//Yellow Flags: (HR 50-59 or 100-119) OR (SBP 90-99 OR 140-159) OR (DBP 40-49 or 90-99) OR (RR 20-24) OR (TºC 35.1-35.9 or 37.5-37.9)
//Red Flags: (HR < 50 or ≥ 120) OR(SBP < 90 or ≥ 160) OR (DBP < 40 or ≥ 100) OR (RR ≤ 10 or ≥ 25) OR (SpO2 ≤ 95%) OR (TºC ≤ 35 or ≥ 38)

    export var Meows = { 
        meows_rules: [
        {
            "name": "Respiratory green",
            "priority": 3,
            "on" : true,
            "condition": function (R) {
                R.when(this.assessment.respiratory_rate != undefined 
                    && this.assessment.respiratory_rate > 10 
                    && this.assessment.respiratory_rate < 20 );
            },
            "consequence": function (R) {
                this.meows.greenFlag = this.meows.greenFlag + 1
                R.next();
            }
        },{
            "name": "Respiratory yellow",
            "priority": 3,
            "on" : true,
            "condition": function (R) {
                R.when(this.assessment.respiratory_rate != undefined 
                    && this.assessment.respiratory_rate >= 20 
                    && this.assessment.respiratory_rate <= 24 );
            },
            "consequence": function (R) {
                this.meows.yellowFlag = this.meows.yellowFlag + 1
                R.next();
            }
        },{
            "name": "Respiratory red",
            "priority": 3,
            "on" : true,
            "condition": function (R) {
                R.when(this.assessment.respiratory_rate != undefined 
                    && (this.assessment.respiratory_rate >= 25 
                    || this.assessment.respiratory_rate <=10 ));
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag +1;
                R.next();
            }
        },{
            "name": "Oxygen Saturarion green",
            "condition": function (R) {
                R.when(this.assessment.oxygen_saturarion != undefined 
                    && this.assessment.oxygen_saturarion > 95 );
            },
            "consequence": function (R) {
                R.next();
            }
        },{
            "name": "Oxygen Saturarion red",
            "condition": function (R) {
                R.when(this.assessment.oxygen_saturarion != undefined 
                    && this.assessment.oxygen_saturarion <= 95);
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag +1;
                R.next();
            }
        },{
            "name": "temperatureGreen",
            "condition": function (R) {
                R.when(this.assessment.temperature != undefined
                    && this.assessment.temperature >= 36
                    && this.assessment.temperature <=38);
            },
            "consequence": function (R) {
                R.next();
            }
        },{
            "name": "temperatureYellow",
            "condition": function (R) {
                R.when(this.assessment.temperature != undefined
                    && ((this.assessment.temperature >= 35.1 && this.assessment.temperature <= 35.9)
                        || (this.assessment.temperature  >= 37.5 && this.assessment.temperature <= 37.9))
                );
            },
            "consequence": function (R) {
                this.meows.yellowFlag = this.meows.yellowFlag + 1
                R.next();
            }
        },{
            "name": "temperatureRed",
            "condition": function (R) {
                R.when(this.assessment.temperature != undefined
                    && (this.assessment.temperature <= 35 || this.assessment.temperature >= 38)
                );
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag + 1
                R.next();
            }
        },{
            "name": "sysBPGreen", 
            "condition": function (R) {
                R.when(this.assessment.systolic_blood_pressure != undefined
                    && this.assessment.systolic_blood_pressure >= 100
                    && this.assessment.systolic_blood_pressure < 140);
            },
            "consequence": function (R) {
                R.next();
            }
        },{
            "name": "sysBPYellow",
            "condition": function (R) {
                R.when(this.assessment.systolic_blood_pressure != undefined
                    && ((this.assessment.systolic_blood_pressure >= 90 && this.assessment.systolic_blood_pressure <= 99)
                        || (this.assessment.systolic_blood_pressure >= 140 && this.assessment.systolic_blood_pressure <= 159))
                );
            },
            "consequence": function (R) {
                this.meows.yellowFlag = this.meows.yellowFlag + 1;
                //console.log('MEOWS CONSOLE');
                //console.log(this.meows)
                R.next();
            }
        },{
            "name": "sysBPRed",
            "condition": function (R) {
                R.when(this.assessment.systolic_blood_pressure != undefined
                    && (this.assessment.systolic_blood_pressure < 90 || this.assessment.systolic_blood_pressure >= 160)
                );
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag +1;
                R.next();
            }
        },{
            "name": "diaBPGreen", 
            "condition": function (R) {
                R.when(this.assessment.diastolic_blood_pressure != undefined 
                    && this.assessment.diastolic_blood_pressure >= 50 
                    && this.assessment.diastolic_blood_pressure <  90);
            },
            "consequence": function (R) {
                R.next();
            }
        },{
            "name": "diaBPYellow", 
            "condition": function (R) {
                R.when(this.assessment.diastolic_blood_pressure != undefined 
                    &&
                    ((this.assessment.diastolic_blood_pressure >= 40 && this.assessment.diastolic_blood_pressure <= 49) 
                    ||
                     (this.assessment.diastolic_blood_pressure >= 90 && this.assessment.diastolic_blood_pressure <= 99))
                );
            },
            "consequence": function (R) {
                this.meows.yellowFlag = this.meows.yellowFlag + 1;
                R.next();
            }
        },{
            "name": "diaBPRed", 
            "condition": function (R) {
                R.when(this.assessment.diastolic_blood_pressure != undefined 
                    && (this.assessment.diastolic_blood_pressure <40 || this.assessment.diastolic_blood_pressure > 100));
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag +1;
                R.next();
            }
        },{
            "name": "maternalHeartRateGreen",
            "condition": function (R) {
                R.when(this.assessment.maternal_heart_rate != undefined
                    && this.assessment.maternal_heart_rate >= 60
                    && this.assessment.maternal_heart_rate < 100);
            },
            "consequence": function (R) {
                R.next();
            }
        },{
            "name": "maternalHeartRateYellow", 
            "condition": function (R) {
                R.when(this.assessment.maternal_heart_rate != undefined
                    && 
                    ((this.assessment.maternal_heart_rate >= 50 && this.assessment.maternal_heart_rate <= 59)
                    ||
                     (this.assessment.maternal_heart_rate >= 100 && this.assessment.maternal_heart_rate <= 119))
                );
            },
            "consequence": function (R) {
                this.meows.yellowFlag = this.meows.yellowFlag + 1;
                
                R.next();
            }
        },{
            "name": "maternalHeartRateRed",
            "condition": function (R) {
                R.when(this.assessment.maternal_heart_rate != undefined
                    && (this.assessment.maternal_heart_rate < 50 || this.assessment.maternal_heart_rate >= 120)
                );
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag +1;
                R.next();
            }
        },{
            "name": "fetalHeartRateGreen",
            "condition": function (R) {
                R.when(this.assessment.fetal_heart_rate != undefined 
                    && this.assessment.fetal_heart_rate >= 120
                    && this.assessment.fetal_heart_rate <= 160 );
            },
            "consequence": function (R) {
                R.next();
            }
        },{
            "name": "fetalHeartRateYellow", 
            "condition": function (R) {
                R.when(this.assessment.fetal_heart_rate != undefined 
                    && this.assessment.fetal_heart_rate >= 110
                    && this.assessment.fetal_heart_rate <= 119 );
            },
            "consequence": function (R) {
                this.meows.yellowFlag = this.meows.yellowFlag + 1
                R.next();
            }
        },{
            "name": "fetalHeartRateRed",
            "condition": function (R) {
                R.when(this.assessment.fetal_heart_rate != undefined 
                    && 
                    ((this.assessment.fetal_heart_rate >= 80 && this.assessment.fetal_heart_rate <= 109 )
                    ||
                     (this.assessment.fetal_heart_rate >= 160 && this.assessment.fetal_heart_rate <= 210 ))
                    
                );
            },
            "consequence": function (R) {
                this.meows.redFlag = this.meows.redFlag +1;
                R.next();
            }
        },
    //     consciousnessGreen: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             R.next();
    //         }
    //     }], 
    //     consciousnessYellow: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.yellowFlag = this.meows.yellowFlag + 1
    //             R.next();
    //         }
    //     }], 
    //     consciousnessRed: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.redFlag = this.meows.redFlag +1;
    //             R.next();
    //         }
    //     }],
    //     painGreen: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             R.next();
    //         }
    //     }], 
    //     painYellow: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.yellowFlag = this.meows.yellowFlag + 1
    //             R.next();
    //         }
    //     }], 
    //     painRed: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.redFlag = this.meows.redFlag +1;
    //             R.next();
    //         }
    //     }],
    //     lochiaGreen: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             R.next();
    //         }
    //     }], 
    //     lochiaYellow: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.yellowFlag = this.meows.yellowFlag + 1
    //             R.next();
    //         }
    //     }], 
    //     lochiaRed: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.redFlag = this.meows.redFlag +1;
    //             R.next();
    //         }
    //     }],
    //     proteinureaGreen: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             R.next();
    //         }
    //     }], 
    //     proteinureaYellow: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.yellowFlag = this.meows.yellowFlag + 1
    //             R.next();
    //         }
    //     }], 
    //     proteinureaRed: [{
    //         "condition": function (R) {
    //             R.when(this.assessment.estimate_gestional_age >= 37);
    //         },
    //         "consequence": function (R) {
    //             this.meows.redFlag = this.meows.redFlag +1;
    //             R.next();
    //         }
    //     }]
    ]}
//};
