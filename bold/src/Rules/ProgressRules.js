import { diffTimeNowInMinutes } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var ProgressRules = {
    evaluate_progress: [
        //second stage
        {
            "name": "Second stage",
            "priority": 3,
            "on" : true,
            "condition": function (R) {
                R.when(
                    this.diagnosis.elegible != undefined
                    && parseInt(this.diagnosis.elegible.value) == 1 
                    && this.assessment.cervical_dilatation_cm != undefined 
                    && this.assessment.cervical_dilatation_cm == 10
                );
            },
            "consequence": function (R) {
                        
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'second_stage', diagnosisValue = 1, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'active_phase', diagnosisValue = 0, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Second stage', admissionId= this.admission.id, this.woman.name, publish = false);
                
                R.stop();
            }
        }, { 
        "name": "Progress",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1
                && this.assessment.slow_than_normal != undefined
                && this.assessment.slow_than_normal == 0
                );
        },
        "consequence": function (R) {    
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'contractions_form', admissionId = this.admission.id, intervalMinutes = 30);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'full_assessment', admissionId = this.admission.id, intervalMinutes = 120);

            R.next();
        }
    },
    { 
        "name": "No progress",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1
                && this.assessment.slow_than_normal != undefined
                && this.assessment.slow_than_normal == 1             
               );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'labour_arrest', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Labour arrest', admissionId= this.admission.id, this.woman.name, publish = false);
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'C-section indication', admissionId= this.admission.id, this.woman.name, publish = false);
                R.next();
        }
    },
    { 
        "name": "Hypercontractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.number_uterine_contractions != undefined
                && this.assessment.duration_uterine_contractions != undefined
                && this.assessment.number_uterine_contractions >= 6 
                && this.assessment.duration_uterine_contractions >= 60);
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypercontraction', diagnosisValue = 1, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypocontraction', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'strong_contractions', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_contractions', diagnosisValue = 0, admissionId= this.admission.id);
                this.diagnosis["hypercontraction"] = {value : 1};
                this.diagnosis["hypocontraction"] = {value : 0};
                this.diagnosis["strong_contractions"] = {value : 0};
                this.diagnosis["normal_contractions"] = {value : 0};
                R.next();
        }
    },
    { 
        "name": "Very strong contractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.number_uterine_contractions != undefined
                && this.assessment.number_uterine_contractions == 5
                );
        },
        "consequence": function (R) {                    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypercontraction', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypocontraction', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'strong_contractions', diagnosisValue = 1, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_contractions', diagnosisValue = 0, admissionId= this.admission.id);
            this.diagnosis["hypercontraction"] = {value : 0};
            this.diagnosis["hypocontraction"] = {value : 0};
            this.diagnosis["strong_contractions"] = {value : 1};
            this.diagnosis["normal_contractions"] = {value : 0};

            R.next();
        }
    },
    { 
        "name": "Normal contractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.number_uterine_contractions != undefined
                && this.assessment.duration_uterine_contractions != undefined
                && this.assessment.number_uterine_contractions >= 3 
                && this.assessment.number_uterine_contractions <= 4 
                && this.assessment.duration_uterine_contractions >= 40 
                && this.assessment.duration_uterine_contractions <=60
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypercontraction', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypocontraction', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'strong_contractions', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_contractions', diagnosisValue = 1, admissionId= this.admission.id);
            this.diagnosis["hypercontraction"] = {value : 0};
            this.diagnosis["hypocontraction"]  = {value : 0};
            this.diagnosis["strong_contractions"] = {value : 0};
            this.diagnosis["normal_contractions"] = {value : 1};
                
            R.next();
        }
    },
    { 
        "name": "Hypocontractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.number_uterine_contractions != undefined
                && this.assessment.duration_uterine_contractions != undefined
                && parseInt(this.assessment.number_uterine_contractions) <= 2 
                && parseInt(this.assessment.duration_uterine_contractions) < 40
                );
        },
        "consequence": function (R) {    
           //(this);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypercontraction', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypocontraction', diagnosisValue = 1, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'strong_contractions', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_contractions', diagnosisValue = 0, admissionId= this.admission.id);
            this.diagnosis["hypercontraction"] = {value : 0};
            this.diagnosis["hypocontraction"]  = {value : 1};
            this.diagnosis["strong_contractions"] = {value : 0};
            this.diagnosis["normal_contractions"] = {value : 0};
                
                R.next();
        }
    },
    { 
        "name": "95% percentile exceeded",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.slow_than_normal != undefined
                && this.assessment.slow_than_normal == 1
                );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'slow_progress', diagnosisValue = 1, admissionId= this.admission.id);   
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'signs_cpd', admissionId = this.admission.id, intervalMinutes = 0); 
                R.next();
        }
    },
    { 
        "name": "Signs of CPD",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && this.diagnosis.normal_contractions != undefined
                && this.diagnosis.normal_contractions.value == 1 
                && this.assessment.signs_cpd == 1
                );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'C-section indication', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "No signs of CPD",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && this.diagnosis.normal_contractions != undefined
                && this.diagnosis.normal_contractions.value == 1 
                && this.assessment.signs_cpd == 0
            );
        },
        "consequence": function (R) {
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'full_assessment', admissionId = this.admission.id, intervalMinutes = 120);    
            R.next();
        }
    },
    { 
        "name": "Fetal distress",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.hypocontraction != undefined
                && this.diagnosis.hypocontraction.value == 1 
                && this.diagnosis.abnormal_fhr != undefined
                && this.diagnosis.abnormal_fhr.value == 1 
                && (this.assessment.meconium != undefined && this.assessment.meconium == 1)
                    ||
                   (this.diagnosis.meconium != undefined && this.diagnosis.meconium.value == 1)
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'C-section indication', admissionId= this.admission.id, this.woman.name, publish = false);
    
            R.next();
        }
    },
    { 
        "name": "[Slow progress] No sings of fetal distress, membrane ruptured with CTG available",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && this.diagnosis.hypocontraction != undefined
                && this.diagnosis.hypocontraction.value == 1 
                && (this.diagnosis.abnormal_fhr == undefined || this.diagnosis.abnormal_fhr.value != 1) 
                && (this.assessment.meconium == undefined || this.assessment.meconium != 1)
                && (this.diagnosis.meconium == undefined || this.diagnosis.meconium.value != 1)
                && this.settings.ctg_available == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_cardiotography', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous fetal cardiotography', admissionId= this.admission.id, this.woman.name, publish = false);

            R.next();
        }
    },
    { 
        "name": "[Slow progress] Meconium with laboratory capacity",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && this.diagnosis.hypocontraction != undefined
                && this.diagnosis.hypocontraction.value == 1
                && (this.assessment.amniotic_membranes_status != undefined && this.assessment.amniotic_membranes_status == 1)
                    ||
                   (this.diagnosis.amniotic_membranes_status != undefined && this.diagnosis.amniotic_membranes_status.value == 1)
                && (this.assessment.meconium != undefined && this.assessment.meconium == 1)
                    ||
                   (this.diagnosis.meconium != undefined && this.diagnosis.meconium.value == 1)
                && this.settings.laboratory_capacity == 1
                 
                );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'meconium', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Collect fetal blood sample', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'fetal_blood_sample', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "[Slow progress] Normal fetal blood sample",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && (this.assessment.meconium != undefined && this.assessment.meconium == 1)
                    ||
                    (this.diagnosis.meconium != undefined && this.diagnosis.meconium.value == 1)
                && this.settings.laboratory_capacity == 1
                && this.assessment.abnormal_fetal_blood_results != undefined
                && this.assessment.abnormal_fetal_blood_results == 0
            );
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId= this.admission.id);    
           
            //console.log("9 INTENSIFIED MONITORING: " + JSON.stringify(this.diagnosis));
            if(this.diagnosis.intensified_monitoring != undefined)
                this.diagnosis.intensified_monitoring.value = 1
            else  
                this.diagnosis["intensified_monitoring"] = { value: 1 };

            R.next();
        }
    },
    { 
        "name": "[Slow progress] Abnormal fetal blood sample",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && (this.assessment.meconium != undefined && this.assessment.meconium == 1)
                    ||
                    (this.diagnosis.meconium != undefined && this.diagnosis.meconium.value == 1)
                && this.settings.laboratory_capacity == 1
                && this.assessment.abnormal_fetal_blood_results != undefined
                && this.assessment.abnormal_fetal_blood_results == 1
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'C-section indication', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Slow progress] Hypercontractions and CTG available",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.slow_progress != undefined
                && this.diagnosis.slow_progress.value == 1 
                && this.diagnosis.hypercontraction != undefined
                && this.diagnosis.hypercontraction.value == 1 
                && this.settings.ctg_available == 1
                );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypercontraction', diagnosisValue = 1, admissionId= this.admission.id);
                
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_cardiotography', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous fetal cardiotography', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "Fetal occipital posterial",
        "priority": 3,
        "on": true,
        "condition": function (R) {
                R.when(this.diagnosis.hypercontraction != undefined
                    && this.diagnosis.hypercontraction.value == 1 
                    && this.settings.ctg_available == 0
                    && this.assessment.position_fetal_head == 2
                );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'C-section indication', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "Cephalopelvic disproportion suspicion and fetus alive",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.hypercontraction != undefined
                && this.diagnosis.hypercontraction.value == 1 
                && this.settings.ctg_available == 0
                && (this.assessment.position_fetal_head != undefined || this.assessment.position_fetal_head != 2)
                && this.assessment.cpd_suspicion != undefined
                && this.assessment.cpd_suspicion == 1
                && this.assessment.fetus_alive != undefined
                && this.assessment.fetus_alive == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'C-section indication', admissionId= this.admission.id, this.woman.name, publish = false);
    
            R.next();
        }
    },
    { 
        "name": "Cephalopelvic disproportion suspicion and fetus not alive",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.hypercontraction != undefined
                && this.diagnosis.hypercontraction.value == 1 
                && this.settings.ctg_available == 0
                && (this.assessment.position_fetal_head != undefined || this.assessment.position_fetal_head != 2)
                && this.assessment.cpd_suspicion != undefined
                && this.assessment.cpd_suspicion == 1
                && this.assessment.fetus_alive != undefined
                && this.assessment.fetus_alive == 0
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'craniotomy', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Craniotomy', admissionId= this.admission.id, this.woman.name, publish = false);
    
            R.next();
        }
    },
    
    { 
        "name": "[Using oxytocin] Hypercontractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.time_to_delivery == undefined || (this.diagnosis.time_to_delivery != undefined  && this.diagnosis.time_to_delivery.value == 0 ))
                && (this.diagnosis.craniotomy == undefined || (this.diagnosis.craniotomy != undefined && this.diagnosis.craniotomy.value == 0))
                && this.diagnosis.hypercontraction != undefined
                && this.diagnosis.hypercontraction.value == 1 
                && this.diagnosis.oxytocin != undefined
                && this.diagnosis.oxytocin.value == 1
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Stop oxytocin', admissionId= this.admission.id, this.woman.name, publish = false);
    
            R.next();
        }
    },
    { 
        "name": "[Using oxytocin] Very strong contractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.time_to_delivery == undefined || (this.diagnosis.time_to_delivery != undefined  && this.diagnosis.time_to_delivery.value == 0 ))
            && (this.diagnosis.craniotomy == undefined || (this.diagnosis.craniotomy != undefined && this.diagnosis.craniotomy.value == 0))
                && this.diagnosis.strong_contractions != undefined
                && this.diagnosis.strong_contractions.value == 1 
                && this.diagnosis.oxytocin != undefined
                && this.diagnosis.oxytocin.value == 1
                && diffTimeNowInMinutes(this.diagnosis.oxytocin.createdAt) >= 60
                );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Reduce oxytocin rate', admissionId= this.admission.id, this.woman.name, publish = false);
            //admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oxytocin', diagnosisValue = 1, admissionId= this.admission.id);
    
            R.next();
        }
    },
    { 
        "name": "[Using oxytocin] Normal contractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.time_to_delivery == undefined || (this.diagnosis.time_to_delivery != undefined  && this.diagnosis.time_to_delivery.value == 0 ))
            && (this.diagnosis.craniotomy == undefined || (this.diagnosis.craniotomy != undefined && this.diagnosis.craniotomy.value == 0))
                && this.diagnosis.normal_contractions != undefined
                && this.diagnosis.normal_contractions.value == 1 
                && this.diagnosis.oxytocin != undefined
                && this.diagnosis.oxytocin.value == 1
                && diffTimeNowInMinutes(this.diagnosis.oxytocin.createdAt) >= 60
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Maintain oxytocin rate', admissionId= this.admission.id, this.woman.name, publish = false);
            //admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oxytocin', diagnosisValue = 1, admissionId= this.admission.id);
    
            R.next();
        }
    },
    { 
        "name": "[Using oxytocin] Hypocontractions",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.time_to_delivery == undefined || (this.diagnosis.time_to_delivery != undefined  && this.diagnosis.time_to_delivery.value == 0 ))
                && (this.diagnosis.craniotomy == undefined || (this.diagnosis.craniotomy != undefined && this.diagnosis.craniotomy.value == 0))
                && this.diagnosis.hypocontraction != undefined
                && this.diagnosis.hypocontraction.value == 1 
                && this.diagnosis.oxytocin != undefined 
                && this.diagnosis.oxytocin.value == 1
                && diffTimeNowInMinutes(this.diagnosis.oxytocin.createdAt) >= 60
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Increase oxytocin rate', admissionId= this.admission.id, this.woman.name, publish = false);
            //admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oxytocin', diagnosisValue = 1, admissionId= this.admission.id);
    
            R.next();
        }
    },
    { 
        "name": "Start oxitocin",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.diagnosis.time_to_delivery == undefined || (this.diagnosis.time_to_delivery != undefined  && this.diagnosis.time_to_delivery.value == 0 ))
                && (this.diagnosis.craniotomy == undefined || (this.diagnosis.craniotomy != undefined && this.diagnosis.craniotomy.value == 0))
                && this.diagnosis.hypocontraction != undefined
                && this.diagnosis.hypocontraction.value == 1 
                && (this.diagnosis.oxytocin == undefined || (this.diagnosis.oxytocin != undefined && this.diagnosis.oxytocin.value == 0))
                );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Start oxytocin', admissionId= this.admission.id, this.woman.name, publish = false);
            //admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oxytocin', diagnosisValue = 1, admissionId= this.admission.id);
    
            R.next();
        }
    },
    { 
        "name": "Slow but normal cervical dilatation",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(
                // this.diagnosis.slow_progress != undefined
                // && this.diagnosis.slow_progress.value == 0  && 
               this.assessment.cervical_dilatation_speed < 0.5
            );
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'slow_but_normal', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Review coping stategies', admissionId= this.admission.id, this.woman.name, publish = false);

            this.diagnosis["slow_but_normal"] = {value: 1}
            R.next();
        }
    }, 
    { 
        "name": "Oxytocin performed",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.assessment.oxytocin_infusion != undefined
                && this.assessment.oxytocin_infusion == 1)
                ||
                (this.assessment.augmentation_labour != undefined
                 && this.assessment.augmentation_labour == 1 )
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oxytocin', diagnosisValue = 1, admissionId= this.admission.id);

            R.next();
        }
    },
    { 
        "name": "Removing oxytocin",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when((this.assessment.oxytocin_infusion != undefined
                && this.assessment.oxytocin_infusion != 1)
                ||
                (this.assessment.augmentation_labour != undefined
                 && this.assessment.augmentation_labour != 1 )
            );
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'oxytocin', diagnosisValue = 0, admissionId= this.admission.id);

            R.next();
        }
    },

    //
    ]
}
