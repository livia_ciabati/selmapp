import { diffTimeNowInMinutes } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var FetalDescentRules = {
    evaluate_fetal_descent: [{ 
        "name": "Fetal descent and advanced first stage of labour",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.cervical_dilatation != undefined 
                && this.assessment.cervical_dilatation >= 7 
                && this.assessment.fetal_change != undefined
                && this.assessment.fetal_change == 0 
                && this.diagnosis.fetal_descent_time != undefined
                && diffTimeNowInMinutes(this.diagnosis.fetal_descent_time.createAt) < (4 * 60)
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'slow_fetal_descent', diagnosisValue = 0, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Slow fetal descent', admissionId= this.admission.id, this.woman.name, publish = false);

            R.next();
        }
    }]
}