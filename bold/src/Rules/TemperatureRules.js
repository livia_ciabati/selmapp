import { diffTimeNowInMinutes } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var TemperatureRules = {
    evaluate_temperature: [
        { 
            "name": "Normal temperature",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.assessment.axillary_temperature >= 35 
                    && this.assessment.axillary_temperature < 37.5);
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fever', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fever_suspicion', diagnosisValue = 0, admissionId= this.admission.id);
        
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'moderate_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'mild_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
                
                R.next();
            }
        },
        { 
        "name": "Fever",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.axillary_temperature != undefined
                && (parseInt(this.assessment.axillary_temperature) >= 38 
                    || (this.assessment.axillary_temperature != undefined
                        && parseFloat(this.assessment.axillary_temperature) > 37.5
                        && this.diagnosis.fever_suspicion != undefined
                        && parseInt(this.diagnosis.fever_suspicion.value) == 1 
                        && diffTimeNowInMinutes(this.diagnosis.fever_suspicion.createdAt) >= 60
                    )
                )
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fever', diagnosisValue = 1, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fever_suspicion', diagnosisValue = 0, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Fever', admissionId= this.admission.id, this.woman.name, publish = false);
                //AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'high_temperature', admissionId = this.admission.id, intervalMinutes = 1);
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'abnormal_temperature', diagnosisValue = 1, admissionId= this.admission.id);

                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'investigate_infection_causes', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Investigate infectious causes of raised temperature ', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'investigate_infection_causes', admissionId = this.admission.id, intervalMinutes = 0);

                R.next();
        }
    },
    { 
        "name": "Fever suspicion",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.axillary_temperature != undefined
                && parseFloat(this.assessment.axillary_temperature) > 37.5 
                && (this.diagnosis.fever_suspicion == null || this.diagnosis.fever_suspicion.value == 0));
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fever', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fever_suspicion', diagnosisValue = 1, admissionId= this.admission.id);
    
            R.next();
        }
    },
    { 
        "name": "[Cold exposure] Mild hypotermia",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.axillary_temperature != undefined
                && this.assessment.cold_exposure != undefined
                && this.assessment.cold_exposure == 1 
                && this.assessment.axillary_temperature > 32 
                && this.assessment.axillary_temperature < 35
                );
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'moderate_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'mild_hypotermia', diagnosisValue =1, admissionId= this.admission.id);

                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Passive rewarming', admissionId= this.admission.id, this.woman.name, publish = false);
                
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'abnormal_temperature', diagnosisValue =1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Ensure delivery room temperature', admissionId= this.admission.id, this.woman.name, publish = false);
                
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'investigate_infection_causes', diagnosisValue =1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Investigate infectious causes of raised temperature', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'investigate_infection_causes', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "[Cold exposure] [Hypotermia] Cardiac arrest",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.axillary_temperature != undefined
                && this.assessment.cold_exposure != undefined
                && this.assessment.cold_exposure == 1 
                && (this.assessment.axillary_temperature < 32 || this.assessment.axillary_temperature > 35) 
                && this.assessment.cardiac_arrest != undefined
                && this.assessment.cardiac_arrest == 1
            );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotermia', diagnosisValue = 1, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'moderate_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'mild_hypotermia', diagnosisValue =0, admissionId= this.admission.id);

                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Severe hypotermia and cardiac arrest', admissionId= this.admission.id, this.woman.name, publish = false);
                
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'out_of_selma', diagnosisValue = 1, admissionId= this.admission.id);
    
                R.next();
        }
    },
    { 
        "name": "[Cold exposure] Severe hypotermia",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.cold_exposure != undefined
                && this.assessment.cold_exposure == 1 
                && this.assessment.axillary_temperature != undefined
                && this.assessment.axillary_temperature < 36 
                && (this.diagnosis.mild_hypotermia == undefined || this.diagnosis.mild_hypotermia.value == 0)
            );
        },
        "consequence": function (R) {
    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotermia', diagnosisValue = 0, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'moderate_hypotermia', diagnosisValue = 1, admissionId= this.admission.id);
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'mild_hypotermia', diagnosisValue =0, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Moderate to severe hypotermia', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Chorioamnionitis/amnionitis",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1 
                && this.assessment.chorioamnionitis_amnionitis != undefined
                && this.assessment.chorioamnionitis_amnionitis == 1
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'chorioamnionitis_amnionitis', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Give  antibiotics', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Dysuria, increased frequency and urgency of urination",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1 
                && this.assessment.urine_infection != undefined
                && this.assessment.urine_infection == 1
                );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'urine_infection', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Urine tract infection', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Pyelonephritiis",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && this.assessment.pyelonephritiis != undefined
                && this.assessment.pyelonephritiis == 1
                );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'pyelonephritiis', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Pyelonephritiis', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Pneumonia",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && this.assessment.pneumonia != undefined
                && this.assessment.pneumonia == 1
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'pneumonia', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Pneumonia', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Uncomplicated malaria ",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && this.assessment.uncomplicated_malaria != undefined
                && this.assessment.uncomplicated_malaria == 1
                );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'uncomplicated_malaria', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Uncomplicated malaria', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Severe malaria ",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1 
                && this.assessment.severe_malaria != undefined
                && this.assessment.severe_malaria == 1
                );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_malaria', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Severe malaria', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Other source",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && this.assessment.other_source != undefined
                && this.assessment.other_source == 1
                );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'other_source', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Other source of infection', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "Infection diagnosed",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && (
                       (this.assessment.chorioamnionitis_amnionitis != undefined && this.assessment.chorioamnionitis_amnionitis == 1 ) 
                    || (this.assessment.urine_infection != undefined && this.assessment.urine_infection == 1 ) 
                    || (this.assessment.pyelonephritiis != undefined && this.assessment.pyelonephritiis == 1 ) 
                    || (this.assessment.pneumonia != undefined && this.assessment.pneumonia == 1 ) 
                    || (this.assessment.uncomplicated_malaria != undefined && this.assessment.uncomplicated_malaria == 1 ) 
                    || (this.assessment.severe_malaria != undefined && this.assessment.severe_malaria == 1 ) 
                    || (this.assessment.other_source!= undefined && this.assessment.other_source == 1) 
                )
            );
        },
        "consequence": function (R) {
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Is possible to transfer woman?', admissionId= this.admission.id, this.woman.name, publish = false);    
            R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Laboratory capacity",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && this.settings.laboratory_capacity != undefined
                && this.settings.laboratory_capacity ==  1
                && (this.diagnosis.infection_laboratory_test == undefined
                    || (this.diagnosis.infection_laboratory_test != undefined && this.diagnosis.infection_laboratory_test.value != 1)
                )
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'infection_laboratory_test', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Laboratory test for sepsis', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'blood_test_for_sepsis', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Sepis high risk",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                    && ( ( (this.assessment.woman_mental_status != undefined      && this.assessment.woman_mental_status >= 3     )
                        || (this.assessment.respiratory_rate != undefined         && this.assessment.respiratory_rate >= 25        )
                        || (this.assessment.maternal_heart_rate != undefined      && this.assessment.maternal_heart_rate > 130     )
                        || (this.assessment.systolic_blood_pressure != undefined  && this.assessment.systolic_blood_pressure <= 90 )
                        || (this.assessment.diastolic_blood_pressure != undefined && this.assessment.diastolic_blood_pressure >140 )
                        || (this.assessment.urine_output != undefined             && this.assessment.urine_output == 0.5           )
                        || (this.assessment.urine_abnormal != undefined           && this.assessment.urine_abnormal == 1           )
                    )
                    || (this.assessment.lactate != undefined && this.assessment.lactate >= 2))
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'sepsis_high_risk', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'High risk of severe illness or death from sepsis', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Sepis moderate risk with continuous monitoring",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                &&  (  (this.assessment.respiratory_rate != undefined        && this.assessment.respiratory_rate >= 21 && this.assessment.respiratory_rate <= 24) 
                    || (this.assessment.maternal_heart_rate != undefined     && this.assessment.maternal_heart_rate >= 100 && this.assessment.maternal_heart_rate <= 130) 
                    || (this.assessment.systolic_blood_pressure != undefined && this.assessment.systolic_blood_pressure >= 91 && this.assessment.systolic_blood_pressure <= 100) 
                    || (this.assessment.urine_output != undefined            && this.assessment.urine_output > 0.5 && this.assessment.urine_output < 1)
                ) 
                &&  (this.assessment.lactate == undefined || this.assessment.lactate < 2)
            );
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'sepsis_moderate_risk', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Moderate to severe risk of illness or death from sepsis', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Continuous monitoring",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1
                && this.settings.continuous_monitoring ==  1
                );
        },
        "consequence": function (R) {
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous monitoring', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_monitoring', admissionId = this.admission.id, intervalMinutes = 1);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal temperature] Without continuous monitoring",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_temperature != undefined
                && this.diagnosis.abnormal_temperature.value == 1 
                && this.settings.continuous_monitoring ==  0
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Intensified monitoring', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'all_vitals', admissionId = this.admission.id, intervalMinutes = 30);
                    
            if(this.diagnosis.intensified_monitoring != undefined)
                this.diagnosis.intensified_monitoring.value = 1
            else  
                this.diagnosis["intensified_monitoring"] = { value: 1 };
                
    
                R.next();
        }
    }]
}