import { diffTimeNowInMinutes } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var FetalHeartRate = {
    evaluate_fetal_heart_rate: [{ 
        "name": "Reassure FHR retacted",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_fhr != undefined 
                    && this.diagnosis.abnormal_fhr.value == 1 
                    && this.assessment.fetal_heart_rate != undefined
                    && this.assessment.fetal_heart_rate > 100 
                    && this.assessment.fetal_heart_rate < 160);
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'abnormal_fhr', diagnosisValue = 0, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Abnormal fetal heart rate retacted', admissionId= this.admission.id, this.woman.name, publish = false);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal fetal heart rate] CTG available",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_fhr != undefined 
                    && this.diagnosis.abnormal_fhr.value == 1 
                    && this.assessment.fetal_heart_rate != undefined
                    && (this.assessment.fetal_heart_rate < 100 || this.assessment.fetal_heart_rate >= 160) 
                    && this.settings.ctg_available == 1
            );
        },
        "consequence": function (R) {    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_cardiotography', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous fetal cardiotography', admissionId= this.admission.id, this.woman.name, publish = false);
                //AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_fhr_monitoring', admissionId = this.admission.id, intervalMinutes = 1);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'all_vitals', admissionId = this.admission.id, intervalMinutes = 1);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal fetal heart rate] CTG not available and fetal scalp ph available",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.diagnosis.abnormal_fhr != undefined 
                    && this.diagnosis.abnormal_fhr.value == 1 
                    && this.assessment.fetal_heart_rate != undefined
                    && (this.assessment.fetal_heart_rate < 100 || this.assessment.fetal_heart_rate >= 160) 
                    && this.settings.ctg_available == 0 && this.settings.fetal_scalp_lactate == 1
            );
        },
        "consequence": function (R) {
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Fetal scalp pH', admissionId= this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'fetal_scalp_ph_lactate', admissionId = this.admission.id, intervalMinutes = 0);
    
                R.next();
        }
    },
    { 
        "name": "[Abnormal fetal heart rate] CTG available and FHR abnormal",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.ctg_abnormal != undefined && this.assessment.ctg_abnormal == 1);
        },
        "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fetal_distress', diagnosisValue = 1, admissionId= this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Delivery for suspect of fetal distress', admissionId= this.admission.id, this.woman.name, publish = false);
    
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
                R.next();
        }
    },
    { 
        "name": "[Abnormal fetal heart rate] CTG not available and fetal scalp ph available and abnormal",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.scalp_ph != undefined && this.assessment.scalp_ph == 1);
        },
        "consequence": function (R) {    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'fetal_distress', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Delivery for suspect of fetal distress', admissionId= this.admission.id, this.woman.name, publish = false);

            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'time_to_delivery', diagnosisValue = 1, admissionId= this.admission.id);
    
            R.next();
        }
    },
    { 
        "name": "Abnormal fetal heart rate suspicion",
        "priority": 3,
        "on": true,
        "condition": function (R) {
            R.when(this.assessment.fetal_heart_rate != undefined
                && (this.assessment.fetal_heart_rate < 100 || this.assessment.fetal_heart_rate >= 160) );
        },
        "consequence": function (R) {
    
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'abnormal_fhr', diagnosisValue = 1, admissionId= this.admission.id);
            admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Abnormal fetal heart rate', admissionId= this.admission.id, this.woman.name, publish = false);
            AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'abnormal_fetal_heart_rate', admissionId = this.admission.id, intervalMinutes = 1);
    
            R.next();
        }
    }]
}