import RuleEngine from "node-rules";
import AdmissionRepository from '../repositories/AdmissionRepository';
import admissionAlertsRepository from '../repositories/admissionAlertsRepository';
import AssessmentScheduleRepository from '../repositories/AssessmentScheduleRepository';
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";

export var RobsonRules = {
    robsons: [{
        "name": "Robson 1",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity == 0
                && this.pregnancy.estimate_gestional_age != undefined
                && this.pregnancy.estimate_gestional_age >= 38 //term
                && this.pregnancy.num_previous_c_section != undefined
                && this.pregnancy.num_previous_c_section == 0
                && (this.pregnancy.previous_uterine_surgery == undefined || this.pregnancy.previous_uterine_surgery == 0)
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                && this.admission.mode_onset_labour != undefined
                && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson1', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson1"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 2",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity == 0
                && this.pregnancy.estimate_gestional_age != undefined
                && this.pregnancy.estimate_gestional_age >= 38 //term
                && this.pregnancy.num_previous_c_section != undefined
                && this.pregnancy.num_previous_c_section == 0
                && (this.pregnancy.previous_uterine_surgery == undefined || this.pregnancy.previous_uterine_surgery == 0)
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                && this.admission.mode_onset_labour != undefined
                && this.admission.mode_onset_labour != 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson2', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson2"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 3",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity > 0
                && this.pregnancy.estimate_gestional_age != undefined
                && this.pregnancy.estimate_gestional_age >= 38 //term
                && this.pregnancy.num_previous_c_section != undefined
                && this.pregnancy.num_previous_c_section == 0
                && (this.pregnancy.previous_uterine_surgery == undefined || this.pregnancy.previous_uterine_surgery == 0)
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                && this.admission.mode_onset_labour != undefined
                && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson3', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson3"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 4",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity > 0
                && this.pregnancy.estimate_gestional_age != undefined
                && this.pregnancy.estimate_gestional_age >= 38 //term
                && this.pregnancy.num_previous_c_section != undefined
                && this.pregnancy.num_previous_c_section == 0
                && (this.pregnancy.previous_uterine_surgery == undefined || this.pregnancy.previous_uterine_surgery == 0)
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                && this.admission.mode_onset_labour != undefined
                && this.admission.mode_onset_labour != 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson4', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson4"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 5",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity >= 0
                && this.pregnancy.estimate_gestional_age != undefined
                && this.pregnancy.estimate_gestional_age >= 38 //term
                && (
                    (this.pregnancy.num_previous_c_section != undefined
                    && this.pregnancy.num_previous_c_section > 0)
                    ||
                    (this.pregnancy.previous_uterine_surgery != undefined 
                    && this.pregnancy.previous_uterine_surgery != 0)
                )
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                && this.admission.mode_onset_labour != undefined
                && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson5', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson5"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 6",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity == 0
                // && this.pregnancy.estimate_gestional_age != undefined
                // && this.pregnancy.estimate_gestional_age >= 38 //term
                // && this.pregnancy.num_previous_c_section != undefined
                // && this.pregnancy.num_previous_c_section == 0
                // && this.pregnancy.previous_uterine_surgery != undefined
                // && this.pregnancy.previous_uterine_surgery == 1
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                // && this.admission.mode_onset_labour != undefined
                // && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 1 //breech
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson6', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson6"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 7",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.pregnancy.parity != undefined
                && this.pregnancy.parity > 0
                // && this.pregnancy.estimate_gestional_age != undefined
                // && this.pregnancy.estimate_gestional_age >= 38 //term
                // && this.pregnancy.num_previous_c_section != undefined
                // && this.pregnancy.num_previous_c_section == 0
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                // && this.admission.mode_onset_labour != undefined
                // && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 1 //breech
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson7', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson7"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 8",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
             R.when(
            //this.pregnancy.parity != undefined
            //     && this.pregnancy.parity == 0
            //     && this.pregnancy.estimate_gestional_age != undefined
            //     && this.pregnancy.estimate_gestional_age >= 38 //term
            //     && this.pregnancy.num_previous_c_section != undefined
            //     && this.pregnancy.num_previous_c_section == 0
                this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy != 0 //multiple
                // && this.admission.mode_onset_labour != undefined
                // && this.admission.mode_onset_labour == 0 //spontenous
                // && this.assessment.fetal_presentation != undefined
                // && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson8', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson8"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 9",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(
                //this.pregnancy.parity != undefined
                // && this.pregnancy.parity == 0
                // && this.pregnancy.estimate_gestional_age != undefined
                // && this.pregnancy.estimate_gestional_age >= 38 //term
                // && this.pregnancy.num_previous_c_section != undefined
                // && this.pregnancy.num_previous_c_section == 0
                this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                // && this.admission.mode_onset_labour != undefined
                // && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 2 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson9', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson9"] = { value: 1 };
            R.next();
        }
    }, {
        "name": "Robson 10",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(
                // this.pregnancy.parity != undefined
                // && this.pregnancy.parity == 0
                this.pregnancy.estimate_gestional_age != undefined
                && this.pregnancy.estimate_gestional_age < 38 //preterm
                && this.pregnancy.num_previous_c_section != undefined
                && this.pregnancy.num_previous_c_section == 0
                && this.admission.multiple_pregnancy != undefined
                && this.admission.multiple_pregnancy == 0 //singleton
                && this.admission.mode_onset_labour != undefined
                && this.admission.mode_onset_labour == 0 //spontenous
                && this.assessment.fetal_presentation != undefined
                && this.assessment.fetal_presentation == 0 //cephalic
                );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson10', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson10"] = { value: 1 };
            R.next();
        }
    },
    {
        "name": "Robson Missing",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when(this.diagnosis.robson1 == undefined
                && this.diagnosis.robson2 == undefined
                && this.diagnosis.robson3 == undefined
                && this.diagnosis.robson4 == undefined
                && this.diagnosis.robson5 == undefined
                && this.diagnosis.robson6 == undefined
                && this.diagnosis.robson7 == undefined
                && this.diagnosis.robson8 == undefined
                && this.diagnosis.robson9 == undefined
                && this.diagnosis.robson10 == undefined
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson99', diagnosisValue = 1, admissionId= this.admission.id);
            
            this.diagnosis["robson99"] = { value: 1 };
            R.next();
        }
    },
    ,
    {
        "name": "Reset Robson Missing",
        "priority": 3,
        "on" : true,
        "condition": function (R) {
            //console.log(this)
            R.when((this.diagnosis.robson1  != undefined
                || this.diagnosis.robson2  != undefined
                || this.diagnosis.robson3  != undefined
                || this.diagnosis.robson4  != undefined
                || this.diagnosis.robson5  != undefined
                || this.diagnosis.robson6  != undefined
                || this.diagnosis.robson7  != undefined
                || this.diagnosis.robson8  != undefined
                || this.diagnosis.robson9  != undefined
                || this.diagnosis.robson10 != undefined)
                && this.diagnosis.robson99 != undefined
                && this.diagnosis.robson99.value == 1
            );
        },
        "consequence": function (R) {
            admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'robson99', diagnosisValue = 0, admissionId= this.admission.id);
            
            this.diagnosis["robson99"] = { value: 0 };
            R.next();
        }
    } ]
};
