import { diffTimeNowInMinutes, getCurrentTimeToDatabase } from "../repositories/BaseRepository";
import admissionDiagnosisRepository from "../repositories/admissionDiagnosisRepository";
import admissionAlertsRepository from "../repositories/admissionAlertsRepository";
import AssessmentScheduleRepository from "../repositories/AssessmentScheduleRepository";

export var BloodPressureRules = {
    evaluate_blood_pressure: [
        {
            "name": "Normal blood pressure",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.assessment.systolic_blood_pressure != undefined
                    && this.assessment.systolic_blood_pressure >= 90
                    && this.assessment.systolic_blood_pressure < 140
                    && this.assessment.diastolic_blood_pressure != undefined
                    && this.assessment.diastolic_blood_pressure >= 50
                    && this.assessment.diastolic_blood_pressure < 90
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypertention', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypertension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_bp', diagnosisValue = 1, admissionId = this.admission.id);
                this.diagnosis["normal_bp"] = { value: 1, createdAt: getCurrentTimeToDatabase() };
                R.next();
            }
        },
        {
            "name": "Hypotension",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.assessment.diastolic_blood_pressure < 50 && this.assessment.diastolic_blood_pressure >= 40
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypertention', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypertension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypotension', diagnosisValue = 1, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_bp', diagnosisValue = 0, admissionId = this.admission.id);

                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Hypotension', admissionId = this.admission.id, this.woman.name, publish = false);
                this.diagnosis["hypotension"] = { value: 1 };
                R.next();
            }
        },
        {
            "name": "Severe hypotension",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.assessment.systolic_blood_pressure != undefined
                    && this.assessment.systolic_blood_pressure < 90)
                    ||
                    (this.assessment.diastolic_blood_pressure != undefined
                        && this.assessment.diastolic_blood_pressure < 40)
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypertention', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypertension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotension', diagnosisValue = 1, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_bp', diagnosisValue = 0, admissionId = this.admission.id);
                this.diagnosis["severe_hypotension"] = { value: 1, createdAt: getCurrentTimeToDatabase() };
                
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Hypotension', admissionId= this.admission.id, this.woman.name, publish = false);
                //admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Call for high level', admissionId= this.admission.id, this.woman.name, publish = false);

                R.next();
            }
        },
        {
            "name": "[Hypotension] Meows triggered with continuous monitoring",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.diagnosis.hypotension != undefined
                    && this.diagnosis.hypotension.value == 1
                    && this.meows != undefined
                    && (this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3)
                    && this.settings.continuous_monitoring == 1
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_monitoring', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Call for high level', admissionId = this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'continuous_monitoring', admissionId = this.admission.id, intervalMinutes = 1);

                R.next();
            }
        },
        {
            "name": "[Hypotension] Meows triggered without continuous monitoring",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.diagnosis.hypotension != undefined
                    && this.diagnosis.hypotension.value == 1
                    && this.meows != undefined
                    && (this.meows.redFlag >= 2 || this.meows.yellowFlag >= 3)
                    && this.settings.continuous_monitoring == 0
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Call for high level', admissionId = this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_vitals', admissionId = this.admission.id, intervalMinutes = 15);

                //console.log("1 INTENSIFIED MONITORING: " + JSON.stringify(this.diagnosis));
                if (this.diagnosis.intensified_monitoring != undefined)
                    this.diagnosis.intensified_monitoring.value = 1
                else
                    this.diagnosis["intensified_monitoring"] = { value: 1 };

                R.next();
            }
        },
        {
            "name": "[Hypotension] Meows not triggered",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.diagnosis.hypotension != undefined
                    && this.diagnosis.hypotension.value == 1
                    && this.meows != undefined
                    && this.meows.redFlag < 2
                    && this.meows.yellowFlag < 3
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId = this.admission.id);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'full_assessment', admissionId = this.admission.id, intervalMinutes = 30);

                if (this.diagnosis.intensified_monitoring != undefined)
                    this.diagnosis.intensified_monitoring.value = 1
                else
                    this.diagnosis["intensified_monitoring"] = { value: 1 };

                //console.log("2 INTENSIFIED MONITORING: " + JSON.stringify(this.diagnosis.intensified_monitoring));
                R.next();
            }
        },
        {
            "name": "[Hypotension] Yellow MEOWS",
            "priority": 3,
            "on" : true,
            "condition": function (R) {
                R.when(this.diagnosis.hypotension != undefined && this.diagnosis.hypotension.value == 1 && this.meows.yellowFlag >= 2);
            },
            "consequence": function (R) {
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Moderate to High risk of illness or death from hypotension', admissionId = this.admission.id, '', publish = false);
                R.next();
            }
        },
        {
            "name": "[Hypotension] Red MEOWS",
            "priority": 3,
            "on" : true,
            "condition": function (R) {
                R.when(this.diagnosis.severe_hypotension != undefined && this.diagnosis.severe_hypotension.value == 1 && this.meows.redFlag >= 2);
            },
            "consequence": function (R) {
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'High risk of illness or death from hypotension', admissionId = this.admission.id, '', publish = false);            
                R.next();
            }
        },
        {
            "name": "Hypertension",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.assessment.systolic_blood_pressure != undefined
                    && this.assessment.systolic_blood_pressure >= 140)
                    ||
                    (this.assessment.diastolic_blood_pressure != undefined
                        && this.assessment.diastolic_blood_pressure >= 90)
                );
            },
            "consequence": function (R) {

                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypertention', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypertension', diagnosisValue = 1, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_bp', diagnosisValue = 0, admissionId = this.admission.id);

                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_vitals', admissionId = this.admission.id, intervalMinutes = 0);

                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Check proteinura', admissionId = this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'severe_pe', admissionId = this.admission.id, intervalMinutes = 0);

                this.diagnosis["hypertension"] = { value: 1 };

                R.next();
            }
        },
        {
            "name": "[Hypertension] Laboratory capacity",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.assessment.systolic_blood_pressure != undefined
                    && this.assessment.systolic_blood_pressure >= 140)
                    ||
                    (this.assessment.diastolic_blood_pressure != undefined
                        && this.assessment.diastolic_blood_pressure >= 90)
                    && this.settings.laboratory_capacity == 1
                );
            },
            "consequence": function (R) {
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Take blood sample', admissionId = this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'hypertension_blood_sample_results', admissionId = this.admission.id, intervalMinutes = -1);

                R.next();
            }
        },
        {
            "name": "[Hypertension] CTG available",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(((this.assessment.systolic_blood_pressure != undefined
                            && this.assessment.systolic_blood_pressure >= 140
                            && this.assessment.systolic_blood_pressure < 160)
                        ||
                        (this.assessment.diastolic_blood_pressure != undefined
                            && this.assessment.diastolic_blood_pressure >= 90
                            && this.assessment.diastolic_blood_pressure < 110))
                    && this.settings.ctg_available == 1);
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_cardiotography', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous cardiotography', admissionId = this.admission.id, this.woman.name, publish = false);

                R.next();
            }
        },
        {
            "name": "[Hypertension] CTG not available",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(((this.assessment.systolic_blood_pressure != undefined
                        && this.assessment.systolic_blood_pressure >= 140)
                    ||
                    (this.assessment.diastolic_blood_pressure != undefined
                        && this.assessment.diastolic_blood_pressure >= 90))
                    && this.settings.ctg_available == '0'
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Intensified monitoring', admissionId = this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'full_assessment', admissionId = this.admission.id, intervalMinutes = 15);

                if (this.diagnosis.intensified_monitoring != undefined)
                    this.diagnosis.intensified_monitoring.value = 1
                else
                    this.diagnosis["intensified_monitoring"] = { value: 1 };

                R.next();
            }
        },
        {
            "name": "Severe hypertension",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.assessment.systolic_blood_pressure != undefined
                    && this.assessment.systolic_blood_pressure >= 160)
                    ||
                    (this.assessment.diastolic_blood_pressure != undefined
                        && this.assessment.diastolic_blood_pressure >= 110)
                );
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypertention', diagnosisValue = 1, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypertension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'hypotension', diagnosisValue = 0, admissionId = this.admission.id);
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'normal_bp', diagnosisValue = 0, admissionId = this.admission.id);
                this.diagnosis["severe_hypotension"] = { value: 1 };

                //admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Abnormal systolic blood pressure', admissionId = this.admission.id, this.woman.name, publish = false);

                R.next();
            }
        },
        {
            "name": "[Hypertension] Pre-eclampsia",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.diagnosis.hypertension != undefined && this.diagnosis.hypertension.value == 1
                    && this.assessment.proteinura != undefined && this.assessment.proteinura >= 2)
                )
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'pre_eclampsia', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Pre-eclampsia', admissionId = this.admission.id, this.woman.name, publish = false);

                R.next();
            }
        },
        {
            "name": "[Hypertension] Severe pre-eclampsia",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.diagnosis.hypertension != undefined && this.diagnosis.hypertension.value == 1
                    || this.diagnosis.severe_hypertention != undefined && this.diagnosis.severe_hypertention.value == 1)
                    && this.settings.laboratory_capacity != undefined && this.settings.laboratory_capacity == 0
                    && ((this.assessment.proteinura != undefined && this.assessment.proteinura >= 2)
                        || (this.diagnosis.proteinura != undefined && this.diagnosis.proteinura.value >= 2)
                        || (this.assessment.vommiting != undefined && this.assessment.vommiting == 1)
                        || (this.assessment.urine_volume_per_hour != undefined && this.assessment.urine_volume_per_hour < 30)
                        || (this.assessment.abnormal_pain != undefined && this.assessment.abnormal_pain == 1)
                        //|| (this.assessment.respiratory_rate != undefined && this.assessment.respiratory_rate < xx)		
                    )
                )
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_pre_eclampsia', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Severe pre-eclampsia', admissionId = this.admission.id, this.woman.name, publish = false);

                R.next();
            }
        },
        {
            "name": "[Hypertension] Severe pre-eclampsia",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when((this.diagnosis.hypertension != undefined && this.diagnosis.hypertension.value == 1
                    || this.diagnosis.severe_hypertention != undefined && this.diagnosis.severe_hypertention.value == 1)
                    && this.settings.laboratory_capacity != undefined && this.settings.laboratory_capacity == 1 &&
                    ((this.assessment.abnormal_liver_function != undefined && this.assessment.abnormal_liver_function == 1)
                        || (this.assessment.abnormal_serum_creatinine != undefined && this.assessment.abnormal_serum_creatinine == 1)
                        || (this.assessment.abnormal_platelet_count != undefined && this.assessment.abnormal_platelet_count == 1))
                )
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'severe_pre_eclampsia', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Severe pre-eclampsia', admissionId = this.admission.id, this.woman.name, publish = false);

                R.next();
            }
        },
        {
            "name": "[Severe pre-eclampsia] CTG available",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.diagnosis.severe_pre_eclampsia != undefined && this.diagnosis.severe_pre_eclampsia == 1
                    && this.settings.ctg_available != undefined && this.settings.ctg_available == 1)
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'continuous_cardiotography', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Continuous cardiotograph', admissionId = this.admission.id, this.woman.name, publish = false);
                R.next();
            }
        },
        {
            "name": "[Severe pre-eclampsia] CTG not available",
            "priority": 3,
            "on": true,
            "condition": function (R) {
                R.when(this.diagnosis.severe_pre_eclampsia != undefined && this.diagnosis.severe_pre_eclampsia == 1
                    && this.settings.ctg_available != undefined && this.settings.ctg_available == 0)
            },
            "consequence": function (R) {
                admissionDiagnosisRepository.insertAdmissionDiagnosisByName(diagnosisName = 'intensified_monitoring', diagnosisValue = 1, admissionId = this.admission.id);
                admissionAlertsRepository.insertAdmissionAlertsByTitle(admissionTitle = 'Intensified monitoring', admissionId = this.admission.id, this.woman.name, publish = false);
                AssessmentScheduleRepository.insertScheduledFormFromName(assessmentFormName = 'maternal_vitals', admissionId = this.admission.id, intervalMinutes = 15);

                if (this.diagnosis.intensified_monitoring != undefined)
                    this.diagnosis.intensified_monitoring.value = 1
                else
                    this.diagnosis["intensified_monitoring"] = { value: 1 };

                R.next();
            }
        }
    ]
}