import React, { Component } from 'react';

import { Badge, FormLabel, Divider, SearchBar, Input, Overlay  } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import moment from 'moment';
import PubSub from 'pubsub-js';

import { Card, CardSection } from './components/common';
import {
	Dimensions, StyleSheet, View,FlatList, TouchableOpacity, Alert, Text
} from 'react-native';

import { Loader } from './components/common/Loader';
import { Navigation } from 'react-native-navigation'
import { outcomeLayout, womanLayout, closeSidebar, dashboardLayout, alertLayout,
	assessmentLayout, labourOutcomeLayout } from './navigation'

import withBadge from './components/common/withBadge';
import WomanRepository from './repositories/WomanRepository';
import admissionAlertsRepository from './repositories/admissionAlertsRepository'
import { diffTimeNowInMinutes } from './repositories/BaseRepository';
import admissionDiagnosisRepository from './repositories/admissionDiagnosisRepository';
import DiagnosisBadge from './components/common/DiagnosisBadge';

import MachineLearning from './decision_models/MachineLearning'
import AssessmentScheduleRepository from './repositories/AssessmentScheduleRepository';
//import OverlayForms from './components/common/OverlayForms';

class WomanList extends Component {

	constructor(props) {
		super(props)	
		console.log(this.props);
		
		this.updateWomanList = this.updateWomanList.bind(this);
		this.newWoman        = this.newWoman.bind(this);
		this.labourOutcome   = this.labourOutcome.bind(this);
		this.newOutcome      = this.newOutcome.bind(this);
		this.newAssessment   = this.newAssessment.bind(this);
		this.alert           = this.alert.bind(this);
		this.longPressNewAssessment = this.longPressNewAssessment.bind(this);
		
		Navigation.events().bindComponent(this);
	}

	state = {
		formLabel: 'Woman in Ward', searchInput:'', womenList :[], numAlerts: 0, overlayScheduledForm: false
	};

	
	showLoading = () => {    
		this.setState({animating: true});
	}

	hideLoading = () => {
		this.setState({animating: false});
	} 

	navigationButtonPressed({buttonId}) {
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId == 'sidebarButton') {
			closeSidebar();
		}else if(buttonId = 'returnButton'){     
			if(this.state.routes[0].woman_id != undefined && this.state.routes[1].woman_id > 0){
					return this.checkIfFormIsCompleted();
			}
			else {
					//Leave if no data is inserted
					Navigation.pop(this.props.componentId);		
					//Navigation.pop('WomanList');
			}
		}
	}


	componentDidMount(){
		PubSub.subscribe('update-list-alert', 
		function(topico, params) {
			//console.log("INSIDE SUB " + JSON.stringify(params));
			alertLayout(params.adm_id, " - " + params.name);
		});
	}
	  
	componentDidAppear() {
		this.updateWomanList();
	}

	componentDidDisappear() {
		//console.log("componentDidDisappear");
	}

  componentWillMount(){
		this.updateWomanList();
		//MachineLearning.prepCaseForMLEvaluation(1, 1);
	}

	updateWomanList(){	
		this.showLoading();	
		var womanList = {};
		WomanRepository.getAllWomanInActiveLabour(this.props.user_id).then((data) => 
		{
			womanList = data;
			admissionDiagnosisRepository.getadmissionDiagnosisForWomanList(this.props.user_id).then((diagnosis) => 
			{			
				var newList = Object.assign([], womanList, []);
				for(var i = 0; i < newList.length; i++){
					result = diagnosis.filter(obj => {
																				return obj.admission_id === newList[i].adm_id
																		});
					if(result.length > 0){
						newList[i]["diagnosis"] = result[0].diagnosis;
					}								
				}
				////console.log(newList);
				this.setState({ womenList: newList });	
				this.hideLoading();
			});

		});
		this.countAlertByUserId(this.props.user_id);
	}

	//method to create or update woman;
	//creates if wom_id is 0 or undefined
	//update if wom_id > 0
	newWoman = async (wom_id, preg_id, adm_id, anc_id, disea_id, user_id) => {
		////console.log("NEW WOMAN PARAM " + wom_id + " " + preg_id + " " + adm_id + " " + user_id)
		womanLayout(this.props.componentId, wom_id, preg_id, adm_id, anc_id, disea_id, user_id);
	}

	newOutcome = async (admission_id, woman_id, user_id) => {
		////console.log("NEW WOMAN PARAM " + admission_id + " " + woman_id  + " " + user_id)
		outcomeLayout(this.props.componentId, admission_id, woman_id, user_id);
	}

	labourOutcome = async (admission_id, woman_id, user_id) => {
		////console.log("LABOUR OUTCOME " + admission_id + " " + woman_id  + " " + user_id)
		labourOutcomeLayout(this.props.componentId, admission_id, woman_id, user_id);
	}

	dashboard(adm_id, user_id, nameExtraInfo) {
		////console.log("*************************************************** EXTRA INFO " + nameExtraInfo);
		dashboardLayout(this.props.componentId, adm_id, user_id, nameExtraInfo);
	}
	
	alert(adm_id, wom_name, user_id) {
		//console.log("alert" + user_id);
		alertLayout(adm_id, wom_name, user_id);
	}
	
	intervention(adm_id, user_id,scheduled_form_id = undefined) {
		assessmentLayout(this.props.componentId, adm_id, user_id, form_name = 'interventions_form', scheduled_form_id);
	}

	newAssessment(adm_id, user_id, form_name = '',scheduled_form_id = undefined) {
		assessmentLayout(this.props.componentId, adm_id, user_id, form_name, scheduled_form_id);
	}

	confirmAssessment(adm_id, user_id){
		Alert.alert(
				'There is time',
				"This assessment schedule is a bit further. Do you want to do it now?",
				[
						{					
								text: 'No, I will do a full assessment', onPress: () => { 
									//TROCAR AQUI	
									this.newAssessment(adm_id, user_id, 'full_assessment');
									//this.newAssessment(adm_id, user_id, 'intrapartum_data');
								}
						},
						{
								text: 'Yes, I will fill it now', 
								onPress: () => { 
										//Leave even form is incompleted
										this.newAssessment(adm_id, user_id);
								}
						}
				],
				{ cancelable: true }
		);
	}

	longPressNewAssessment(adm_id, user_id) {
		//trocar pra complete
		this.newAssessment(adm_id, user_id, 'full_assessment');
	}

	showListDelayedAssessment(adm_id, user_id){
		this.setState({ adm_id: adm_id });
		this.setState({ user_id: user_id });
		AssessmentScheduleRepository.getAllAssessmentScheduleByAdmissionId(adm_id).then((data) => {
			console.log(data);
			this.setState({ listScheduledForm: data });
			this.setState({ overlayScheduledForm: true });
		});
	}

	bell(wom_name, user_id){
		alertLayout(0, wom_name, user_id)
	}

	countAlertByUserId(user_id){
		admissionAlertsRepository.getAllAdmissionAlertsByUserId(user_id).then((data) => {
			this.setState({ numAlerts: data.length });
		});
	}

	  
	render() {
		const BadgedIcon = withBadge(this.state.numAlerts, { status: "error"} )(Icon);

		return (
			<View style={styles.container} >				
				<Overlay
					isVisible={this.state.overlayScheduledForm}
					windowBackgroundColor="rgba(255, 255, 255, .5)"
					width="auto"
					height="auto"
					>										
					<View>
					<View>
						<TouchableOpacity onPress={() => { this.setState({ overlayScheduledForm: !this.state.overlayScheduledForm })}} >
							<FontAwesome5 name={"times-circle"}  size={25} />	
						</TouchableOpacity>
						<Text style={styles.label}> Choose the form you want to fill in </Text>
					</View>	
					
					<FlatList
						data={this.state.listScheduledForm}
						renderItem={({ item }) => (
							<TouchableOpacity
								onPress={() => { 
									console.log(this.props);
									console.log(item);
									this.setState({ overlayScheduledForm: !this.state.overlayScheduledForm });
									assessmentLayout(this.props.componentId, this.state.adm_id, this.state.user_id, item.name, item.id);
								}}
								style={styles.itemList}
							>
								<Text style={styles.title}>{item.label_form}</Text>
							</TouchableOpacity>
						)}
						keyExtractor={item => item.id.toString()}
					/>
					</View>	
				</Overlay>
				<View style={styles.searchArea}>
					<SearchBar
						lightTheme
						onChangeText={ searchInput => this.setState({searchInput: searchInput})}
						onClearText={ searchInput => this.setState({searchInput: searchInput})}
						value={this.state.searchInput}
						icon={{ type: 'font-awesome', name: 'search' }}
						placeholder='Search by Name'
						containerStyle={styles.searchBar}
						/>
					<TouchableOpacity style={styles.bellIcon} onPress={() => this.bell(" - All woman", this.props.user_id)} >
						<BadgedIcon	
							type="ionicon"
						/>
						<Icon 
							name="bell" 
							size={30}  />
					</TouchableOpacity>	
				</View>			
				<FlatList style={{ marginBottom: 40 }}
					data={this.state.womenList.filter(item => item.wom_name.includes(this.state.searchInput))}
					keyExtractor={item => item.wom_id != null ? item.wom_id.toString() : "0" }
					renderItem={({ item }) => 
					{
						//timeToNextAssessment = moment.duration(moment(item.schedule_time ).diff(moment(new Date()))).asMinutes().toFixed(0)
						timeToNextAssessment = diffTimeNowInMinutes(item.schedule_time )* -1;
						let BadgedDelayed = withBadge(item.delayedForm, { status: "error"} )(Icon);
						let BadgedAlert = withBadge(item.num_alerts, { status: item.num_alerts == undefined || item.num_alerts == 0 ? "success": "error"} )(Icon);
						return (
							<Card style={styles.item} >								
								<CardSection>								
									<TouchableOpacity
										style={styles.name}  
										onPress={() => this.newWoman(item.wom_id, item.pre_id, item.adm_id, item.anc_id, item.disea_id, this.props.user_id)} 
									>
										<Text style={styles.label}>{item.wom_name}</Text>
										<Text style={styles.smallLabel}>G{parseInt(item.pre_gravidity)} P{parseInt(item.pre_parity)} C{parseInt(item.pre_num_previous_c_section)} A{parseInt(item.pre_num_previous_abort)}</Text>
									</TouchableOpacity>
									<View> 
										<DiagnosisBadge diagnosis={item.diagnosis == undefined? [] : item.diagnosis} />
									</View>
								</CardSection>
								<CardSection style={{ flexDirection: 'column'}}>
									<View style={styles.listElement}>
										{/* access to assessment */}
										{ 
											(item.schedule_time  == undefined) ? 											
											<View style={styles.dashboardButtonView}>	
												<View	style={{position: 'absolute', top: -10, right: 20}}>
													<BadgedDelayed type="ionicon" />
												</View>	
												<TouchableOpacity style={styles.dashboardIcon} onPress={() => this.newAssessment(item.adm_id, this.props.user_id)} >
													<FontAwesome5 name={"file-alt"} size={25} /> 		
													<Text style={styles.smallLabel}> - </Text>
												</TouchableOpacity>
											</View>
											:
											(parseInt(timeToNextAssessment) > 0 ) ?												
											<View style={styles.dashboardButtonView}>	
												<View	style={{position: 'absolute', top: -10, right: 20}}>
													<BadgedDelayed type="ionicon" />
												</View>	
												<TouchableOpacity style={styles.dashboardIcon} onPress={() => this.confirmAssessment(item.adm_id, this.props.user_id)} >
													<FontAwesome5 name={"clock"}  size={25} />	
													<Text style={styles.smallLabel}> { 
														timeToNextAssessment > 60 && timeToNextAssessment/60 > 1 ? 
															parseInt(timeToNextAssessment/60) +"h" + parseInt(timeToNextAssessment%60) + "m"
														:
															parseInt(timeToNextAssessment) + " min" } </Text>
												</TouchableOpacity>
											</View>	
											:
											<View style={styles.dashboardButtonView}>		
												<View	style={{position: 'absolute', top: -10, right: 20}}>
													<BadgedDelayed type="ionicon" />
												</View>		
												
												<TouchableOpacity style={styles.dashboardIcon} 
													 onPress={() => {
														if(item.delayedForm > 1) {
															//corrigir aqui
															//this.newAssessment(item.adm_id, this.props.user_id)
															this.showListDelayedAssessment(item.adm_id, this.props.user_id);
														}
														else {
															this.newAssessment(item.adm_id, this.props.user_id)
														}
													}} 
													onLongPress={() => this.longPressNewAssessment(item.adm_id, this.props.user_id)}
												>		
													<FontAwesome5 name={"exclamation-triangle"} size={25} /> 		
													<Text style={ styles.smallLabel }> Now </Text>
												</TouchableOpacity>
											</View>
										}										
										{/* access dashboard */}
										
										<View style={styles.dashboardButtonView}>		
											<TouchableOpacity style={styles.dashboardIcon} onPress={
											() => this.dashboard(item.adm_id, this.props.user_id, 
													" - "+ item.wom_name +
													" G" + (item.pre_gravidity==undefined? "" : item.pre_gravidity) +
													" P" + (item.pre_parity==undefined? "" : item.pre_parity) + 
													" C" + (item.pre_num_previous_c_section==undefined? "" : item.pre_num_previous_c_section) + 
													" A" + (item.pre_num_previous_abort==undefined? "" : item.pre_num_previous_abort) 
												)} 
											>
											<FontAwesome5 name={"notes-medical"} size={25} />
											<Text style={styles.smallLabel}> Labour </Text>
										</TouchableOpacity>
										</View>
										{/* access list to intervention */}										
										<View style={styles.dashboardButtonView}>		
											<TouchableOpacity style={styles.dashboardIcon} onPress={() => this.intervention(item.adm_id, this.props.user_id)} >
												<FontAwesome5 name={"prescription-bottle-alt"} iconStyle={styles.icon} size={25} />
												<Text style={styles.smallLabel}> Intervention </Text>
											</TouchableOpacity>
										</View>
										{/* access alerts */}
										<View style={styles.dashboardButtonView}>		
											<View	style={{position: 'absolute', top: -10, right: 20}}>
												<BadgedAlert	type="ionicon" style={{fontSize: 30}} />
											</View>		
																		
											<TouchableOpacity style={styles.dashboardIcon} onPress={() => this.alert(item.adm_id, " - " + item.wom_name)} >					
											<FontAwesome5 name={"bell"} iconStyle={styles.icon} size={25} />
											<Text style={styles.smallLabel}> Alerts </Text>
										</TouchableOpacity>
										</View>
										{/* finilaze case */}
										
										<View style={styles.dashboardButtonView}>			
											<TouchableOpacity style={styles.dashboardIcon} onPress={ () => this.labourOutcome(item.adm_id, item.wom_id, this.props.user_id) }>
											<FontAwesome5 name={"baby"} iconStyle={styles.icon} size={25} />
											<Text style={styles.smallLabel}> Delivery </Text>
										</TouchableOpacity>
										</View>
										{/* access Discharge */}
										
										<View style={styles.dashboardButtonView}>	
											<TouchableOpacity style={styles.dashboardIcon} onPress={() => this.newOutcome(item.adm_id, item.wom_id, this.props.user_id)} >
												<FontAwesome5 name={"sign-out-alt"} iconStyle={styles.icon} size={25} />
												<Text style={styles.smallLabel}> Discharge </Text>
											</TouchableOpacity>	
										</View>		
									</View>
								</CardSection>
							</Card>
						)
					}}
				/>
					<Loader animating = { this.state.animating } />
				<TouchableOpacity style={styles.plusButton} 
					onPress={() => this.newWoman(0, 0, 0, 0, 0, this.props.user_id)} 
					testID={"newWoman"}
					accessibilityLabel="newWoman" >
					<Icon name="plus" size={35} color="#FFF" />
				</TouchableOpacity>
			</View>
		);
	}
}

const deviceWidth = Dimensions.get('window').width;
const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
	},
	item: {		
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.2)',
		margin: 2
	},
	listElement: {
		flexDirection: 'row',
		padding: 2,
		alignSelf:'baseline'
	},
	name: {
		paddingTop: 10,
		flex:5,
		borderRadius: 10, 
		height: 55,
	},
	info: {
		alignSelf: 'center'
	},
	plusButton: {
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.2)',
		alignItems: 'center',
		justifyContent: 'center',
		width: 70,
		height: 70,
		backgroundColor: '#B22222',
		borderRadius: 100,
		alignSelf: 'flex-end',
		position: 'absolute',
		bottom: 10,
		right: 10
	},	
	icon: {
		margin: 12,
		alignSelf: 'center',
		color:"#000"
	},
	alert: {
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.2)',
		alignItems: 'center',
		justifyContent: 'center',
		width: 30,
		height: 30,
		backgroundColor: 'green',
		borderRadius: 100, 
		alignSelf: 'center',
	},
	alertMissing: {
		backgroundColor: '#B22222'
	},
	dashboardButtonView: {
		flex: 1,
		flexDirection: 'column',
		position: 'relative',
		marginTop: 5
	},
	dashboardIcon: {
		borderRadius: 10, 
		alignItems: "center",
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.2)',
		height: 65,
		alignItems: "center",
		paddingTop:10
	},
	bellIcon: {
		margin: 5, 
		width: 30,
		backgroundColor: "#e1e8ee"
	},
	searchBar: { 
		width: deviceWidth-50
	},
	searchArea: {
        flexDirection: 'row',
		width: deviceWidth,
		backgroundColor: "#e1e8ee"
	},
	whiteText : {
		color:'#FFF',
		fontWeight: 'bold'
	},
	label: {
	  fontSize: 16, 
	  fontWeight: 'bold',
	  color: '#86939e',	  
	  textAlign: 'center'
	},
	smallLabel: {
	  fontSize: 10, 
	  color: '#86939e',	  
	  textAlign: 'center'
	},
	closeListArea:{
		paddingTop: 10,
		borderColor: '#86939e',
		height: 60,
		textAlign:'right',
	},
	itemList: {
		borderRadius: 10, 
		height: 60,
		justifyContent: 'center',
		backgroundColor: '#FFF',
		alignItems:'center',
		width: deviceWidth-50,
		borderWidth: 1,
		borderColor: 'rgba(0,0,0,0.2)'
	}
});
  
export default WomanList;
