import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
//import LoginForm from './components/LoginForm';
import { womanListLayout } from './navigation';
 
// Get device width
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  image: {
    height: deviceHeight *0.85,
    width: deviceWidth *0.8
  }
});
 
const slides = [
  {
    key: 's1',
    image: require('../img/Selma-screen-1.png'),
    imageStyle: styles.image,
    backgroundColor: '#59b2ab',
  },
  {
    key: 's2',
    image: require('../img/Selma-screen-2.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 's3',
    image: require('../img/Selma-screen-3.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
  {
    key: 's4',
    image: require('../img/Selma-screen-4.png'),
    imageStyle: styles.image,
    backgroundColor: '#59b2ab',
  },
  {
    key: 's5',
    image: require('../img/Selma-screen-5.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 's6',
    image: require('../img/Selma-screen-6.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
  {
    key: 's7',
    image: require('../img/Selma-screen-7.png'),
    imageStyle: styles.image,
    backgroundColor: '#59b2ab',
  },
  {
    key: 's8',
    image: require('../img/Selma-screen-8.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 's9',
    image: require('../img/Selma-screen-9.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
  {
    key: 's10',
    image: require('../img/Selma-screen-10.png'),
    imageStyle: styles.image,
    backgroundColor: '#59b2ab',
  },
  {
    key: 's11',
    image: require('../img/Selma-screen-11.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 's12',
    image: require('../img/Selma-screen-12.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
  {
    key: 's13',
    image: require('../img/Selma-screen-13.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  },
  {
    key: 's14',
    image: require('../img/Selma-screen-14.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
  {
    key: 's15',
    image: require('../img/Selma-screen-15.png'),
    imageStyle: styles.image,
    backgroundColor: '#22bcb5',
  },
  {
    key: 's16',
    image: require('../img/Selma-screen-16.png'),
    imageStyle: styles.image,
    backgroundColor: '#febe29',
  }
];
 
export default class Tutorial extends React.Component {
    constructor(props) {
        super(props);
        //console.log(props);
        this.state = {
            showRealApp: false,
            //To show the main page of the app
        };
    }

    componentDidMount(){
    }

    componentDidUpdate(){
        //console.log("componentDidUpdate");
        if(this.state.showRealApp){
            womanListLayout(this.props.user_id);
        }
    }

  _onDone = () => {
    // User finished the introduction. Show "real" app
    this.setState({ showRealApp: true });
  }
  render() {
    //   if(this.state.showRealApp){
    //     return (
    //         <LoginForm />
    //     )
    //   }
    //   else{
        return (
            <AppIntroSlider
                slides={slides}
                onDone={this._onDone}
            />
        );
    //  }
  }
}