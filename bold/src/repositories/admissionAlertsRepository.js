import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase, getIdFromDateTime } from './BaseRepository';
import alertRepository from "./alertRepository";

export default class admissionAlertsRepository {

    static sqlAdmissionAlerts = { select : 'Select  aa.id admission_alerts_id, aa.readed, aa.alert_id, aa.admission_id, aa.createdAt, aa.updatedAt, ' +
                                '    al.id, al.title, al.text, al.link, al.icon_name, al.risk_level                ' +
                                '    from admissionAlerts aa                                                       ' +
                                '    join alert al on aa.alert_id = al.id                                          ' + 
                                '    join admission adm on adm.id = aa.admission_id' 
                            }

    static getNextId(){
        
        return new Promise((resolve, reject) => {
            resolve(getIdFromDateTime());
        })
        // return new Promise((resolve, reject) => {
        //     database.getDatabase().then(db => { 
        //         db.transaction((tx) => {            
        //             tx.executeSql('Select max(id) as max_id from admissionAlerts', [], (tx, results) => {
        //                 var max_id = 1;
        //                 if(results.rows.length > 0 && results.rows.item(0).max_id != null){
        //                     max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
        //                 }
        //                 resolve(max_id)
        //             },
        //             (tx, err) => {
        //                 //console.log("erro: " + JSON.stringify(err));
        //                 reject(err);
        //             });
        //         });
        //     });
        // })
    }

    static insertadmissionAlerts(admissionAlerts){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into admissionAlerts (id, readed, alert_id, admission_id, createdAt, updatedAt, snozzed, readedAt, snozzedAt)'+
                    ' values (?, ?, ?, ?, ?, ?, ?, ?, ?);', 
                        [(parseInt(next_id)+ parseInt(admissionAlerts.alert_id)) , 0, admissionAlerts.alert_id, admissionAlerts.admission_id, 
                            getCurrentTimeToDatabase(), 
                            getCurrentTimeToDatabase(), 
                            0, null, null], (tx, results) => {
                                admissionAlerts.id = results.insertId;
                                resolve(admissionAlerts);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateadmissionAlerts(admissionAlerts){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admissionAlerts set readed= ?, alert_id = ?, admission_id = ?,  updatedAt = ?, snozzed = ?, readedAt = ?, snozzedAt = ? where id = ?', 
                        [ admissionAlerts.readed,admissionAlerts.alert_id,admissionAlerts.admission_id, getCurrentTimeToDatabase(), 
                            admissionAlerts.snozzed, admissionAlerts.readedAt, admissionAlerts.snozzedAt,
                            admissionAlerts.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteadmissionAlerts(admissionAlerts_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from admissionAlerts where id = ?', [admissionAlerts_id], (tx, results) => {
                        var admissionAlerts = results.rows.item(0);
                        resolve(admissionAlerts)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getadmissionAlertsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, readed, snozzed, alert_id, admission_id, createdAt, updatedAt, readedAt, snozzedAt from admissionAlerts where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var admissionAlerts = results.rows.item(0);                    
                        resolve(admissionAlerts)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAlladmissionAlerts(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, readed, snozzed, alert_id, admission_id, createdAt, updatedAt, readedAt, snozzedAt from admissionAlerts  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static getAllAdmissionAlertsByAdmissionId(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = this.sqlAdmissionAlerts.select +
                                '    where aa.admission_id = ? ' +
                                '          and aa.readed= 0 and aa.snozzed = 0 ' +
                                '          and adm.admission_status = 1   ' +
                                ' order by al.risk_level desc, aa.createdAt desc'     
                    tx.executeSql(query, 
                    [admission_id], (tx, results) => {
                        var len = results.rows.length;
                        var data = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            data.push(row);
                        }     
                        resolve(data);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllAdmissionAlertsByUserId(usr_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = this.sqlAdmissionAlerts.select +
                                '    where adm.userBoldId = ? ' +
                                '          and aa.readed= 0 and aa.snozzed = 0 ' +
                                '          and adm.admission_status = 1   ' +
                                ' order by aa.createdAt desc';
                    //console.log(query); 
                    tx.executeSql(query, 
                    [usr_id], (tx, results) => {
                        var len = results.rows.length;
                        var data = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            // //console.log("getAllAdmissionAlertsByUserId")
                            // //console.log(row)
                            data.push(row);
                        }     
                        resolve(data);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }
    
    static getadmissionAlertsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, readed, snozzed, alert_id, admission_id, createdAt, updatedAt, readedAt, snozzedAt from admissionAlerts where id = ? ' ;        
                    tx.executeSql(query, 
                    [id], (tx, results) => {
                        var admissionAlerts = results.rows.item(0);                    
                        resolve(admissionAlerts)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static markAlertAsRead(admission_alerts_id, alert_id){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admissionAlerts set readed= 1, updatedAt = ?, readedAt = ? where id = ? and alert_id = ?', 
                        [ 
                            getCurrentTimeToDatabase(), 
                            getCurrentTimeToDatabase(), 
                            admission_alerts_id,
                            alert_id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static markAlertAsSnozzed(admission_alerts_id, alert_id){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admissionAlerts set snozzed = 1, updatedAt = ?, snozzedAt = ? where id = ? and alert_id = ? ', 
                        [ 
                            getCurrentTimeToDatabase(), 
                            getCurrentTimeToDatabase(), 
                            admission_alerts_id,
                            alert_id
                        ], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static insertAdmissionAlertsByTitle(admissionTitle, admissionId, womanName, publish) {
        //console.log("getalertByTitle: " + admissionTitle);
        return alertRepository.getalertByTitle(admissionTitle)
        .then((data) => {
            //console.log(data);
            if(data!= undefined && data.id > 0) {
                admissionAlerts = { alert_id: data.id, admission_id: admissionId }
            
                this.insertadmissionAlerts(admissionAlerts)
                .then(() => {
                    if(publish) {
                        //console.log("INSERT ADMISSION ALERTS BY TITLE");
                        PubSub.publish('update-list-alert', {adm_id: admissionAlerts.admission_id, name: womanName});
                    }
                });
            }
        });
    }

    
    static cancelDuplicatedAdmissionAlertsByAdmissionId(admissionId){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql(
                    'UPDATE admissionAlerts set readed= 2                                                                                    ' +                                                                                       
                    ' where id NOT IN                                                                                                       ' +
                    '     (                                                                                                                 ' +
                    '     select min(ass.id) from admissionAlerts as ass                                                                    ' +
                    '     inner join                                                                                                        ' +
                    '     (select alert_id, count(alert_id) countForms from admissionAlerts                                                 ' +
                    '       where admission_id = ? and (readed= 0 or snozzed = 0) and schedule_time  > \''+ getCurrentTimeToDatabase(-2) + '\' ' +
                    '       group by alert_id                                                                                               ' +
                    '       HAVING (count(alert_id)>1)) ass2 on ass.alert_id = ass2.alert_id                                                ' +
                    '       where admission_id = ? and (readed= 0 or snozzed = 0) and schedule_time  > \''+ getCurrentTimeToDatabase(-2) +'\'  ' +
                    '       group by ass.alert_id                                                                                           ' +
                    'UNION                                                                                                                  ' +
                    '    select min(ass.id) from admissionAlerts as ass                                                                     ' +
                    '     inner join                                                                                                        ' +
                    '     (select alert_id, count(alert_id) countForms from admissionAlerts                                                 ' +
                    '       where admission_id = ? and (readed= 0 or snozzed = 0) and schedule_time  > \''+ getCurrentTimeToDatabase(-2) + '\'  ' +
                    '       group by alert_id                                                                                               ' +
                    '       HAVING (count(alert_id)=1)) ass2 on ass.alert_id = ass2.alert_id                                                ' +
                    '       where admission_id = ? and (readed= 0 or snozzed = 0) and schedule_time  > \''+ getCurrentTimeToDatabase(-2) +'\'  ' +
                    '       group by ass.alert_id  )                                                                                        ' +
                    'and admission_id = ? and (readed= 0 or snozzed = 0) and schedule_time  > \''+ getCurrentTimeToDatabase() + '\'          '                          
                    , [admissionId, admissionId, admissionId, admissionId, admissionId], (tx, results) => {
                        resolve(results);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    }
                )
                });
            });
        });
    }

    static backToTheFutureAdmissionAlerts(minutes){        
        //console.log(minutes);
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admissionAlerts set createdAt = datetime(createdAt, \'-'+ minutes + ' minutes\')', 
                        [], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}

	   