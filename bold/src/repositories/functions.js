import assessmentResultRepository from './assessmentResultRepository';
import AssessmentScheduleRepository from './AssessmentScheduleRepository';
import admissionDiagnosisRepository from './admissionDiagnosisRepository';
import admissionAlertsRepository from './admissionAlertsRepository';

export function backToTheFuture(minutes){
    return Promise.all([
        assessmentResultRepository.backToTheFutureAssessmentResult(minutes),
        AssessmentScheduleRepository.backToTheFutureScheduleForm(minutes),
        admissionDiagnosisRepository.backToTheFutureAdmissionDiagnosis(minutes),
        admissionAlertsRepository.backToTheFutureAdmissionAlerts(minutes)
    ]).then();
}