import  { database }  from "./DatabaseManager";

export default class hospitalRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from hospital', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static inserthospital(hospital){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into hospital (id , name, site, telephone, address, county, city, state, country, zip_code, createdAt, updatedAt, number, userBoldId)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , name, site, telephone, address, county, city, state, country, zip_code, createdAt, updatedAt, number, userBoldId], (tx, results) => {
                                hospital.id = results.insertId;
                                resolve(hospital);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatehospital(hospital){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update hospital set   name = ?, site = ?, telephone = ?, address = ?, county = ?, city = ?, state = ?, country = ?, zip_code = ?, createdAt = ?, updatedAt = ?, number = ?, userBoldId = ? where id = ?', 
                        [ hospital.name,hospital.site,hospital.telephone,hospital.address,hospital.county,hospital.city,hospital.state,hospital.country,hospital.zip_code,hospital.createdAt,hospital.updatedAt,hospital.number,hospital.userBoldId,hospital.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletehospital(hospital_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from hospital where id = ?', [hospital_id], (tx, results) => {
                        var hospital = results.rows.item(0);
                        resolve(hospital)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static gethospitalById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , name, site, telephone, address, county, city, state, country, zip_code, createdAt, updatedAt, number, userBoldId from hospital where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var hospital = results.rows.item(0);                    
                        resolve(hospital)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllhospital(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, name, site, telephone, address, county, city, state, country, zip_code, createdAt, updatedAt, number, userBoldId from hospital  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
