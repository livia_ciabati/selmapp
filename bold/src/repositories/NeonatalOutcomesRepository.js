import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase } from './BaseRepository';

export default class NeonatalOutcomesRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from neonatalOutcomes', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertNeonatalOutcome(neo){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into neonatalOutcomes (id, final_mode_delivery, fetal_presentation_delivery, infant_sex, birth_weight, condition_vital_status, condition_apgar_5min, '+
                    'morbidity_admission_ICU, morbidity_intubation, morbidity_nasal_CPAP, morbidity_surfactant_administration, morbidity_cardio_pulmonary_resuscitation, morbidity_use_vasoactive_drug, '+
                    'morbidity_use_anticonvulsants, morbidity_use_phototerapy_first_24hours, morbidity_any_blood_products, morbidity_use_steroids, morbidity_therapeutic_intravenous_antibiotics, '+
                    'morbidity_any_surgery, morbidity_any_severe_malformation, status_hospital_discharge, discharge_transfer_death_date, discharge_transfer_death_hour, admission_id, createdAt, '+
                    'updatedAt, userBoldId, date_delivery, time_delivery)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , neo.final_mode_delivery, neo.fetal_presentation_delivery, neo.infant_sex, neo.birth_weight, neo.condition_vital_status, neo.condition_apgar_5min, 
                            neo.morbidity_admission_ICU, neo.morbidity_intubation, neo.morbidity_nasal_CPAP, neo.morbidity_surfactant_administration, neo.morbidity_cardio_pulmonary_resuscitation, 
                            neo.morbidity_use_vasoactive_drug, neo.morbidity_use_anticonvulsants, neo.morbidity_use_phototerapy_first_24hours, neo.morbidity_any_blood_products, 
                            neo.morbidity_use_steroids, neo.morbidity_therapeutic_intravenous_antibiotics, neo.morbidity_any_surgery, neo.morbidity_any_severe_malformation, 
                            neo.status_hospital_discharge, neo.discharge_transfer_death_date, neo.discharge_transfer_death_hour, neo.admission_id, getCurrentTimeToDatabase(), getCurrentTimeToDatabase(), 
                            neo.userBoldId, neo.date_delivery, neo.time_delivery], (tx, results) => {
                                neo.id = results.insertId;
                                resolve(neo);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateNeonatalOutcome(neonatalOutcomes){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update neonatalOutcomes set final_mode_delivery = ?, fetal_presentation_delivery = ?, infant_sex = ?, birth_weight = ?, condition_vital_status = ?, condition_apgar_5min = ?, '+
                    ' morbidity_admission_ICU = ?, morbidity_intubation = ?, morbidity_nasal_CPAP = ?, morbidity_surfactant_administration = ?, morbidity_cardio_pulmonary_resuscitation = ?,  '+
                    'morbidity_use_vasoactive_drug = ?, morbidity_use_anticonvulsants = ?, morbidity_use_phototerapy_first_24hours = ?, morbidity_any_blood_products = ?, morbidity_use_steroids = ?,  '+
                    'morbidity_therapeutic_intravenous_antibiotics = ?, morbidity_any_surgery = ?, morbidity_any_severe_malformation = ?, status_hospital_discharge = ?, discharge_transfer_death_date = ?,  '+
                    'discharge_transfer_death_hour = ?, admission_id = ?, updatedAt = ?, userBoldId = ?, date_delivery = ?, time_delivery = ?  where id = ?', 
                        [ neonatalOutcomes.final_mode_delivery,neonatalOutcomes.fetal_presentation_delivery,neonatalOutcomes.infant_sex,neonatalOutcomes.birth_weight,neonatalOutcomes.condition_vital_status,
                            neonatalOutcomes.condition_apgar_5min,neonatalOutcomes.morbidity_admission_ICU,neonatalOutcomes.morbidity_intubation,neonatalOutcomes.morbidity_nasal_CPAP,
                            neonatalOutcomes.morbidity_surfactant_administration,neonatalOutcomes.morbidity_cardio_pulmonary_resuscitation,neonatalOutcomes.morbidity_use_vasoactive_drug,
                            neonatalOutcomes.morbidity_use_anticonvulsants,neonatalOutcomes.morbidity_use_phototerapy_first_24hours,neonatalOutcomes.morbidity_any_blood_products,
                            neonatalOutcomes.morbidity_use_steroids,neonatalOutcomes.morbidity_therapeutic_intravenous_antibiotics,neonatalOutcomes.morbidity_any_surgery,
                            neonatalOutcomes.morbidity_any_severe_malformation,neonatalOutcomes.status_hospital_discharge,neonatalOutcomes.discharge_transfer_death_date,
                            neonatalOutcomes.discharge_transfer_death_hour,neonatalOutcomes.admission_id, getCurrentTimeToDatabase(),neonatalOutcomes.userBoldId, 
                            neonatalOutcomes.date_delivery, neonatalOutcomes.time_delivery, neonatalOutcomes.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteNeonatalOutcome(neonatalOutcomes_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from neonatalOutcomes where id = ?', [neonatalOutcomes_id], (tx, results) => {
                        var neonatalOutcomes = results.rows.item(0);
                        resolve(neonatalOutcomes)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getNeonatalOutcomeById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , final_mode_delivery, fetal_presentation_delivery, infant_sex, birth_weight, condition_vital_status, condition_apgar_5min, morbidity_admission_ICU, morbidity_intubation, morbidity_nasal_CPAP, morbidity_surfactant_administration, morbidity_cardio_pulmonary_resuscitation, morbidity_use_vasoactive_drug, morbidity_use_anticonvulsants, morbidity_use_phototerapy_first_24hours, morbidity_any_blood_products, morbidity_use_steroids, morbidity_therapeutic_intravenous_antibiotics, morbidity_any_surgery, morbidity_any_severe_malformation, status_hospital_discharge, discharge_transfer_death_date, discharge_transfer_death_hour, admission_id, createdAt, updatedAt, userBoldId, date_delivery, time_delivery from neonatalOutcomes where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var neonatalOutcomes = results.rows.item(0);                    
                        resolve(neonatalOutcomes)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getNeonatalOutcomeByAdmissionId(admissionId){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , final_mode_delivery, fetal_presentation_delivery, infant_sex, birth_weight, condition_vital_status, condition_apgar_5min, morbidity_admission_ICU, morbidity_intubation, morbidity_nasal_CPAP, morbidity_surfactant_administration, morbidity_cardio_pulmonary_resuscitation, morbidity_use_vasoactive_drug, morbidity_use_anticonvulsants, morbidity_use_phototerapy_first_24hours, morbidity_any_blood_products, morbidity_use_steroids, morbidity_therapeutic_intravenous_antibiotics, morbidity_any_surgery, morbidity_any_severe_malformation, status_hospital_discharge, discharge_transfer_death_date, discharge_transfer_death_hour, admission_id, createdAt, updatedAt, userBoldId, date_delivery, time_delivery from neonatalOutcomes where admission_id = ' + admissionId;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var neonatalOutcomes = results.rows.item(0);                    
                        resolve(neonatalOutcomes)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }



    static getAllNeonatalOutcomes(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, final_mode_delivery, fetal_presentation_delivery, infant_sex, birth_weight, condition_vital_status, condition_apgar_5min, morbidity_admission_ICU, morbidity_intubation, morbidity_nasal_CPAP, morbidity_surfactant_administration, morbidity_cardio_pulmonary_resuscitation, morbidity_use_vasoactive_drug, morbidity_use_anticonvulsants, morbidity_use_phototerapy_first_24hours, morbidity_any_blood_products, morbidity_use_steroids, morbidity_therapeutic_intravenous_antibiotics, morbidity_any_surgery, morbidity_any_severe_malformation, status_hospital_discharge, discharge_transfer_death_date, discharge_transfer_death_hour, admission_id, createdAt, updatedAt, userBoldId, date_delivery, time_delivery from neonatalOutcomes  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
