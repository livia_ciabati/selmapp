import  { database }  from "./DatabaseManager";
import { diffTimeNowInMinutes } from "./BaseRepository";

export default class UserBoldRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from UserBold', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertUserBold(UserBold){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                // this.getNextId().then((next_id) =>
                // {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into UserBold (id , realm, username, password, email, emailVerified, verificationToken, createdAt, updatedAt) '+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [UserBold.id, UserBold.realm, UserBold.username, UserBold.password, UserBold.email, UserBold.emailVerified, UserBold.verificationToken, diffTimeNowInMinutes(), diffTimeNowInMinutes()],
                         (tx, results) => {
                                UserBold.id = results.insertId;
                                resolve(UserBold);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
               // });    
            });        
        });
    }

    static updateUserBold(UserBold){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update UserBold set realm = ?, username = ?, password = ?, email = ?, emailVerified = ?, verificationToken = ?, createdAt = ?, updatedAt = ? where id = ?', 
                        [ UserBold.realm,UserBold.username,UserBold.password,UserBold.email,UserBold.emailVerified,UserBold.verificationToken,UserBold.createdAt,UserBold.updatedAt,UserBold.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteUserBold(UserBold_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from UserBold where id = ?', [UserBold_id], (tx, results) => {
                        var UserBold = results.rows.item(0);
                        resolve(UserBold)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getUserBoldById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, realm, username, password, email, emailVerified, verificationToken, createdAt, updatedAt from UserBold where id = ?';        
                    tx.executeSql(query, 
                    [id], (tx, results) => {
                        var UserBold = results.rows.item(0);                    
                        resolve(UserBold)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllUserBold(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, realm, username, password, email, emailVerified, verificationToken, createdAt, updatedAt from UserBold  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
