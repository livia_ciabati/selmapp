import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase, getIdFromDateTime } from './BaseRepository';

export default class assessmentResultRepository {

    static getNextId(){
        
        return new Promise((resolve, reject) => {
            resolve(getIdFromDateTime());
        })
        // return new Promise((resolve, reject) => {
        //     database.getDatabase().then(db => { 
        //         db.transaction((tx) => {            
        //             tx.executeSql('Select max(id) as max_id from assessmentResult', [], (tx, results) => {
        //                 var max_id = 1;
        //                 if(results.rows.length > 0 && results.rows.item(0).max_id != null){
        //                     max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
        //                 }
        //                 resolve(max_id)
        //             },
        //             (tx, err) => {
        //                 //console.log("erro: " + JSON.stringify(err));
        //                 reject(err);
        //             });
        //         });
        //     });
        // })
    }

    static insertassessmentResult(assessmentResult){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into assessmentResult (id , answer, admission_id, assessment_questions_id, assessment_options_id, createdAt, updatedAt, forms_id, userBoldId, assessment_answer_info_id)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , assessmentResult.answer, assessmentResult.admission_id, assessmentResult.assessment_questions_id, assessmentResult.assessment_options_id, 
                            getCurrentTimeToDatabase(),
                            getCurrentTimeToDatabase(), 
                            assessmentResult.forms_id, assessmentResult.userBoldId, assessmentResult.assessment_answer_info_id], (tx, results) => {
                                assessmentResult.id = next_id;
                                console.log(next_id)
                                resolve(assessmentResult);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateassessmentResult(assessmentResult){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentResult set   answer = ?, admission_id = ?, assessment_questions_id = ?, assessment_options_id = ?, updatedAt = ?, forms_id = ?, userBoldId = ?, assessment_answer_info_id = ? where id = ?', 
                        [ assessmentResult.answer,assessmentResult.admission_id,assessmentResult.assessment_questions_id,assessmentResult.assessment_options_id,
                            getCurrentTimeToDatabase(), 
                            assessmentResult.forms_id,
                            assessmentResult.userBoldId,
                            assessmentResult.assessment_answer_info_id,
                            assessmentResult.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteassessmentResult(assessmentResult_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from assessmentResult where id = ?', [assessmentResult_id], (tx, results) => {
                        var assessmentResult = results.rows.item(0);
                        resolve(assessmentResult)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getassessmentResultById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , answer, admission_id, assessment_questions_id, assessment_options_id, createdAt, updatedAt, forms_id, userBoldId, assessment_answer_info_id from assessmentResult where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var assessmentResult = results.rows.item(0);                    
                        resolve(assessmentResult)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllassessmentResult(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, answer, admission_id, assessment_questions_id, assessment_options_id, createdAt, updatedAt, forms_id, userBoldId, assessment_answer_info_id from assessmentResult  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    
    static getAllAssessmentResultByNameAndAdmissionId(name, admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select r.id, r.answer, r.admission_id, r.assessment_questions_id, r.assessment_options_id, r.createdAt, '+ 
                        ' r.updatedAt, r.forms_id, r.userBoldId, r.assessment_answer_info_id '+ 
                        ' from assessmentResult r'+ 
                        ' join assessmentQuestions q on q.id = r.assessment_questions_id'+ 
                        ' where q.name = ? and admission_id = ? '
                        , [name, admission_id], (tx, results) => {                     
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push({value: row.answer, createdAt: row.createdAt})
                            }           
                            resolve(data)
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }


    
    static backToTheFutureAssessmentResult(minutes){        
        //console.log(minutes);
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentResult set createdAt = datetime(createdAt, \'-'+ minutes + ' minutes\')', 
                        [], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
