import  { database }  from "./DatabaseManager";

export default class assessmentsFormsRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then((db) => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from assessmentsForms', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("assessmentsFormsRepository getNextId erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertassessmentsForms(assessmentsForms){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into assessmentsForms (id , name, label_form, version, createdAt, updatedAt)'+
                    ' values ( ?, ?, ?, ?, ?, ?)', 
                        [next_id , name, label_form, version, createdAt, updatedAt], (tx, results) => {
                                assessmentsForms.id = results.insertId;
                                resolve(assessmentsForms);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateassessmentsForms(assessmentsForms){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentsForms set name = ?, label_form = ?, version = ?, createdAt = ?, updatedAt = ? where id = ?', 
                        [ assessmentsForms.name,assessmentsForms.label_form,assessmentsForms.version,assessmentsForms.createdAt,assessmentsForms.updatedAt,assessmentsForms.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteassessmentsForms(assessmentsForms_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from assessmentsForms where id = ?', [assessmentsForms_id], (tx, results) => {
                        var assessmentsForms = results.rows.item(0);
                        resolve(assessmentsForms)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getassessmentsFormsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                //console.log(id);
            db.transaction(
                (tx) => {
                    tx.executeSql(
                        'Select af.id as formId, af.name as formName, af.label_form,                                                    ' +
                        ' aq.id as questionId, aq.name as questionName, aq.label_question, aq.type, aq.minimum_value, aq.maximum_value, ' +
                        ' ao.id as optionId, ao.value, ao.label_reply from questionsForms qf                                            ' +
                        ' join assessmentsForms af on af.id = qf.assessments_forms_id                                                   ' +
                        ' JOIN assessmentQuestions aq on aq.id = qf.assessment_questions_id                                             ' +
                        ' left join questionsOptions qo on qo.assessment_questions_id = qf.assessment_questions_id                      ' +
                        ' left join assessmentOptions ao on ao.id = qo.assessment_options_id                                            ' +
                        ' where formId =                                                                                                ' + id +                                                                           
                        ' order by aq.id, cast(ao.value as integer)                                                                     ', 
                        [], (tx, results) => {      
                            if(results.rows.length > 0 )   {                   
                                resolve(this.parseFormAssessment(results));
                            }
                            else{
                                resolve({});
                            }
                        },
                        (tx, err) => {
                           //console.warn(tx);
                        }
                    )
                });
            });
        });       
    }

    static getAllassessmentsForms(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, name, label_form, version, createdAt, updatedAt from assessmentsForms  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAll(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('delete from assessmentsForms;', [], (tx, results) => {
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static getAssessmentFormByName(name)
    {
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
            db.transaction(
                (tx) => {
                    tx.executeSql(
                        'Select af.id as formId, af.name as formName, af.label_form,                                                    ' +
                        ' aq.id as questionId, aq.name as questionName, aq.label_question, aq.type, aq.minimum_value, aq.maximum_value, ' +
                        ' ao.id as optionId, ao.value, ao.label_reply from questionsForms qf                                            ' +
                        ' join assessmentsForms af on af.id = qf.assessments_forms_id                                                   ' +
                        ' JOIN assessmentQuestions aq on aq.id = qf.assessment_questions_id                                             ' +
                        ' left join questionsOptions qo on qo.assessment_questions_id = qf.assessment_questions_id                      ' +
                        ' left join assessmentOptions ao on ao.id = qo.assessment_options_id                                            ' +
                        ' WHERE af.name = \''+ name +'\'                                                                                ' +
                        ' order by aq.id, cast(ao.value as integer)                                                                     ', 
                        [], (tx, results) => {                            
                            resolve(this.parseFormAssessment(results));
                        },
                        (tx, err) => {
                           //console.warn(tx);
                        }
                    )
                });
            });
        });
    }

    
    static getassessmentsFormIdByName(name){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
            db.transaction(
                (tx) => {
                    tx.executeSql(
                        'Select id , name, label_form, version, createdAt, updatedAt          ' +
                        ' from assessmentsForms                                               ' + 
                        ' where name = ?', 
                        [name], (tx, results) => {    
                            var assessmentForm = results.rows.item(0);                             
                            resolve(assessmentForm);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                        }
                    )
                });
            });
        });       
    }

    static parseFormAssessment(results){
        var len = results.rows.length;
        var data = [];
        let row = results.rows.item(0);

        var aux = {
            formId: row.formId, formName: row.formName, label_form: row.label_form, questionId: row.questionId,
            questionName: row.questionName, label_question: row.label_question, type: row.type, minimum_value: row.minimum_value, maximum_value: row.maximum_value,
            options: [{ optionId: row.optionId, value: row.value, label_reply: row.label_reply }]
        }

        if(len == 1 )
        {
            data.push(aux);
        }

        for (let i = 1; i < len; i++)
        {
            while (i<len && aux.questionId == results.rows.item(i).questionId)
            {
                row = results.rows.item(i);                                    
                aux.options.push({ optionId: row.optionId, value: row.value, label_reply: row.label_reply });
                i++;
            }

            data.push(aux);

            if(i<len)
            {
                row = results.rows.item(i);
                aux = {
                    formId: row.formId, formName: row.formName, label_form: row.label_form, questionId: row.questionId,
                    questionName: row.questionName, label_question: row.label_question, type: row.type, minimum_value: row.minimum_value, maximum_value: row.maximum_value,
                    options: [{ optionId: row.optionId, value: row.value, label_reply: row.label_reply }]
                }                                      
            }    
            if(i+1 == len && aux.type != 3){
                data.push(aux);
            }
                            
        }

        assessment = { questionList: data, formLabel: data[0].label_form };
        return assessment;
                            
    }
}
