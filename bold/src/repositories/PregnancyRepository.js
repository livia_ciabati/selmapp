import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase } from './BaseRepository';

export default class PregnancyRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from pregnancy', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertPregnancy(pregnancy){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into pregnancy (id, gravidity, parity, num_previous_abort, n_previous_induced_abort, n_previous_spontaneous_abort, num_previous_c_section, previous_uterine_surgery, n_previous_stillbirths, outcome_last_childbirth, childbirth_last_delivery, referred, estimate_gestional_age, estimate_gestional_age_method, date_of_last_mensturation, expected_delivery_date, woman_id, createdAt, updatedAt, userBoldId)'+
                    ' values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id, pregnancy.gravidity, pregnancy.parity, pregnancy.num_previous_abort, 
                            pregnancy.n_previous_induced_abort, pregnancy.n_previous_spontaneous_abort, 
                            pregnancy.num_previous_c_section, pregnancy.previous_uterine_surgery, 
                            pregnancy.n_previous_stillbirths, pregnancy.outcome_last_childbirth, 
                            pregnancy.childbirth_last_delivery, pregnancy.referred, pregnancy.estimate_gestional_age, 
                            pregnancy.estimate_gestional_age_method, pregnancy.date_of_last_mensturation, 
                            pregnancy.expected_delivery_date, pregnancy.woman_id, 
                            getCurrentTimeToDatabase(),
                            getCurrentTimeToDatabase(), 
                            pregnancy.userBoldId], (tx, results) => {
                                pregnancy.id = results.insertId;
                                resolve(pregnancy);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatePregnancy(pregnancy){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update pregnancy set gravidity = ?, parity = ?, num_previous_abort = ?, n_previous_induced_abort = ?, n_previous_spontaneous_abort = ?, num_previous_c_section = ?, previous_uterine_surgery = ?,'+
                    ' n_previous_stillbirths = ?, outcome_last_childbirth = ?, childbirth_last_delivery = ?, referred = ?, estimate_gestional_age = ?, estimate_gestional_age_method = ?, date_of_last_mensturation = ?, '+
                    ' expected_delivery_date = ?, woman_id = ?, updatedAt = ?, userBoldId = ? where id = ?', 
                        [ pregnancy.gravidity,pregnancy.parity,pregnancy.num_previous_abort,pregnancy.n_previous_induced_abort,pregnancy.n_previous_spontaneous_abort,
                            pregnancy.num_previous_c_section,pregnancy.previous_uterine_surgery,pregnancy.n_previous_stillbirths,pregnancy.outcome_last_childbirth,
                            pregnancy.childbirth_last_delivery,pregnancy.referred,pregnancy.estimate_gestional_age,pregnancy.estimate_gestional_age_method,
                            pregnancy.date_of_last_mensturation,pregnancy.expected_delivery_date,pregnancy.woman_id,
                            getCurrentTimeToDatabase(),
                            pregnancy.userBoldId,pregnancy.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletePregnancy(pregnancy_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from pregnancy where id = ?', [pregnancy_id], (tx, results) => {
                        var pregnancy = results.rows.item(0);
                        resolve(pregnancy)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getPregnancyById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , gravidity, parity, num_previous_abort, n_previous_induced_abort, n_previous_spontaneous_abort, num_previous_c_section, previous_uterine_surgery, n_previous_stillbirths, outcome_last_childbirth, childbirth_last_delivery, referred, estimate_gestional_age, estimate_gestional_age_method, date_of_last_mensturation, expected_delivery_date, woman_id, createdAt, updatedAt, userBoldId from pregnancy where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var pregnancy = results.rows.item(0);                    
                        resolve(pregnancy)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getPregnancyByWomanId(woman_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , gravidity, parity, num_previous_abort, n_previous_induced_abort, n_previous_spontaneous_abort, num_previous_c_section, previous_uterine_surgery, n_previous_stillbirths, outcome_last_childbirth, childbirth_last_delivery, referred, estimate_gestional_age, estimate_gestional_age_method, date_of_last_mensturation, expected_delivery_date, woman_id, createdAt, updatedAt, userBoldId from pregnancy where woman_id = ' + woman_id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var pregnancy = results.rows.item(0);                    
                        resolve(pregnancy)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    
    static getAllPregnancy(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, gravidity, parity, num_previous_abort, n_previous_induced_abort, n_previous_spontaneous_abort, num_previous_c_section, previous_uterine_surgery, n_previous_stillbirths, outcome_last_childbirth, childbirth_last_delivery, referred, estimate_gestional_age, estimate_gestional_age_method, date_of_last_mensturation, expected_delivery_date, woman_id, createdAt, updatedAt, userBoldId from pregnancy  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }
    
    static getLastSavedId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from pregnancy where woman_id is not null', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10);
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

}
