import  { database }  from "./DatabaseManager";

export default class questionsFormsRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from questionsForms', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("questionsFormsRepository getNextId erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertquestionsForms(questionsForms){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into questionsForms (id, assessment_questions_id, assessments_forms_id, order_question_number )'+
                    ' values ( ?, ?, ?, ?)', 
                        [next_id, assessment_questions_id, assessments_forms_id, order_question_number ], (tx, results) => {
                                questionsForms.id = results.insertId;
                                resolve(questionsForms);
                            },
                            (tx, err) => {
                                //console.log("questionsFormsRepository insertquestionsForms erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatequestionsForms(questionsForms){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update questionsForms set assessment_questions_id = ?, assessments_forms_id = ?, order_question_number = ?  where id = ?', 
                        [questionsForms.assessment_questions_id,questionsForms.assessments_forms_id,questionsForms.order_question_number, questionsForms.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletequestionsForms(questionsForms_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from questionsForms where id = ?', [questionsForms_id], (tx, results) => {
                        var questionsForms = results.rows.item(0);
                        resolve(questionsForms)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getquestionsFormsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, assessment_questions_id, assessments_forms_id, order_question_number  from questionsForms where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var questionsForms = results.rows.item(0);                    
                        resolve(questionsForms)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllquestionsForms(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, assessment_questions_id, assessments_forms_id, order_question_number from questionsForms  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAll(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('delete from questionsForms;', [], (tx, results) => {
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("questionsFormsRepository deleteAll erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }
}
