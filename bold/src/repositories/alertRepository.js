import  { database }  from "./DatabaseManager";

export default class alertRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from alert', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertalert(alert){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into alert (id , title, text, link)'+
                    ' values ( ?, ?, ?, ?)', 
                        [next_id , title, text, link], (tx, results) => {
                                alert.id = results.insertId;
                                resolve(alert);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatealert(alert){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update alert set   title = ?, text = ?, link = ? where id = ?', 
                        [ alert.title,alert.text,alert.link,alert.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletealert(alert_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from alert where id = ?', [alert_id], (tx, results) => {
                        var alert = results.rows.item(0);
                        resolve(alert)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getalertById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , title, text, link from alert where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var alert = results.rows.item(0);                    
                        resolve(alert)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllalert(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, title, text, link, icon_name, risk_level from alert  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static getalertByTitle(title) {
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, title, text, link from alert where title = \'' + title + '\'';        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        if( results.rows.length > 0) {
                            var alert = results.rows.item(0);                    
                            resolve(alert)
                        }
                        else{
                            resolve({});
                        }
                    },
                    (tx, err) => {
                        //console.log("getalertByTitle");
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }
}
