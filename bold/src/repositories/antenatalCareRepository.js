import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase } from './BaseRepository';

export default class antenatalCareRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from antenatalCare', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertantenatalCare(antenatalCare){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into antenatalCare (id, weight, height, placenta_praevia, accreta_increta_percreta, abruptio_placentae, other_obstetric_haemorrhage, pre_eclampsia, eclampsia, '+ 
                    'pyelonephritis, malaria, preterm_rupture_membranes, anaemia, gestational_diabetes, other_pregnancy_complications, pregnancy_id, createdAt, updatedAt, userBoldId)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id, antenatalCare.weight,antenatalCare.height,antenatalCare.placenta_praevia,antenatalCare.accreta_increta_percreta,antenatalCare.abruptio_placentae,
                            antenatalCare.other_obstetric_haemorrhage,antenatalCare.pre_eclampsia,antenatalCare.eclampsia,antenatalCare.pyelonephritis,antenatalCare.malaria,
                            antenatalCare.preterm_rupture_membranes,antenatalCare.anaemia,antenatalCare.gestational_diabetes,antenatalCare.other_pregnancy_complications,
                            antenatalCare.pregnancy_id,getCurrentTimeToDatabase(),getCurrentTimeToDatabase(),antenatalCare.userBoldId], (tx, results) => {
                                antenatalCare.id = results.insertId;
                                resolve(antenatalCare);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateantenatalCare(antenatalCare){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update antenatalCare set  weight = ?, height = ?, placenta_praevia = ?, accreta_increta_percreta = ?, abruptio_placentae = ?, other_obstetric_haemorrhage = ?, pre_eclampsia = ?, eclampsia = ?, pyelonephritis = ?, malaria = ?, preterm_rupture_membranes = ?, anaemia = ?, gestational_diabetes = ?, other_pregnancy_complications = ?, pregnancy_id = ?, updatedAt = ?, userBoldId = ? where id = ?', 
                        [ antenatalCare.weight,antenatalCare.height,antenatalCare.placenta_praevia,antenatalCare.accreta_increta_percreta,antenatalCare.abruptio_placentae,
                            antenatalCare.other_obstetric_haemorrhage,antenatalCare.pre_eclampsia,antenatalCare.eclampsia,antenatalCare.pyelonephritis,antenatalCare.malaria,
                            antenatalCare.preterm_rupture_membranes,antenatalCare.anaemia,antenatalCare.gestational_diabetes,antenatalCare.other_pregnancy_complications,
                            antenatalCare.pregnancy_id,getCurrentTimeToDatabase(),antenatalCare.userBoldId,antenatalCare.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteantenatalCare(antenatalCare_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from antenatalCare where id = ?', [antenatalCare_id], (tx, results) => {
                        var antenatalCare = results.rows.item(0);
                        resolve(antenatalCare)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }


    static getantenatalCareById(ant_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select ant.id , weight, height, placenta_praevia, accreta_increta_percreta, abruptio_placentae, '+
                        ' other_obstetric_haemorrhage, pre_eclampsia, eclampsia, '+
                        ' pyelonephritis, malaria, preterm_rupture_membranes, anaemia, gestational_diabetes,'+
                        ' other_pregnancy_complications, '+
                        ' pregnancy_id, ant.createdAt, ant.updatedAt, ant.userBoldId '+
                        '     from antenatalCare ant '+
                        '     where ant.id = ? ';
                    tx.executeSql(query, 
                    [ant_id], (tx, results) => {
                        var antenatalCare = results.rows.item(0);                    
                        resolve(antenatalCare)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }
    static getAntenatalCareByWomanId(woman_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select ant.id , weight, height, placenta_praevia, accreta_increta_percreta, abruptio_placentae, '+
                        ' other_obstetric_haemorrhage, pre_eclampsia, eclampsia, '+
                        ' pyelonephritis, malaria, preterm_rupture_membranes, anaemia, gestational_diabetes, other_pregnancy_complications, '+
                        ' pregnancy_id, ant.createdAt, ant.updatedAt, ant.userBoldId '+
                        '     from antenatalCare ant '+
                        '     join pregnancy p on ant.pregnancy_id = p.id '+
                        '     join woman     w on p.woman_id     = w.id '+
                        '     where woman_id = ? ';
                    tx.executeSql(query, 
                    [woman_id], (tx, results) => {
                        var antenatalCare = results.rows.item(0);                    
                        resolve(antenatalCare)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllantenatalCare(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, weight, height, placenta_praevia, accreta_increta_percreta, abruptio_placentae, other_obstetric_haemorrhage, pre_eclampsia, eclampsia, pyelonephritis, malaria, preterm_rupture_membranes, anaemia, gestational_diabetes, other_pregnancy_complications, pregnancy_id, createdAt, updatedAt, userBoldId from antenatalCare  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
