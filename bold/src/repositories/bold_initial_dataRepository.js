import  { database }  from "./DatabaseManager";

export default class bold_initial_dataRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from bold_initial_data', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertbold_initial_data(bold_initial_data){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into bold_initial_data (id , augmentation_labour, education_level, axillary_temperature, c18_temperature_c, cervical_dilatation_cm, c18_cervical_dilatation, fetal_heart_rate_bpm, c18_fetal_heart_rate, maternal_heart_rate, c18_maternal_heart_rate, num_antenatal_care_visits, systolic_blood_pressure, c18_syst_blood_pressure, caesarean_section_cs, outcome, robson)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , augmentation_labour, education_level, axillary_temperature, c18_temperature_c, cervical_dilatation_cm, c18_cervical_dilatation, fetal_heart_rate_bpm, c18_fetal_heart_rate, maternal_heart_rate, c18_maternal_heart_rate, num_antenatal_care_visits, systolic_blood_pressure, c18_syst_blood_pressure, caesarean_section_cs, outcome, robson], (tx, results) => {
                                bold_initial_data.id = results.insertId;
                                resolve(bold_initial_data);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatebold_initial_data(bold_initial_data){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update bold_initial_data set   augmentation_labour = ?, education_level = ?, axillary_temperature = ?, c18_temperature_c = ?, cervical_dilatation_cm = ?, c18_cervical_dilatation = ?, fetal_heart_rate_bpm = ?, c18_fetal_heart_rate = ?, maternal_heart_rate = ?, c18_maternal_heart_rate = ?, num_antenatal_care_visits = ?, systolic_blood_pressure = ?, c18_syst_blood_pressure = ?, caesarean_section_cs = ?, outcome = ?, robson = ? where id = ?', 
                        [ bold_initial_data.augmentation_labour,bold_initial_data.education_level,bold_initial_data.axillary_temperature,bold_initial_data.c18_temperature_c,bold_initial_data.cervical_dilatation_cm,bold_initial_data.c18_cervical_dilatation,bold_initial_data.fetal_heart_rate_bpm,bold_initial_data.c18_fetal_heart_rate,bold_initial_data.maternal_heart_rate,bold_initial_data.c18_maternal_heart_rate,bold_initial_data.num_antenatal_care_visits,bold_initial_data.systolic_blood_pressure,bold_initial_data.c18_syst_blood_pressure,bold_initial_data.caesarean_section_cs,bold_initial_data.outcome,bold_initial_data.robson,bold_initial_data.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletebold_initial_data(bold_initial_data_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from bold_initial_data where id = ?', [bold_initial_data_id], (tx, results) => {
                        var bold_initial_data = results.rows.item(0);
                        resolve(bold_initial_data)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getbold_initial_dataById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , augmentation_labour, education_level, axillary_temperature, c18_temperature_c, cervical_dilatation_cm, c18_cervical_dilatation, fetal_heart_rate_bpm, c18_fetal_heart_rate, maternal_heart_rate, c18_maternal_heart_rate, num_antenatal_care_visits, systolic_blood_pressure, c18_syst_blood_pressure, caesarean_section_cs, outcome, robson from bold_initial_data where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var bold_initial_data = results.rows.item(0);                    
                        resolve(bold_initial_data)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllbold_initial_data(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, augmentation_labour, education_level, axillary_temperature, c18_temperature_c, cervical_dilatation_cm, c18_cervical_dilatation, fetal_heart_rate_bpm, c18_fetal_heart_rate, maternal_heart_rate, c18_maternal_heart_rate, num_antenatal_care_visits, systolic_blood_pressure, c18_syst_blood_pressure, caesarean_section_cs, outcome, robson from bold_initial_data  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
