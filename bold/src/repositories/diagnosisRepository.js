import  { database }  from "./DatabaseManager";

export default class diagnosisRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from diagnosis', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0){
                            max_id = results.rows.item(0).max_id + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertdiagnosis(diagnosis){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into diagnosis (id , name, label)'+
                    ' values ( ?, ?, ?)', 
                        [next_id , diagnosis.name, diagnosis.label], (tx, results) => {
                                diagnosis.id = results.insertId;
                                resolve(diagnosis);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatediagnosis(diagnosis){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update diagnosis set  name = ?, label=? where id = ?', 
                        [ diagnosis.name,diagnosis.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletediagnosis(diagnosis_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from diagnosis where id = ?', [diagnosis_id], (tx, results) => {
                        var diagnosis = results.rows.item(0);
                        resolve(diagnosis)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getdiagnosisById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, name, label, risk_level from diagnosis where id = ? ';        
                    tx.executeSql(query, 
                    [id], (tx, results) => {
                        var diagnosis = results.rows.item(0);                    
                        resolve(diagnosis)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    
    static getdiagnosisByName(name){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, name, label, risk_level from diagnosis where name = ?';        
                    tx.executeSql(query, 
                    [name], (tx, results) => {
                        var diagnosis = results.rows.item(0);    
                        resolve(diagnosis)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }


    static getAlldiagnosis(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select id, name, label, risk_level from diagnosis  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
