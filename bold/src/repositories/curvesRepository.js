// import  { database }  from "./DatabaseManager";

// export default class curvesRepository {

//     static getNextId(){
//         return new Promise((resolve, reject) => {
//             database.getDatabase().then(db => { 
//                 db.transaction((tx) => {            
//                     tx.executeSql('Select max(id) as max_id from curves', [], (tx, results) => {
//                         var max_id = 1;
//                         if(results.rows.length > 0 && results.rows.item(0).max_id != null){
//                             max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
//                         }
//                         resolve(max_id)
//                     },
//                     (tx, err) => {
//                         //console.log("erro: " + JSON.stringify(err));
//                         reject(err);
//                     });
//                 });
//             });
//         })
//     }

//     static insertcurves(curves){
//         return new Promise((resolve, reject) => {
//             database.getDatabase().then(db => { 
//                 this.getNextId().then((next_id) =>
//                 {
//                     db.transaction((tx) => {
//                     tx.executeSql('Insert into curves (id, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 , intervention, curve, start)'+
//                     ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
//                         [next_id, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 , intervention, curve, start], (tx, results) => {
//                                 curves.id = results.insertId;
//                                 resolve(curves);
//                             },
//                             (tx, err) => {
//                                 //console.log("erro: " + JSON.stringify(err));
//                                 reject(err);
//                             }
//                         )
//                     });
//                 });    
//             });        
//         });
//     }

//     static updatecurves(curves){        
//         return new Promise((resolve, reject) => {
//             database.getDatabase().then(db => { 
//                 db.transaction((tx) => {
//                     tx.executeSql('Update curves set , 0 = ?, 1 = ?, 2 = ?, 3 = ?, 4 = ?, 5 = ?, 6 = ?, 7 = ?, 8 = ?, 9 = ?, 10 = ?, 11 = ?, 12 = ?, 13 = ?, 14 = ?, 15 = ?, 16 = ?, 17 = ?, 18 = ?, 19 = ?, 20 = ?, 21 = ?, 22 = ?, 23 = ?, 24 = ?, 25 = ?, 26 = ?, 27 = ?, 28 = ?, 29 = ?, 30 = ? , intervention = ?, curve = ?, start = ? where id = ?', 
//                         [curves.0,curves.1,curves.2,curves.3,curves.4,curves.5,curves.6,curves.7,curves.8,curves.9,curves.10,curves.11,curves.12,curves.13,curves.14,curves.15,curves.16,curves.17,curves.18,curves.19,curves.20,curves.21,curves.22,curves.23,curves.24,curves.25,curves.26,curves.27,curves.28,curves.29,curves.30, curves.intervention,curves.curve,curves.start,curves.id], 
//                         (tx, results) => {
//                             resolve(results);
//                         },
//                         (tx, err) => {
//                            //console.warn(tx);
//                             reject(err);
//                         }
//                     )
//                 });
//             });
//         });
//     }

//     static deletecurves(curves_id){
//         return new Promise((resolve, reject) => {
//             database.getDatabase().then(db => { 
//                 db.transaction((tx) => {            
//                     tx.executeSql('Delete from curves where id = ?', [curves_id], (tx, results) => {
//                         var curves = results.rows.item(0);
//                         resolve(curves)
//                     },
//                     (tx, err) => {
//                        //console.warn(tx);
//                         reject(err);
//                     });
//                 });
//             });
//         });
//     }

//     static getcurvesById(id){
//         return new Promise((resolve, reject) => {
//             database.getDatabase().then(db => { 
//                 db.transaction((tx) => {    
//                     var query = 'Select id, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 , intervention, curve, start from curves where id = ' + id;        
//                     tx.executeSql(query, 
//                     [], (tx, results) => {
//                         var curves = results.rows.item(0);                    
//                         resolve(curves)
//                     },
//                     (tx, err) => {
//                        //console.warn(tx);
//                         reject(err);
//                     });
//                 });
//             });
//         });
//     }

//     static getAllcurves(){
//         return new Promise((resolve, reject) => {
//             database.getDatabase().then(db => { 
//                 db.transaction(
//                 (tx) => {
//                     tx.executeSql(
//                         'select  id, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, intervention, curve, start from curves  '
//                         , [], (tx, results) => {
//                             var len = results.rows.length;
//                             var data = [];
//                             for (let i = 0; i < len; i++) {
//                                 let row = results.rows.item(i);
//                                 data.push(row);
//                             }     
//                             resolve(data);
//                         },
//                         (tx, err) => {
//                            //console.warn(tx);
//                             reject(err);
//                         }
//                     )
//                 });
//             });
//         });
//     }

// }
