import  { database }  from "./DatabaseManager";

export default class SettingsRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from Settings', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0){
                            max_id = results.rows.item(0).max_id + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertSettings(Settings){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into Settings (id, name, label, value)'+
                    ' values (, ?, ?, ?, ?)', 
                        [next_id, Settings.name,Settings.label,Settings.value], (tx, results) => {
                                Settings.id = results.insertId;
                                resolve(Settings);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateSettings(Settings){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update Settings set name = ?, label = ?, value = ? where id = ?', 
                        [Settings.name,Settings.label,Settings.value,Settings.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }


    static updateSettingsValueById(value, id){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update Settings set value = ? where id = ?', 
                        [value, id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteSettings(Settings_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from Settings where id = ?', [Settings_id], (tx, results) => {
                        var Settings = results.rows.item(0);
                        resolve(Settings)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getSettingsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, name, label, value from Settings where id = ? ';        
                    tx.executeSql(query, 
                    [id], (tx, results) => {
                        var Settings = results.rows.item(0);                    
                        resolve(Settings)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getSettingsByName(name){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, name, label, value from Settings where name = ? ';        
                    tx.executeSql(query, 
                    [name], (tx, results) => {
                        var Settings = results.rows.item(0);  
                        //console.log("SETTINGS BY NAME" +JSON.stringify(Settings))                  
                        resolve(Settings)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllSettings(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select id, name, label, value from Settings  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }
    
    static getAllSettingsForRules(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select id, name, label, value from Settings  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = {};
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data[row.name] = row.value;
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
