import  { database }  from "./DatabaseManager";

export default class questionsOptionsRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from questionsOptions', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertquestionsOptions(questionsOptions){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into questionsOptions (id, assessment_questions_id, assessment_options_id )'+
                    ' values ( ?, ?, ?)', 
                        [next_id, assessment_questions_id, assessment_options_id ], (tx, results) => {
                                questionsOptions.id = results.insertId;
                                resolve(questionsOptions);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatequestionsOptions(questionsOptions){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update questionsOptions set assessment_questions_id = ?, assessment_options_id = ?  where id = ?', 
                        [questionsOptions.assessment_questions_id,questionsOptions.assessment_options_id, questionsOptions.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletequestionsOptions(questionsOptions_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from questionsOptions where id = ?', [questionsOptions_id], (tx, results) => {
                        var questionsOptions = results.rows.item(0);
                        resolve(questionsOptions)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getquestionsOptionsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, assessment_questions_id, assessment_options_id  from questionsOptions where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var questionsOptions = results.rows.item(0);                    
                        resolve(questionsOptions)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllquestionsOptions(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, assessment_questions_id, assessment_options_id from questionsOptions  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAll(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('delete from questionsOptions;', [], (tx, results) => {
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }
}
