import  { database }  from "./DatabaseManager";

export default class assessmentOptionsRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from assessmentOptions', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertassessmentOptions(assessmentOptions){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into assessmentOptions (id , value, label_reply)'+
                    ' values ( ?, ?, ?)', 
                        [next_id , value, label_reply], (tx, results) => {
                                assessmentOptions.id = results.insertId;
                                resolve(assessmentOptions);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateassessmentOptions(assessmentOptions){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentOptions set   value = ?, label_reply = ? where id = ?', 
                        [ assessmentOptions.value,assessmentOptions.label_reply,assessmentOptions.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteassessmentOptions(assessmentOptions_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from assessmentOptions where id = ?', [assessmentOptions_id], (tx, results) => {
                        var assessmentOptions = results.rows.item(0);
                        resolve(assessmentOptions)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getassessmentOptionsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , value, label_reply from assessmentOptions where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var assessmentOptions = results.rows.item(0);                    
                        resolve(assessmentOptions)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllassessmentOptions(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, value, label_reply from assessmentOptions  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAll(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('delete from assessmentOptions;', [], (tx, results) => {
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }
}
