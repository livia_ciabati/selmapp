import  { database }  from "./DatabaseManager";

export default class interventionParametersRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from interventionParameters', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertinterventionParameters(interventionParameters){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into interventionParameters (id , name, type, assessment_questions_id)'+
                    ' values ( ?, ?, ?, ?)', 
                        [next_id , name, type, assessment_questions_id], (tx, results) => {
                                interventionParameters.id = results.insertId;
                                resolve(interventionParameters);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateinterventionParameters(interventionParameters){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update interventionParameters set   name = ?, type = ?, assessment_questions_id = ? where id = ?', 
                        [ interventionParameters.name,interventionParameters.type,interventionParameters.assessment_questions_id,interventionParameters.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteinterventionParameters(interventionParameters_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from interventionParameters where id = ?', [interventionParameters_id], (tx, results) => {
                        var interventionParameters = results.rows.item(0);
                        resolve(interventionParameters)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getinterventionParametersById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , name, type, assessment_questions_id from interventionParameters where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var interventionParameters = results.rows.item(0);                    
                        resolve(interventionParameters)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllinterventionParameters(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, name, type, assessment_questions_id from interventionParameters  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
