import  { database }  from "./DatabaseManager";

export default class ACLRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from ACL', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertACL(ACL){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into ACL (id , model, property, accessType, permission, principalType, principalId)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , model, property, accessType, permission, principalType, principalId], (tx, results) => {
                                ACL.id = results.insertId;
                                resolve(ACL);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateACL(ACL){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update ACL set   model = ?, property = ?, accessType = ?, permission = ?, principalType = ?, principalId = ? where id = ?', 
                        [ ACL.model,ACL.property,ACL.accessType,ACL.permission,ACL.principalType,ACL.principalId,ACL.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteACL(ACL_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from ACL where id = ?', [ACL_id], (tx, results) => {
                        var ACL = results.rows.item(0);
                        resolve(ACL)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getACLById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , model, property, accessType, permission, principalType, principalId from ACL where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var ACL = results.rows.item(0);                    
                        resolve(ACL)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllACL(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, model, property, accessType, permission, principalType, principalId from ACL  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
