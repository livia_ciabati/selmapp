import moment, { min } from 'moment';


export function getCurrentTimeToDatabase(addTime = 0){
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    var localISOTime = (new Date(Date.now() + (addTime * 60000) - tzoffset)).toISOString().slice(0, -1).replace('T', ' ');
    ////console.log(localISOTime);
    return localISOTime;
}

export function getIdFromDateTime(){
    var date = new Date();
    var components = [
        date.getYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
        date.getMilliseconds()
    ];

    var id = parseInt(components.join(""));
    return id;
}

export function formatDateToTimelinefromDatabase(dateToFormat){
    var formatedDate = "";
    var dateString = dateToFormat.split(" ")[0];
    var day   = dateString.split("-")[2];
    var month = dateString.split("-")[1];
    var year  = dateString.split("-")[0];

    var hourString = dateToFormat.split(" ")[1];

    formatedDate = day + "/" + month + "\n" + hourString;
    return formatedDate;
}

export function calculateAge(birthday) { 
    //console.log(birthday)   
    var from = birthday.split("-")
    var f = new Date(from[2], from[1] - 1, from[0])

    var ageDifMs = Date.now() - f.getTime();    
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

export function diffTimeNowInMinutes(date){
    //var tzoffset = (new Date()).getTimezoneOffset() * 60000; 
    //var dateNow = (new Date(Date.now() - tzoffset));
    var dateNow = new Date(Date.now());
    var mydate = moment(date, "YYYY-MM-DD HH:mm:ss").toDate();

    var diffMinutes = (dateNow.getTime() - mydate.getTime())/(1000*60);
    ////console.log("Create at " + date + "; Now " + dateNow  + "; Diff " +  diffMinutes );
    return diffMinutes;
}


export function diffTimeInMinutes(date1, date2){
    var mydate1 = moment(date1, "YYYY-MM-DD HH:mm:ss").toDate();
    var mydate2 = moment(date2, "YYYY-MM-DD HH:mm:ss").toDate();

    var diffMinutes = (mydate1.getTime() - mydate2.getTime())/(1000*60);
    console.log("Create at " + mydate1 + "; Now " + mydate2  + "; Diff " +  diffMinutes );
    return diffMinutes;
}

