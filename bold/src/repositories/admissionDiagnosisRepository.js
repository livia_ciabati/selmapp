import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase, getIdFromDateTime } from './BaseRepository';
import diagnosisRepository from "./diagnosisRepository";

export default class admissionDiagnosisRepository {

    static getNextId(){
        
        return new Promise((resolve, reject) => {
            resolve(getIdFromDateTime());
        })
        // return new Promise((resolve, reject) => {
        //     database.getDatabase().then(db => { 
        //         db.transaction((tx) => {            
        //             tx.executeSql('Select max(id) as max_id from admissionDiagnosis', [], (tx, results) => {
        //                 var max_id = 1;
        //                 if(results.rows.length > 0){
        //                     max_id = results.rows.item(0).max_id + 1;
        //                 }
        //                 resolve(max_id)
        //             },
        //             (tx, err) => {
        //                 //console.log("erro: " + JSON.stringify(err));
        //                 reject(err);
        //             });
        //         });
        //     });
        // })
    }

    static insertadmissionDiagnosis(admissionDiagnosis){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into admissionDiagnosis (id , diagnosis_id, admission_id, value, createdAt, updatedAt)'+
                    ' values (?, ?, ?, ?, ?, ?)', 
                        [next_id , admissionDiagnosis.diagnosis_id, admissionDiagnosis.admission_id, admissionDiagnosis.value, getCurrentTimeToDatabase(), getCurrentTimeToDatabase()], (tx, results) => {
                                admissionDiagnosis.id = results.insertId;
                                resolve(admissionDiagnosis);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateadmissionDiagnosis(admissionDiagnosis){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admissionDiagnosis set diagnosis_id = ?, admission_id = ?, value = ?, updatedAt = ? where id = ?', 
                        [ admissionDiagnosis.diagnosis_id,admissionDiagnosis.admission_id, admissionDiagnosis.value, getCurrentTimeToDatabase(), admissionDiagnosis.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteadmissionDiagnosis(admissionDiagnosis_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from admissionDiagnosis where id = ?', [admissionDiagnosis_id], (tx, results) => {
                        var admissionDiagnosis = results.rows.item(0);
                        resolve(admissionDiagnosis)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getadmissionDiagnosisById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , diagnosis_id, admission_id, value, createdAt, updatedAt from admissionDiagnosis where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var admissionDiagnosis = results.rows.item(0);                    
                        resolve(admissionDiagnosis)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    
    static getadmissionDiagnosisByAdmissionId(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, diagnosis_id, admission_id, value, createdAt, updatedAt from admissionDiagnosis where admission_id = ? ';        
                    tx.executeSql(query, 
                    [admission_id], (tx, results) => {
                        var admissionDiagnosis = results.rows.item(0);                    
                        resolve(admissionDiagnosis)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getadmissionDiagnosisByAdmissionIdForRules(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select a.id, a.diagnosis_id, a.admission_id, a.value, a.createdAt, a.updatedAt, '+
                    ' d.id, d.name, d.label, d.risk_level '+
                    ' from admissionDiagnosis a '+
                    ' Join diagnosis d on diagnosis_id = d.id' +
                    ' where admission_id = ? ';        
                    tx.executeSql(query, 
                    [admission_id], (tx, results) => {                           
                        var len = results.rows.length;
                        var data = {};
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            data[row.name] = {value: row.value, createdAt: row.createdAt, risk_level: row.risk_level }
                        }           
                        resolve(data)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    
    static getadmissionDiagnosisForWomanList(user_id){
        //console.log("ADMISSION DIAGNOSIS FOR WOMAN LIST")
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 
                    'Select d.id, d.name, d.label, d.risk_level, a.value, a.diagnosis_id, a.admission_id,            ' +
                    ' max(a.createdAt)                                                                               ' +
                    ' from admissionDiagnosis a                                                                      ' +
                    ' Join diagnosis d on diagnosis_id = d.id                                                        ' +
                    ' Join admission adm on a.admission_id = adm.id                                                  ' +
                    ' where adm.userBoldId = ? and adm.admission_status = 1                                          ' +
                    ' group by adm.id, d.id                                                                         ' +
                    ' order by adm.id, d.risk_level, d.name                                                                   ' 
                    ;

                    tx.executeSql(query, 
                    [user_id], (tx, results) => {                           
                        var len = results.rows.length;
                        var obj = [];
                        var data = [];
                        var diagList = {}

                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            obj.push(row);
                        }    

                        for (let i = 0; i < obj.length; i++) {
                            let row = obj[i];
                            var admission_id = row.admission_id;

                            if(data.length == 0){
                                diagList = obj.filter(obj => {
                                    return obj.admission_id === admission_id
                                });
                                data.push({admission_id: admission_id, diagnosis: diagList})
                            }
                            else if(data.length > 0) {
                                ////console.log(data[data.length - 1].admission_id + " " + admission_id );
                                if(data[data.length - 1].admission_id != admission_id){
                                    diagList = obj.filter(obj => {
                                        return obj.admission_id === admission_id
                                    });
                                    data.push({admission_id: admission_id, diagnosis: diagList});
                                }
                            }
                        }
                        
                        resolve(data)
                    },
                    (tx, err) => {
                        //console.log('ERRO')
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }


    static getAlladmissionDiagnosis(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, diagnosis_id, admission_id, value, createdAt, updatedAt from admissionDiagnosis  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    
    static getAlladmissionDiagnosisByNameAndAdmissionId(diagnosis_name, admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  a.id, a.diagnosis_id, a.admission_id, a.value, a.createdAt, a.updatedAt from admissionDiagnosis a ' +
                        ' Join diagnosis d on diagnosis_id = d.id                                                                  ' +
                        ' Join admission adm on a.admission_id = adm.id                                                            ' +
                        ' where d.name = ? and adm.id = ?                                                                          '
                        , [diagnosis_name, admission_id], (tx, results) => {                         
                            var len = results.rows.length;
                            var data = {};
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data[row.name] = {value: row.value, createdAt: row.createdAt}
                            }           
                            resolve(data)
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static insertAdmissionDiagnosisByName(diagnosisName, diagnosisValue, admissionId)
    {
        ////console.log("******************** Before NAME: " + diagnosisName);
        return diagnosisRepository.getdiagnosisByName(diagnosisName).then((diagnosis) => 
        {   
            if(diagnosis != undefined){                 
                admissionDiagnosis = { diagnosis_id: diagnosis.id, admission_id: admissionId, value: diagnosisValue }
                this.insertadmissionDiagnosis(admissionDiagnosis);
            }
            else {
                ////console.log("******************** After (null) NAME: " + diagnosisName);
                ////console.log(diagnosis);
            }
        });
    }

    static backToTheFutureAdmissionDiagnosis(minutes){        
        //console.log(minutes);
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admissionDiagnosis set createdAt = datetime(createdAt, \'-'+ minutes + ' minutes\')', 
                        [], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
