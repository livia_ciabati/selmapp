import  { database }  from "./DatabaseManager";

export default class DashboardRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from curves', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    
    static getDashboardByNameAndAdmId(name, adm_id){
        return new Promise((resolve, reject) => {            
            database.getDatabase().then(db => {  
                db.transaction(
                (tx) => {
                    //maternal data
                    tx.executeSql(
                    ' SELECT  max(ar.createdAt) as createdAt, aq.id, aq.label_question,      '+ 
                    '              aq.name as name_question, ar.admission_id, ar.answer      ' +
                    ' FROM assessmentsForms af                                               ' +          
                    ' JOIN questionsForms qf ON qf.assessments_forms_id = af.id              ' +             
                    ' JOIN assessmentQuestions aq ON aq.id = qf.assessment_questions_id      ' +
                    ' LEFT JOIN assessmentResult ar on ar.assessment_questions_id = aq.id    ' +
                    ' AND (ar.admission_id = ? or ar.admission_id is NULL)                   ' +
                    ' WHERE af.name = ?                                                      ' + 
                    ' group by aq.id  ',
                    [adm_id, name], 
                    (tx, results) => {
                        var len = results.rows.length;
                        var d = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            d.push({id: row.id,
                                admission_id: row.admission_id,
                                label_question: row.label_question,
                                name_question: row.name_question,
                                answer: (row.answer!=null ? row.answer : "?"),
                                createdAt: (row.createdAt!=null ? 
                                    row.createdAt : "--/--/----")});  
                        }
                        ////console.log("DASHBOARD: " + JSON.stringify(d));
                        resolve(d);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    }); // end tx.executeSql()
                }); //end db.transaction
            })
        });
    }


    
    
    //Return all results for the last assessment;
    //if parameter first = true, return all results for the first assessment
    static getAssessmentResultsByAdmId(adm_id, first = false){
        return new Promise((resolve, reject) => {            
            database.getDatabase().then(db => {  
                db.transaction(
                (tx) => {
                    //maternal data
                    tx.executeSql(
                    ' SELECT '+ (first ? 'min' : 'max') +'(ar.createdAt) as createdAt, aq.id, aq.label_question,      '+ 
                    '              aq.name as name_question, ar.admission_id, ar.answer                               ' +
                    ' FROM assessmentsForms af                                                                        ' +          
                    ' JOIN questionsForms qf ON qf.assessments_forms_id = af.id                                       ' +             
                    ' JOIN assessmentQuestions aq ON aq.id = qf.assessment_questions_id                               ' +
                    ' LEFT JOIN assessmentResult ar on ar.assessment_questions_id = aq.id                             ' +
                    ' AND (ar.admission_id = ? or ar.admission_id is NULL)                                            ' +
                    ' WHERE af.name = \'full_assessment\'                                                             ' + 
                    ' group by aq.id  ',
                    [adm_id], 
                    (tx, results) => {
                        var len = results.rows.length;
                        var d = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            d[row.name_question] = (row.answer!=null ? row.answer : "?");
                        }   
                        //console.log(d);
                        resolve(d);
                    },
                    (tx, err) => {
                        //console.log("ERROU")
                       //console.warn(tx);
                        reject(err);
                    }); // end tx.executeSql()
                }); //end db.transaction
            })
        });
    }
}