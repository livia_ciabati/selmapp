import  { database }  from "./DatabaseManager";

export default class MaternalOutcomesRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from maternalOutcomes', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertMaternalOutcome(mat){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into maternalOutcomes (id , dystocia_labour_obstruction, dystocia_first_stage_labour, dystocia_prolonged_second_stage, haemorrhage_placenta_praevia, haemorrhage_accreta_increta_percreta_placenta, haemorrhage_abruptio_placenta, haemorrhage_ruptured_uterus, haemorrhage_intrapartum, haemorrhage_postpartum, infection_puerperal_endometritis, infection_puerperal_sepsis, infection_wound, infection_systemic_septicaemia, hypertension_severe, hypertension_pre_eclampsia, hypertension_eclampsia, other_embolic_disease, otherPotentiallyLifeThreateningCondition, conditions_during_hospital_stay, cardiovascular_dysfunction, respiratory_dysfunction, renal_dysfunction, coagulation_dysfunction, hepatic_dysfunction, neurologic_dysfunction, uterine_dysfunction, dysfunction_identified, nmm_haemorrhage_oxytocin_treatment_PPH, nmm_haemorrhage_misoprostol_treatment_PPH, nmm_haemorrhage_ergotamine_treatment_PPH, nmm_haemorrhage_other_uterotonic_treatment_PPH, nmm_haemorrhage_artery_ligation_or_embolization, nmm_haemorrhage_balloon_or_condom_tamponade, nmm_haemorrhage_repair_cervical_laceration, nmm_haemorrhage_repair_uterine_rupture, nmm_haemorrhage_b_lynch_suture, nmm_haemorrhage_hysterectomy, nmm_infection_therapeutic_antibiotics, nmm_hypertension_magnesium_sulphate_as_anticonvulsant, nmm_hypertension_other_anticonvulsant, nmm_other_removal_retained_products, nmm_other_manual_removal_placenta, nmm_other_blood_transfusion, nmm_other_laparotomy, nmm_other_admission_ICU, intervention_identified, referred_higher_complexity_hospital, status_hospital_discharge, discharge_transfer_death_date, admission_id, createdAt, updatedAt, userBoldId)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , mat.dystocia_labour_obstruction, mat.dystocia_first_stage_labour, mat.dystocia_prolonged_second_stage, 
                            mat.haemorrhage_placenta_praevia, mat.haemorrhage_accreta_increta_percreta_placenta, mat.haemorrhage_abruptio_placenta, 
                            mat.haemorrhage_ruptured_uterus, mat.haemorrhage_intrapartum, mat.haemorrhage_postpartum, 
                            mat.infection_puerperal_endometritis, mat.infection_puerperal_sepsis, mat.infection_wound, 
                            mat.infection_systemic_septicaemia, mat.hypertension_severe, mat.hypertension_pre_eclampsia, mat.hypertension_eclampsia, 
                            mat.other_embolic_disease, mat.otherPotentiallyLifeThreateningCondition, mat.conditions_during_hospital_stay, 
                            mat.cardiovascular_dysfunction, mat.respiratory_dysfunction, mat.renal_dysfunction, mat.coagulation_dysfunction, mat.hepatic_dysfunction, 
                            mat.neurologic_dysfunction, mat.uterine_dysfunction, mat.dysfunction_identified, mat.nmm_haemorrhage_oxytocin_treatment_PPH, 
                            mat.nmm_haemorrhage_misoprostol_treatment_PPH, mat.nmm_haemorrhage_ergotamine_treatment_PPH, mat.nmm_haemorrhage_other_uterotonic_treatment_PPH, 
                            mat.nmm_haemorrhage_artery_ligation_or_embolization, mat.nmm_haemorrhage_balloon_or_condom_tamponade, mat.nmm_haemorrhage_repair_cervical_laceration, 
                            mat.nmm_haemorrhage_repair_uterine_rupture, mat.nmm_haemorrhage_b_lynch_suture, mat.nmm_haemorrhage_hysterectomy, mat.nmm_infection_therapeutic_antibiotics, 
                            mat.nmm_hypertension_magnesium_sulphate_as_anticonvulsant, mat.nmm_hypertension_other_anticonvulsant, mat.nmm_other_removal_retained_products, 
                            mat.nmm_other_manual_removal_placenta, mat.nmm_other_blood_transfusion, mat.nmm_other_laparotomy, mat.nmm_other_admission_ICU, 
                            mat.intervention_identified, mat.referred_higher_complexity_hospital, mat.status_hospital_discharge, mat.discharge_transfer_death_date, 
                            mat.admission_id, 
                            getCurrentTimeToDatabase(),
                            getCurrentTimeToDatabase(),  mat.userBoldId], (tx, results) => {
                                maternalOutcomes.id = results.insertId;
                                resolve(maternalOutcomes);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateMaternalOutcome(maternalOutcomes){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update maternalOutcomes set dystocia_labour_obstruction = ?, dystocia_first_stage_labour = ?, dystocia_prolonged_second_stage = ?, haemorrhage_placenta_praevia = ?, haemorrhage_accreta_increta_percreta_placenta = ?, haemorrhage_abruptio_placenta = ?, haemorrhage_ruptured_uterus = ?, haemorrhage_intrapartum = ?, haemorrhage_postpartum = ?, infection_puerperal_endometritis = ?, infection_puerperal_sepsis = ?, infection_wound = ?, infection_systemic_septicaemia = ?, hypertension_severe = ?, hypertension_pre_eclampsia = ?, hypertension_eclampsia = ?, other_embolic_disease = ?, otherPotentiallyLifeThreateningCondition = ?, conditions_during_hospital_stay = ?, cardiovascular_dysfunction = ?, respiratory_dysfunction = ?, renal_dysfunction = ?, coagulation_dysfunction = ?, hepatic_dysfunction = ?, neurologic_dysfunction = ?, uterine_dysfunction = ?, dysfunction_identified = ?, nmm_haemorrhage_oxytocin_treatment_PPH = ?, nmm_haemorrhage_misoprostol_treatment_PPH = ?, nmm_haemorrhage_ergotamine_treatment_PPH = ?, nmm_haemorrhage_other_uterotonic_treatment_PPH = ?, nmm_haemorrhage_artery_ligation_or_embolization = ?, nmm_haemorrhage_balloon_or_condom_tamponade = ?, nmm_haemorrhage_repair_cervical_laceration = ?, nmm_haemorrhage_repair_uterine_rupture = ?, nmm_haemorrhage_b_lynch_suture = ?, nmm_haemorrhage_hysterectomy = ?, nmm_infection_therapeutic_antibiotics = ?, nmm_hypertension_magnesium_sulphate_as_anticonvulsant = ?, nmm_hypertension_other_anticonvulsant = ?, nmm_other_removal_retained_products = ?, nmm_other_manual_removal_placenta = ?, nmm_other_blood_transfusion = ?, nmm_other_laparotomy = ?, nmm_other_admission_ICU = ?, intervention_identified = ?, referred_higher_complexity_hospital = ?, status_hospital_discharge = ?, discharge_transfer_death_date = ?, admission_id = ?, updatedAt = ?, userBoldId = ? where id = ?', 
                        [ maternalOutcomes.dystocia_labour_obstruction,maternalOutcomes.dystocia_first_stage_labour,maternalOutcomes.dystocia_prolonged_second_stage,
                            maternalOutcomes.haemorrhage_placenta_praevia,maternalOutcomes.haemorrhage_accreta_increta_percreta_placenta,
                            maternalOutcomes.haemorrhage_abruptio_placenta,maternalOutcomes.haemorrhage_ruptured_uterus,maternalOutcomes.haemorrhage_intrapartum,
                            maternalOutcomes.haemorrhage_postpartum,maternalOutcomes.infection_puerperal_endometritis,maternalOutcomes.infection_puerperal_sepsis,
                            maternalOutcomes.infection_wound,maternalOutcomes.infection_systemic_septicaemia,maternalOutcomes.hypertension_severe,
                            maternalOutcomes.hypertension_pre_eclampsia,maternalOutcomes.hypertension_eclampsia,maternalOutcomes.other_embolic_disease,
                            maternalOutcomes.otherPotentiallyLifeThreateningCondition,maternalOutcomes.conditions_during_hospital_stay,
                            maternalOutcomes.cardiovascular_dysfunction,maternalOutcomes.respiratory_dysfunction,maternalOutcomes.renal_dysfunction,
                            maternalOutcomes.coagulation_dysfunction,maternalOutcomes.hepatic_dysfunction,maternalOutcomes.neurologic_dysfunction,
                            maternalOutcomes.uterine_dysfunction,maternalOutcomes.dysfunction_identified,maternalOutcomes.nmm_haemorrhage_oxytocin_treatment_PPH,
                            maternalOutcomes.nmm_haemorrhage_misoprostol_treatment_PPH,maternalOutcomes.nmm_haemorrhage_ergotamine_treatment_PPH,
                            maternalOutcomes.nmm_haemorrhage_other_uterotonic_treatment_PPH,maternalOutcomes.nmm_haemorrhage_artery_ligation_or_embolization,
                            maternalOutcomes.nmm_haemorrhage_balloon_or_condom_tamponade,maternalOutcomes.nmm_haemorrhage_repair_cervical_laceration,
                            maternalOutcomes.nmm_haemorrhage_repair_uterine_rupture,maternalOutcomes.nmm_haemorrhage_b_lynch_suture,
                            maternalOutcomes.nmm_haemorrhage_hysterectomy,maternalOutcomes.nmm_infection_therapeutic_antibiotics,
                            maternalOutcomes.nmm_hypertension_magnesium_sulphate_as_anticonvulsant,maternalOutcomes.nmm_hypertension_other_anticonvulsant,
                            maternalOutcomes.nmm_other_removal_retained_products,maternalOutcomes.nmm_other_manual_removal_placenta,maternalOutcomes.nmm_other_blood_transfusion,
                            maternalOutcomes.nmm_other_laparotomy,maternalOutcomes.nmm_other_admission_ICU,maternalOutcomes.intervention_identified,
                            maternalOutcomes.referred_higher_complexity_hospital,maternalOutcomes.status_hospital_discharge,maternalOutcomes.discharge_transfer_death_date,
                            maternalOutcomes.admission_id,
                            getCurrentTimeToDatabase(),maternalOutcomes.userBoldId,maternalOutcomes.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletematernalOutcomes(maternalOutcomes_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from maternalOutcomes where id = ?', [maternalOutcomes_id], (tx, results) => {
                        var maternalOutcomes = results.rows.item(0);
                        resolve(maternalOutcomes)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getMaternalOutcomeById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , dystocia_labour_obstruction, dystocia_first_stage_labour, dystocia_prolonged_second_stage, haemorrhage_placenta_praevia, haemorrhage_accreta_increta_percreta_placenta, haemorrhage_abruptio_placenta, haemorrhage_ruptured_uterus, haemorrhage_intrapartum, haemorrhage_postpartum, infection_puerperal_endometritis, infection_puerperal_sepsis, infection_wound, infection_systemic_septicaemia, hypertension_severe, hypertension_pre_eclampsia, hypertension_eclampsia, other_embolic_disease, otherPotentiallyLifeThreateningCondition, conditions_during_hospital_stay, cardiovascular_dysfunction, respiratory_dysfunction, renal_dysfunction, coagulation_dysfunction, hepatic_dysfunction, neurologic_dysfunction, uterine_dysfunction, dysfunction_identified, nmm_haemorrhage_oxytocin_treatment_PPH, nmm_haemorrhage_misoprostol_treatment_PPH, nmm_haemorrhage_ergotamine_treatment_PPH, nmm_haemorrhage_other_uterotonic_treatment_PPH, nmm_haemorrhage_artery_ligation_or_embolization, nmm_haemorrhage_balloon_or_condom_tamponade, nmm_haemorrhage_repair_cervical_laceration, nmm_haemorrhage_repair_uterine_rupture, nmm_haemorrhage_b_lynch_suture, nmm_haemorrhage_hysterectomy, nmm_infection_therapeutic_antibiotics, nmm_hypertension_magnesium_sulphate_as_anticonvulsant, nmm_hypertension_other_anticonvulsant, nmm_other_removal_retained_products, nmm_other_manual_removal_placenta, nmm_other_blood_transfusion, nmm_other_laparotomy, nmm_other_admission_ICU, intervention_identified, referred_higher_complexity_hospital, status_hospital_discharge, discharge_transfer_death_date, admission_id, createdAt, updatedAt, userBoldId from maternalOutcomes where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var maternalOutcomes = results.rows.item(0);                    
                        resolve(maternalOutcomes)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllmaternalOutcomes(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, dystocia_labour_obstruction, dystocia_first_stage_labour, dystocia_prolonged_second_stage, haemorrhage_placenta_praevia, haemorrhage_accreta_increta_percreta_placenta, haemorrhage_abruptio_placenta, haemorrhage_ruptured_uterus, haemorrhage_intrapartum, haemorrhage_postpartum, infection_puerperal_endometritis, infection_puerperal_sepsis, infection_wound, infection_systemic_septicaemia, hypertension_severe, hypertension_pre_eclampsia, hypertension_eclampsia, other_embolic_disease, otherPotentiallyLifeThreateningCondition, conditions_during_hospital_stay, cardiovascular_dysfunction, respiratory_dysfunction, renal_dysfunction, coagulation_dysfunction, hepatic_dysfunction, neurologic_dysfunction, uterine_dysfunction, dysfunction_identified, nmm_haemorrhage_oxytocin_treatment_PPH, nmm_haemorrhage_misoprostol_treatment_PPH, nmm_haemorrhage_ergotamine_treatment_PPH, nmm_haemorrhage_other_uterotonic_treatment_PPH, nmm_haemorrhage_artery_ligation_or_embolization, nmm_haemorrhage_balloon_or_condom_tamponade, nmm_haemorrhage_repair_cervical_laceration, nmm_haemorrhage_repair_uterine_rupture, nmm_haemorrhage_b_lynch_suture, nmm_haemorrhage_hysterectomy, nmm_infection_therapeutic_antibiotics, nmm_hypertension_magnesium_sulphate_as_anticonvulsant, nmm_hypertension_other_anticonvulsant, nmm_other_removal_retained_products, nmm_other_manual_removal_placenta, nmm_other_blood_transfusion, nmm_other_laparotomy, nmm_other_admission_ICU, intervention_identified, referred_higher_complexity_hospital, status_hospital_discharge, discharge_transfer_death_date, admission_id, createdAt, updatedAt, userBoldId from maternalOutcomes  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }


    static getMaternalOutcomeByAdmissionId(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , dystocia_labour_obstruction, dystocia_first_stage_labour, dystocia_prolonged_second_stage, haemorrhage_placenta_praevia, haemorrhage_accreta_increta_percreta_placenta, haemorrhage_abruptio_placenta, haemorrhage_ruptured_uterus, haemorrhage_intrapartum, haemorrhage_postpartum, infection_puerperal_endometritis, infection_puerperal_sepsis, infection_wound, infection_systemic_septicaemia, hypertension_severe, hypertension_pre_eclampsia, hypertension_eclampsia, other_embolic_disease, otherPotentiallyLifeThreateningCondition, conditions_during_hospital_stay, cardiovascular_dysfunction, respiratory_dysfunction, renal_dysfunction, coagulation_dysfunction, hepatic_dysfunction, neurologic_dysfunction, uterine_dysfunction, dysfunction_identified, nmm_haemorrhage_oxytocin_treatment_PPH, nmm_haemorrhage_misoprostol_treatment_PPH, nmm_haemorrhage_ergotamine_treatment_PPH, nmm_haemorrhage_other_uterotonic_treatment_PPH, nmm_haemorrhage_artery_ligation_or_embolization, nmm_haemorrhage_balloon_or_condom_tamponade, nmm_haemorrhage_repair_cervical_laceration, nmm_haemorrhage_repair_uterine_rupture, nmm_haemorrhage_b_lynch_suture, nmm_haemorrhage_hysterectomy, nmm_infection_therapeutic_antibiotics, nmm_hypertension_magnesium_sulphate_as_anticonvulsant, nmm_hypertension_other_anticonvulsant, nmm_other_removal_retained_products, nmm_other_manual_removal_placenta, nmm_other_blood_transfusion, nmm_other_laparotomy, nmm_other_admission_ICU, intervention_identified, referred_higher_complexity_hospital, status_hospital_discharge, discharge_transfer_death_date, admission_id, createdAt, updatedAt, userBoldId from maternalOutcomes where admission_id = ' + admission_id;        

                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var maternalOutcomes = results.rows.item(0);                    
                        resolve(maternalOutcomes)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }


}
