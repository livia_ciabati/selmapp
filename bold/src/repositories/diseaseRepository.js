import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase } from './BaseRepository';

export default class diseaseRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from disease', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertdisease(disease){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into disease (id , chronic_hypertension, diabetes_mellitus, hiv, aids_hiv, chronic_anaemia, obesity, heart_disease, lung_disease, renal_disease, other_chronic_disease, n_antenatal_care, neurologic_disease, liver_disease, cancer, pregnancy_id, createdAt, updatedAt, userBoldId)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id, disease.chronic_hypertension,disease.diabetes_mellitus,disease.hiv,disease.aids_hiv,disease.chronic_anaemia,disease.obesity,disease.heart_disease,disease.lung_disease,
                            disease.renal_disease,disease.other_chronic_disease,disease.n_antenatal_care,disease.neurologic_disease,disease.liver_disease,disease.cancer,disease.pregnancy_id,
                            getCurrentTimeToDatabase(),getCurrentTimeToDatabase(),disease.userBoldId], (tx, results) => {
                                disease.id = results.insertId;
                                resolve(disease);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updatedisease(disease){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update disease set chronic_hypertension = ?, diabetes_mellitus = ?, hiv = ?, aids_hiv = ?, chronic_anaemia = ?, obesity = ?, heart_disease = ?, lung_disease = ?, renal_disease = ?, other_chronic_disease = ?, n_antenatal_care = ?, neurologic_disease = ?, liver_disease = ?, cancer = ?, pregnancy_id = ?, updatedAt = ?, userBoldId = ? where id = ?', 
                        [ disease.chronic_hypertension,disease.diabetes_mellitus,disease.hiv,disease.aids_hiv,disease.chronic_anaemia,disease.obesity,disease.heart_disease,disease.lung_disease,
                            disease.renal_disease,disease.other_chronic_disease,disease.n_antenatal_care,disease.neurologic_disease,disease.liver_disease,disease.cancer,disease.pregnancy_id,
                            getCurrentTimeToDatabase(),disease.userBoldId,disease.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deletedisease(disease_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from disease where id = ?', [disease_id], (tx, results) => {
                        var disease = results.rows.item(0);
                        resolve(disease)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getDiseasesById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , chronic_hypertension, diabetes_mellitus, hiv, aids_hiv, chronic_anaemia, obesity, heart_disease, lung_disease, renal_disease, other_chronic_disease, n_antenatal_care, neurologic_disease, liver_disease, cancer, pregnancy_id, createdAt, updatedAt, userBoldId from disease where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var disease = results.rows.item(0);    
                        //console.log(JSON.stringify(disease))                
                        resolve(disease)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    
    static getDiseasesByWomanId(woman_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select d.id , chronic_hypertension, diabetes_mellitus, hiv, aids_hiv, chronic_anaemia, obesity, heart_disease, lung_disease, renal_disease, ' +
                        ' other_chronic_disease, n_antenatal_care, neurologic_disease, liver_disease, cancer, pregnancy_id, d.createdAt, d.updatedAt, d.userBoldId ' +
                        ' from disease d' +
                        ' join pregnancy p on d.pregnancy_id = p.id ' +
                        ' join woman     w on p.woman_id     = w.id ' +
                        ' where woman_id = ?';
                    tx.executeSql(query, 
                    [woman_id], (tx, results) => {
                        var disease = results.rows.item(0);    
                        //console.log(JSON.stringify(disease))                
                        resolve(disease)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAlldisease(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, chronic_hypertension, diabetes_mellitus, hiv, aids_hiv, chronic_anaemia, obesity, heart_disease, lung_disease, renal_disease, other_chronic_disease, n_antenatal_care, neurologic_disease, liver_disease, cancer, pregnancy_id, createdAt, updatedAt, userBoldId from disease  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
