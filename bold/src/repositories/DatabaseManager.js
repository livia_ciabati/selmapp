import RNFS from "react-native-fs";

let SQLite = require('react-native-sqlite-storage');

export default class DatabaseManager {

  database = undefined;

    databaseName = "selma.db";
    database_version = "1.0";
    database_displayname = "SQLite Test Database";
    database_size = 200000;

  // Open the connection to the database
  open() {
    SQLite.DEBUG(true);
    SQLite.enablePromise(true);
    
    return SQLite.openDatabase(this.databaseName, this.database_version, this.database_displayname, this.database_size)
      .then((db) => {
          //console.log("[db] Database open! " + JSON.stringify(db));
          this.database = db;
        return db;
      }).catch(function(e) {
        //console.log("Ah, não! " + e); // "Ah, não!"
      }); 
  }

  getDatabase(){
    if (this.database !== undefined) {
      ////console.log("CHAMA GET");
      return Promise.resolve(this.database);
    }
    ////console.log("CHAMA OPEN");
    // otherwise: open the database first
    Promise.resolve(this.open());
  }

  // Close the connection to the database
  close() {
    if (this.database === undefined) {
      return Promise.reject("[db] Database was not open; unable to close.");
    }
    return this.database.close().then(status => {
      //console.log("[db] Database closed.");
      this.database = undefined;
    });
  }
  
}

// Export a single instance of DatabaseImpl
export const database = DatabaseManager = new DatabaseManager();