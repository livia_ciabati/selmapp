//Classe de acesso ao banco
//Nao usa elementos do react, apenas recupera dados e repassa
//import {womenAdmissionListing, womenListing, womanRetrive} from '../actions/actionCreator';
import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase } from './BaseRepository';

export default class WomanRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from woman', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertWoman(woman){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) => {
                    db.transaction((tx) => {
                    //console.log("getNextId results " + JSON.stringify(next_id));
                    tx.executeSql('Insert into woman (id, name, enabled, modified_by_user_id, birthday, age, marital_status, education_level, gainful_occupation, createdAt, updatedAt, userBoldId)'+
                    ' values (?,?,?,?,?,?,?,?,?,?,?,?)', 
                        [next_id, woman.name, woman.enabled, woman.modified_by_user_id,
                            woman.birthday, woman.age, woman.marital_status, woman.education_level,
                            woman.gainful_occupation, getCurrentTimeToDatabase(), getCurrentTimeToDatabase(), 
                            woman.userBoldId], (tx, results) => {
                                woman.id = results.insertId;
                                ////console.log("insertWoman results" + JSON.stringify(results));
                                //console.log("insertWoman" + JSON.stringify(woman));
                                resolve(woman);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateWoman(woman){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update woman set name=?, enabled=?, modified_by_user_id=?, birthday=?, age=? , marital_status=?, education_level=?, gainful_occupation=?, updatedAt=?, userBoldId=? where id = ?', 
                        [woman.name, woman.enabled, woman.modified_by_user_id,
                            woman.birthday, woman.age, woman.marital_status, woman.education_level,
                            woman.gainful_occupation, getCurrentTimeToDatabase(), 
                            woman.userBoldId, woman.id], 
                        (tx, results) => {
                            //console.log("updateWoman " + JSON.stringify(results));
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteWoman(woman_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from woman where id = ?', [woman_id], (tx, results) => {
                        var woman = results.rows.item(0);
                        resolve(woman)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getWomanById(id){
        //console.log("GET WOMAN BY ID 1" + id);
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, name, enabled, modified_by_user_id, birthday, age, marital_status, education_level,' +
                    ' gainful_occupation, createdAt, updatedAt, userBoldId from woman where id = ?';        
                    tx.executeSql(query, 
                    [id], (tx, results) => {
                        //console.log("GET WOMAN BY ID");
                        var woman = results.rows.item(0);                    
                        resolve(woman)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllWoman(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, name, enabled, modified_by_user_id, birthday, age, marital_status, education_level, gainful_occupation, createdAt, updatedAt, userBoldId from woman  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static getWomanByAdmissionId(adm_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select w.id, w.name, w.enabled, w.modified_by_user_id, w.birthday, w.age, w.marital_status, w.education_level, '+
                    ' w.gainful_occupation, w.createdAt, w.updatedAt, w.userBoldId from woman w ' +                    
                    '   join pregnancy p on w.id = woman_id                         ' + 
                    '   join admission a on p.id = a.pregnancy_id                   ' + 
                    ' where a.id = ? ';        
                    tx.executeSql(query, 
                    [adm_id], (tx, results) => {
                        var woman = results.rows.item(0);                    
                        resolve(woman)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }
    static getAllWomanInActiveLabour(user_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql(
                        'select                                                                 ' + 
                        ' w.id as wom_id                                                        ' + 
                        ',w.name as wom_name                                                    ' + 
                        ',w.enabled as wom_enabled                                              ' + 
                        ',w.userBoldId as wom_userBoldId                                        ' + 
                        ',p.id as pre_id                                                        ' + 
                        ',p.gravidity as pre_gravidity                                          ' + 
                        ',p.parity as pre_parity                                                ' + 
                        ',p.num_previous_abort as pre_num_previous_abort                        ' + 
                        ',p.num_previous_c_section as pre_num_previous_c_section                ' + 
                        ',p.estimate_gestional_age as pre_estimate_gestional_age                ' + 
                        ',p.woman_id as pre_woman_id                                            ' + 
                        ',a.id as adm_id                                                        ' + 
                        ',anc.id as anc_id                                                      ' + 
                        ',disea.id as disea_id                                                  ' + 
                        ',admission_status as adm_admission_status                              ' + 
                        ',num_alerts, aas.schedule_time                                         ' + 
                        ', ctas.delayedForm                                                     ' +
                        '  from woman w                                                         ' + 
                        '   join pregnancy p on w.id = woman_id                                 ' + 
                        '   join admission a on p.id = a.pregnancy_id                           ' + 
                        '   join antenatalCare anc on p.id = anc.pregnancy_id                   ' + 
                        '   join disease disea on p.id = disea.pregnancy_id                     ' + 
                        ' left join (select a.id as admissionId, count(*) as num_alerts         ' + 
                        ' from admissionAlerts aa                                               ' + 
                        ' join admission a on aa.admission_id = a.id                            ' + 
                        ' where aa.readed= 0 and aa.snozzed = 0                                 ' + 
                        ' group by aa.admission_id) as aa on adm_id = aa.admissionId            ' + 
                        '  left join (select MIN(schedule_time) as schedule_time ,              ' +
                        '  admission_id from AssessmentSchedule where filled_in = 0             ' + 
                        '   group by admission_id )                                             ' + 
                        ' as aas on aas.admission_id = adm_id                                   ' + 
                        '  left join (select count(id) as delayedForm,                          ' +
                        '  admission_id from AssessmentSchedule where filled_in = 0             ' +
                        '  and schedule_time  < \'' +  getCurrentTimeToDatabase() + '\'' +
                        '  group by admission_id  ) as ctas on ctas.admission_id = adm_id       ' +                      
                        ' where w.userBoldId =  ? and a.admission_status = 1                    '                             
                        , [user_id], (tx, results) => {
                        var len = results.rows.length;
                        var data = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            data.push(row);
                        }
                        //console.log("GET ALL IN ACTIVE LABOUR " + JSON.stringify(data))
                        resolve(data);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    }
                )})
            });
        });
    }
    static getCmodelData(adm_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql(
                        ' select parity, num_previous_c_section, multiple_pregnancy, mode_onset_labour, estimate_gestional_age, age,  fetal_presentation ' +
                        ' from woman w ' +
                        ' join pregnancy p on w.id = p.woman_id ' +
                        ' join admission a on p.id = a.pregnancy_id ' +
                        ' join (select max(ar.createdAt), ao.value as fetal_presentation from assessmentResult ar ' +
                        ' join assessmentQuestions aq on ar.assessment_questions_id = aq.id ' +
                        ' join assessmentOptions ao on ar.assessment_options_id = ao.id ' +
                        ' where aq.name = \'fetal_presentation\' and ar.admission_id = ?) ' +
                        ' where a.id = ?                       ' 
                        , [adm_id, adm_id], (tx, results) => {
                            var len = results.rows.length;
                            if(len > 0)
                            {
                                let row = results.rows.item(0);
                                resolve(row);
                            }
                            ////console.log("GET ALL IN ACTIVE LABOUR " + JSON.stringify(data))
                            resolve({});
                        },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    }
                )})
            });
        });
    }
}