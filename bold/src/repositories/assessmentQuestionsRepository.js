import  { database }  from "./DatabaseManager";

export default class assessmentQuestionsRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from assessmentQuestions', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertassessmentQuestions(assessmentQuestions){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into assessmentQuestions (id , name, label_question, type, minimum_value, maximum_value)'+
                    ' values ( ?, ?, ?, ?, ?, ?)', 
                        [next_id , name, label_question, type, minimum_value, maximum_value], (tx, results) => {
                                assessmentQuestions.id = results.insertId;
                                resolve(assessmentQuestions);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateassessmentQuestions(assessmentQuestions){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentQuestions set   name = ?, label_question = ?, type = ?, minimum_value = ?, maximum_value = ? where id = ?', 
                        [ assessmentQuestions.name,assessmentQuestions.label_question,assessmentQuestions.type,assessmentQuestions.minimum_value,assessmentQuestions.maximum_value,assessmentQuestions.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteassessmentQuestions(assessmentQuestions_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from assessmentQuestions where id = ?', [assessmentQuestions_id], (tx, results) => {
                        var assessmentQuestions = results.rows.item(0);
                        resolve(assessmentQuestions)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getassessmentQuestionsById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , name, label_question, type, minimum_value, maximum_value from assessmentQuestions where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var assessmentQuestions = results.rows.item(0);                    
                        resolve(assessmentQuestions)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }
    
    static getassessmentQuestionsByName(name){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {         
                    var sql = 'Select aq.id as questionId, aq.name as questionName, aq.label_question, aq.type, aq.minimum_value, aq.maximum_value, '+
                            '    ao.id as optionId, ao.value, ao.label_reply                                                                      '+
                            '    from assessmentQuestions aq                                                                                      '+
                            '    left join questionsOptions qo on qo.assessment_questions_id = aq.id                                              '+
                            '    left join assessmentOptions ao on ao.id = qo.assessment_options_id                                               '+
                            '    where name = ? ';
                
                    tx.executeSql(sql, [name], 
                        (tx, results) => {
                            var assessmentQuestions = this.parseFormQuestionList(results);                            
                            resolve(assessmentQuestions);
                        },
                        (tx, err) => {
                           //console.warn("erro ta estourando aqui " + tx);
                        })
                    });
                });
            });
    }

    static getAllassessmentQuestions(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, name, label_question, type, minimum_value, maximum_value from assessmentQuestions  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAll(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('delete from assessmentQuestions;', [], (tx, results) => {
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }
    
    // static getAssessmentsQuestionById(id){
    //     return new Promise((resolve, reject) => {
    //         database.getDatabase().then(db => { 
    //             db.transaction(
    //                 (tx) => {
    //                     tx.executeSql(
    //                         'Select aq.id as questionId, aq.name as questionName, aq.label_question, aq.type, aq.minimum_value, aq.maximum_value, '+
    //                         '    ao.id as optionId, ao.value, ao.label_reply                                                                      '+
    //                         '    from assessmentQuestions aq                                                                                      '+
    //                         '    left join questionsOptions qo on qo.assessment_questions_id = aq.id                                              '+
    //                         '    left join assessmentOptions ao on ao.id = qo.assessment_options_id                                               '+
    //                         '    where questionId = ?                                                                                             ',
    //                         [ id ], 
    //                         (tx, results) => {   
    //                             var assessmentQuestions = results.rows.item(0);                            
    //                             resolve(assessmentQuestions);
    //                         },
    //                         (tx, err) => {
    //                            //console.warn(tx);
    //                         }
    //                     )
    //                 });
    //             });
    //     });
    // }
    
    static getAssessmentsQuestionById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                var where = ''
                if(id.toString().includes(",")){
                    var ids = id.split(",");
                    for(var i=0; i < ids.length; i++)
                    {
                        if(i == 0)
                            where = where + ' questionId = ' + ids[i];
                        else
                            where = where + ' or questionId = ' + ids[i];
                    }
                }
                else
                {
                    where = where + ' questionId = ' + id;                    
                }
                //console.log(id + " " + where);

                var sql = 'Select aq.id as questionId, aq.name as questionName, aq.label_question, aq.type, aq.minimum_value, aq.maximum_value, '+
                '    ao.id as optionId, ao.value, ao.label_reply                                                                      '+
                '    from assessmentQuestions aq                                                                                      '+
                '    left join questionsOptions qo on qo.assessment_questions_id = aq.id                                              '+
                '    left join assessmentOptions ao on ao.id = qo.assessment_options_id                                               '+
                '    where ' + where.toString();
                
                db.transaction( (tx) => {
                    tx.executeSql(sql, [], 
                        (tx, results) => {
                            var assessmentQuestions = this.parseFormQuestionList(results);                            
                            resolve(assessmentQuestions);
                        },
                        (tx, err) => {
                           //console.warn("erro ta estourando aqui " + tx);
                        })
                    });
                });
        });
    }

    
    static parseFormQuestionList(results){
        var len = results.rows.length;
        var data = [];
        let row = results.rows.item(0);
        
        var aux = {
            questionId: row.questionId, questionName: row.questionName, label_question: row.label_question, 
            type: row.type, minimum_value: row.minimum_value, maximum_value: row.maximum_value,
            options: [{ optionId: row.optionId, value: row.value, label_reply: row.label_reply }]
        }

        if(len == 1 )
        {
            data.push(aux);
        }

        for (let i = 1; i < len; i++)
        {
            while (i<len && aux.questionId == results.rows.item(i).questionId)
            {
                row = results.rows.item(i);                                    
                aux.options.push({ optionId: row.optionId, value: row.value, label_reply: row.label_reply });
                i++;
            }

            data.push(aux);

            if(i<len)
            {
                row = results.rows.item(i);
                aux = {
                    questionId: row.questionId, questionName: row.questionName, label_question: row.label_question, 
                    type: row.type, minimum_value: row.minimum_value, maximum_value: row.maximum_value,
                    options: [{ optionId: row.optionId, value: row.value, label_reply: row.label_reply }]
                }                                      
            }
            
            if(i+1 == len && aux.type != 3){
                data.push(aux);
            }                            
        }
        assessment = { questionList: data};

        return assessment;
                            
    }

}
