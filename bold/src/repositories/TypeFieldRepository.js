import  { database }  from "./DatabaseManager";

export default class TypeFieldRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from TypeField', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0){
                            max_id = results.rows.item(0).max_id + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertTypeField(TypeField){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into TypeField (id, id, type_name)'+
                    ' values (, ?, ?)', 
                        [next_id, id, type_name], (tx, results) => {
                                TypeField.id = results.insertId;
                                resolve(TypeField);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateTypeField(TypeField){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update TypeField set , id = ?, type_name = ? where id = ?', 
                        [TypeField.id,TypeField.type_name,TypeField.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteTypeField(TypeField_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from TypeField where id = ?', [TypeField_id], (tx, results) => {
                        var TypeField = results.rows.item(0);
                        resolve(TypeField)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getTypeFieldById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id, id, type_name from TypeField where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var TypeField = results.rows.item(0);                    
                        resolve(TypeField)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllTypeField(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, id, type_name from TypeField  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
