import  { database }  from "./DatabaseManager";

export default class AccessTokenRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from AccessToken', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertAccessToken(AccessToken){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into AccessToken (id , ttl, scopes, created, userId, token)'+
                    ' values ( ?, ?, ?, ?, ?, ?)', 
                        [next_id , ttl, scopes, created, userId, token], (tx, results) => {
                                AccessToken.id = results.insertId;
                                resolve(AccessToken);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateAccessToken(AccessToken){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update AccessToken set   ttl = ?, scopes = ?, created = ?, userId = ?, token = ? where id = ?', 
                        [ AccessToken.ttl,AccessToken.scopes,AccessToken.created,AccessToken.userId,AccessToken.token,AccessToken.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAccessToken(AccessToken_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from AccessToken where id = ?', [AccessToken_id], (tx, results) => {
                        var AccessToken = results.rows.item(0);
                        resolve(AccessToken)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAccessTokenById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , ttl, scopes, created, userId, token from AccessToken where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var AccessToken = results.rows.item(0);                    
                        resolve(AccessToken)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllAccessToken(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, ttl, scopes, created, userId, token from AccessToken  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
