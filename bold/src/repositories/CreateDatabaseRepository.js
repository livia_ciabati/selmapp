import { database } from "./DatabaseManager";
import assessmentsFormsRepository from "./assessmentsFormsRepository";
import assessmentQuestionsRepository from "./assessmentQuestionsRepository";
import assessmentOptionsRepository from "./assessmentOptionsRepository";
import questionsFormsRepository from "./questionsFormsRepository";
import questionsOptionsRepository from "./questionsOptionsRepository";
import alertRepository from "./alertRepository";
import admissionAlertsRepository from "./admissionAlertsRepository";

export default class CreateDatabaseRepository {

    static listTables(){
        return new Promise((resolve, reject) => {
            database.getDatabase()
            .then((db) => {        
                //console.log(db); 
                db.transaction((tx) => {
                    tx.executeSql('SELECT name FROM sqlite_master WHERE type=\'table\';', [], (tx, results) => {
                        //console.log("SHOW TABLES: " + JSON.stringify(results));                    
                        for (let i = 0; i < results.rows.length; i++) {
                            if (i < results.rows.length) {
                                //console.log("TABLE " + JSON.stringify(results.rows.item(i)));
                            }
                            else {
                                //console.log("BAD DATASET, NO TABLES FOR YOU.");
                            }
                        }
                        resolve( results.rows.length);           
                    },
                    (tx, err) => {
                        //console.log("listTables erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static createIfNotExistDatabase (){
        return new Promise((resolve, reject) => {
            database.getDatabase()
            .then((db) => {
                ////console.log("BANCO 2 " + JSON.stringify(db));
                ////console.log("QUERY 2 " + this.getCreateTables);
                db.transaction((tx) => {
                    ////console.log("TRANSACTION " + JSON.stringify(this.getCreateTables.woman));
                    tx.executeSql(this.getCreateTables.woman, [], (tx, results) => {
                        //console.log('CREATE DATABASE woman loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.interventionParameters, [], (tx, results) => {
                        //console.log('CREATE DATABASE interventionParameters loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.antenatalCare, [], (tx, results) => {
                        //console.log('CREATE DATABASE antenatalCare loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.disease, [], (tx, results) => {
                        //console.log('CREATE DATABASE disease loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.Role, [], (tx, results) => {
                        //console.log('CREATE DATABASE Role loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.curves, [], (tx, results) => {
                        //console.log('CREATE DATABASE curves loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.neonatalOutcomes, [], (tx, results) => {
                        //console.log('CREATE DATABASE neonatalOutcomes loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.Rules, [], (tx, results) => {
                        //console.log('CREATE DATABASE Rules loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.assessmentQuestions, [], (tx, results) => {
                        //console.log('CREATE DATABASE assessmentQuestions loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.admissionAlerts, [], (tx, results) => {
                        //console.log('CREATE DATABASE admissionAlerts loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.hospital, [], (tx, results) => {
                        //console.log('CREATE DATABASE hospital loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.assessmentResult, [], (tx, results) => {
                        //console.log('CREATE DATABASE assessmentResult loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.AssessmentSchedule, [], (tx, results) => {
                        //console.log('CREATE DATABASE AssessmentSchedule loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.admission, [], (tx, results) => {
                        //console.log('CREATE DATABASE admission loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.questionsForms, [], (tx, results) => {
                        //console.log('CREATE DATABASE questionsForms loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.alert, [], (tx, results) => {
                        //console.log('CREATE DATABASE alert loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.maternalOutcomes, [], (tx, results) => {
                        //console.log('CREATE DATABASE maternalOutcomes loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.maternalOutcomes, [], (tx, results) => {
                        //console.log('CREATE DATABASE maternalOutcomes loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.ACL, [], (tx, results) => {
                        //console.log('CREATE DATABASE ACL loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.questionsOptions, [], (tx, results) => {
                        //console.log('CREATE DATABASE questionsOptions loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.bold_initial_data, [], (tx, results) => {
                        //console.log('CREATE DATABASE bold_initial_data loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.assessmentOptions, [], (tx, results) => {
                        //console.log('CREATE DATABASE assessmentOptions loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.UserBold, [], (tx, results) => {
                        //console.log('CREATE DATABASE UserBold loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.pregnancy, [], (tx, results) => {
                        //console.log('CREATE DATABASE pregnancy loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.assessmentsForms, [], (tx, results) => {
                        //console.log('CREATE DATABASE assessmentsForms loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.assessmentAnswerInfo, [], (tx, results) => {
                        //console.log('CREATE DATABASE assessmentAnswerInfo loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    
                    tx.executeSql(this.getCreateTables.AccessToken, [], (tx, results) => {
                        //console.log('CREATE DATABASE AccessToken loaded ' + JSON.stringify(results));                
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });
                    tx.executeSql(this.getCreateTables.RoleMapping, [], (tx, results) => {
                        //console.log('CREATE DATABASE RoleMapping loaded ' + JSON.stringify(results));    
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });

                    tx.executeSql(this.getCreateTables.Settings, [], (tx, results) => {
                        //console.log('CREATE DATABASE Settings loaded ' + JSON.stringify(results));    
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });


                    tx.executeSql(this.getCreateTables.TypeFields, [], (tx, results) => {
                        //console.log('CREATE DATABASE TypeFields loaded ' + JSON.stringify(results));    
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });

                    tx.executeSql(this.getCreateTables.diagnosis, [], (tx, results) => {
                        //console.log('CREATE DATABASE diagnosis loaded ' + JSON.stringify(results));    
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });

                    tx.executeSql(this.getCreateTables.admissionDiagnosis, [], (tx, results) => {
                        //console.log('CREATE DATABASE TypeFields admissionDiagnosis ' + JSON.stringify(results));    
                    },
                    (tx, err) => {
                        //console.log("0 erro: " + JSON.stringify(err));
                        reject(err);
                    });

                    //console.log('fim dos CREATES');
                }).then(() =>{
                    //console.log("BEFORE RESOLVE"); 
                    resolve();
                }).catch((error) =>{ 
                    //console.log("0 erro: " + JSON.stringify(error)); 
                });
            })
        })
    }

    static loadDataToDatabase() {
        ////console.log("BANCO 1 " + db);
        return new Promise((resolve, reject) => {
            database.getDatabase()
            .then(db => {
                assessmentsFormsRepository.getNextId()
                .then((next_id) => {
                    ////console.log("BANCO 1 " + next_id);
                    if (next_id == 1) {
                        db.transaction((tx) => {
                            tx.executeSql(this.getInserts.assessmentsForms, [], (tx, results) => {
                                //console.log('INITIALDATAASSESSMENTSFORMS loaded');                
                            },
                            (tx, err) => {
                                //console.log("1 erro: " + JSON.stringify(err));
                                reject(err);
                            });
                        })
                    }
                }).catch(function(error) {
                    //console.log('There has been a problem with your fetch operation: ' + error.message);
                        // ADD THIS THROW error
                        throw error;
                });
                return (db);
            })
            .then(db => {
                assessmentQuestionsRepository.getNextId()
                .then((next_id) => {
                    if (next_id == 1) {
                        db.transaction((tx) => {
                            tx.executeSql(this.getInserts.assessmentQuestions, [], (tx, results) => {
                                //console.log('initialDataAssessmentQuestions loaded');                
                            },
                            (tx, err) => {
                                //console.log("2 erro: " + JSON.stringify(err));
                                reject(err);
                            });
                        })
                    }
                }).catch(function(error) {
                    //console.log('There has been a problem with your fetch operation: ' + error.message);
                        // ADD THIS THROW error
                        throw error;
                });
                return (db);
            })
            .then(db => {
                assessmentOptionsRepository.getNextId()
                .then((next_id) => {
                    if (next_id == 1) {
                        db.transaction((tx) => {
                            tx.executeSql(this.getInserts.assessmentOptions, [], (tx, results) => {
                                //console.log('initialDataAssessmentOptions loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("3 erro: " + JSON.stringify(err));
                                reject(err);
                            });
                        })
                    }
                }).catch(function(error) {
                    //console.log('There has been a problem with your fetch operation: ' + error.message);
                        // ADD THIS THROW error
                        throw error;
                });
                return (db);
            })
            .then(db => {
                questionsFormsRepository.getNextId()
                .then((next_id) => {
                    if (next_id == 1) {
                        db.transaction((tx) => {
                            tx.executeSql(this.getInserts.questionsForms, [], (tx, results) => {
                                //console.log('initialDataQuestionsForms loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("4 erro: " + JSON.stringify(err));
                                reject(err);
                            });
                        })
                    }
                }).catch(function(error) {
                    //console.log('There has been a problem with your fetch operation: ' + error.message);
                        // ADD THIS THROW error
                        throw error;
                });
                return (db);
            })
            .then(db => {
                questionsOptionsRepository.getNextId()
                .then((next_id) => {
                    if (next_id == 1) {
                        db.transaction((tx) => {
                            tx.executeSql(this.getInserts.questionsOptions, [], (tx, results) => {
                                //console.log('initialDataQuestionsOptions loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        })
                    }
                }).catch(function(error) {
                    //console.log('There has been a problem with your fetch operation: ' + error.message);
                        // ADD THIS THROW error
                        throw error;
                });
                return (db);
            })
            .then(db => {
                alertRepository.getNextId()
                .then((next_id) => {
                    if (next_id == 1) {
                        db.transaction((tx) => {
                            //console.log('initialDataAlert loaded ' + this.getInserts.alert1);
                            tx.executeSql(this.getInserts.alert1, [], (tx, results) => {
                                //console.log('initialDataAlert loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        db.transaction((tx) => {
                            //console.log('initialDataAlert loaded ' + this.getInserts.alert2);
                            tx.executeSql(this.getInserts.alert2, [], (tx, results) => {
                                //console.log('initialDataAlert loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        db.transaction((tx) => {
                            //console.log('initialDataAlert loaded ' + this.getInserts.alert3);
                            tx.executeSql(this.getInserts.alert3, [], (tx, results) => {
                                //console.log('initialDataAlert loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        db.transaction((tx) => {
                            //console.log('initialDataAlert loaded ' + this.getInserts.alert4);
                            tx.executeSql(this.getInserts.alert4, [], (tx, results) => {
                                //console.log('initialDataAlert loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        db.transaction((tx) => {
                            //console.log('initialDataAlert loaded ' + this.getInserts.alert5);
                            tx.executeSql(this.getInserts.alert5, [], (tx, results) => {
                                //console.log('initialDataAlert loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        
                        db.transaction((tx) => {
                            //console.log('initial loaded ' + this.getInserts.typeFields);
                            tx.executeSql(this.getInserts.typeFields, [], (tx, results) => {
                                //console.log('initialTypeFieldsLoaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        db.transaction((tx) => {
                            //console.log('initial loaded ' + this.getInserts.settings);
                            tx.executeSql(this.getInserts.settings, [], (tx, results) => {
                                //console.log('initialSettings Loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                        
                        db.transaction((tx) => {
                            //console.log('initial loaded ' + this.getInserts.diagnosis);
                            tx.executeSql(this.getInserts.diagnosis, [], (tx, results) => {
                                //console.log('initialDiagnosis Loaded');
                                resolve();                   
                            },
                            (tx, err) => {
                                //console.log("5 erro: " + JSON.stringify(err));
                                reject(err);     
                            });
                        });
                    }
                }).catch(function(error) {
                    //console.log('There has been a problem with your loaded operation: ' + error.message);
                        throw error;
                });
                return (db);
            })
        })
    };
    
    static getCreateTables = {
        woman : " CREATE TABLE IF NOT EXISTS `woman` (                                    "+
        "    `id` INT,                                                                    "+
        "    `name` TEXT,                                                                 "+
        "    `enabled` TEXT,                                                              "+
        "    `modified_by_user_id` INT,                                                   "+
        "    `birthday` TEXT,                                                             "+
        "    `age` INT,                                                                   "+
        "    `marital_status` TEXT,                                                       "+
        "    `education_level` TEXT,                                                      "+
        "    `gainful_occupation` TEXT,                                                   "+
        "    `createdAt` datetime,                                                        "+
        "    `updatedAt` datetime,                                                        "+
        "    `userBoldId` INT                                                             "+
        " );                                                                              "
        ,
        questionsOptions: " CREATE TABLE IF NOT EXISTS `questionsOptions` (               " +
        "    `assessment_questions_id` INT,                                               " +
        "    `assessment_options_id` INT,                                                 " +
        "    `id` INT                                                                     " +
        " );                                                                              " 
        ,
        questionsForms: " CREATE TABLE IF NOT EXISTS `questionsForms` (                    " +
        "    `assessment_questions_id` INT,                                                " +
        "    `assessments_forms_id` INT,                                                   " +
        "    `order_question_number` TEXT,                                                 " +
        "    `id` INT                                                                      " +
        " );                                                                               " 
        ,
        pregnancy: " CREATE TABLE IF NOT EXISTS `pregnancy` (                              " +
        "    `id` INT,                                                                     " +
        "    `gravidity` TEXT,                                                             " +
        "    `parity` TEXT,                                                                " +
        "    `num_previous_abort` TEXT,                                                    " +
        "    `n_previous_induced_abort` TEXT,                                              " +
        "    `n_previous_spontaneous_abort` TEXT,                                          " +
        "    `num_previous_c_section` TEXT,                                                " +
        "    `previous_uterine_surgery` TEXT,                                              " +
        "    `n_previous_stillbirths` TEXT,                                                " +
        "    `outcome_last_childbirth` TEXT,                                               " +
        "    `childbirth_last_delivery` TEXT,                                              " +
        "    `referred` TEXT,                                                              " +
        "    `estimate_gestional_age` TEXT,                                                " +
        "    `estimate_gestional_age_method` TEXT,                                         " +
        "    `date_of_last_mensturation` TEXT,                                             " +
        "    `expected_delivery_date` TEXT,                                                " +
        "    `woman_id` INT,                                                               " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `userBoldId` INT                                                              " +
        " );                                                                               " 
        ,
        neonatalOutcomes: " CREATE TABLE IF NOT EXISTS `neonatalOutcomes` (                " +
        "    `id` INT,                                                                     " +
        "    `date_delivery` datetime,                                                     " +
        "    `time_delivery` datetime,                                                     " +
        "    `final_mode_delivery` TEXT,                                                   " +
        "    `fetal_presentation_delivery` TEXT,                                           " +
        "    `infant_sex` TEXT,                                                            " +
        "    `birth_weight` TEXT,                                                          " +
        "    `condition_vital_status` TEXT,                                                " +
        "    `condition_apgar_5min` TEXT,                                                  " +
        "    `morbidity_admission_ICU` TEXT,                                               " +
        "    `morbidity_intubation` TEXT,                                                  " +
        "    `morbidity_nasal_CPAP` TEXT,                                                  " +
        "    `morbidity_surfactant_administration` TEXT,                                   " +
        "    `morbidity_cardio_pulmonary_resuscitation` TEXT,                              " +
        "    `morbidity_use_vasoactive_drug` TEXT,                                         " +
        "    `morbidity_use_anticonvulsants` TEXT,                                         " +
        "    `morbidity_use_phototerapy_first_24hours` TEXT,                               " +
        "    `morbidity_any_blood_products` TEXT,                                          " +
        "    `morbidity_use_steroids` TEXT,                                                " +
        "    `morbidity_therapeutic_intravenous_antibiotics` TEXT,                         " +
        "    `morbidity_any_surgery` TEXT,                                                 " +
        "    `morbidity_any_severe_malformation` TEXT,                                     " +
        "    `status_hospital_discharge` TEXT,                                             " +
        "    `discharge_transfer_death_date` TEXT,                                         " +
        "    `discharge_transfer_death_hour` TEXT,                                         " +
        "    `admission_id` INT,                                                          " +
        "    `createdAt` datetime,                                                             " +
        "    `updatedAt` datetime,                                                             " +
        "    `userBoldId` INT                                                             " +
        " );                                                                               " 
        ,
        maternalOutcomes: " CREATE TABLE IF NOT EXISTS `maternalOutcomes` (                " +
        "    `id` INT,                                                                    " +
        "    `dystocia_labour_obstruction` TEXT,                                           " +
        "    `dystocia_first_stage_labour` TEXT,                                           " +
        "    `dystocia_prolonged_second_stage` TEXT,                                       " +
        "    `haemorrhage_placenta_praevia` TEXT,                                          " +
        "    `haemorrhage_accreta_increta_percreta_placenta` TEXT,                         " +
        "    `haemorrhage_abruptio_placenta` TEXT,                                         " +
        "    `haemorrhage_ruptured_uterus` TEXT,                                           " +
        "    `haemorrhage_intrapartum` TEXT,                                               " +
        "    `haemorrhage_postpartum` TEXT,                                                " +
        "    `infection_puerperal_endometritis` TEXT,                                      " +
        "    `infection_puerperal_sepsis` TEXT,                                            " +
        "    `infection_wound` TEXT,                                                       " +
        "    `infection_systemic_septicaemia` TEXT,                                        " +
        "    `hypertension_severe` TEXT,                                                   " +
        "    `hypertension_pre_eclampsia` TEXT,                                            " +
        "    `hypertension_eclampsia` TEXT,                                                " +
        "    `other_embolic_disease` TEXT,                                                 " +
        "    `otherPotentiallyLifeThreateningCondition` TEXT,                              " +
        "    `conditions_during_hospital_stay` TEXT,                                       " +
        "    `cardiovascular_dysfunction` TEXT,                                            " +
        "    `respiratory_dysfunction` TEXT,                                               " +
        "    `renal_dysfunction` TEXT,                                                     " +
        "    `coagulation_dysfunction` TEXT,                                               " +
        "    `hepatic_dysfunction` TEXT,                                                   " +
        "    `neurologic_dysfunction` TEXT,                                                " +
        "    `uterine_dysfunction` TEXT,                                                   " +
        "    `dysfunction_identified` TEXT,                                                " +
        "    `nmm_haemorrhage_oxytocin_treatment_PPH` TEXT,                                " +
        "    `nmm_haemorrhage_misoprostol_treatment_PPH` TEXT,                             " +
        "    `nmm_haemorrhage_ergotamine_treatment_PPH` TEXT,                              " +
        "    `nmm_haemorrhage_other_uterotonic_treatment_PPH` TEXT,                        " +
        "    `nmm_haemorrhage_artery_ligation_or_embolization` TEXT,                       " +
        "    `nmm_haemorrhage_balloon_or_condom_tamponade` TEXT,                           " +
        "    `nmm_haemorrhage_repair_cervical_laceration` TEXT,                            " +
        "    `nmm_haemorrhage_repair_uterine_rupture` TEXT,                                " +
        "    `nmm_haemorrhage_b_lynch_suture` TEXT,                                        " +
        "    `nmm_haemorrhage_hysterectomy` TEXT,                                          " +
        "    `nmm_infection_therapeutic_antibiotics` TEXT,                                 " +
        "    `nmm_hypertension_magnesium_sulphate_as_anticonvulsant` TEXT,                 " +
        "    `nmm_hypertension_other_anticonvulsant` TEXT,                                 " +
        "    `nmm_other_removal_retained_products` TEXT,                                   " +
        "    `nmm_other_manual_removal_placenta` TEXT,                                     " +
        "    `nmm_other_blood_transfusion` TEXT,                                           " +
        "    `nmm_other_laparotomy` TEXT,                                                  " +
        "    `nmm_other_admission_ICU` TEXT,                                               " +
        "    `intervention_identified` TEXT,                                               " +
        "    `referred_higher_complexity_hospital` TEXT,                                   " +
        "    `status_hospital_discharge` TEXT,                                             " +
        "    `discharge_transfer_death_date` TEXT,                                         " +
        "    `admission_id` INT,                                                           " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `userBoldId` INT                                                              " +
        " );                                                                               " 
        ,
        interventionParameters: " CREATE TABLE IF NOT EXISTS `interventionParameters` (    " +
        "    `id` INT,                                                                    " +
        "    `name` TEXT,                                                                  " +
        "    `type` TEXT,                                                                  " +
        "    `assessment_questions_id` INT                                                " +
        " );                                                                               " 
        ,
        hospital: " CREATE TABLE IF NOT EXISTS `hospital` (                                " +
        "    `id` INT,                                                                    " +
        "    `name` TEXT,                                                                  " +
        "    `site` TEXT,                                                                  " +
        "    `telephone` TEXT,                                                             " +
        "    `address` TEXT,                                                               " +
        "    `county` TEXT,                                                                " +
        "    `city` TEXT,                                                                  " +
        "    `state` TEXT,                                                                 " +
        "    `country` TEXT,                                                               " +
        "    `zip_code` TEXT,                                                              " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `number` TEXT,                                                                " +
        "    `userBoldId` INT                                                              " +
        " );                                                                               " 
        ,
        disease: " CREATE TABLE IF NOT EXISTS `disease` (                                  " +
        "    `id` INT,                                                                    " +
        "    `chronic_hypertension` TEXT,                                                  " +
        "    `diabetes_mellitus` TEXT,                                                     " +
        "    `hiv` TEXT,                                                                   " +
        "    `aids_hiv` TEXT,                                                              " +
        "    `chronic_anaemia` TEXT,                                                       " +
        "    `obesity` TEXT,                                                               " +
        "    `heart_disease` TEXT,                                                         " +
        "    `lung_disease` TEXT,                                                          " +
        "    `renal_disease` TEXT,                                                         " +
        "    `other_chronic_disease` TEXT,                                                 " +
        "    `n_antenatal_care` TEXT,                                                      " +
        "    `neurologic_disease` TEXT,                                                    " +
        "    `liver_disease` TEXT,                                                         " +
        "    `cancer` TEXT,                                                                " +
        "    `pregnancy_id` INT,                                                          " +
        "    `createdAt` datetime,                                                             " +
        "    `updatedAt` datetime,                                                             " +
        "    `userBoldId` INT                                                             " +
        " );                                                                               " 
        ,
        curves: " CREATE TABLE IF NOT EXISTS `curves` (                                    " +
        "    `0` TEXT,                                                                     " +
        "    `1` TEXT,                                                                     " +
        "    `2` TEXT,                                                                     " +
        "    `3` TEXT,                                                                     " +
        "    `4` TEXT,                                                                     " +
        "    `5` TEXT,                                                                     " +
        "    `6` TEXT,                                                                     " +
        "    `7` TEXT,                                                                     " +
        "    `8` TEXT,                                                                     " +
        "    `9` TEXT,                                                                     " +
        "    `10` TEXT,                                                                    " +
        "    `11` TEXT,                                                                    " +
        "    `12` TEXT,                                                                    " +
        "    `13` TEXT,                                                                    " +
        "    `14` TEXT,                                                                    " +
        "    `15` TEXT,                                                                    " +
        "    `16` TEXT,                                                                    " +
        "    `17` TEXT,                                                                    " +
        "    `18` TEXT,                                                                    " +
        "    `19` TEXT,                                                                    " +
        "    `20` TEXT,                                                                    " +
        "    `21` TEXT,                                                                    " +
        "    `22` TEXT,                                                                    " +
        "    `23` TEXT,                                                                    " +
        "    `24` TEXT,                                                                    " +
        "    `25` TEXT,                                                                    " +
        "    `26` TEXT,                                                                    " +
        "    `27` TEXT,                                                                    " +
        "    `28` TEXT,                                                                    " +
        "    `29` TEXT,                                                                    " +
        "    `30` TEXT,                                                                    " +
        "    `group` TEXT,                                                                 " +
        "    `intervention` TEXT,                                                          " +
        "    `curve` TEXT,                                                                 " +
        "    `start` TEXT                                                                  " +
        " );                                                                               " 
        ,
        bold_initial_data: " CREATE TABLE IF NOT EXISTS `bold_initial_data` (              " +
        "    `id` INT,                                                                    " +
        "    `augmentation_labour` TEXT,                                                   " +
        "    `education_level` TEXT,                                                       " +
        "    `axillary_temperature` TEXT,                                                  " +
        "    `c18_temperature_c` TEXT,                                                     " +
        "    `cervical_dilatation_cm` TEXT,                                                " +
        "    `c18_cervical_dilatation` TEXT,                                               " +
        "    `fetal_heart_rate_bpm` TEXT,                                                  " +
        "    `c18_fetal_heart_rate` TEXT,                                                  " +
        "    `maternal_heart_rate` TEXT,                                                   " +
        "    `c18_maternal_heart_rate` TEXT,                                               " +
        "    `num_antenatal_care_visits` TEXT,                                             " +
        "    `systolic_blood_pressure` TEXT,                                               " +
        "    `c18_syst_blood_pressure` TEXT,                                               " +
        "    `caesarean_section_cs` TEXT,                                                  " +
        "    `outcome` TEXT,                                                               " +
        "    `robson` TEXT                                                                 " +
        " );                                                                               " 
    ,
    assessmentsForms: " CREATE TABLE IF NOT EXISTS `assessmentsForms` (                    " +
        "    `id` INT,                                                                     " +
        "    `name` TEXT,                                                                  " +
        "    `label_form` TEXT,                                                            " +
        "    `version` TEXT,                                                               " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `priority` INT                                                                " +
        " );                                                                               " 
    ,
    assessmentAnswerInfo: " CREATE TABLE IF NOT EXISTS `assessmentAnswerInfo` (            " +
        "    `id` INT,                                                                     " +
        "    `begin_fill_form` DATETIME NULL,                                              " +
        "    `end_fill_form` DATETIME NULL,                                                " +
        "    `assessment_form_id` INT NULL,                                                " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `admission_id` INT                                                            " +
        "  );                                                                              " 
    ,
    assessmentResult:" CREATE TABLE IF NOT EXISTS `assessmentResult` (                    " +
        "    `id` INT,                                                                    " +
        "    `answer` TEXT,                                                               " +
        "    `admission_id` INT,                                                          " +
        "    `assessment_questions_id` INT,                                               " +
        "    `assessment_options_id` INT,                                                 " +
        "    `createdAt` datetime,                                                        " +
        "    `updatedAt` datetime,                                                        " +
        "    `forms_id` INT,                                                              " +
        "    `userBoldId` INT,                                                             " +
        "    `assessment_answer_info_id` INT                                              " +
        " );                                                                              " 
    ,
    assessmentQuestions:" CREATE TABLE IF NOT EXISTS `assessmentQuestions` (               " +
        "    `id` INT,                                                                    " +
        "    `name` TEXT,                                                                  " +
        "    `label_question` TEXT,                                                        " +
        "    `type` TEXT,                                                                  " +
        "    `minimum_value` TEXT,                                                         " +
        "    `maximum_value` TEXT                                                          " +
        " );                                                                               " 
    ,
    assessmentOptions:" CREATE TABLE IF NOT EXISTS `assessmentOptions` (                   " +
        "    `id` INT,                                                                    " +
        "    `value` TEXT,                                                                 " +
        "    `label_reply` TEXT                                                            " +
        " );                                                                               " 
    ,
    antenatalCare:" CREATE TABLE IF NOT EXISTS `antenatalCare` (                           " +
        "    `id` INT,                                                                    " +
        "    `weight` TEXT,                                                                " +
        "    `height` TEXT,                                                                " +
        "    `placenta_praevia` TEXT,                                                      " +
        "    `accreta_increta_percreta` TEXT,                                              " +
        "    `abruptio_placentae` TEXT,                                                    " +
        "    `other_obstetric_haemorrhage` TEXT,                                           " +
        "    `pre_eclampsia` TEXT,                                                         " +
        "    `eclampsia` TEXT,                                                             " +
        "    `pyelonephritis` TEXT,                                                        " +
        "    `malaria` TEXT,                                                               " +
        "    `preterm_rupture_membranes` TEXT,                                             " +
        "    `anaemia` TEXT,                                                               " +
        "    `gestational_diabetes` TEXT,                                                  " +
        "    `other_pregnancy_complications` TEXT,                                         " +
        "    `pregnancy_id` INT,                                                           " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `userBoldId` INT                                                              " +
        " );                                                                               " 
    ,
    alert: " CREATE TABLE IF NOT EXISTS `alert` (                                          " +
        "    `id` INT,                                                                     " +
        "    `title` TEXT,                                                                 " +
        "    `text` TEXT,                                                                  " +
        "    `link` TEXT,                                                                  " +
        "    `icon_name` TEXT,                                                             " +
        "    `risk_level` TEXT                                                             " +
        " );                                                                               " 
    ,
    admissionAlerts:" CREATE TABLE IF NOT EXISTS `admissionAlerts` (                                    " +
        "    `id` INT,                                                                    " +
        "    `readed` INT,                                                                " +
        "    `snozzed` INT,                                                               " +
        "    `alert_id` INT,                                                              " +
        "    `admission_id` INT,                                                          " +
        "    `createdAt` datetime,                                                        " +
        "    `updatedAt` datetime,                                                        " +
        "    `readedAt` datetime,                                                         " +
        "    `snozzedAt` datetime                                                         " +
        " );                                                                              " 
    ,
    admission: " CREATE TABLE IF NOT EXISTS `admission` (                                  " +
        "    `id` INT,                                                                     " +
        "    `mode_onset_labour` TEXT,                                                     " +
        "    `hours_labour_before_admission` TEXT,                                         " +
        "    `fetal_movements_last_2_hours` TEXT,                                          " +
        "    `first_stage_labour` TEXT,                                                    " +
        "    `fetal_death` TEXT,                                                           " +
        "    `advanced_first_stage_labour` TEXT,                                           " +
        "    `multiple_pregnancy` TEXT,                                                    " +
        "    `gestational_age_less_34_week` TEXT,                                          " +
        "    `elective_c_section` TEXT,                                                    " +
        "    `pre_labour_c_section` TEXT,                                                  " +
        "    `emergency_c_section_or_laparotomy` TEXT,                                     " +
        "    `attemped_labour_induction` TEXT,                                             " +
        "    `false_labour` TEXT,                                                          " +
        "    `non_emancipated_minors_without_guardian` TEXT,                               " +
        "    `unable_give_consent_or_health_problem` TEXT,                                 " +
        "    `elegible` TEXT,                                                              " +
        "    `symphysis_fundal_height` TEXT,                                               " +
        "    `abdominal_circumference` TEXT,                                               " +
        "    `pregnancy_id` INT,                                                           " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `userBoldId` INT,                                                             " +
        "    `admission_status` TEXT                                                       " +
        " );                                                                               " 
    ,
    UserBold: " CREATE TABLE IF NOT EXISTS `UserBold` (                                          " +
        "    `id` INT,                                                                    " +
        "    `realm` TEXT,                                                                 " +
        "    `username` TEXT,                                                              " +
        "    `password` TEXT,                                                              " +
        "    `email` TEXT,                                                                 " +
        "    `emailVerified` TEXT,                                                         " +
        "    `verificationToken` TEXT,                                                     " +
        "    `createdAt` datetime,                                                             " +
        "    `updatedAt` datetime                                                              " +
        " );                                                                               " 
    ,
    Rules: " CREATE TABLE IF NOT EXISTS `Rules` (                                              " +
        "    `id` INT,                                                                     " +
        "    `name` TEXT,                                                                  " +
        "    `priority` TEXT,                                                              " +
        "    `on` TEXT,                                                                    " +
        "    `condition` TEXT,                                                             " +
        "    `consequence` TEXT                                                            " +
        " );                                                                               " 
    ,
    RoleMapping: " CREATE TABLE IF NOT EXISTS `RoleMapping` (                                        " +
        "    `id` INT,                                                                     " +
        "    `principalType` TEXT,                                                         " +
        "    `principalid` INT,                                                            " +
        "    `roleid` INT                                                                  " +
        " );                                                                               " 
    ,
    Role: " CREATE TABLE IF NOT EXISTS `Role` (                                            " +
        "    `id` INT,                                                                     " +
        "    `name` TEXT,                                                                  " +
        "    `description` TEXT,                                                           " +
        "    `created` TEXT,                                                               " +
        "    `modified` TEXT                                                               " +
        " );                                                                               " 
    ,
    AssessmentSchedule: " CREATE TABLE IF NOT EXISTS `AssessmentSchedule` (                " +
        "    `id` INT,                                                                     " +
        "    `createdAt` datetime,                                                         " +
        "    `updatedAt` datetime,                                                         " +
        "    `schedule_time` TEXT,                                                         " +
        "    `assessments_forms_id` int,                                                   " +
        "    `filled_in` int,                                                              " +
        "    `admission_id` INT                                                            " +
        " );                                                                               " 
    ,
    AccessToken: " CREATE TABLE IF NOT EXISTS `AccessToken` (                              " +
        "    `id` INT,                                                                     " +
        "    `ttl` TEXT,                                                                   " +
        "    `scopes` TEXT,                                                                " +
        "    `created` TEXT,                                                               " +
        "    `userid` INT,                                                                " +
        "    `token` TEXT                                                                  " +
        " );                                                                               " 
    ,
    ACL: " CREATE TABLE IF NOT EXISTS `ACL` (                                              " +
        "    `id` INT,                                                                     " +
        "    `model` TEXT,                                                                 " +
        "    `property` TEXT,                                                              " +
        "    `accessType` TEXT,                                                            " +
        "    `permission` TEXT,                                                            " +
        "    `principalType` TEXT,                                                         " +
        "    `principalid` INT                                                            " +
        " );                                                                               " 
    ,
    TypeFields: " CREATE TABLE IF NOT EXISTS `TypeField` (                             " +
                "    `id` INT,                                                         " +
                "    `type_name` TEXT                                                 " +
                " );      " 
    ,
    Settings: " CREATE TABLE IF NOT EXISTS `Settings` (                           " +
                "    `id` INT,                                                    " +
                "    `name` TEXT,                                                 " +
                "    `label` TEXT,                                                 " +
                "    `value` TEXT                                                 " +
                " );      "
                ,
                
        diagnosis: " CREATE TABLE IF NOT EXISTS `diagnosis` (                            " +
        "    `id` INT,                                                                   " +
        "    `name` TEXT,                                                                " +
        "    `label` TEXT,                                                                " +
        "    `risk_level` INT                                                                " +
        " );                                                                             " 
        ,
        admissionDiagnosis:" CREATE TABLE IF NOT EXISTS `admissionDiagnosis` (                " +
            "    `id` INT,                                                                    " +
            "    `diagnosis_id` INT,                                                          " +
            "    `admission_id` INT,                                                          " +
            "    `value` TEXT,                                                                " +
            "    `createdAt` datetime,                                                        " +
            "    `updatedAt` datetime                                                         " +
            " );                                                                              " 
    }

    static getInserts = {
        assessmentsForms: "INSERT INTO `assessmentsForms` (`id`, `name`, `label_form`, `version`, `createdAt`, `updatedAt`, `priority`) VALUES " + 
        "(1   ,'first_assessment','First Assessment','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  0),   " +
        "(2   ,'intrapartum_data','Intrapartum Data','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " +
        "(3   ,'interventions_form','Interventions Form','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " +
        "(4   ,'maternal_dashboard','Maternal Dashboard','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " +
        "(5   ,'fetal_dashboard','Neo Natal Dashboard','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " +
        "(6   ,'intervention_dashboard','Interventions Dashboard','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " + 
        "(7   ,'revaluate_latent_phase','Revaluate Latent Phase','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " + 
        "(8   ,'maternal_vitals','Maternal Vitals','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " + 
        "(9   ,'continuous_monitoring','Continuous Monitoring','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),   " + 
        "(10  ,'causes_tachcardia_maternal_vitals','Causes of Tachycardia','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(11  ,'severe_pe','Severe pre-eclampsia','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(12  ,'hypertension_blood_sample_results','Blood test results - Hypertension','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(13  ,'full_assessment','Full assessment','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(14  ,'continuous_fhr_monitoring','Continuous monitoring for fetal heart rate','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(15  ,'fetal_scalp_ph_lactate','Fetal scalpe or pH lactate result','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(16  ,'abnormal_fetal_heart_rate','Abnormal fetal heart rate revaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(17  ,'high_temperature','High temperature','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(18  ,'investigate_infection_causes','Infections causes','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(19  ,'blood_test_for_sepsis','Blood test results - SEPSIS','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(20  ,'all_vitals','Evaluated all vitals','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(21  ,'venous_plasma_glucose','Venous plasma glucose result','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(22  ,'capillary_glucose','Capillary glucose evaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(23  ,'dehydration_form','Dehydration form','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(24  ,'oliguria_suspicion','Oliguria suspicion evaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(25  ,'urine_form','Urine evaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(26  ,'pre_eclapmsia_symptoms','Pre-eclapmsia symptoms','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(27  ,'pcr_proteinura_results','PCR proteinura results','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(28  ,'contractions_form','Contractions evaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(29  ,'signs_cpd','Signs of CPD','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(30  ,'fetal_blood_sample','Blood test results - fetal blood','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(31  ,'amnio_perform','Amniotomy evaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(32  ,'rupture_membranes_blood','Blood test results - Rupture membranes','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(33  ,'maternal_blood_loss_hemodinamic_conditions','Maternal blood loss and hemodinamic conditions','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1),  " +
        "(34  ,'maternal_blood_loss','Maternal blood loss','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1)  " +
        ",(35, 'urine_diagnosis', 'Urine ketone and glycosuria evaluation','1','2018-04-16 19:00:37','2018-04-16 19:00:37',  1)" +
        ";"
        ,
        assessmentQuestions: "INSERT INTO `assessmentQuestions` (`id`, `name`, `label_question`, `type`, `minimum_value`, `maximum_value`) VALUES                                                     " +
            "(  1,'history_prolonged_labour', 'Antecedent history of prolonged labour / obstructed labour', 2, 12, 123),                                                                               " +
            "(  2,'num_previous_stillbirth','Number of previous stillbirths',1,NULL,NULL),                                                                                                             " +
            "(  3,'outcome_of_last_pregnancy','Outcome of last pregnancy',1,NULL,NULL),                                                                                                                " +
            "(  4,'date_last_delivery','In case of previous pregnancies, what is the date of last delivery?',2,NULL,NULL),                                                                             " +
            "(  5,'chronic_hypertension','Chronic Hypertension',1,NULL,NULL),                                                                                                                          " +
            "(  6,'diabetes','Diabetes Mellitus',1,NULL,NULL),                                                                                                                                         " +
            "(  7,'hiv','HIV +',1,NULL,NULL),                                                                                                                                                          " +
            "(  8,'aids_hiv_syndrome','AIDS / HIV wasting syndrome ',1,NULL,NULL),                                                                                                                     " +
            "(  9,'chronic_anaemia','Chronic Anaemia',1,NULL,NULL),                                                                                                                                    " +
            "( 10,'obesity','Obesity',1,NULL,NULL),                                                                                                                                                    " +
            "( 11,'heart_disease','Heart disease',1,NULL,NULL),                                                                                                                                        " +
            "( 12,'lung_disease','Lung disease',1,NULL,NULL),                                                                                                                                          " +
            "( 13,'renal_disease','Renal disease',1,NULL,NULL),                                                                                                                                        " +
            "( 14,'other_chronic_disease','Other chronic disease',1,NULL,NULL),                                                                                                                        " +
            "( 15,'placenta_praevia','Placenta praevia',1,NULL,NULL),                                                                                                                                  " +
            "( 16,'accreta_increta_percreta','Accreta/increta/percreta placenta',1,NULL,NULL),                                                                                                         " +
            "( 17,'abruptio_placentae','Abruptio placentae',1,NULL,NULL),                                                                                                                              " +
            "( 18,'other_obstetric_haemorrhag','Other obstetric haemorrhage',1,NULL,NULL),                                                                                                             " +
            "( 19,'pre_eclampsia','Pre-eclampsia (excludes eclampsia)',1,NULL,NULL),                                                                                                                   " +
            "( 20,'eclampsia','Eclampsia',1,NULL,NULL),                                                                                                                                                " +
            "( 21,'pyelonephritis','Pyelonephritis',1,NULL,NULL),                                                                                                                                      " +
            "( 22,'malaria','Malaria',1,NULL,NULL),                                                                                                                                                    " +
            "( 23,'preterm_rupture_membrane','Preterm rupture of membranes',1,NULL,NULL),                                                                                                              " +
            "( 24,'anaemia','Anaemia',1,NULL,NULL),                                                                                                                                                    " +
            "( 25,'gestational_diabetes','Gestational diabetes',1,NULL,NULL),                                                                                                                          " +
            "( 26,'other_pregnancy_complications','Other pregnancy complications',1,NULL,NULL),                                                                                                        " +
            "( 27,'num_antenatal_care_visits','Number of antenatal care visits',1,NULL,NULL),                                                                                                          " +
            "( 28,'referred_from_another_facility','Women referred in labour from another health facility',1,NULL,NULL),                                                                               " +
            "( 29,'best_estimate_of_gestation','Best estimate of gestational age',1,NULL,NULL),                                                                                                        " +
            "( 30,'method_of_gestational_age','Method of gestational age estimation',1,NULL,NULL),                                                                                                     " +
            "( 31,'mode_of_onset_of_labour','Mode of onset of labour',1,NULL,NULL),                                                                                                                    " +
            "( 32,'duration_of_labour','If spontaneous onset of labour, specify duration of labour before admission in hours',1,NULL,NULL),                                                            " +
            "( 33,'fetal_movements_last_2h','Fetal movements since last assessment',1,NULL,NULL),                                                                                    " +
            "( 34,'time_first_assessment','Time of first assessment ',6,NULL,NULL),                                                                                                                    " +
            "( 35,'current_weight_kg','Current weight (kg)',1,NULL,NULL),                                                                                                                              " +
            "( 36,'height_cm','Height (cm)',1,NULL,NULL),                                                                                                                                              " +
            "( 37,'foot_length_cm','Foot length (cm)',1,NULL,NULL),                                                                                                                                    " +
            "( 38,'maternal_heart_rate','Maternal heart rate',1,5,150),                                                                                                                                " +
            "( 39,'systolic_blood_pressure','Systolic blood pressure (mmHg) ',1,10,280),                                                                                                               " +
            "( 40,'diastolic_blood_pressure','Diastolic blood pressure (mmHg)',1,10,150),                                                                                                              " +
            "( 41,'axillary_temperature','Axillary temperature (°C)',1,30,42.5),                                                                                                                       " +
            "( 42,'symphysis_fundal_height_cm','Symphysis-fundal height (cm)',1,10,50),                                                                                                                " +
            "( 43,'abdominal_circumference_cm','Abdominal circumference (cm)',1,NULL,NULL),                                                                                                            " +
            "( 44,'number_uterine_contractions','Number of uterine contractions (10 min)',1,0,10),                                                                                                     " +
            "( 45,'duration_uterine_contractions','Duration of uterine contractions (seconds)',1,NULL,NULL),                                                                                           " +
            "( 46,'fetal_heart_rate','Fetal heart rate (bpm)',1,NULL,NULL),                                                                                                                        " +
            "( 47,'cervical_dilatation_cm','Cervical dilatation (cm)',1,NULL,NULL),                                                                                                                    " +
            "( 48,'cervix_effacement','Cervix effacement',1,NULL,NULL),                                                                                                                                " +
            "( 49,'cervix_position','Cervix position',1,NULL,NULL),                                                                                                                                    " +
            "( 50,'cervix_consistency','Cervix consistency',1,NULL,NULL),                                                                                                                              " +
            "( 51,'fetal_presentation','Fetal presentation',1,NULL,NULL),                                                                                                                              " +
            "( 52,'fetal_station','Fetal station',1,NULL,NULL),                                                                                                                                        " +
            "( 53,'position_fetal_head','Position of fetal head',1,NULL,NULL),                                                                                                                         " +
            "( 54,'caput_succedaneum','Caput succedaneum',1,NULL,NULL),                                                                                                                                " +
            "( 55,'moulding','Moulding',1,NULL,NULL),                                                                                                                                                  " +
            "( 56,'amniotic_membranes_status','Amniotic membranes status',1,NULL,NULL),                                                                                                                " +
            "( 57,'sacral_promontory_reached','Sacral promontory reached',1,NULL,NULL),                                                                                " +
            "( 58,'ischial_spines_prominent','Ischial spines prominent',1,NULL,NULL),                                                                                                                  " +
            "( 59,'pubic_angle_less_2_fingers','Pubic angle admits less than two fingers',1,NULL,NULL),                                                                                                " +
            "( 60,'labour_companionship','Labour Companionship',1,NULL,NULL),                                                                                                                          " +
            "( 61,'time_of_assessment','Time of Assessment',6,NULL,NULL),                                                                                                                              " +
            "( 62,'full_assessment','Full Assessment',1,NULL,NULL),                                                                                                                                    " +
            "( 63,'painful_distress','Since the last assessment, how much the woman has been bothered by labour pain?',1,NULL,NULL),                                                                " +
            "( 64,'woman_mental_status','Since the last assessment, how much the woman has been bothered by emotional problems such as fear, anxiety, depression, irritability, or sadness?',1,NULL,NULL)," +
            "( 65,'oxytocin_infusion','Oxytocin infusion',1,NULL,NULL),                                                                                                                                " +
            "( 66,'oxytocin_dilution (amp/500ml)','Oxytocin dilution',1,NULL,NULL),                                                                                                                                " +
            "( 67,'oxytocin_rate_drops','Oxytocin rate (drops/min)',1,NULL,NULL),                                                                                                                      " +
            "( 68,'analgesia','Analgesia',1,NULL,NULL),                                                                                                                                                " +
            "( 69,'maternal_position','Predominant maternal position between assessments ',1,NULL,NULL),                                                                                               " +
            "( 70,'oral_fluid_intake','Oral fluid intake between assessments',1,NULL,NULL),                                                                                                            " +
            "( 71,'oral_food_intake','Oral food intake between assessments',1,NULL,NULL),                                                                                                              " +
            "( 72,'iv_fluids','IV Fluids',1,NULL,NULL),                                                                                                                                                " +
            "( 73,'cervical_ripening','Cervical Ripening',1,NULL,NULL),                                                                                                                                " +
            "( 74,'induction_of_labour','Induction of labour',1,NULL,NULL),                                                                                                                            " +
            "( 75,'augmentation_labour','Augmentation of labour',1,NULL,NULL),                                                                                                                         " +
            "( 76,'amniotomy','Amniotomy',1,NULL,NULL),                                                                                                                                                " +
            "( 77,'c_section','Caesarean section',1,NULL,NULL),                                                                                                                                        " +
            "( 78, 'able_intake', 'Able to intake fluids?',3,NULL,NULL),                                                                                                                             " +
            "( 79, 'able_pass_urine', 'Able to pass urine?',3,NULL,NULL),                                                                                                                            " +
            "( 80, 'abnormal_fetal_blood_results', 'Abnormal fetal blood results?',3,NULL,NULL),                                                                                                     " +
            "( 81, 'altered_mental_state', '',3,NULL,NULL),                                                                                                                                          " +
            "( 82, 'bladder_papabble', 'Bladder is papabble or using catheter?',3,NULL,NULL),                                                                                                        " +
            "( 83, 'blood_loss', 'Blood loss?',3,NULL,NULL),                                                                                                                                                   " +
            "( 84, 'burning_micturarion', 'Burning micturarion?',3,NULL,NULL),                                                                                                                       " +
            "( 85, 'cardiac_arrest', 'Cardiac arrest?',3,NULL,NULL),                                                                                                                                 " +
            "( 86, 'catheter', 'Catheter?',3,NULL,NULL),                                                                                                                                             " +
            "( 87, 'cervical_dilatation_speed', 'Cervical_Dilatation es speed?',3,NULL,NULL),                                                                                                        " +
            "( 88, 'chorioamnionitis_amnionitis', 'Chorioamnionitis or amnionitis?',3,NULL,NULL),                                                                                                    " +
            "( 89, 'coagulopathy', 'Coagulopathy?',3,NULL,NULL),                                                                                                                                     " +
            "( 90, 'cold_exposure', 'Cold exposure?',3,NULL,NULL),                                                                                                                                   " +
            "( 91, 'cpd_suspicion', 'Cephalopelvic suspicion?',3,NULL,NULL),                                                                                                                           " +
            "( 92, 'ctg_abnormal', 'Cardiotocography abnormal?',3,NULL,NULL),                                                                                                                        " +
            "( 93, 'diarrhoea', 'Diarrhoea?',3,NULL,NULL),                                                                                                                                           " +
            "( 94, 'fasting_glucose', 'Fasting glucose? (mmol/l)',1,3,12),                                                                                                                           " +
            "( 95, 'fetal_change', 'Fetal descent change?',3,NULL,NULL),                                                                                                                             " +
            "( 97, 'fetus_alive', 'Fetus is alive?',3,NULL,NULL),                                                                                                                                    " +
            "( 98, 'flank_pain', 'Flank pain?',3,NULL,NULL),                                                                                                                                         " +
            "( 99, 'glycosuria', 'Glycosuria?',3,NULL,NULL),                                                                                                                                         " +
            "(100, 'ketones_present', 'Ketones present?',3,NULL,NULL),                                                                                                                               " +
            "(101, 'leucocyte_esterase', 'Leucocyte esterase?',3,NULL,NULL),                                                                                                                         " +
            "(102, 'meconium', 'Meconium?',3,NULL,NULL),                                                                                                                                             " +
            "(103, 'nitrites', 'Nitrites?',3,NULL,NULL),                                                                                                                                             " +
            "(104, 'oliguiria_suspicion', 'Oliguiria suspicion?',3,NULL,NULL),                                                                                                                         " +
            "(105, 'other_source', 'Other source?',3,NULL,NULL),                                                                                                                                     " +
            "(106, 'oxygen_saturarion', 'Oxygen saturation?',1,0,100),                                                                                                                               " +
            "(107, 'pneumonia', 'Pneumonia?',3,NULL,NULL),                                                                                                                                           " +
            "(108, 'purulent', 'Amniotics fluids is purulent?',3,NULL,NULL),                                                                                                                         " +
            "(109, 'pyelonephritiis', 'Pyelonephritiis?',3,NULL,NULL),                                                                                                                               " +
            "(110, 'rales_lung', 'Rales in the lungs?',3,NULL,NULL),                                                                                                                                 " +
            "(111, 'random_glucose', 'Random glucose? (mmol/l)',1,3,12),                                                                                                                             " +
            "(112, 'reduced_fluids_intake', 'Volume of fluid intake is reduced?',3,NULL,NULL),                                                                                                       " +
            "(113, 'respiratory_rate', 'Respiratory rate?',1,10,40),                                                                                                                                 " +
            "(114, 'scalp_ph', 'Fetal scalp blood pH?',1,0,14),                                                                                                                                      " +
            "(115, 'severe_malaria', 'Severe malaria?',3,NULL,NULL),                                                                                                                                 " +
            "(116, 'signs_cpd', 'Are there signs of cephalopelvic disproportion?',3,NULL,NULL),                                                                                                      " +
            "(117, 'slow_than_normal', 'Progression is slow than normal?',1,NULL,NULL),                                                                                                              " +
            "(120, 'uncomplicated_malaria', 'Uncomplicated es malaria?',3,NULL,NULL),                                                                                                                " +
            "(121, 'urine_abnormal', 'Urine is abnormal?',3,NULL,NULL),                                                                                                                              " +
            "(122, 'urine_cloudy', 'Boiled urine is cloudy adding acetic acid?',3,NULL,NULL),                                                                                                        " +
            "(123, 'urine_infection', 'Urine infection diagnosis confirmed?',3,NULL,NULL),                                                                                                           " +
            "(124, 'urine_output', 'Urine volume output? (ml)',1,0,2500),                                                                                                                            " +
            "(125, 'urine_volume_per_hour', 'Urine volume/hour (ml/h)',1,NULL,NULL),                                                                                                                 " +
            "(126, 'vaginal_bleeding', 'Is there any vaginal bleeding',3,NULL,NULL),                                                                                                                 " +
            "(127, 'vommiting', 'Vommiting?',3,NULL,NULL),                                                                                                                                           " +
            "(128, 'foul_smelling_vaginal_discharge', 'Foul smeling vagina discharge?', 3, NULL, NULL),                                                                                              " +
            "(129, 'hypoxia', 'Hypoxia?', 3, NULL, NULL),                                                                                                                                               " +
            "(130, 'shortness of breath', 'Shortness of breath', 3, NULL, NULL),                                                                                                                     " +
            "(131, 'medications', 'Medications', 3, NULL, NULL),                                                                                                                                     " +
            "(132, 'proteinura', 'Proteinura? (from dipstick)', 1, NULL, NULL ),                                                                                                                                     " +
            "(133, 'abnormal_liver_function', 'Abnormal liver function?', '3', NULL, NULL),                                                                                                          " + 
            "(134, 'abnormal_serum_creatinine', 'Abnormal serum creatine?', '3', NULL, NULL),                                                                                                        " + 
            "(135, 'abnormal_platelet_count', 'Abnormal platelet count?', '3', NULL, NULL),                                                                                                          " + 
            "(136, 'lactate', 'Lactate (mmol/L)', '1', NULL, NULL),                                                                                                                                  " + 
            "(137, 'dehydration_signs', 'Are there signs of dehydration?', '1', NULL, NULL),                                                                                                         " + 
            "(138, 'uterine_tenderness', 'Uterine tenderness?', '3', NULL, NULL),                                                                                                                    " + 
            "(139, 'suspected_placenta_previa', 'Suspected lowing placenta previa?', '3', NULL, NULL),                                                                                               " + 
            "(140, 'suspected_placenta_abruptio', 'Suspected placental abruptio?', '3', NULL, NULL);                                                                                                 " 
  ,
    assessmentOptions: "INSERT INTO `assessmentOptions` (`id`,`value`,`label_reply`) VALUES   " +
            "(  1,1,'Yes'),                                                                                         " +
            "(  2,0,'No'),                                                                                          " +
            "(  3,0,'Normal'),                                                                                      " +
            "(  4,0,'Altered'),                                                                                     " +
            "(  5,1,'LOW RISK'),                                                                                    " +
            "(  6,1,'HIGH RISK'),                                                                                   " +
            "(  7,0,'Auscultation or Doppler ultrasound'),                                                          " +
            "(  8,0,'Record accelerations or decelerations'),                                                       " +
            "(  9,0,'If FHR < 110 or FHR > 160 maternal heart rate'),                                               " +
            "( 10,0,'Chorioamnionitis/sepsis,temperature of 38º C'),                                                " +
            "( 11,0,'Severe hypertension (160/110 mmHg or above)'),                                                 " +
            "( 12,0,'Oxytocine use'),                                                                               " +
            "( 13,0,'Presence of significant meconium'),                                                            " +
            "( 14,0,'Vaginal bleeding that develops in labour'),                                                    " +
            "( 15,0,'No previous childbirth'),                                                                      " +
            "( 16,1,'Live birth, still alive'),                                                                     " +
            "( 17,2,'Live birth, deceased'),                                                                        " +
            "( 18,3,'Stillbirth'),                                                                                  " +
            "( 19,0,'Last menstrual period'),                                                                       " +
            "( 20,1,'Ultra-sound scan'),                                                                            " +
            "( 21,2,'Symphysis-fundal height (cm)'),                                                                " +
            "( 22,3,'Birth weight assessment'),                                                                     " +
            "( 23,4,'Neonatal physical findings'),                                                                  " +
            "( 24,0,'Spontaneous'),                                                                                 " +
            "( 25,1,'Induced'),                                                                                     " +
            "( 26,0,'No changes/increased'),                                                                        " +
            "( 27,1,'Reduced'),                                                                                     " +
            "( 28,2,'Absent'),                                                                                      " +
            "( 29,0,'0 cm'),                                                                                        " +
            "( 30,1,'1 cm'),                                                                                        " +
            "( 31,2,'2 cm'),                                                                                        " +
            "( 32,3,'3 cm'),                                                                                        " +
            "( 33,4,'4 cm'),                                                                                        " +
            "( 34,5,'5 cm'),                                                                                        " +
            "( 35,6,'6 cm'),                                                                                        " +
            "( 36,7,'7 cm'),                                                                                        " +
            "( 37,8,'8 cm'),                                                                                        " +
            "( 38,9,'9 cm'),                                                                                        " +
            "( 39,10,'10 cm'),                                                                                      " +
            "( 40,0,'Thick (less than 30% effaced)'),                                                               " +
            "( 41,1,'Medium (up to 50% effaced)'),                                                                  " +
            "( 42,2,'Thin (up to 80% effaced)'),                                                                    " +
            "( 43,3,'Very thin / paper-thin (more than 80% effaced)'),                                              " +
            "( 44,0,'Anterior'),                                                                                    " +
            "( 45,1,'Central'),                                                                                     " +
            "( 46,2,'Posterior'),                                                                                   " +
            "( 47,0,'Soft'),                                                                                        " +
            "( 48,1,'Medium'),                                                                                      " +
            "( 49,2,'Firm'),                                                                                        " +
            "( 50,0,'Cephalic'),                                                                                    " +
            "( 51,1,'Breech'),                                                                                      " +
            "( 52,2,'Transverse lie'),                                                                              " +
            "( 53,0,'Above ischial spine'),                                                                         " +
            "( 54,1,'At ischial spine'),                                                                            " +
            "( 55,2,'Below ischial spine '),                                                                        " +
            "( 56,0,'Occiput Anterior (includes right and left)'),                                                  " +
            "( 57,1,'Occiput transverse'),                                                                          " +
            "( 58,2,'Occiput posterior'),                                                                           " +
            "( 59,3,'Other'),                                                                                       " +
            "( 60,0,'None'),                                                                                        " +
            "( 61,1,'Mild'),                                                                                        " +
            "( 62,2,'Moderate'),                                                                                    " +
            "( 63,3,'Severe'),                                                                                      " +
            "( 64,1,'First degree'),                                                                                " +
            "( 65,2,'Second degree'),                                                                               " +
            "( 66,3,'Third degree '),                                                                               " +
            "( 67,0,'Intact'),                                                                                      " +
            "( 68,1,'Ruptured without meconium'),                                                                   " +
            "( 69,2,'Ruptured with stale meconium'),                                                                " +
            "( 70,3,'Ruptured with fresh meconium'),                                                                " +
            "( 71,2,'Not assessed'),                                                                                " +
            "( 72,2,'Transverse lie / compound / other'),                                                           " +
            "( 73,1,'At ischial spine'),                                                                            " +
            "( 74,0,'Not at all'),                                                                                  " +
            "( 75,1,'Slightly'),                                                                                    " +
            "( 76,2,'Moderately'),                                                                                  " +
            "( 77,3,'Quite a bit'),                                                                                 " +
            "( 78,4,'Extremely'),                                                                                   " +
            "( 79,0,'0.25 amp/500ml'),                                                                              " +
            "( 80,1,'0.5 amp/500ml'),                                                                               " +
            "( 81,2,'1 amp/500ml'),                                                                                 " +
            "( 82,3,'2amp/500ml'),                                                                                  " +
            "( 83,4,'Other'),                                                                                       " +
            "( 84,1,'IV/IM Opioid'),                                                                                " +
            "( 85,2,'Epidural'),                                                                                    " +
            "( 86,3,'Spinal'),                                                                                      " +
            "( 87,5,'Combined'),                                                                                    " +
            "( 88,0,'Upright, sitting, standing, walking, kneeing, squatting, all-4'),                              " +
            "( 89,1,'Recumbent, semi-recumbent, lateral, supine'),                                                  " +
            "( 90,0,'>93%'),                                                                                        " +       
            "( 91,1,'90-93%'),                                                                                      " +
            "( 92,2,'<90'),                                                                                         " +
            "( 93,0,'0'),                                                                                           " +
            "( 94,1,'1'),                                                                                           " +
            "( 95,2,'2'),                                                                                           " +
            "( 96,3,'>2')                                                                                           " + 
            ";"
    ,
    questionsForms: "insert into `questionsForms` (`id`,`assessment_questions_id`, `assessments_forms_id`, `order_question_number`) values  "+
    //"(2,35,1  , 2),"+
    //"(3,36,1  , 3),"+
    "(4,38,1  , 4),"+
    "(5,39,1  , 5),"+
    "(6,40,1  , 6),"+
    "(7,41,1  , 7),"+
    "(8,42,1  , 8),"+
    "(9,43,1  , 9),"+
    "(10,44,1 , 10),"+
    "(11,45,1 , 11),"+
    "(12,46,1 , 12),"+
    "(13,33,1 , 13),"+
    "(14,47,1 , 14),"+
    "(15,48,1 , 15),"+
    "(16,49,1 , 16),"+
    "(17,50,1 , 17),"+
    "(18,51,1 , 18),"+
    "(19,52,1 , 19),"+
    "(20,53,1 , 20),"+
    "(21,54,1 , 21),"+
    "(22,55,1 , 22),"+
    "(23,56,1 , 23),"+
    "(24,57,1 , 24),"+
    "(25,58,1 , 25),"+
    "(26,59,1 , 26),"+
    "(27,63,1 , 27),"+
    "(28,64,1 , 28),"+
    "(29,60,1 , 29),"+
    "(30,106,1, 30),"+
    "(31,113,1, 31),"+
    "(32 , 44 , 2, 1  ),"+
    "(33 , 45 , 2, 2  ),"+
    "(34 , 46 , 2, 3  ),"+
    "(35 , 33 , 2, 4  ),"+
    "(36 , 47 , 2, 5  ),"+
    "(37 , 51 , 2, 6  ),"+
    "(38 , 52 , 2, 7  ),"+
    "(39 , 55 , 2, 8  ),"+
    "(40 , 54 , 2, 9  ),"+
    "(41 , 53 , 2, 10 ),"+
    "(42 , 56 , 2, 11 ),"+
    "(43 , 39 , 2, 12 ),"+
    "(44 , 40 , 2, 13 ),"+
    "(45 , 38 , 2, 14 ),"+
    "(46 , 41 , 2, 15 ),"+
    "(47 , 64 , 2, 16 ),"+
    "(48 , 63 , 2, 17 ),"+
    "(49 , 65 , 2, 18 ),"+
    "(50 , 66 , 2, 19 ),"+
    "(51 , 67 , 2, 20 ),"+
    "(52 , 68 , 2, 21 ),"+
    "(53 , 60 , 2, 22 ),"+
    "(54 , 69 , 2, 23 ),"+
    "(55 , 70 , 2, 24 ),"+
    "(56 , 71 , 2, 25 ),"+
    "(57 , 72 , 2, 26 ),"+
    "(58 , 113, 2, 27 ),"+
    "(59, 73, 3, 1 ),"+
    "(60, 74, 3, 2 ),"+
    "(61, 75, 3, 3 ),"+
    "(62, 76, 3, 4 ),"+
    "(63, 77, 3, 5 ),"+
    "(64, 38, 4, 1  ),"+
    "(65, 39, 4, 2  ),"+
    "(66, 40, 4, 3  ),"+
    "(67, 41, 4, 4  ),"+
    "(68, 44, 4, 5  ),"+
    "(69, 45, 4, 6  ),"+
    "(70, 47, 4, 7  ),"+
    "(71, 56, 4, 8  ),"+
    "(72, 64, 4, 9  ),"+
    "(73, 63, 4, 10 ),"+
    "(74, 60, 4, 11 ),"+
    "(75,46,5, 1 ),"+
    "(76,33,5, 2 ),"+
    "(77,53,5, 3 ),"+
    "(78,52,5, 4 ),"+
    "(79,51,5, 5 ),"+
    "(80,73,6, 1 ),"+
    "(81,74,6, 2 ),"+
    "(82,75,6, 3 ),"+
    "(83,76,6, 4 ),"+
    "(84,77,6, 5 ),"+
    "(85,47,7, 1 ),"+
    "(86,44,7, 2 ),"+
    "(87,45,7, 3 ),"+
    "(88,48,7, 4 ),"+
    "(89,49,7, 5 ),"+
    "(90,50,7, 6 ),"+
    "(91,33,7, 7 ),"+
    "(92,38,8, 1 ),"+
    "(93,39,8, 2 ),"+
    "(94,40,8, 3 ),"+
    "(95,41,8, 4 ),"+
    "(96,44,8, 5 ),"+
    "(97,45,8, 6 ),"+
    "(98,63,8, 7 ),"+
    "(99,64,8, 8 ),"+
    "(100,78,8, 9 ),"+
    "(101,79,8, 10),"+
    "(102,113,8, 11),"+
    "(103,38,9  , 1 ),"+
    "(104,46,9  , 2 ),"+
    "(105,63,10 , 1 ),"+
    "(106,126,10, 2 ),"+
    "(107,128,10, 3 ),"+
    "(108,129,10, 4 ),"+
    "(109,130,10, 5 ),"+
    "(110,131,10, 6 ),"+
    "(111,127,11, 1 ),"+
    "(112,124,11, 2 ),"+
    "(113,63,11 , 3 ),"+
    "(114,113,11, 4 ),"+
    "(115,132,11, 5 ),"+
    "(116,133,12, 1 ),"+
    "(117,134,12, 2 ),"+
    "(118,135,12, 3 ),"+
    "(119,132,12, 4 ),"+
    "(120,44,13 , 1 ),"+
    "(121,45,13 , 2 ),"+
    "(122,46,13 , 3 ),"+
    "(123,47,13 , 4 ),"+
    "(124,51,13 , 5 ),"+
    "(125,52,13 , 6 ),"+
    "(126,53,13 , 7 ),"+
    "(127,54,13 , 8 ),"+
    "(128,56,13 , 9 ),"+
    "(129,39,13 , 10),"+
    "(130,55,13 , 11),"+
    "(131,40,13 , 12),"+
    "(132,38,13 , 13),"+
    "(133,41,13 , 14),"+
    "(134,63,13 , 15),"+
    "(135,64,13 , 16),"+
    "(136,65,13 , 17),"+
    "(137,66,13 , 18),"+
    "(138,67,13 , 19),"+
    "(139,68,13 , 20),"+
    "(140,60,13 , 21),"+
    "(141,69,13 , 22),"+
    "(142,70,13 , 23),"+
    "(143,71,13 , 24),"+
    "(144,72,13 , 25),"+
    "(145,113,13, 26),"+
    "(146,106,13, 27),"+
    "(147,79,13 , 28),"+
    "(148,124,13, 29),"+
    "(149,126,13, 30),"+
    "(150,127,13, 31),"+
    "(151,97,14 , 1 ),"+
    "(152,46,14 , 2 ),"+
    "(153,45,14 , 3 ),"+
    "(154,44,14 , 4 ),"+
    "(155,114,15, 1),"+
    "(156,46,15 , 2),"+
    "(157,116,15, 3),"+
    "(158,46,16, 1),"+
    "(159,88,18 , 1 ),"+
    "(160,123,18, 2 ),"+
    "(161,109,18, 3 ),"+
    "(162,107,18, 4 ),"+
    "(163,120,18, 5 ),"+
    "(164,115,18, 6 ),"+
    "(165,105,18, 7 ),"+
    "(166,136,19, 1 ),"+
    "(167,63,19 , 2 ),"+
    "(168,64,19 , 3 ),"+
    "(169,38,19 , 4 ),"+
    "(170,39,19 , 5 ),"+
    "(171,40,19 , 6 ),"+
    "(172,124,19, 7 ),"+
    "(173,121,19, 8 ),"+
    "(174,38,20, 1 ),"+
    "(175,39,20, 2 ),"+
    "(176,40,20, 3 ),"+
    "(177,41,20, 4 ),"+
    "(178,46,20, 5 ),"+
    "(179,83,20, 6 ),"+
    "(180,91,20, 7 ),"+
    "(181,92,20, 8 ),"+
    "(182,97,20, 9 ),"+
    "(183,102,20,10),"+
    "(184,106,20,11),"+
    "(185,113,20,12),"+
    "(186,124,20,13),"+
    "(187,126,20,14),"+
    "(188,111,21, 1),"+
    "(189, 94,21, 2),"+
    "(190,111,22, 1),"+
    "(191, 94,22, 2),"+
    "(192,137,23, 1 ),"+
    "(193, 83,23, 2 ),"+
    "(194,110,23, 3 ),"+
    "(195,127,23, 4 ),"+
    "(196, 93,23, 5 ),"+
    "(197, 78,23, 6 ),"+
    "(198, 82,24, 1 ),"+
    "(199, 86,24, 2 ),"+
    "(200, 79,24, 3 ),"+
    "(201,124,24, 4 ),"+
    "(202, 79,25, 1 ),"+
    "(203,121,25, 2 ),"+
    "(204,124,25, 3 ),"+
    "(205,101,26, 1 ),"+
    "(206,103,26, 2 ),"+
    "(207,132,26, 3 ),"+
    "(208, 84,26, 1 ),"+
    "(209, 98,26, 2 ),"+
    "(210,101,27, 1 ),"+
    "(211,103,27, 2 ),"+
    "(212, 44,28, 1 ),"+
    "(213, 45,28, 2 ),"+
    "(214,116,29, 1 ),"+
    "(215,102,29, 2 ),"+
    "(216, 80,30, 1 ),"+
    "(217,102,30, 2 ),"+
    "(218,102,31, 1 ),"+
    "(219,126,31, 2 ),"+
    "(220, 46,31, 3 ),"+
    "(221,128,31, 4 ),"+
    "(222, 46,32, 1 ),"+
    "(223,126,32, 2 ),"+
    "(224, 39,32, 3 ),"+
    "(225,141,32, 4 ),"+
    "(226,138,32, 5 ),"+
    "(227,139,32, 6 ),"+
    "(228,140,32, 7 ),"+
    "(229, 24,33, 1 ),"+
    "(230, 89,33, 2 ),"+
    "(231, 38,34, 1 ),"+
    "(232, 39,34, 2 ),"+
    "(233, 40,34, 3 ),"+
    "(234, 41,34, 4 ),"+
    "(235, 44,34, 5 ),"+
    "(236, 45,34, 6 ),"+
    "(237, 63,34, 7 ),"+
    "(238, 64,34, 8 ),"+
    "(239, 78,34, 9 ),"+
    "(240, 79,34, 10),"+
    "(241,113,34, 11),"+    
    "(242, 92,16, 2 )"+
    ",(243, 99,35, 1 )"+
    ",(244,100,35, 2 )"+
    ";"
    ,
    questionsOptions: "insert into `questionsOptions` (`id`,`assessment_questions_id`, `assessment_options_id`) values " +
            "(1,47,29),(2,47,30),(3,47,31),(4,47,32),(5,47,33),(6,47,34),(7,47,35),(8,47,36),(9,47,37),(10,47,38),(11,47,39),(12,33, 1),(13,33, 2)," + 
            "(14,48,40),(15,48,41),(16,48,42),(17,48,43),(18,49,44),(19,49,45),(20,49,46),(21,50,47)," +
            "(22,50,48),(23,50,49),(24,51,50),(25,51,51),(26,51,52),(27,52,53),(28,52,54),(29,52,55),(30,53,56),(31,53,57),(32,53,58),"+
            "(33,53,59),(34,54,60),(35,54,61),(36,54,62),(37,54,63),(38,55,60),(39,55,64),(40,55,65),(41,55,66),(42,56,67),"+
            "(43,56,68),(44,56,69),(45,56,70),(46,57, 1),(47,57, 2),(48,58, 1),(49,58, 2),(50,59, 1),(51,59, 2),(52,63,74),(53,63,75),(54,63,76),"+
            "(55,63,77),(56,63,78),(57,64,74),(58,64,75),(59,64,76),(60,64,77),(61,64,78),"+
            "(62,73,1),(63,73,2),(64,74,1),(65,74,2),(66,75,1),(67,75,2),(68,76,1),(69,76,2),"+
            "(70,77,1),(71,77,2),(72,62,1),(73,62,2),(74,60,1),(75,60,2),(76,69,88),(77,69,89), " +
            "(78  , 78 , 1), (79  , 78 , 2),  (80  , 79 , 1), (81  , 79 , 2),  (82  , 80 , 1), (83  , 80 , 2),  (84  , 81 , 1), (85  , 81 , 2),  (86  , 82 , 1), (87  , 82 , 2),  (88  , 83 , 1), (89  , 83 , 2),  (90  , 84 , 1), (91  , 84 , 2),  (92  , 85 , 1), (93  , 85 , 2),  " +
            "(94  , 86 , 1), (95  , 86 , 2),  (96  , 88 , 1), (97  , 88 , 2),  (98  , 89 , 1), (99  , 89 , 2),  (100 , 90 , 1), (101 , 90 , 2),  (102 , 91 , 1), (103 , 91 , 2),  (104 , 92 , 1), (105 , 92 , 2),  (106 , 93 , 1), (107 , 93 , 2),  (108 , 97 , 1), (109 , 97 , 2),  " +
            "(110 , 98 , 1), (111 , 98 , 2),  (112 , 99 , 93), (113 , 99 , 94),  (114 , 100, 1), (115 , 100, 2),  (116 , 101, 1), (117 , 101, 2),  (118 , 102, 1), (119 , 102, 2),  (120 , 103, 1), (121 , 103, 2),  (122 , 104, 1), (123 , 104, 2),  (124 , 105, 1), (125 , 105, 2),  " +
            "(126 , 107, 1), (127 , 107, 2),  (128 , 108, 1), (129 , 108, 2),  (130 , 109, 1), (131 , 109, 2),  (132 , 110, 1), (133 , 110, 2),  (134 , 112, 1), (135 , 112, 2),  (136 , 115, 1), (137 , 115, 2),  (138 , 116, 1), (139 , 116, 2),  (140 , 120, 1), (141 , 120, 2),  " +
            "(142 , 121, 1), (143 , 121, 2),  (144 , 122, 1), (145 , 122, 2),  (146 , 123, 1), (147 , 123, 2),  (148 , 126, 1), (149 , 126, 2),  (150 , 127, 1), (151 , 127, 2),  (152 , 128, 1), (153 , 128, 2),  (154 , 129, 1), (155 , 129, 2),  (156 , 130, 1), (157 , 130, 2),  " +
            "(158 , 131, 1), (159 , 131, 2),  (160 , 133, 1), (161 , 133, 2),  (162 , 134, 1), (163 , 134, 2),  (164 , 135, 1), (165 , 135, 2),  (166 , 138, 1), (167 , 138, 2),  (168 , 139, 1), (169 , 139, 2),  (170 , 140, 1), (171 , 140, 2),  (172 , 106, 90), (173, 106, 91), " +
            "(174 , 106, 92), (174 , 132, 93), (174 , 132, 94), (174 , 132, 95), (174 , 132, 96), (175 , 65, 1), (176 , 65, 2), (177 , 66, 79), (178 , 66, 80), (179 , 66, 81), (180 , 66, 82), (181 , 66, 83), (182 , 24, 1), (183 , 24, 2), (184 , 136, 93), (185 , 136, 94), (186 , 136, 95), (187 , 136, 96), " +
            "(188 , 68, 1), (189 , 68, 2), (190 , 70, 1), (191 , 70, 2), (192 , 71, 1), (193 , 71, 2), (194 , 72, 1), (195 , 72, 2), (196 , 99 , 95), (197 , 99 , 96), (197 , 137 , 1), (197 , 137, 2);"



    ,
    alert1: "INSERT INTO `alert` (`id`, `title`, `text`, `link`) VALUES                                                                                                                                                                                                                                                                                                                      " +
           " (1,  'Maternal Heart Frequency Warning', 'Pulse over 120 beats/minute, please reassess in 30 minutes', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                                                 " +
           " (2,  'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: pulse over 120 beats/minute on 2 occasions 30 minutes apart', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                 " + 
           " (3,  'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: elevated diastolic blood pressure of 110 mmHg or more', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                       " + 
           " (4,  'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: elevated systolic blood pressure of 160 mmHg or more', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                        " + 
           " (5,  'Maternal Diastolic Blood Pressure warning', 'Elevated diastolic blood pressure of 90 mmHg or more, please reassess in 30 minutes', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                               " +
           " (6,  'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: elevated diastolic blood pressure of 90 mmHg or more on 2 consecutive readings taken 30 minutes apart', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                       " + 
           " (7,  'Maternal Systolic Blood Pressure warning', 'Elevated systolic blood pressure of 140 mmHg or more, please reassess in 30 minutes', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                " +
           " (8,  'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: elevated systolic blood pressure of 140 mmHg or more on 2 consecutive readings taken 30 minutes apart', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                       " +
           " (9,  'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: reading of 2+ of protein on urinalysis and a single reading of either elevated diastolic blood pressure (90 mmHg or more) or elevated systolic blood pressure (140 mmHg or more)', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),            " +
           " (10, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: temperature of 38°C or above', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment')                                                                                                                                                                ;"
    ,
    alert2: "INSERT INTO `alert` (`id`, `title`, `text`, `link`) VALUES  " + 
            "(11, 'Maternal Temperature warning', 'Temperature of 37.5°C or above, please reassess in 1 hour', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                                                      " +
           " (12, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: temperature of 37.5°C or above on 2 consecutive readings 1 hour apart', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                       " + 
           " (13, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: vaginal blood loss', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                                          " + 
           " (14, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: presence of significant meconium', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                            " + 
           " (15, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: pain reported by the woman that differs from the pain normally associated with contractions', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                 " + 
           " (16, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: abnormal presentation, including cord presentation', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                          " + 
           " (17, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: transverse or oblique lie', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                                   " + 
           " (18, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: free-floating head in a nulliparous woman', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                   " + 
           " (19, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: suspected fetal growth restriction or macrosomia', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment')                                                                                                                                          ;"
           ,
           alert3: "INSERT INTO `alert` (`id`, `title`, `text`, `link`) VALUES  " + 
           " (20, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: suspected anhydramnios or polyhydramnios', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                    " + 
           " (21, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: fetal heart rate below 110 beats/minute', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                     " + 
           " (22, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: fetal heart rate above 160 beats/minute', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                                                     " + 
           " (23, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: deceleration in fetal heart rate heard on intermittent auscultation', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                         " + 
           " (24, 'Intensified Monitoring Recommendation', 'Transfer the woman to obstetric-led care: reduced fetal movements in the last 24 hours reported by the woman.', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment'),                                                                                                                         " + 
           " (26, 'Suspected Delay in labour', 'Please assess the frequency and duration of contractions', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations'),                                                                                                                                                                                                             " + 
           " (27, 'Caesarean Section Recommendation', 'Hyperactive Uterus (Suspected Obstruction): Peform Caesarean Section', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations'),                                                                                                                                                                                          " + 
           " (28, 'Amniotomy Recommendation', 'Progress inadequate (Hypoactivity), please offer amniotomy if the membranes are intact.', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations'),                                                                                                                                                                               " + 
           " (29, 'Suspected cephalopelvic disproportion', 'Please check for signs of cephalopelvic disproportion.', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations'),                                                                                                                                                                                                   " + 
           " (30, 'Suspected Fetal Distress', 'Please, consider performing a Caesarean Section. In the meantime, apply fetal resuscitation', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations')                                                                                                                                                                           ;"
           ,
           alert4: "INSERT INTO `alert` (`id`, `title`, `text`, `link`) VALUES  " + 
           " (31, 'Intensified Monitoring Recommendation', 'If oxytocin was being administered, please reassess the need and reintroduce if needed', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations'),                                                                                                                                                                   " + 
           " (32, 'Intensified Monitoring Recommendation', 'Suspected fetal distress not confirmed, please intensify monitoring', 'https://www.nice.org.uk/guidance/cg190/chapter/1-Recommendations'),                                                                                                                                                                                      " + 
           " (33, 'Caesarean Section Recommendation', 'Signs of cephalopelvic disproportion/obstruction, Please perform Caesarean Section', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations'),                                                                                                                                                                            " + 
           " (34, 'Progress Assessment Recommendation', 'Please, reassess progress by vaginal examination after 2 hours', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations'),                                                                                                                                                                                              " + 
           " (35, 'Caesarean Section Recommendation', 'Percentile 95 crossed: Please, perform Caesarean Section', 'http://www.ncbi.nlm.nih.gov/pubmed/26006758'),                                                                                                                                                                                                                           " + 
           " (36, 'Oxytocin Infusion Recommendation', 'Dilute 5IU of oxytocin (1 amp) in 500ml of crystalloid solution </br> Initial infusion rate: 5 drops/min or 15ml/h', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations'),                                                                                                                                            " + 
           " (37, 'Oxytocin Infusion Recommendation', 'Increase the infusion rate of oxytocin in 5 drops/min', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations'),                                                                                                                                                                                                         " + 
           " (38, 'Oxytocin Infusion Recommendation', 'Maintain the infusion rate of oxytocin', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations'),                                                                                                                                                                                                                        " + 
           " (39, 'Oxytocin Infusion Recommendation', 'Reduce the infusion rate of oxytocin in 5 drops/min', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations'),                                                                                                                                                                                                           " + 
           " (40, 'Intensified Monitoring Recommendation', '<br />Transfer the woman to obstetric-led care: temperature of 35.5°C or below', 'https://www.nice.org.uk/guidance/cg190/chapter/1-recommendations#initial-assessment')    ;                                                                                                                                                          ;"
           ,
           alert5: "INSERT INTO `alert` (`id`, `title`, `text`, `link`) VALUES  " + 
           " (41, 'Caesarean Section  Recommendation', 'Chance to have a bad outcome increased and C-Section is recomended. <br/>Number needed to treat =', 'http://www.ncbi.nlm.nih.gov/pubmed/26006758'),                                                                                                                                                                                 " +
           " (42, 'Expectante Management Recommendation', 'Chance to have a bad outcome increased but C-Section is NOT recomended. <br/>Number needed to harm =', 'http://www.ncbi.nlm.nih.gov/pubmed/26006758'),                                                                                                                                                                           " + 
           " (43, 'Possible latent phase of labour', 'Please reassess the woman in 1-2 hours and confirm that the woman is in the active phase of labour', ''),                                                                                                                                                                                                                             " + 
           " (44, 'Prolonged rupture of membranes', 'Consider the use of atibiotics for prolonged rupture of membranes', ''),                                                                                                                                                                                                                                                               " + 
           " (45, 'Forceps Recommendation', 'The contractions are good, forceps is recommended.', ''),                                                                                                                                                                                                                                                                                      " +      
           " (46, 'Eligible for SELMA', 'SELMA will give advices on this case. First assessment was scheduled', ''),                                                                                                                                                                                                                                                                        " +       
           " (47, 'Not eligible for SELMA', 'SELMA will not give advices on this case, but you can save data from this case.', ''),                " +
           " (48, 'Active phase', 'From now on, SELMA will give advices on this case.', ''),                 " +
           " (49 , 'Discharge - Long latent phase', 'Discharge the woman and advise her to return if: pain/discomfort increases, vaginal bleeding, membranes rupture', ''), " +
           " (50 , 'Latent phase', 'Please reassess the woman in 4 hours and confirm that the woman is in the active phase of labour', ''), " +
           " (51 , 'Second stage', '', ''), " +
           " (52 , 'Bradycardia', 'Measure all set of maternal parameters and the pulse should be assessed manually for noting rate, rhythm and strength.', ''), " +
           " (53 , 'Call for high level', 'Call high level assistance and request immediate review \nTransfer woman to obstetric led care, unless the risks of transfer outweigh the benefits.', ''), " +
           " (54 , 'Continuous monitoring', 'Monitor continuosly maternal heart rate and fetal heart rate.', ''), " +
           " (55 , 'Intensified monitoring', 'Repeat full set of observations within 15 min.', ''), " +
           " (56 , 'Tachycardia', 'Measure all set of maternal parameters and the pulse should be assessed manually for noting rate, rhythm and strength', ''), " +
           " (57 , 'Severe Tachycardia', 'Position the woman on her left side with legs higher than chest\nGive IV fluids rapidly (500 ml)\nKeep her warm (cover her)', ''), " +
           //" (58 , 'Call high assistance', '', ''), " +
           " (59 , 'Is possible to transfer woman?', 'Transfer woman to obstetric‑led care, unless the risks of transfer outweigh the benefits.\nIf birth is imminent, assess whether birth in the current location is preferable to transferring the woman to an obstetric unit.', ''), " +
           //" (60 , 'Give fluids rapidly', '', ''), " +
            " (62 ,'Continuous cardiotography',    '',   ''),                                " +
            " (63 ,'Severe hypotension', 'Call high level assistance and request immediate review. \nTransfer woman to obstetric‑led care, unless the risks of transfer outweigh the benefits.',   ''),                                      " +
            " (64 ,'Check proteinura', 'Assess signs and symptoms of Severe pre-eclampsia',   ''),                                        " +
            " (65 ,'Take blood sample', 'Take blood sample for liver function, serum creatinine and platelet count',   ''),                                       " +
            //" (66 ,'Abnormal systolic blood pressure',    '',   ''),                        " +
            " (67 ,'Abnormal fetal heart rate retacted', 'Normal fetal heart rate. Return to usual care.',   ''),                      " +
            " (68 ,'Continuous fetal cardiotography', 'Offer continous cardiotography for 20min',   ''),                          " +
            " (69 ,'Delivery for suspect of fetal distress', 'Prepare for newborn resuscitation.\nExplain the situation to the woman and her companion that baby may not be well.\nExpedite birth',   ''),                  " +
            " (70 ,'Abnormal fetal heart rate', 'Perform fetal resuscitation:\n.Non-supine position\n.Correct dehydration\n.Tocolytic if in labour\n.Amnioinfusion for persistent variable decelerations\n.No supplemental oxygen\n.Talk to the woman and her companion\n.Inform a senior midwife or an obstetrician',   ''),                               " +
            " (71 ,'Fever', 'Encourage rest. Ensure adequate hydration PO/IV. Use fan or tepid sponge, and if necessary open a window. Give paracetamol 500- 1000mg (oral tablets or rectal suppositories) every 6-8h (max. 4000mg in 24hours). Take history, perform examination. Assess maternal temperature, heart rate, respiratory rate, oxygen saturation, blood pressure, level of consciousness. Ask person, partner or carer about urinary output in past 18 hours.Assess colour of skin, for mottled or ashen appearance, cyanosis or rash. Assess fetal wellbeing if intrapartum.',   ''),                                                   " +
            " (72 ,'Investigate infectious causes of raised temperature.',    '',   ''),    " +
            " (73 ,'Passive rewarming',    'Passive external rewarming with continuous core temperature monitoring',   ''),                                       " +
            " (74 ,'Ensure delivery room temperature',    'Ensure delivery room temperature is >=25°C.',   ''),                        " +
            " (75 ,'Severe hypotermia and cardiac arrest',    'Call for high level of assistancy. SELMA will not give adivices in this case.',   ''),                    " +
            " (76 ,'Moderate to severe hypotermia',    'Supportive treatment (early endotracheal intubation) + warmed IV fluids + active maternal rewarming + humidified oxygen with continuous core temperature monitoring ',   ''),                           " +
            " (77 ,'Give antibiotics',    'Give a combination of antibiotics until the woman gives birth(page C-49):- ampicillin 2 g IV every six hours;- PLUS gentamicin 5 mg/kg body weight IV every 24 hours. Consider delivery in consultation with senior decision maker.',   ''),                                       " +
            " (78 ,'Urine tract infection',    'Urine dipstick, microscopy and culture. Give amoxicillin 500mg PO every 8 hours for 3 days. ',   ''),                                   " +
            " (79 ,'Pyelonephritiis',    'Ampicillin 2g IV every 6 hours + Gentamicin 5mg/kg IV every 24 hours.\nOnce fever free for 48hours, change IV to PO Amoxicillin 1g PO every 8 hours for 14 days.',   ''),                                         " +
            " (80 ,'Pneumonia',    'CXR. Erythromycin 500mg PO every 6h for 7 days.  Give steam inhalation. Test for TB in endemic area.',   ''),                                               " +
            " (81 ,'Uncomplicated malaria',    'Rapid antigen detection test or microscopy. Treat with artemether (80 mg) PLUS lumefantrine (480 mg) twice daily for 3 days PO',   ''),                                   " +
            " (82 ,'Severe malaria',    'Give loading dose IV artesunate 2.4mg/kg body weight every 12h for 24h. Maintenance dose IV 1.2mg/kg once daily until woman is conscious and able to swallow. Then 2mg/kg artesunate PO once daily to complete 7 days of treatment. ',   ''),                                          " +
            " (83 ,'Other souce of infection',    'Tailor treatment to source',   ''),                                " +
            " (84 ,'Laboratory test for sepsis',    'FBC, U&Es, CRP, clotting. VBG for lactate & glucose. Blood cultures.',   ''),                              " +
            " (85 ,'High risk of severe illness or death from sepsis', 'Urgent review by senior clinical decision maker. Take bloods: FBC, U&Es, CRP, clotting. VBG for lactate & glucose. Blood cultures, vaginal swab, SMU. Give IV antibiotics and fluids within 1 hour. Monitor all vital signs continuously or at least every 30 minutes.',   ''),        " +
            " (86 ,'Moderate to severe risk of illness or death from sepsis', 'Take bloods: FBC, U&Es, CRP, clotting. A/VBG and blood cultures. Review results within 1 hour. Monitor all vital signs at least hourly.',   ''), " +
            " (87 ,'Check venous plasma glucose',    'Check if random venous plasma glucose ≥11.1mmol/L OR fasting venous plasma glucose ≥ 7 mmol/L.',   ''),                             " +
            " (88 ,'Undiagnosed diabetes in labour suspicion',    ' Request medical review glycaemia. Schedulle capillary glucose monitoring for 1 hour. Consider DKI drip if hyperglycaemia. Test for ketonaemia if hyperglycaemia',   ''),              " +
            " (89 ,'Ketones present',    'Assess the woman for signs of dehydration',   ''),                                          " +
            " (90 ,'Encourage woman to increase oral intake', '',   ''),                  " +
            " (91 ,'Give IV fluids',    '',   ''),                                          " +
            " (92 ,'Replace lost volume',    '',   ''),                                     " +
            " (93 ,'Catetherization',    'Assess for signs of dehydration, blood loss and alscutate lungs',   ''),                                         " +
            " (94 ,'Rales on lungs',    'Do not give intravenous fluids.\n Request medical review to assess for previously undiagnosed cardiac or chronic renal disease.\n Referral for specialist input if suspected cardiac/renal disease.',   ''),                                          " +
            " (95 ,'Anti-emetics',    'Consider anti-emetics +/- stool culture and sensitivity. Give IV fluids.',   ''),                                            " +
            " (96 ,'Encourage oral fluids intake',    '',   ''),                             " +
            " (97 ,'Sent PCR to confirm proteinura', 'Send PCR to confirm signficant protienuria and send urine for microscopy, culture, sensitivity.',   ''),                          " +
            " (98 ,'Labour arrest',    '',   ''),                                           " +
            " (99 ,'C-section indication',    'Due the risk factors, there is an indication to perform a c-section.',   ''),                                    " +
            " (100   ,'Collect fetal blood sample',    '',   ''),                           " +
            " (101   ,'Stop oxytocin',    'Prop up the woman or place her on her left side.\nTerbutaline 0.25 mg SC. Request human specialist opinion.',   ''),                                        " +
            " (102   ,'Reduce oxytocin rate',    'Reduce infusion rate (-5drops/min)',   ''),                                 " +
            " (103   ,'Maintain oxytocin rate',    '',   ''),                               " +
            " (104   ,'Increase oxytocin rate',    'Increase infusion rate (+5drops/min)',   ''),                               " +
            " (105   ,'Start oxytocin',    'Dilute 5IU of oxytocin (1 amp) in 500ml of crystalloid solution. Initial infusion rate:5 drops/min or 15mL/h ',   ''),                                       " +
            " (106   ,'Craniotomy',    '',   ''),                                           " +
            " (107   ,'Review coping stategies',    'Change woman position.\nProvide analgesia.\nReview hydration.',   ''),                              " +
            " (108   ,'Offer amniotomy',    '',   ''),                                      " +
            " (109   ,'Check penicillin alergy',    'Antibiotic treatment: Cefazolin 2g IV every 8 hours and gentamicin 2mg/kg IV load followed by 1.5mg/kg every 8 hours or 5mg/kg IV every 24 hours',   ''),                              " +
            //" (109   ,'With penicillin alergy',    'Antibiotic treatment: Cefazolin 2g IV every 8 hours and gentamicin 2mg/kg IV load followed by 1.5mg/kg every 8 hours or 5mg/kg IV every 24 hours',   ''),                              " +
            //" (109   ,'WIthout penicillin alergy',    'Antipecilin 2g IV every 6 hours and gentamicin 2mg/kg IV load followed by 1.5 mg/kg every 8 hours or 5mg/kg IV every 24 hours given until no fever for 24-48 hours.',   ''),                              " +
            " (110   ,'Correct anaemia',    '',   ''),                                       " +
            " (111   ,'Correct coagulopathy',    '',   ''),                                 " +
            " (112   ,'Expedite birth',    'Due the risk factors, consider actions to expedite birth.',   ''),                                       " +
            " (113   ,'Fetal distress suspicion',    '',   ''),                              " +
            " (114   ,'Slow fetal descent',    '',   '') ,                                  " +
            " (116   ,'Other source of infection',    '',   ''),                                " +
            " (117   ,'Consider DKI drip and test for ketonaemia',    'Test for ketonaemia if hyperglycaemia',   ''),                                " +
            " (118   ,'Fetal distress susption',    '',   ''),                                " +
            " (119   ,'Request medical review',    '',   ''),                                " +
            " (120   ,'Hypertension', 'Assess signs and symptoms of Severe PE AND in facilities with laboratory capacity: take blood sample for liver function, serum creatinine and platelet count',   ''),                                " +
            " (121   ,'Hypotension', 'Tilted woman to the left or place a pillow or folded linen under the woman’s right lower back, to decrease supine hypotension syndrome',   ''),                                " +
            " (122   ,'Undiagnosed diabetes in labour suspition',    ' Request medical review for glycaemia. Schedulle capillary glucose monitoring for 1 hour. ',   ''),        " +
            " (123   ,'High risk of illness or death from bradycardia', 'Call high level assistance and request immediate review.\nTransfer woman to obstetric‑led care, unless the risks of transfer outweigh the benefits. ',   ''), " +
            " (124   ,'Moderate to High risk of illness or death from bradycardia','Access possible causes of bradycardia (bleeding, pain, hypoxia, shortness of breath, medications, aminiotic fluid embolism)',   '')," +
            " (123   ,'High risk of illness or death from tachycardia', 'Call high level assistance and request immediate review.\nTransfer woman to obstetric‑led care, unless the risks of transfer outweigh the benefits. ',   ''), " +
            " (124   ,'Moderate to High risk of illness or death from tachycardia','Access possible causes of tachycardia (fever, foul-smelling vaginal discharge, bleeding, pain)', '') " +
            " , (125   ,'Pre-eclampsia', 'Call for urgent medical review.\nConsider antihypertensive: labetalol (oral or intravenous) OR oral nifedipine OR intravenous hydralazine.\nConsider magnesium sulfate: a loading dose of 4 g should be given intravenously over 5 to 15 minutes, followed by an infusion of 1 g/hour maintained for 24 hours.',   '')                                " +
            " , (115   ,'Severe pre-eclampsia', 'Call for urgent medical review.\nGive antihypertensive: labetalol (oral or intravenous) OR oral nifedipine OR intravenous hydralazine .\nGive magnesium sulfate: a loading dose of 4 g should be given intravenously over 5 to 15 minutes, followed by an infusion of 1 g/hour maintained for 24 hours..\nFHR monitoring.\nRepeat full set of observations within 15 min or monitor continuously.\nConsider expediting delivery',   '') " +
            " , (126   ,'High risk of illness or death from hypotension', 'Call high level assistance and request immediate review\nTransfer woman to obstetric‑led care, unless the risks of transfer outweigh the benefits. ',   '')                                " +
            " , (127   ,'Moderate to High risk of illness or death from hypotension', 'Repeat full set of observations within 15 min or monitor continuosly.\nFHR monitoring.\nGive IV fluids rapidly (500 ml).\nConduct full history and examination of woman.\nInvestigate possible causes of hypotension',   '')                                " +
            " , (128   ,'Fetal scalp pH or lactate', '',   '')                                " +
            " , (129   ,'Assess for acute kidney injury',    'Request medical review to assess for acute kidney injury (AKI) and consider specialist referral.\nSend blood for renal function tests and repeat as needed.\nAssess for and correct possible causes of AKI.\n Individualise plan for fluid balance to avoid dehydration and pulmonary edema.\nConsider giving single small bolus of fluid if dehydrated.\nDo not give nephrotoxic drugs (NSAIDS) in labour.',   '')                                " +
            " , (130   ,'Reassess urine output in 2 hours',    'Due to oliguria susption, reassess urine output in 2 ours. SELMA will send an alert to remind you.',   '')                                " +
            " , (131   ,'Oliguria suspicion',    '',   '')                                " +
            // " , (132   ,'',    '',   '')                                " +
            // " , (133   ,'',    '',   '')                                " +
            // " , (134   ,'',    '',   '')                                " +
            // " , (135   ,'',    '',   '')                                " +
            ";"
           ,
           typeFields: "INSERT INTO `TypeField` (`id`, `type_name`) VALUES" +
               "(1, 'integer'), " +
               "(2, 'float'  ), " +
               "(3, 'options'), " +
               "(4, 'datetime')," +
               "(5, 'date')    ," +
               "(6, 'time'),    " +
               "(7, 'text'),    " +
               "(8, 'password') " 
           ,  
           settings: "INSERT INTO `Settings` (`id`, `name`, `label`, `value`) VALUES      " +
               " (1, 'server_ip', 'Server IP address', 'http://200.144.254.4:3333/api/'), " +
               " (2, 'continuous_monitoring', 'Continuous monitoring?', '1'),               " +
               " (3, 'ctg_available', 'CTG available?', '1'),                             " +
               " (4, 'laboratory_capacity', 'Laboratory capacity', '1'),                  " +
               " (5, 'fetal_scalp_lactate', 'Fetal scalp pH or lactate available', '1'),  " +
               " (6, 'dipstick_available', 'Dipstick available', '1')                     " 
            ,
            diagnosis: "INSERT INTO `diagnosis` (`id`, `name`, `label`, `risk_level`) VALUES " +
            "(1   ,'elegible'                    ,'Eligible'                     , 0),       " +
            "(2   ,'active_phase'                ,'Active Phase'                 , 1),       " +
            "(5   ,'long_latent_phase'           ,'Long latent phase'            , 2),       " +
            "(7   ,'second_stage'                ,'2nd stage'                    , 2),       " +
            "(8   ,'normal_mhr'                  ,'Normal MHR'                   , 0),       " +
            "(19  ,'bradycardia'                 ,'Bradycardia'                  , 3),       " +
            "(20  ,'severe_bradycardia'          ,'Severe bradycardia'           , 3),       " +
            "(21  ,'continuous_monitoring'        ,'Continuous monitoring'         , 3),       " +
            "(22  ,'intensified_monitoring'      ,'Intensified monitoring'       , 3),       " +
            "(23  ,'tachycardia'                 ,'Tachycardia'                  , 3),       " +
            "(24  ,'severe_tachycardia'          ,'Severe tachycardia'           , 3),       " +
            "(25  ,'need_high_assistance'        ,'High assistance'              , 3),       " +
            "(26  ,'continuous_cardiotography'    ,'Continuous CTG'                , 3),       " +
            "(27  ,'hypotension'                 ,'Hypotension'                  , 3),       " +
            "(28  ,'severe_hypotension'          ,'Severe hypotension'           , 3),       " +
            "(29  ,'hypertension'                ,'Hypertension'                 , 3),       " +
            "(30  ,'severe_hypertention'         ,'Severe hypertention'          , 3),       " +
            "(31  ,'abnormal_fhr'                ,'Abnormal FHR'                 , 3),       " +
            "(32  ,'fetal_distress'              ,'Fetal distress'               , 3),       " +
            "(33  ,'time_to_delivery'            ,'Time to delivery'             , 3),       " +
            "(34  ,'fever'                       ,'Fever'                        , 3),       " +
            "(35  ,'abnormal_temperature'        ,'Abnormal temperature'         , 3),       " +
            "(36  ,'investigate_infection_causes','Investigate infection causes' , 3),       " +
            "(37  ,'fever_suspicion'             ,'Fever suspicion'              , 2),       " +
            "(38  ,'mild_hypotermia'             ,'Mild hypotermia'              , 3),       " +
            "(39  ,'severe_hypotermia'           ,'Severe hypotermia'            , 3),       " +
            "(40  ,'out_of_selma'                ,'Out of selma'                 , 3),       " +
            "(41  ,'moderate_hypotermia'         ,'Moderate hypotermia'          , 3),       " +
            "(42  ,'chorioamnionitis_amnionitis' ,'Chorioamnionitis amnionitis'  , 3),       " +
            "(43  ,'urine_infection'             ,'Urine infection'              , 3),       " +
            "(44  ,'pyelonephritiis'             ,'Pyelonephritiis'              , 3),       " +
            "(45  ,'pneumonia'                   ,'Pneumonia'                    , 3),       " +
            "(46  ,'uncomplicated_malaria'       ,'Uncomplicated malaria'        , 3),       " +
            "(47  ,'severe_malaria'              ,'Severe malaria'               , 3),       " +
            "(48  ,'other_source'                ,'Other source'                 , 3),       " +
            "(49  ,'infection_laboratory_test'   ,'Infection'                    , 3),       " +
            "(50  ,'sepsis_high_risk'            ,'Sepsis high risk'             , 3),       " +
            "(51  ,'sepsis_moderate_risk'        ,'Sepsis moderate risk'         , 2),       " +
            "(52  ,'glycosuria'                  ,'Glycosuria'                   , 3),       " +
            "(53  ,'diabetes_labour_suspicion'   ,'Diabetes suspicion'           , 2),       " +
            "(54  ,'glycosuria_suspicion'        ,'Glycosuria suspicion'         , 3),       " +
            "(55  ,'ketones'                     ,'Ketones'                      , 3),       " +
            "(56  ,'ketones_present'             ,'Ketones present'              , 3),       " +
            "(57  ,'iv_fluids'                   ,'Iv_fluids'                    , 3),       " +
            "(58  ,'ketones_vomiting'            ,'Vomiting'                     , 3),       " +
            "(59  ,'ketones_diarrhoea'           ,'Diarrhoea'                    , 3),       " +
            "(60  ,'oliguria_suspicion'          ,'Oliguria suspicion'           , 3),       " +
            "(61  ,'oliguria_catetherization'    ,'Catetherization'              , 3),       " +
            "(62  ,'oliguria_rales_lungs'        ,'Rales lungs'                  , 3),       " +
            "(63  ,'oliguria_vomitting_diarrhoea','Vomitting diarrhoea'          , 3),       " +
            "(64  ,'oliguria_reduced_intake'     ,'Reduced intake'               , 3),       " +
            "(65  ,'oliguria_able_intake'        ,'Oliguria'                     , 3),       " +
            "(66  ,'proteinura'                  ,'Proteinura'                   , 3),       " +
            "(67  ,'labour_arrest'               ,'Labour arrest'                , 3),       " +
            "(68  ,'slow_progress'               ,'Slow progress'                , 3),       " +
            "(69  ,'meconium'                    ,'Meconium'                     , 3),       " +
            "(70  ,'hypercontraction'            ,'Hypercontraction'             , 3),       " +
            "(71  ,'strong_contractions'         ,'Strong contractions'          , 3),       " +
            "(72  ,'hypocontraction'             ,'Hypocontraction'              , 3),       " +
            "(73  ,'oxytocin'                    ,'Oxytocin'                     , 3),       " +
            "(74  ,'craniotomy'                  ,'Craniotomy'                   , 3),       " +
            "(75  ,'slow_but_normal_cd'          ,'Slowbut normal'               , 3),       " +
            "(76  ,'ruptured_membranes'          ,'Ruptured membranes'           , 2),       " +
            "(77  ,'long_ruptured_membranes'     ,'Long ruptured membranes'      , 3),       " +
            "(78  ,'purulent_af_fever'           ,'Purulent AF'                  , 3),       " +
            "(79  ,'af_bleeding'                 ,'AF bleeding'                  , 3),       " +
            "(80  ,'vaginal_bleeding'            ,'Vaginal bleeding'             , 3),       " +
            "(81  ,'anaemia'                     ,'Anaemia'                      , 3),       " +
            "(82  ,'coagulopathy'                ,'Coagulopathy'                 , 3),       " +
            "(83  ,'fetal_distress_suspicion'    ,'Fetal distress suspicion'     , 3),       " +
            "(84  ,'slow_fetal_descent'          ,'Slow fetal descent'           , 3),       " +
            "(85  , 'severe_pre_eclampsia'       ,'Severe pre-eclampsia'         , 3),       " +
            "(86  , 'robson1'                    , 'Robson 1'                    , 1),       " +
            "(87  , 'robson2'                    , 'Robson 2'                    , 1),       " +
            "(88  , 'robson3'                    , 'Robson 3'                    , 1),       " +
            "(89  , 'robson4'                    , 'Robson 4'                    , 1),       " +
            "(90  , 'robson5'                    , 'Robson 5'                    , 1),       " +
            "(91  , 'robson6'                    , 'Robson 6'                    , 1),       " +
            "(92  , 'robson7'                    , 'Robson 7'                    , 1),       " +
            "(93  , 'robson8'                    , 'Robson 8'                    , 1),       " +
            "(94  , 'robson9'                    , 'Robson 9'                    , 1),       " +
            "(95  , 'robson10'                   , 'Robson 10'                   , 1),       " +
            "(96  , 'robson99'                   , 'Robson missing'              , 1)        " +
            ",(97  , 'pre-eclampsia'             , 'Pre-eclampsia'               , 1)        " +
            ",(98  , 'pre-eclampsia'             , 'standard_monitoring'         , 1)        " +
            ";"
  
        }
}



