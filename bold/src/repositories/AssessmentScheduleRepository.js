import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase, diffTimeNowInMinutes, getIdFromDateTime } from './BaseRepository';
import assessmentsFormsRepository from "./assessmentsFormsRepository";
import PushNotification from 'react-native-push-notification';
import moment from 'moment';
import WomanRepository from "./WomanRepository";

export default class AssessmentScheduleRepository {

    static getNextId(){
        
        return new Promise((resolve, reject) => {
            resolve(getIdFromDateTime());
        })
        // return new Promise((resolve, reject) => {
        //     database.getDatabase().then(db => { 
        //         db.transaction((tx) => {            
        //             tx.executeSql('Select max(id) as max_id from AssessmentSchedule', [], (tx, results) => {
        //                 var max_id = 1;
        //                 if(results.rows.length > 0 && results.rows.item(0).max_id != null){
        //                     max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
        //                 }
        //                 resolve(max_id)
        //             },
        //             (tx, err) => {
        //                 //console.log("erro: " + JSON.stringify(err));
        //                 reject(err);
        //             });
        //         });
        //     });
        // })
    }

    static insertAssessmentSchedule(AssessmentSchedule){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into AssessmentSchedule (id , schedule_time , assessments_forms_id, admission_id, filled_in, createdAt, updatedAt)'+
                    ' values (?, ?, ?, ?, ?, ?, ?)', 
                        [(parseInt(next_id) + parseInt(AssessmentSchedule.assessments_forms_id)), AssessmentSchedule.schedule_time , 
                            AssessmentSchedule.assessments_forms_id, AssessmentSchedule.admission_id, AssessmentSchedule.filled_in,
                            getCurrentTimeToDatabase(),getCurrentTimeToDatabase()], (tx, results) => {
                                AssessmentSchedule.id = next_id;
                                resolve(AssessmentSchedule);
                            },
                            (tx, err) => {
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateAssessmentSchedule(AssessmentSchedule){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update AssessmentSchedule set schedule_time  = ?, assessments_forms_id = ?, admission_id = ?, filled_in = ?, updatedAt = ? where id = ?', 
                        [ AssessmentSchedule.schedule_time , AssessmentSchedule.assessments_forms_id, AssessmentSchedule.id, 
                            AssessmentSchedule.filled_in, AssessmentSchedule.admission_id, getCurrentTimeToDatabase()], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAssessmentSchedule(AssessmentSchedule_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from AssessmentSchedule where id = ?', [AssessmentSchedule_id], (tx, results) => {
                        var AssessmentSchedule = results.rows.item(0);
                        resolve(AssessmentSchedule)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAssessmentScheduleById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , schedule_time , assessments_forms_id, admission_id, filled_in, createdAt, updatedAt from AssessmentSchedule where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var AssessmentSchedule = results.rows.item(0);                    
                        resolve(AssessmentSchedule)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllAssessmentSchedule(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, schedule_time , assessments_forms_id, admission_id, filled_in, createdAt, updatedAt from AssessmentSchedule  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static getAllAssessmentScheduleByAdmissionId(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select ash.id, ash.schedule_time , ash.assessments_forms_id, ash.admission_id, ash.filled_in, ash.createdAt, ash.updatedAt, af.name, af.label_form ' + 
                        ' from AssessmentSchedule as ash ' + 
                        ' join assessmentsForms af on assessments_forms_id = af.id ' + 
                        ' where admission_id = ? and filled_in = 0 and DATETIME(ash.schedule_time) < DATETIME(\'' + getCurrentTimeToDatabase() + '\') ' +
                        ' order by priority, schedule_time  '
                        , [admission_id], (tx, results) => {
                            //console.log(JSON.stringify(results.rows.item(0)));
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static getLastAssessmentScheduleByAdmissionId(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select ash.id, ash.schedule_time , ash.assessments_forms_id, ash.admission_id, ash.filled_in, ash.createdAt, ash.updatedAt ' + 
                        ' from AssessmentSchedule as ash ' + 
                        ' join assessmentsForms af on assessments_forms_id = af.id ' + 
                        ' where admission_id = ? and filled_in = 0 ' + 
                        ' order by priority, schedule_time  '
                        , [admission_id], (tx, results) => {
                            //console.log(JSON.stringify(results.rows.item(0)));
                            if(results.rows.length > 0)
                            {
                                var AssessmentSchedule = results.rows.item(0);                    
                                resolve(AssessmentSchedule);
                            }
                            else
                            {                    
                                resolve(undefined);
                            }
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }
    
    static setAssessmentScheduleAsFilledIn(scheduled_id){      
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update AssessmentSchedule set filled_in = 1 where id = ?', 
                        [ scheduled_id ], 
                        (tx, results)  => {  
                            //console.log(results)
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static setAssessmentScheduleAsCanceledByAdmissionId(assessments_forms_id,admission_id){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update AssessmentSchedule set filled_in = 2 where assessments_forms_id = ? and admission_id = ?', 
                        [ assessments_forms_id,admission_id ], 
                        (tx, results)  => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static insertScheduledFormFromName(assessmentFormName, admissionId, intervalMinutes = 0)
    {        
        return assessmentsFormsRepository.getassessmentsFormIdByName(assessmentFormName).then((data)=> {
            var schedule = { 
                schedule_time : getCurrentTimeToDatabase(intervalMinutes), 
                assessments_forms_id: data.id, 
                admission_id: admissionId,
                filled_in: 0
            };
            
            var label = data.label_form;
            this.insertAssessmentSchedule(schedule).then((data)=> {
                WomanRepository.getWomanByAdmissionId(admissionId).then((woman) => {
                    var name = '';
                    if(woman != undefined && woman.name.indexOf(' ') > 0){
                        name = woman.name.split(' ')[0];
                    }
                    else{
                        name = woman.name;
                    }

                    PushNotification.localNotificationSchedule({
                        id: data.id,
                        message: "Time to fill in form " + label + " for patient " + name, // (required)
                        date: new Date(Date.now() + (intervalMinutes * 60000)) // in 60 secs
                    });
                })
            });            
        });
    }

    static getLastScheduleForms(admissionId) {
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('select assessments_forms_id from AssessmentSchedule '+
                    ' where admission_id = ? and filled_in = 0 and schedule_time  > DATETIME(?, \'-3 minutes\')' + 
                    ' group by assessments_forms_id '+
                    ' HAVING (count(assessments_forms_id)>1)'
                    , [admissionId, diffTimeNowInMinutes], (tx, results) => {
                        var len = results.rows.length;
                        var data = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            data.push(row);
                        }     
                        resolve(data);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    }
                )
                });
            });
        });
    }

    static getScheduledDuplicatedFormsIdByAdmissionId(admissionId){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql(
                    'Select id From AssessmentSchedule                                                                                             '+
                    ' where id NOT IN                                                                                                              '+
                    '     (select min(ass.id) from AssessmentSchedule as ass                                                                       '+
                    '     inner join                                                                                                               '+
                    '     (select assessments_forms_id, count(assessments_forms_id) countForms from AssessmentSchedule                             '+
                    '       where admission_id = ? and filled_in = 0                                                                               '+ 
                    '       group by assessments_forms_id                                                                                          '+
                    '       HAVING (count(assessments_forms_id)>1)) ass2 on ass.assessments_forms_id = ass2.assessments_forms_id                   '+
                    '       where admission_id = ? and filled_in = 0                                                                               '+
                    '       group by ass.assessments_forms_id                                                                                      '+
                    ' UNION                                                                                                                        '+
                    '         select min(ass.id) from AssessmentSchedule as ass                                                                    '+          
                    '                  inner join                                                                                                  '+
                    '                  (select assessments_forms_id, count(assessments_forms_id) countForms from AssessmentSchedule                '+
                    '                    where admission_id = ? and filled_in = 0                                                                  '+
                    '                    group by assessments_forms_id                                                                             '+
                    '                    HAVING (count(assessments_forms_id)=1)) ass2 on ass.assessments_forms_id = ass2.assessments_forms_id      '+
                    '                    where admission_id = ? and filled_in = 0                                                                  '+
                    '                    group by ass.assessments_forms_id )                                                                       '+                                                                                            
                    '       and admission_id = ? and filled_in = 0                                                                                 '
                    , [admissionId, admissionId, admissionId, admissionId, admissionId], (tx, results) => {
                        var len = results.rows.length;
                        var data = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            data.push(row);
                        }     
                        resolve(data);
                    },
                    (tx, err) => {
                        reject(err);
                    }
                )
                });
            });
        });
    }

    static cancelDuplicatedFormsByAdmissionId(admissionId){
        return new Promise((resolve, reject) => {
            this.getScheduledDuplicatedFormsIdByAdmissionId(admissionId).then(arrayId => {
                arrayId.forEach(id => {
                    //console.log(id);
                    PushNotification.cancelLocalNotifications({id: id});
                });
            }).then(()=>{
                //console.log("cancelDuplicatedFormsByAdmissionId");
                database.getDatabase().then(db => { 
                    db.transaction((tx) => {
                        tx.executeSql(
                        'UPDATE AssessmentSchedule set filled_in = 2                                                                                   '+
                        ' where id NOT IN                                                                                                              '+
                        '     (select min(ass.id) from AssessmentSchedule as ass                                                                       '+
                        '     inner join                                                                                                               '+
                        '     (select assessments_forms_id, count(assessments_forms_id) countForms from AssessmentSchedule                             '+
                        '       where admission_id = ? and filled_in = 0                                                                               '+ 
                        '       group by assessments_forms_id                                                                                          '+
                        '       HAVING (count(assessments_forms_id)>1)) ass2 on ass.assessments_forms_id = ass2.assessments_forms_id                   '+
                        '       where admission_id = ? and filled_in = 0                                                                               '+
                        '       group by ass.assessments_forms_id                                                                                      '+
                        ' UNION                                                                                                                        '+
                        '         select min(ass.id) from AssessmentSchedule as ass                                                                    '+          
                        '                  inner join                                                                                                  '+
                        '                  (select assessments_forms_id, count(assessments_forms_id) countForms from AssessmentSchedule                '+
                        '                    where admission_id = ? and filled_in = 0                                                                  '+
                        '                    group by assessments_forms_id                                                                             '+
                        '                    HAVING (count(assessments_forms_id)=1)) ass2 on ass.assessments_forms_id = ass2.assessments_forms_id      '+
                        '                    where admission_id = ? and filled_in = 0                                                                  '+
                        '                    group by ass.assessments_forms_id )                                                                       '+                                                                                            
                        '       and admission_id = ? and filled_in = 0                                                                                 '
                        , [admissionId, admissionId, admissionId, admissionId, admissionId], (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                        //console.warn(tx);
                            reject(err);
                        }
                    )
                    });
                 });
            });
        });
    }

    static cancelScheduledFormFromNameAndAdmissionId(assessmentFormName, admissionId)
    {        
        assessmentsFormsRepository.getassessmentsFormIdByName(assessmentFormName)
        .then((data)=> {
            this.setAssessmentScheduleAsCanceledByAdmissionId(data.id, admissionId);
            
        });
    }

    
    static backToTheFutureScheduleForm(minutes){        
        //console.log(minutes);
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update AssessmentSchedule set schedule_time = datetime(schedule_time, \'-'+ minutes + ' minutes\')', 
                        [], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
