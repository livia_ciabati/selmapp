import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase } from './BaseRepository';

export default class AdmissionRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Select max(id) as max_id from admission', [], (tx, results) => {
                        var max_id = 1;
                        if(results.rows.length > 0 && results.rows.item(0).max_id != null){
                            max_id = Number.parseInt(results.rows.item(0).max_id, 10) + 1;
                        }
                        resolve(max_id)
                    },
                    (tx, err) => {
                        //console.log("erro: " + JSON.stringify(err));
                        reject(err);
                    });
                });
            });
        })
    }

    static insertAdmission(admission){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into admission (id , mode_onset_labour, hours_labour_before_admission, fetal_movements_last_2_hours, first_stage_labour, fetal_death, advanced_first_stage_labour, multiple_pregnancy, '+
                        'gestational_age_less_34_week, elective_c_section, pre_labour_c_section, emergency_c_section_or_laparotomy, attemped_labour_induction, false_labour, non_emancipated_minors_without_guardian, ' + 
                        'unable_give_consent_or_health_problem, elegible, symphysis_fundal_height, abdominal_circumference, pregnancy_id, createdAt, updatedAt, userBoldId, admission_status)'+
                    ' values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id , admission.mode_onset_labour, admission.hours_labour_before_admission, 
                            admission.fetal_movements_last_2_hours, admission.first_stage_labour, admission.fetal_death, 
                            admission.advanced_first_stage_labour, admission.multiple_pregnancy, 
                            admission.gestational_age_less_34_week, admission.elective_c_section, 
                            admission.pre_labour_c_section, 
                            admission.emergency_c_section_or_laparotomy, admission.attemped_labour_induction, 
                            admission.false_labour, admission.non_emancipated_minors_without_guardian, 
                            admission.unable_give_consent_or_health_problem, admission.elegible, 
                            admission.symphysis_fundal_height,  admission.abdominal_circumference, admission.pregnancy_id, 
                            getCurrentTimeToDatabase(), getCurrentTimeToDatabase(), 
                            admission.userBoldId, 1], (tx, results) => {
                                //console.log("insertWoman" + JSON.stringify(admission));
                                admission.id = results.insertId;
                                //console.log("insertWoman" + JSON.stringify(admission));
                                resolve(admission);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            }
                        )
                    });
                });    
            });        
        });
    }

    static updateAdmission(admission){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admission set mode_onset_labour = ?, hours_labour_before_admission = ?, fetal_movements_last_2_hours = ?, first_stage_labour = ?, fetal_death = ?, advanced_first_stage_labour = ?,' +
                    ' multiple_pregnancy = ?, gestational_age_less_34_week = ?, elective_c_section = ?, pre_labour_c_section = ?, emergency_c_section_or_laparotomy = ?, attemped_labour_induction = ?, false_labour = ?, ' +  
                    ' non_emancipated_minors_without_guardian = ?, unable_give_consent_or_health_problem = ?, elegible = ?, symphysis_fundal_height = ?, abdominal_circumference = ?, pregnancy_id = ?, updatedAt = ?, '+
                    ' userBoldId = ?, admission_status = ? where id = ?', 
                        [ admission.mode_onset_labour,admission.hours_labour_before_admission,admission.fetal_movements_last_2_hours,admission.first_stage_labour,
                            admission.fetal_death,admission.advanced_first_stage_labour,admission.multiple_pregnancy,admission.gestational_age_less_34_week,
                            admission.elective_c_section,admission.pre_labour_c_section,admission.emergency_c_section_or_laparotomy,
                            admission.attemped_labour_induction,admission.false_labour,admission.non_emancipated_minors_without_guardian,
                            admission.unable_give_consent_or_health_problem,admission.elegible,admission.symphysis_fundal_height,
                            admission.abdominal_circumference,admission.pregnancy_id,getCurrentTimeToDatabase(),admission.userBoldId,
                            admission.admission_status, admission.id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteAdmission(admission_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from admission where id = ?', [admission_id], (tx, results) => {
                        var admission = results.rows.item(0);
                        resolve(admission)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAdmissionById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , mode_onset_labour, hours_labour_before_admission, fetal_movements_last_2_hours, first_stage_labour, fetal_death, advanced_first_stage_labour, multiple_pregnancy, gestational_age_less_34_week, elective_c_section, pre_labour_c_section, emergency_c_section_or_laparotomy, attemped_labour_induction, false_labour, non_emancipated_minors_without_guardian, unable_give_consent_or_health_problem, elegible, symphysis_fundal_height, abdominal_circumference, pregnancy_id, createdAt, updatedAt, userBoldId, admission_status from admission where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var admission = results.rows.item(0);                    
                        resolve(admission)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAdmissionByPregnancyId(pregnancy_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , mode_onset_labour, hours_labour_before_admission, fetal_movements_last_2_hours, first_stage_labour, fetal_death, advanced_first_stage_labour, multiple_pregnancy, gestational_age_less_34_week, elective_c_section, pre_labour_c_section, emergency_c_section_or_laparotomy, attemped_labour_induction, false_labour, non_emancipated_minors_without_guardian, unable_give_consent_or_health_problem, elegible, symphysis_fundal_height, abdominal_circumference, pregnancy_id, createdAt, updatedAt, userBoldId, admission_status from admission where pregnancy_id = ' + pregnancy_id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var admission = results.rows.item(0);                    
                        resolve(admission)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllAdmission(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, mode_onset_labour, hours_labour_before_admission, fetal_movements_last_2_hours, first_stage_labour, fetal_death, advanced_first_stage_labour, multiple_pregnancy, gestational_age_less_34_week, elective_c_section, pre_labour_c_section, emergency_c_section_or_laparotomy, attemped_labour_induction, false_labour, non_emancipated_minors_without_guardian, unable_give_consent_or_health_problem, elegible, symphysis_fundal_height, abdominal_circumference, pregnancy_id, createdAt, updatedAt, userBoldId, admission_status from admission  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static finalizeAdmission(adm_id){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update admission set admission_status = ? where id = ?', 
                        [ 0, adm_id], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

}
