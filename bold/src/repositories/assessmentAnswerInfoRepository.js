import  { database }  from "./DatabaseManager";
import  { getCurrentTimeToDatabase, getIdFromDateTime } from './BaseRepository';


export default class assessmentAnswerInfoRepository {

    static getNextId(){
        return new Promise((resolve, reject) => {
            resolve(getIdFromDateTime());
        })
    }

    static insertassessmentAnswerInfo(assessmentAnswerInfo){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                this.getNextId().then((next_id) =>
                {
                    db.transaction((tx) => {
                    tx.executeSql('Insert into assessmentAnswerInfo (id , begin_fill_form, end_fill_form, assessment_form_id, admission_id, createdAt, updatedAt)'+
                    ' values ( ?, ?, ?, ?, ?, ?, ?)', 
                        [next_id, assessmentAnswerInfo.begin_fill_form,
                            assessmentAnswerInfo.end_fill_form,
                            assessmentAnswerInfo.assessment_form_id,
                            assessmentAnswerInfo.admission_id,
                            getCurrentTimeToDatabase(),
                            getCurrentTimeToDatabase()
                        ], (tx, results) => {
                            //console.log(results);
                                assessmentAnswerInfo.id = next_id;
                                resolve(assessmentAnswerInfo);
                            },
                            (tx, err) => {
                                //console.log("erro: " + JSON.stringify(err));
                                reject(err);
                            } 
                        )
                    });
                });    
            });        
        });
    }

    static updateassessmentAnswerInfo(assessmentAnswerInfo){        
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentAnswerInfo set begin_fill_form = ?, end_fill_form = ?, assessment_form_id = ?, admission_id = ?, updatedAt = ? where id = ?', 
                        [ assessmentAnswerInfo.begin_fill_form,assessmentAnswerInfo.end_fill_form,assessmentAnswerInfo.assessment_form_id, 
                            assessmentAnswerInfo.admission_id, assessmentAnswerInfo.id, getCurrentTimeToDatabase()], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    
    static updateAssessmentTimeEndFillAnswerInfo(info_id){    
        //console.log("****************updateAssessmentTimeEndFillAnswerInfo: " + info_id)    
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {
                    tx.executeSql('Update assessmentAnswerInfo set end_fill_form = ? where id = ?', 
                         [getCurrentTimeToDatabase(), info_id ], 
                        (tx, results) => {
                            resolve(results);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }

    static deleteassessmentAnswerInfo(assessmentAnswerInfo_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {            
                    tx.executeSql('Delete from assessmentAnswerInfo where id = ?', [assessmentAnswerInfo_id], (tx, results) => {
                        var assessmentAnswerInfo = results.rows.item(0);
                        resolve(assessmentAnswerInfo)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getassessmentAnswerInfoById(id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select id , begin_fill_form, end_fill_form, assessment_form_id, admission_id, createdAt, updatedAt from assessmentAnswerInfo where id = ' + id;        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var assessmentAnswerInfo = results.rows.item(0);                    
                        resolve(assessmentAnswerInfo)
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

    static getAllassessmentAnswerInfo(){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction(
                (tx) => {
                    tx.executeSql(
                        'select  id, begin_fill_form, end_fill_form, assessment_form_id, admission_id, createdAt, updatedAt from assessmentAnswerInfo  '
                        , [], (tx, results) => {
                            var len = results.rows.length;
                            var data = [];
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                data.push(row);
                            }     
                            resolve(data);
                        },
                        (tx, err) => {
                           //console.warn(tx);
                            reject(err);
                        }
                    )
                });
            });
        });
    }
    

    static getassessmentAnswerInfoByAdmissionId(adm_id){
        return new Promise((resolve, reject) => {
            database.getDatabase().then(db => { 
                db.transaction((tx) => {    
                    var query = 'Select info.id, info.begin_fill_form, info.end_fill_form, info.assessment_form_id, info.admission_id, '+
                    ' info.createdAt, info.updatedAt,' +
                    ' form.id, form.name, form.label_form ' +
                    ' from assessmentAnswerInfo info '+
                    ' join admission adm on info.admission_id = adm.id ' +
                    ' join assessmentsForms form on form.id = assessment_form_id ' + 
                    ' where admission_id = ' + adm_id + ' order by begin_fill_form desc ';        
                    tx.executeSql(query, 
                    [], (tx, results) => {
                        var len = results.rows.length;
                        var data = [];
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            data.push(row);
                        }     
                        resolve(data);
                    },
                    (tx, err) => {
                       //console.warn(tx);
                        reject(err);
                    });
                });
            });
        });
    }

}
