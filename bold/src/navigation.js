import {
  Navigation
} from 'react-native-navigation'

export const womanListLayout = (user_id) => Navigation.setRoot({
  root: {
    sideMenu: {
      id: 'sidemenu',
      center: {
        stack: {
          children: [{
							component: {
								id: "WomanList",
								name: 'WomanList',
								passProps: {
									user_id: user_id
								},
								options: {
									topBar: {
										title: {
											text: "Women in Ward"
										},
										rightButtons: [{
											id: 'sidebarButton',
											icon: require('../img/Icon-App-40x40.png'),       
											showAsAction: 'always',
											buttonFontSize: 14,
											buttonFontWeight: '600'
										}],
										visible: true
									}
								}
							},
						},
					],
        }
      },
      right: {
				id: 'sidemenu',
        component: {
          name: 'SideBar',
					passProps: {
						user_id: user_id
					},
        }
      }
    }
  }
});

export const tutorial = (user_id) => 
	Navigation.setRoot({
		root: {
				component: {
						name: 'Tutorial',
						passProps: {
							user_id: user_id,
							showRealApp: false
						},
				}
		}
});

export const womanLayout = (componenteId, wom_id, preg_id, adm_id, anc_id, disea_id, user_id) =>
	Navigation.push(componenteId, {
			component: {
				id: "WomanForm",
				name: 'WomanForm',
				passProps: {
					text: 'Teste',
					wom_id: wom_id, 
					preg_id: preg_id, 
					adm_id: adm_id,
					anc_id: anc_id,
					disea_id: disea_id,
					user_id: user_id
				},
				options: {
					topBar: {    
						title: {
							text: "Woman Form",
							visible: true
						},
						rightButtons: [{
							id: 'sidebarButton',
							icon: require('../img/Icon-App-40x40.png'),       
							showAsAction: 'always',
							buttonFontSize: 14,
							buttonFontWeight: '600'
						}],
						leftButtons: [{
							id: 'returnButton',
							//icon: require('../img/back.png'),
							text: "Return"
						}]
					}
				}
			}
});

export const labourOutcomeLayout = (componenteId, admission_id, woman_id, user_id) =>
	Navigation.push(componenteId, {
			component: {
				id: "LabourOutcome",
				name: 'LabourOutcome',
				passProps: {
					text: 'Labour Outcome',
					woman_id: woman_id,
					user_id: user_id,
					admission_id: admission_id
				},
				options: {
					topBar: {
						title: {
							text: "Labour Outcome",
							visible: true
						},
						rightButtons: [{
							id: 'sidebarButton',
							icon: require('../img/Icon-App-40x40.png'),       
							showAsAction: 'always',
							buttonFontSize: 14,
							buttonFontWeight: '600'
						}]
					}
				}
			}
	});

export const outcomeLayout = (componenteId, admission_id, woman_id, user_id) =>
	Navigation.push(componenteId, {
			component: {
				id: "OutcomeForm",
				name: 'OutcomeForm',
				passProps: {
					text: 'Pushed screen',
					woman_id: woman_id,
					user_id: user_id,
					admission_id: admission_id
				},
				options: {
					topBar: {
						title: {
							text: "Discharge Form",
							visible: true
						},
						leftButtons: [{
							id: 'returnButton',
							//icon: require('../img/back.png'),
							text: "Return"
						}],
						rightButtons: [{
							id: 'sidebarButton',
							icon: require('../img/Icon-App-40x40.png'),       
							showAsAction: 'always',
							buttonFontSize: 14,
							buttonFontWeight: '600'
						}]
					}
				}
			}
	});

export const goHome = () => Navigation.setRoot({
  root: {
    stack: {
      id: 'WomanList',
      children: [{
        component: {
          name: 'WomanList',
        }
      }],
    }
  }
})

export const tabBar = () => Navigation.setRoot({
	root: {
    bottomTabs: {
      children: [
        {
          component: {
            name: 'Simple',
						options: {
              bottomTab: {
								text: 'Text1',
								icon: require('../img/menu.png')
              }
            }
          },
        },
        {
          component: {
            name: 'SideBar',
						passProps: {
							user_id: user_id
						},
						options: {
              bottomTab: {
								text: 'Text1',
								icon: require('../img/menu.png')
              }
            }
          },
        },
      ],
    },
  }
});

export const closeSidebar = () =>
	Navigation.mergeOptions('sidemenu', {
		sideMenu: {
			right: {
				visible: true
			}
		}
	});

export const dashboardLayout = (componenteId, adm_id, user_id, nameExtraInfo) =>
{
	////console.log("EXTRA INFO DASHBOARD " + nameExtraInfo);
	Navigation.push(componenteId, {
			component: {
				id: "DashboardHOC",
				name: 'DashboardHOC',
				passProps: {
					text: 'Pushed screen',
					adm_id: adm_id,
					user_id: user_id
				},
				options: {
					topBar: {
						title: {
							text: "Labour area " + (nameExtraInfo == undefined ? "" : nameExtraInfo),
							visible: true
						},
						rightButtons: [{
							id: 'sidebarButton',
							icon: require('../img/Icon-App-40x40.png'),       
							showAsAction: 'always',
							buttonFontSize: 14,
							buttonFontWeight: '600'
						}]
					}
				}
			}
		})
};


export const assessmentLayout = (componenteId, adm_id, user_id, form_name, scheduled_form_id) =>
		Navigation.push(componenteId, {
			component: {
			  name: 'CustomForm',
			  passProps : { 
				  adm_id: adm_id, 
					user_id: user_id,
					form_name: form_name,
					scheduled_form_id: scheduled_form_id,
				},
			  options: {
				topBar: {
				  title: {
						text: 'Custom Assessment'
				  },
					leftButtons: [{
						id: 'returnButton',
						//icon: require('../img/back.png'),
						text: "Return"
					}],
					rightButtons: [{
						id: 'sidebarButton',
						icon: require('../img/Icon-App-40x40.png'),       
						showAsAction: 'always',
						buttonFontSize: 14,
						buttonFontWeight: '600'
					}]
				}
			}
		}
});

export const alertLayout = (adm_id, wom_name, user_id) =>
	Navigation.showModal({
		stack: {
			children: [{
				component: {
					name: 'Alerts',
					passProps: {
						text: 'stack with one child',
						adm_id: adm_id,
						user_id: user_id
					},
					options: {
						topBar: {
							title: {
								text: 'Alerts ' + wom_name
							},
							visible: true,
							leftButtons: [{
								id: 'cancelBtn',
								text: 'Cancel'
							}]
						}
					}
				}
			}]
		}
});

export const settingsLayout = (componenteId, user_id) =>
	Navigation.showModal({
		stack: {
			children: [{
			component: {
			  name: 'Settings',
			  passProps : { 
					user_id: user_id
				},
			  options: {
					topBar: {
						title: {
							text: 'Settings'
						},
						visible: true,
						leftButtons: [{
							id: 'cancelBtn',
							text: 'Cancel'
						}]
					}
					}
				}
			}]
		}
});

export const openModalSimulateTime = () => 
	Navigation.showModal({
		stack: {
			children: [{
			component: {
			  name: 'SimulateTime',
			  passProps : { 
				},
			  options: {
					topBar: {
						title: {
							text: 'Simulate Time'
						},
						visible: true,
						leftButtons: [{
							id: 'cancelBtn',
							text: 'Cancel'
						}]
					}
					}
				}
			}]
		}
});
