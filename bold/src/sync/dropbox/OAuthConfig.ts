/**
 * React Native SQLite Demo
 * Copyright (c) 2018 Bruce Lefebvre <bruce@brucelefebvre.com>
 * https://github.com/blefebvre/react-native-sqlite-demo/blob/master/LICENSE
 */
export const OAUTH_CONFIG = {
  // Replace with YOUR unique redirect URI
  OAUTH_REDIRECT_URI: "selmapp.ciis.fmrp.usp://oauthredirect",
  // Replace with YOUR unique client ID
  OAUTH_CLIENT_ID: "ab4tnbqksgn60zg"
};
