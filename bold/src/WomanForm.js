import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Animated, BackHandler, Alert, ActivityIndicator, Text } from "react-native";
import { Navigation } from 'react-native-navigation';
import { TabView, SceneMap, PagerExperimental  } from 'react-native-tab-view';
import Woman from "./components/Woman" ;
import Admission from './components/Admission';
import Pregnancy from './components/Pregnancy';
import AntenatalCare from './components/AntenatalCare';
import Diseases from './components/Diseases';
import { closeSidebar } from './navigation';

import WomanRepository from './repositories/WomanRepository';
import AdmissionRepository from './repositories/AdmissionRepository';
import PregnancyRepository from './repositories/PregnancyRepository';
import antenatalCareRepository from './repositories/antenatalCareRepository';
import diseaseRepository from './repositories/diseaseRepository';

//import AdmissionRules from './Rules/AdmissionRules';
import RulesControl from './Rules/RulesControl';

class WomanForm extends Component {
	constructor(props) {
        super(props) 
        ////console.log("props womanform " + JSON.stringify(props))
        Navigation.events().bindComponent(this);
        this.backHandler= this.backHandler.bind(this);
	}
	
	navigationButtonPressed({buttonId}) {
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId == 'sidebarButton') {
			closeSidebar();
		}else if(buttonId = 'returnButton'){     
            if(this.state.routes[0].woman_id != undefined && this.state.routes[1].woman_id > 0){
                return this.checkIfFormIsCompleted();
            }
            else {
                //Leave if no data is inserted
                Navigation.pop(this.props.componentId);		
                //Navigation.pop('WomanList');
            }
		}
	}

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.backHandler);
        ////console.log("COMPONENT DID MOUNT " + JSON.stringify(this.props));
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
        ////console.log("COMPONENT WILL UNMOUNT " + JSON.stringify(this.props));
    }
    

	componentDidAppear() {
        ////console.log("COMPONENT DID APPEAR " + JSON.stringify(this.props));
	}

	componentDidDisappear() {        
        ////console.log("COMPONENT DID DISAPEAR " + JSON.stringify(this.props));
	}

    backHandler = () => {
        if(this.state.routes[0].woman_id != undefined && this.state.routes[1].woman_id > 0){
            this.checkIfFormIsCompleted();
        }
        else {
            //Leave if no data is inserted
            Navigation.pop(this.props.componentId);		
			//Navigation.pop('WomanList');
        }
        //console.log("vai sair")
        return true;
    }

    verifyObject(data)
    {
        if(Object.keys(data).length == 0){
            return false;
        }
        var completedForm = true;
        Object.keys(data).forEach(function(key) {
            if(data[key] === null || data[key] === ''){
                completedForm = false;
                ////console.log(key + " " + data[key])
            }
        });

        // if(!completedForm){
        //     //console.log(JSON.stringify(data))
        // }
        ////console.log("COMPLETED FORM " + completedForm)
        return completedForm;
    }

    checkIfFormIsCompleted(){
        WomanRepository.getWomanById(this.state.routes[0].woman_id)
        .then((data) => {
            this.setState({woman:data});
            return this.verifyObject(data);
        })
        .then((completedForm) => {
            //if the questionary is already incompleted, return empty json
            //if(completedForm){
                return PregnancyRepository.getPregnancyByWomanId( this.state.routes[0].woman_id);        
            // }
            // else
            //     return {};
        })
        .then((data) => {        
            this.setState({pregnancy: data});
            if(Object.keys(data).length > 0)   {             
                delete data.expected_delivery_date; 
                delete data.childbirth_last_delivery; 
                delete data.date_of_last_mensturation; 
            }
            return this.verifyObject(data);
        })
        .then((completedForm) => {
            //if the questionary is already incompleted, return empty json
            //if(completedForm)
                return AdmissionRepository.getAdmissionById( this.state.routes[0].adm_id);
            // else
            //     return {};
        })
        .then((data) => {  
            this.setState({admission: data})          
            if(Object.keys(data).length > 0) {  
                delete data.gestational_age_less_34_week;
                delete data.abdominal_circumference;    
                
                if(data.mode_onset_labour == 0){
                    delete data.hours_labour_before_admission;
                }
            }

            // if(parseInt(data.elegible) != 1)
            //     data.elegible = 0;
            
            return this.verifyObject(data);
        })
        .then((completedForm) => {            
            //if the questionary is already incompleted, return empty json
            //if(completedForm)
                return antenatalCareRepository.getantenatalCareById(this.state.routes[0].anc_id)
            // else
            //     return {};
        })
        .then((data) => {
            this.setState({antenatalcare: data});
            return this.verifyObject(data);           
        })
        .then((completedForm) => {            
            //if the questionary is already incompleted, return empty json
            //if(completedForm)
                return diseaseRepository.getDiseasesById( this.state.routes[0].disea_id);
            // else
            //     return {};
        })
        .then((data) => {
            this.setState({disease: data})  
            if(Object.keys(data).length > 0) {  
                delete data.n_antenatal_care;
            }

            return this.verifyObject(data);           
        })
        .then((completedForm) => {
            if(!completedForm) {
                Alert.alert(
                    'Incompleted assessment',
                    "There are questions not answered. This can affect the evaluations and support decision. Are you sure you want to finish?",
                    [
                        {					
                            text: 'No', onPress: () => { 
                                //Stay if form is incompleted
                                //console.log('Ask me later pressed'); 
                            }
                        },
                        {
                            text: 'Yes', 
                            onPress: () => { 
                                //Leave even form is incompleted
                                this.leaveAndEvaluateAdmission();
                            }
                        }
                    ],
                    { cancelable: false }
                );
            }
            else{
                //Leave if form is completed
                this.leaveAndEvaluateAdmission();
            }
            return true;
        })
    }

    leaveAndEvaluateAdmission(){        
        var fact = this.state;
		//console.log(JSON.stringify(fact))   
        RulesControl.elegibility(fact);
        Navigation.pop(this.props.componentId);		
			//Navigation.pop("WomanList");
    }

    state = {
        index: 0, 
        routes: [
          { key: 'woman',         title: 'Woman',     woman_id: this.props.wom_id, preg_id: this.props.preg_id, adm_id: this.props.adm_id, anc_id: this.props.anc_id, disea_id: this.props.disea_id, user_id: this.props.user_id },
          { key: 'pregnancy',     title: 'Pregnancy', woman_id: this.props.wom_id, preg_id: this.props.preg_id, adm_id: this.props.adm_id, anc_id: this.props.anc_id, disea_id: this.props.disea_id, user_id: this.props.user_id },
          { key: 'antenatalcare', title: 'ANC',       woman_id: this.props.wom_id, preg_id: this.props.preg_id, adm_id: this.props.adm_id, anc_id: this.props.anc_id, disea_id: this.props.disea_id, user_id: this.props.user_id },
          { key: 'admission',     title: 'Admission', woman_id: this.props.wom_id, preg_id: this.props.preg_id, adm_id: this.props.adm_id, anc_id: this.props.anc_id, disea_id: this.props.disea_id, user_id: this.props.user_id },
        ],

        updateIndex: this.updateIndex,
        saveButton: 0
    };

    _handleIndexChange = (index) =>
    {
        return this.setState({ index })
    };

    _renderTabBar = props => {
        
        var womanFromProps = this.state.routes[0];
        ////console.log("WOMAN FROM PROPS " + JSON.stringify(womanFromProps)); 
        
        if(womanFromProps.woman_id != undefined && womanFromProps.woman_id > 0 ){
            this.state.routes[1].woman_id = womanFromProps.woman_id;
            this.state.routes[2].woman_id = womanFromProps.woman_id;
            this.state.routes[3].woman_id = womanFromProps.woman_id;
            
            this.state.routes[1].preg_id = womanFromProps.preg_id;
            this.state.routes[2].preg_id = womanFromProps.preg_id;
            this.state.routes[3].preg_id = womanFromProps.preg_id;
            
            this.state.routes[1].adm_id = womanFromProps.adm_id;
            this.state.routes[2].adm_id = womanFromProps.adm_id;
            this.state.routes[3].adm_id = womanFromProps.adm_id;
            
            this.state.routes[1].anc_id = womanFromProps.anc_id;
            this.state.routes[2].anc_id = womanFromProps.anc_id;
            this.state.routes[3].anc_id = womanFromProps.anc_id;
            
            this.state.routes[1].disea_id = womanFromProps.disea_id;
            this.state.routes[2].disea_id = womanFromProps.disea_id;
            this.state.routes[3].disea_id = womanFromProps.disea_id;
        }

        ////console.log("HANDLE INDEX CHANGE " + JSON.stringify(this.state.routes)) 
        const inputRange = props.navigationState.routes.map((x, i) => i);
    
        return (
            <View style={styles.tabBar}>
               {
                   props.navigationState.routes.map((route, i) => {
                        const color = props.position.interpolate({
                        inputRange,
                        outputRange: inputRange.map(
                            inputIndex => (inputIndex === i ? '#D6356C' : '#222')
                        ),
                    });
                    return (
                        
                    <TouchableOpacity                    
                    style={
                        route.key === props.navigationState.routes[this.state.index].key
                        ? styles.selectedTabStyle
                        : styles.tabStyle
                    }
                    onPress={() => this.setState({ index: i })}
                    key={i}
                    >
                    <Animated.Text               
                    style={
                        route.key === props.navigationState.routes[this.state.index].key
                        ? styles.selectedTabTextStyle
                        : styles.tabTextStyle
                    }
                    >{route.title}</Animated.Text>
                </TouchableOpacity>
                    );
                })}
            </View>
        );
    };
      
    _renderScene = SceneMap({
        woman: Woman,
        pregnancy: Pregnancy,
        admission: Admission,
        antenatalcare: AntenatalCare
    });

    render() {
        return (
            <TabView
                navigationState={this.state}
                renderScene={this._renderScene}
                renderTabBar={this._renderTabBar}
                onIndexChange={ this._handleIndexChange}                  
            />
        );
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
    },
    tabBar: {
      flexDirection: 'row',
    },
    tabStyle: {
      flex: 3,
      alignItems: 'center',
      padding: 10, 
      paddingTop: 16,
      paddingBottom: 16,
      borderWidth: 1, 
      borderColor: "#FFFFFF"
    },
    selectedTabStyle: {  
        flex: 3, 
        alignItems: 'center',
        padding: 10, 
        paddingTop: 16,
        paddingBottom: 16,
		borderWidth: 1, 
		borderColor: "#86939e",   
        backgroundColor: '#2096F3',
        borderRadius:3
    },
    tabTextStyle:{
        color: "#86939e"
    },
    selectedTabTextStyle:{
        color: "#FFF"
    },
    tabIcon: {
      flex: 1,
      alignItems: 'flex-end',
      padding: 16
    },
    buttonGroupContainer: { 
        flexDirection: 'column', 
        flex:1,
        height: null 
    },
    buttonGroupButton :{ 
        width: 'auto', 
        height: 'auto', 
        padding: 10, 
        borderWidth: 1, 
        borderColor: "#FFFFFF"
    }
});

export default WomanForm;
