import React, { Component } from 'react';
import {  Badge  } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

import {
	StyleSheet,
	View,
	FlatList,
	TouchableOpacity,
	Text,
	TextInput
} from 'react-native';

import { Navigation } from 'react-native-navigation'
import { closeSidebar } from './navigation';
import AlertsList from './components/common/AdmissionAlertsList';
import AdmissionAlertsRepository from './repositories/admissionAlertsRepository';
import WomanRepository from './repositories/WomanRepository';


class Alerts extends Component {

	constructor(props) {
		super(props)		

		this.state = {
			alertList: [], formLabel: 'Alerts for:', searchInput:  ''
		};	

		Navigation.events().bindComponent(this);
	}

	listAlerts(){		
		////console.log("PRINTA LISTA ALERTAS" + JSON.stringify(this.props));
		if(this.props.user_id != undefined && this.props.user_id != 0){
			AdmissionAlertsRepository.getAllAdmissionAlertsByUserId(this.props.user_id)
			.then((data) => {
				this.setState({alertList: data});
				//console.log(data);
			});
		}
		else if(this.props.adm_id != undefined && this.props.adm_id != 0) {
			AdmissionAlertsRepository.getAllAdmissionAlertsByAdmissionId(this.props.adm_id)
			.then((data) => {
				this.setState({alertList: data});
				//console.log(data);
			});
		}
	}
	
    markAlertAsRead(admission_alerts_id, alert_id) {
        AdmissionAlertsRepository.markAlertAsRead(admission_alerts_id, alert_id)
			.then((data) => {
				this.listAlerts();
			});
    }
        
    markAlertAsSnozzed = (admission_alerts_id, alert_id) => {
		AdmissionAlertsRepository.markAlertAsSnozzed(admission_alerts_id, alert_id)
		.then((data) => {
			this.listAlerts();
		});
		}
		
		navigationButtonPressed({buttonId}) {
			console.log(buttonId); 	
			if (buttonId === 'cancelBtn') {
				Navigation.dismissModal(this.props.componentId);
			} else if (buttonId === 'saveBtn') {
				alert('saveBtn');
			}else if(buttonId == 'sidebarButton') {
				closeSidebar();
			}
		}
	

	componentDidMount()
	{
		if(this.props.adm_id == undefined || this.props.adm_id == 0){
			WomanRepository.getWomanByAdmissionId(this.props.adm_id)
			.then((woman) => {				
				this.props.navigator.setTitle({title: woman.name});				
			})
		}
	}

	componentDidAppear(event) {
		this.listAlerts();
	}

	componentDidMount(){}

	render() {
		return (
			<View style={styles.container} >
				<AlertsList 
					alert={this.state.alertList} 
					markAlertAsRead={this.markAlertAsRead.bind(this)} 
					markAlertAsSnozzed={this.markAlertAsSnozzed.bind(this)} 
				/>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
	}
});

export default Alerts;
