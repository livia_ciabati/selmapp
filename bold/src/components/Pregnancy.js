import React, { Component } from 'react';
import { Button, Card, CardSection, ButtonGroupSection, InputMasked } from './common';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import { DateTimeInputMask }  from './common/DateTimeInputMask';
import { ButtonGroup } from 'react-native-elements';
import Toast from 'react-native-easy-toast';

import PregnancyRepository from '../repositories/PregnancyRepository';

class Pregnancy extends Component {

  constructor(props) {
    super(props)

    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(stateVariable, selectedIndex) {
    this.state[stateVariable] = selectedIndex;
    
    var preState = Object.assign({}, this.state, {});
    this.setState(preState);

    if(this.state.gravidity == 1){
      this.state["parity"] = 0;
      this.state["num_previous_c_section"] = 0;
      this.state["n_previous_induced_abort"] = 0;
      this.state["n_previous_spontaneous_abort"] = 0;
    } 


    if(this.state.parity + this.state.num_previous_c_section == 0){
      this.state["n_previous_stillbirths"] = 0;
      this.state["outcome_last_childbirth"] = 0;
      this.state["childbirth_last_delivery"] = null;
    }

    console.log(this.state);
    var inconsistency = false;
    //console.log("DATA CONSISTENCY " + JSON.stringify(this.state));
    //Consistency among gravity, parity, c-section and abortions
    if(parseInt(this.state.parity) + parseInt(this.state.num_previous_c_section) + parseInt(this.state.n_previous_induced_abort) + parseInt(this.state.n_previous_spontaneous_abort) >= parseInt(this.state.gravidity))
    {            
      this.refs.toast.show('Obstetric history is not consistent. Data not saved.', 2500);
      
      this.state["parity"] = 0;
      this.state["num_previous_c_section"] = 0;
      this.state["n_previous_induced_abort"] = 0;
      this.state["n_previous_spontaneous_abort"] = 0;
      
      inconsistency = true;
    }

    if(this.state.parity + this.state.num_previous_c_section < this.state.n_previous_stillbirths)
    {
      this.refs.toast.show('Number of stillbirths are higher than parity and c-section. Data not saved.', 2500);

      this.state["n_previous_stillbirths"] = 0;   
      inconsistency = true;   
    }
    
    this.state.num_previous_abort = parseInt(this.state.n_previous_induced_abort, 10) + parseInt(this.state.n_previous_spontaneous_abort, 10);

    var posState = Object.assign({}, this.state, {});
    this.setState(posState);

    if(!inconsistency){
      this.savePregnancy();
    }
  }

  componentDidMount(){
		//console.log("pregnancy componentDidDisappear");
    if(this.props.route.preg_id != undefined && this.props.route.preg_id > 0 &&
      this.props.route.woman_id != undefined && this.props.route.woman_id > 0)
    {
      this.state.id = this.props.route.preg_id;
      this.state.woman_id = this.props.route.wom_id;
      ////console.log("PREGNANCY STATE: " + JSON.stringify(this.state))
      PregnancyRepository.getPregnancyById(this.props.route.preg_id)
      .then((data) => {
        ////console.log("PREGNANCY DATA: " + JSON.stringify(data))
        this.setState(data);
      })
    }
    else if (this.props.route.woman_id != undefined && this.props.route.woman_id > 0) 
    {  
      this.state.woman_id = this.props.route.wom_id;
      PregnancyRepository.getPregnancyByWomanId(this.props.route.woman_id)
      .then((data) => {
        ////console.log("PREGNANCY DATA: " + JSON.stringify(data))
        this.setState(data);
        this.props.route.preg_id = data.id;
      })
    }
  }

	componentDidAppear() {    
		//console.log("pregnancy componentDidAppear");
  }
  
	componentDidDisappear() {    
		//console.log("pregnancy componentDidDisappear");
  }
  
  savePregnancy() {
    this.state.enabled = 1;
    this.state.woman_id = this.props.route.woman_id;
    this.state.modified_by_user_id = this.props.route.user_id;
    this.state.userBoldId = this.props.route.user_id;
    
    if(this.state.id == null) 
    {    
      this.state.id = this.props.route.preg_id;
    }

    //console.log("SAVE PREGNANCY " + JSON.stringify(this.state))
    PregnancyRepository.updatePregnancy(this.state)
    .then((data) => {
      this.refs.toast.show('Updated', 500);
    });

  }

  state = {
    gravidity:0,
    parity:0,
    n_previous_induced_abort:0,
    n_previous_spontaneous_abort:0,
    num_previous_c_section:0,
    previous_uterine_surgery:null,
    n_previous_stillbirths:null,
    outcome_last_childbirth:null,
    childbirth_last_delivery:null,
    referred:null,
    estimate_gestional_age:null,
    estimate_gestational_age_method:null,
    date_of_last_mensturation:null
  }


  render() {
    return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView>
            <Card>              
                <CardSection>
                    <ButtonGroupSection 
                        label="Gravidity (Number of pregnancies including current)"
                        onPress={ (i) => { this.updateIndex('gravidity', i + 1) } }
                        selectedIndex={parseInt(this.state.gravidity)-1} 
                        buttons={[
                          '1','2','3','4','5','+6'
                        ]}
                        vertical={false}
                    />
                </CardSection> 
                {
                  this.state.gravidity > 1 ?
                  <View>
                    <CardSection>    
                      <ButtonGroupSection 
                          label="Parity"
                          onPress={ (i) => { this.updateIndex('parity', i) } }
                          selectedIndex={parseInt(this.state.parity)} 
                          buttons={[
                            '0','1','2','3','4','5','+6'
                          ]}
                          vertical={false}
                      /> 
                  </CardSection> 
                  <CardSection> 
                  <ButtonGroupSection 
                      label="Number of previous C-Sections "
                      onPress={ (i) => { this.updateIndex('num_previous_c_section', i) } }
                      selectedIndex={parseInt(this.state.num_previous_c_section)} 
                      buttons={[
                        '0','1','2','3','4','5','+6'
                      ]}
                      vertical={false}
                  /> 
                  </CardSection>
                  <CardSection>
                    <ButtonGroupSection 
                        label="Number of previous induced abortions"
                        onPress={ (i) => { this.updateIndex('n_previous_induced_abort', i) } }
                        selectedIndex={parseInt(this.state.n_previous_induced_abort)} 
                        buttons={[
                          '0','1','2','3','4','5','+6'
                        ]}
                        vertical={false}
                    /> 
                  </CardSection>
                  <CardSection>
                    <ButtonGroupSection 
                        label="Number of previous spontaneous abortions"
                        onPress={ (i) => { this.updateIndex('n_previous_spontaneous_abort', i) } }
                        selectedIndex={parseInt(this.state.n_previous_spontaneous_abort)} 
                        buttons={[
                          '0','1','2','3','4','5','+6'
                        ]}
                        vertical={false}
                    /> 
                  </CardSection> 
                
                {(this.state.parity + this.state.num_previous_c_section > 0) ?
                  <View>
                    <CardSection>                    
                      <ButtonGroupSection 
                            label="Number of previous stillbirths"
                            onPress={ (i) => { this.updateIndex('n_previous_stillbirths', i) } }
                            selectedIndex={parseInt(this.state.n_previous_stillbirths)} 
                            buttons={[
                              '0','1','2','3','4','5','+6'
                            ]}
                            vertical={false}
                        />
                    </CardSection>      
                    <CardSection>
                      <ButtonGroupSection 
                        label="Outcome of last childbirth"
                        onPress={ (i) => { this.updateIndex('outcome_last_childbirth', i) } }
                        selectedIndex={parseInt(this.state.outcome_last_childbirth)} 
                        buttons={[
                          'No previous childbirth',
                          'Live birth, still alive',
                          'Live birth, deceased',
                          'Stillbirth'
                        ]}     />
                    </CardSection>
                    {  
                      (parseInt(this.state.outcome_last_childbirth) > 0) ?
                        <CardSection>
                          <DateTimeInputMask
                              options={{ format: 'DD-MM-YYYY' }}		
                              placeholder="dd-MM-yyyy"
                              label="In case of previous childbirths, what is the date of last delivery?"
                              value={this.state.childbirth_last_delivery}
                              underlineColorAndroid='#BBBBBB'
                              keyboardType="numeric"
                              onChangeText={ childbirth_last_delivery => {
                                if(childbirth_last_delivery.length == 10){                                
                                  this.setState({'childbirth_last_deliveryError': null});
                                } else {
                                  this.setState({'childbirth_last_deliveryError':'Please, provide a valid date.'});
                                }
                                this.setState({'childbirth_last_delivery': childbirth_last_delivery})
                              }} 
                              onEndEditing={this.savePregnancy.bind(this) }
                              errorMessage={this.state.childbirth_last_deliveryError}
                          />
                      </CardSection> : null                  
                    }
                  </View>
                   : null
                }               
                </View>
                  : null
                }
               
                <CardSection>
                    <ButtonGroupSection 
                        label="Any previous uterine surgery. Note: Previous CS not included"
                        onPress={ (i) => { this.updateIndex('previous_uterine_surgery', i) } }
                        selectedIndex={parseInt(this.state.previous_uterine_surgery)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection> 
               
                <CardSection>
                    <ButtonGroupSection 
                        label="Women referred in labour from another health facility"
                        onPress={ (i) => { this.updateIndex('referred', i) } }
                        selectedIndex={parseInt(this.state.referred)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                <CardSection>
                    <InputMasked
                        maskType={'only-numbers'}
                        maskRef = { ref => this.estimate_gestional_age = ref }
                        label = "Best estimate of gestational age"
                        placeholder= "in complete weeks"
                        value={this.state.estimate_gestional_age}
                        underlineColorAndroid='#BBBBBB'
                        keyboardType="numeric"
                        onChangeText={ estimate_gestional_age => { 
                          this.setState({'estimate_gestional_age': estimate_gestional_age})
                        }} 
                        minValue={34}
                        maxValue={42}
                        onEndEditing={this.savePregnancy.bind(this) }
                        errorMessage={this.state.estimate_gestational_ageError}
                      />
                </CardSection>
                <CardSection>
                    <ButtonGroupSection 
                        label="Method of gestational age estimation"
                        onPress={ (i) => { this.updateIndex('estimate_gestional_age_method', i) } }
                        selectedIndex={parseInt(this.state.estimate_gestional_age_method)} 
                        buttons={[
                          'Symphysis-fundal height (cm)',
                          'Birth weight assessment',
                          'Last menstrual period',
                          'Neonatal physical findings (e.g. Ballard)',
                          'Ultra-sound scan'
                        ]}
                    />
                </CardSection>             
                
            </Card>
        </ScrollView>
    </View> 
    );
  }

 }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  },
  label: {
    fontSize: 16, 
    fontWeight: 'bold',
    color: '#86939e',
    marginLeft: 10
  }
});



export default Pregnancy;