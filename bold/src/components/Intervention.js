import React, { Component } from 'react';
import { StyleSheet,Text, ScrollView, View, FlatList, BackHandler, TouchableOpacity } from "react-native";
import { Navigation } from 'react-native-navigation';
import { closeSidebar } from '../navigation';
import { Button, Card, CardSection, Input } from './common';
import { Icon, Divider } from 'react-native-elements';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import WomanRepository     from '../repositories/WomanRepository';
import PregnancyRepository from '../repositories/PregnancyRepository';
import AdmissionRepository from '../repositories/AdmissionRepository';
import DashboardRepository from '../repositories/DashboardRepository';
import { cmodel } from '../decision_models/Cmodel';


class Intervention extends Component{
    constructor(props){
        super(props);
        this.state = {intervention_data:[], cmodel_data: {}};
        this.loadDashboardData = this.loadDashboardData.bind(this);    
    }
  
    backHandler = () => {
        return false;
    }

    componentDidMount(route, navigator) 
    {
        this.loadDashboardData(this.props.route.adm_id);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    }
    
		navigationButtonPressed({buttonId}) {
			if (buttonId === 'cancelBtn') {
				Navigation.dismissModal(this.props.componentId);
			} else if (buttonId === 'saveBtn') {
				alert('saveBtn');
			}else if(buttonId == 'sidebarButton') {
				closeSidebar();
			}
		}
	
    loadDashboardData(adm_id)
    { 
      WomanRepository.getCmodelData(adm_id)
      .then((data) => {
        //console.log("DATAAAAA" + JSON.stringify( data));
        this.setState({cmodel_data: data});
      })
      .then(() => { 
        DashboardRepository.getDashboardByNameAndAdmId('interventions_form', adm_id).then(data => {
          for (let i = 0; i < data.length; i++) {
            //console.log(data[i]);
            let row = data[i];  
            if(row.name_question == 'c_section') {
              csection_prob = cmodel(this.state.cmodel_data);

              if(csection_prob != undefined)
              {
                data[i] = {
                  id: row.id,
                  admission_id: row.admission_id,
                  label_question: row.label_question,
                  name_question: row.name_question,
                  answer: parseFloat(csection_prob*100).toFixed(1) +"%",
                  createdAt: (row.createdAt!=null ? row.createdAt : "--/--/----")};
              }
            }
          }

          this.setState({ intervention_data: data }); 
        });  
      })
    }

    render(){
        return (            
            <View style={styles.container}>
                <View style={styles.containerInfo}>
                    <ScrollView>
                        <View style={styles.maternalDataView}>
                            {/* <Icon 
                                name='prescription-bottle-alt' 
                                type='font-awesome' 
                                size={12}
                            />  */}
                            
												    <FontAwesome5 name={"prescription-bottle-alt"} size={25} style={{textAlign:'center', color:'#000' }} />
                            <Text style={styles.iconDashboard}>
                            {"Interventions"}
                            </Text>
                            <Divider style={{ backgroundColor: '#bfbfbf' }} />
                            <FlatList
                            data={this.state.intervention_data}
                            keyExtractor={item => item.id.toString()}
                            numColumns={2}
                            renderItem={
                                ({ item }) => {
                                    return (
                                    <View style={(item.answer!="?" ? styles.item : styles.itemNoInformation)}>
                                        <TouchableOpacity style={styles.name}  /*onPress={() => this.getAnswersQuestion(item.id)}*/ >
                                        <Text style={styles.questionFormat}>
                                            {item.label_question}
                                        </Text>
                                        <Text style={styles.answerFormat}>
                                            {item.answer}
                                        </Text>
                                        <Text style={styles.dateLastAnswerFormat}>
                                            {item.createdAt}
                                        </Text>
                                        </TouchableOpacity>
                                    </View> 
                                    )  
                                }
                            }
                            />
                        </View>
                    </ScrollView>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: '#F5FCFF'
      },
    item: {
      // alignItems: "flex-start",
      backgroundColor: "#00CC00",
      flexGrow: 1,
      margin: 4,
      padding: 5,
      flexBasis: 0,
    },
    itemNoInformation: {
      backgroundColor: "#bfbfbf",
      flexGrow: 1,
      margin: 4,
      padding: 5,
      flexBasis: 0,
    },
    questionFormat: {
      fontSize: 7,
      fontWeight: 'normal',
      textAlign: 'center',
    },
    answerFormat: {
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'center',
    },
    dateLastAnswerFormat: {
      fontSize: 7,
      fontWeight: 'normal',
      textAlign: 'center',
    },
    text: {
      color: "#333333"
    },
    informationText: {
      fontSize: 20,
      fontWeight: 'bold',
      textAlign: 'left',
    },
    containerLabel: {
      backgroundColor: "#f2f2f2",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
    containerInfo: {
      backgroundColor: "#f2f2f2",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      flex: 9,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
  
    maternalDataView: {
      backgroundColor: "#e7e7e7",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      flex: 9,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
  
    fetalDataView: {
      backgroundColor: "#e7e7e7",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      flex: 9,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
  
    admissionDataView: {
      backgroundColor: "#e7e7e7",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      // flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
  
    otherDataView: {
      backgroundColor: "#e7e7e7",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      flex: 9,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
  
    warnningAdmissionView: {
      backgroundColor: "#f08000",
      // flexGrow: 1,
      margin: 4,
      padding: 2,
      // flexBasis: 1,
      flex: 9,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
    },
  
    warnningAdmissionText: {
      fontSize: 12,
      fontWeight: 'bold',
      textAlign: 'center',
    },
  
    iconDashboard: {
      fontSize: 11,
      fontWeight: 'bold',
      textAlign: 'center',
    }
  
  
  });

export default Intervention;