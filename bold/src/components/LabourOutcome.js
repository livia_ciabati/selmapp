import React, { Component } from 'react';
import { Button, Card, CardSection, ButtonGroupSection, InputMasked } from './common';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import { Loader } from './common/Loader';
import Toast from 'react-native-easy-toast'
import NeonatalOutcomesRepository from '../repositories/NeonatalOutcomesRepository';
import { Navigation } from 'react-native-navigation';
import { closeSidebar } from '../navigation';

import { TextInputMask } from 'react-native-masked-text'

class LabourOutcome extends Component {

  constructor(props) {
    super(props)
    //console.log("PROPS LABOUR: " + JSON.stringify(props));

    this.updateIndex = this.updateIndex.bind(this);
    this.state = {  
      name : "",
      animating: false,
      backgroundColor:'#F5FCFF88',
      height: 0 
    }
		Navigation.events().bindComponent(this);
  }

	navigationButtonPressed({buttonId}) {
    console.log(buttonId);

		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId === 'sidebarButton') {
			closeSidebar();
		}else if(buttonId === 'backButton'){
      if(!this.confirmCloseCase()){	
        Navigation.pop(this.props.componentId);		
      }
		}
  }
  
  updateIndex(stateVariable, selectedIndex) {
    //computed property name
    this.state[stateVariable] = selectedIndex;
    var newState = Object.assign({}, this.state, {});
    this.setState(newState);
    this.saveNeonatalOutcome();
  }

  componentDidMount(){
    this.showLoading();
    woman_id = this.props.woman_id;
    this.state.admission_id = this.props.admission_id;
    this.state.modified_by_user_id = this.props.user_id;
    this.state.userBoldId = this.props.user_id;

    if(this.props.admission_id != undefined && this.props.admission_id > 0)
    {
      //console.log('Admission id: ' + this.props.admission_id);
      NeonatalOutcomesRepository.getNeonatalOutcomeByAdmissionId(this.props.admission_id)
      .then((data) => {
        if(data != undefined){
          this.setState(data);
        }
        //console.log(data);
        //console.log("getNeonatalOutcomeByAdmissionId: " + JSON.stringify(data));
        this.hideLoading();
      });
    }
    else{
      this.hideLoading();
    }
  }

  saveNeonatalOutcome() {
    if(this.state.admission_id == undefined){
      this.refs.toast.show('Erro - sem admission_id', 500);
    }else{
      if(this.state.id) {
        NeonatalOutcomesRepository.updateNeonatalOutcome(this.state)
          .then((data) => {
            this.refs.toast.show('Updated', 500);
          });
      }
      else {           
        NeonatalOutcomesRepository.insertNeonatalOutcome(this.state)
        .then((data) => {
          this.setState(data);
          this.props.neonatal_outcome_id = data.id;
          this.refs.toast.show('Saved', 500);
        }); 
      }
    }
  }

  state = {
    date_delivery: null,
    time_delivery: null,
    final_mode_delivery: null,
    fetal_presentation_delivery:null,
    infant_sex:null,
    birth_weight: null,
    condition_vital_status: null,
    condition_apgar_5min: null,
    condition_apgar_5min: null,
    morbidity_admission_ICU: null,
    morbidity_intubation: null,
    morbidity_nasal_CPAP:null,
    morbidity_surfactant_administration: null,
    morbidity_cardio_pulmonary_resuscitation: null,
    morbidity_use_vasoactive_drug: null,
    morbidity_use_anticonvulsants: null,
    morbidity_use_phototerapy_first_24hours: null,
    morbidity_any_blood_products: null,
    morbidity_use_steroids: null,
    morbidity_therapeutic_intravenous_antibiotics: null,
    morbidity_any_surgery: null,
    morbidity_any_severe_malformation: null,
    status_hospital_discharge: null,
    discharge_transfer_death_date: null,
    discharge_transfer_death_hour: null,
    dateDeliveryError: null,
    timeDeliveryError: null,
    dateDischargeError: null,
    timeDischargeError: null
  }

  showLoading = () => {    
    this.setState({animating: true});
  }

  hideLoading = () => {
      this.setState({animating: false});
  } 

  render() {
    return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView> 
        
        <Card>
            <CardSection>
              <View style={{flex:1}}>
              <Text
                style={styles.label}>
                Date of delivery
              </Text>
              <TextInputMask
                  type={'datetime'}
                  ref = { ref => this._date_delivery = ref }
                  options={{ format: 'DD-MM-YYYY' }}
                  style={{ width: '80%', 
                    fontSize: 16,
                    marginLeft: 5
                  }}
                  placeholder="dd-MM-yyyy"
                  value= {this.state.date_delivery}
                  underlineColorAndroid='#BBBBBB'
                  keyboardType="numeric"
                  onChangeText={ date_delivery_tmp => {
                    if(date_delivery_tmp.length == 10){
                      if(!this._date_delivery.isValid()){
                        this.setState({'dateDeliveryError':'Please, provide a valid date.'});
                      }else{
                        this.setState({'dateDeliveryError': null});
                      }
                    }else{
                          this.setState({'dateDeliveryError':'Please, provide a valid date.'});
                    }
                    this.setState({date_delivery: date_delivery_tmp})
                  }} 
                  onEndEditing={this.saveNeonatalOutcome.bind(this) }
                  errorMessage={this.state.dateDeliveryError}
              />
              </View>
            </CardSection> 
            <CardSection>
              <View style={{flex:1}}>
              <Text
                style={styles.label}>
                Time of delivery
              </Text>
              <TextInputMask
                  type={'datetime'}
                  ref = { ref => this._time_delivery = ref }
                  options={{ format: 'HH-MM' }}
                  style={{ width: '80%', 
                    fontSize: 16,
                    marginLeft: 5
                  }}
                  placeholder="HH-mm"
                  value= {this.state.time_delivery}
                  underlineColorAndroid='#BBBBBB'
                  keyboardType="numeric"
                  onChangeText={ time_delivery => {
                    if(time_delivery.length == 5){
                      if(!this._time_delivery.isValid()){
                        this.setState({'timeDeliveryError':'Please, provide a valid time.'});
                      }else{
                        this.setState({'timeDeliveryError': null});
                      }
                    }else{
                          this.setState({'timeDeliveryError':'Please, provide a valid time.'});
                    }
                    this.setState({'time_delivery': time_delivery})
                  }} 
                  onEndEditing={this.saveNeonatalOutcome.bind(this) }
                  errorMessage={this.state.timeDeliveryError}
              />
              </View>
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Final mode of delivery'
                selectedIndex={ parseInt(this.state.final_mode_delivery) } 
                onPress={ (i) => { this.updateIndex('final_mode_delivery', i) } }
                buttons={[
                  'Spontaneous vaginal delivery without episiotomy',
                  'Spontaneous vaginal delivery with episiotomy',
                  'Operative vaginal delivery (forceps or vacuum)',
                  'Caesarean section',
                  'Laparotomy'
                ]}
              />
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Fetal presentation at delivery'
                selectedIndex={parseInt(this.state.fetal_presentation_delivery)} 
                onPress={ (i) => { this.updateIndex('fetal_presentation_delivery', i) } }
                buttons={[
                  'Cephalic',
                  'Breech',
                  'Transverse Lie./Other',
                ]}
              />
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Infant sex'
                selectedIndex={parseInt(this.state.infant_sex)} 
                onPress={ (i) => { this.updateIndex('infant_sex', i) } }
                buttons={[
                  'Male',
                  'Female'
                ]}
              />
            </CardSection>
            <CardSection>
              <InputMasked
                placeholder="in grams"
                label="Birth weight(g)"
                keyboardType='numeric'
                value= {this.state.birth_weight}
                onChangeText={(birth_weight) => this.setState({'birth_weight': birth_weight}) }
                onEndEditing={birth_weight => { 
                  this.saveNeonatalOutcome('birth_weight', birth_weight)
                } }                           
                minValue={200}
                maxValue={8000}
              />
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Vital status'
                selectedIndex= {parseInt(this.state.condition_vital_status)} 
                onPress={ (i) => { this.updateIndex('condition_vital_status', i) } }
                buttons={[
                  'Alive',
                  'Stillbirth'
                ]}
              />
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Apgar score at 5 min'
                onPress={ (i) => { this.updateIndex('condition_apgar_5min', i + 1) } }
                selectedIndex={parseInt(this.state.condition_apgar_5min)-1} 
                buttons={[
                  '1','2','3','4','5','6', '7', '8', '9', '10'
                ]}
              />
            </CardSection> 
          </Card>
        </ScrollView>
        <Loader animating = { this.state.animating } />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  }
});



export default LabourOutcome;
