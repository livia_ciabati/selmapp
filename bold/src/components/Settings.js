import React, { Component } from 'react';
import {
	StyleSheet,
	View,
    FlatList,
    ScrollView, Dimensions
} from 'react-native';
import { Card, CardSection, InputMasked, ButtonGroupSection } from './common';
import { Navigation } from 'react-native-navigation'
import { closeSidebar } from '../navigation';
import Toast from 'react-native-easy-toast'

import { Loader } from './common/Loader';
import SettingsRepository from '../repositories/SettingsRepository'
import { Input } from 'react-native-elements';

class Settings extends Component {
	constructor(props) {
        super(props)
        //console.log("++++++++++++++++++++++++++++++++++ SETTINGS FORM PROPS: " + JSON.stringify(props));
        
        this.state = {
			settingList: [], admissionId: null
        }
        this.saveSetting = this.saveSetting.bind(this);
        Navigation.events().bindComponent(this);
    }

    componentDidMount() {
        this.showLoading();
        SettingsRepository.getAllSettings()
			.then((setting) => {
                this.setState({ settingList : setting });
                this.hideLoading();
        });
    }

	showLoading = () => {    
		this.setState({animating: true});
	}

	hideLoading = () => {
		this.setState({animating: false});
    } 
    
    saveSetting() {
        this.state.enabled = 1;
        this.state.woman_id = this.props.route.woman_id;
        this.state.modified_by_user_id = this.props.route.user_id;
        this.state.userBoldId = this.props.route.user_id;
        
        if(this.state.id == null) 
        {    
        this.state.id = this.props.route.preg_id;
        }

        //console.log("SAVE PREGNANCY" + JSON.stringify(this.state))
        //console.log("SAVE PREGNANCY" + this.state.woman_id)
        PregnancyRepository.updatePregnancy(this.state)
        .then((data) => {
        this.refs.toast.show('Updated', 500);
        });

    }


    navigationButtonPressed({buttonId}) {
        if (buttonId === 'cancelBtn') {
            Navigation.dismissModal(this.props.componentId);
        } else if (buttonId === 'saveBtn') {
            alert('saveBtn');
        }else if(buttonId == 'sidebarButton') {
            closeSidebar();
        }
    }
	
    

    render(){
        const {settingList} = this.state;
        return (            
			<View style={styles.container} >
                <Toast ref="toast" position='top'/>
                <ScrollView>
                
                        <FlatList
                            style = {{width: dimensions.width}}
                            data={settingList}
                            keyExtractor={item => item.id.toString()}
                            renderItem={({ item }) => {
                                return (		
                                    <CardSection>
                                    { (parseInt(item.value) != 0 && parseInt(item.value) != 1) ?
                                        <InputMasked
                                            keyboardType='default'
                                            label={ item.label }
                                            id={ item.id }
                                            value={ item.value }
                                            //onChangeText={ value => {} }//this.setState({ name : value })} 
                                            //onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.id) }                       
                                        /> 
                                        :
                                        <ButtonGroupSection 
                                            label={ item.label }
                                            //onPress={ (i) => { this.updateIndex('previous_uterine_surgery', i) } }
                                            selectedIndex={parseInt(item.value)} 
                                            buttons={[
                                            'No',
                                            'Yes'
                                            ]}
                                            vertical={false}
                                        />
                                    }
                                    </CardSection>
                                )
                            }}
                        />							
                        <Loader animating = { this.state.animating } />
                </ScrollView>
            </View>
        )
    }

}

const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
	},	
	containerAvatar: {
		flex: 3,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF',
		alignItems:'center'
	},	
	containerList: {
		height: 60,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF',
		alignItems:'center'
	}
});


export default Settings;