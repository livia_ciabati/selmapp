import React, { Component } from 'react';
import { MultipleButtonGroupSection, Card, CardSection, InputMasked, ButtonGroupSection } from './common';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import { ButtonGroup } from 'react-native-elements'
import MaternalOutcomesRepository from '../repositories/MaternalOutcomesRepository';
import { FormLabel } from './common/FormLabel';
import { Loader } from './common/Loader';
import Toast from 'react-native-easy-toast'

class MaternalOutcome extends Component {

  constructor(props) {
    super(props)
    
    //console.log("MATERNAL OUTCOME PROPS: " + JSON.stringify(props));

    this.updateIndex = this.updateIndex.bind(this);
    this.state = {  
      name : "",
      animating: false,
      backgroundColor:'#F5FCFF88',
      height: 0 
    }
  }

  updateIndex(stateVariable, selectedIndex) {
    //computed property name
    this.state[stateVariable] = selectedIndex;
    var newState = Object.assign({}, this.state, {});
    this.setState(newState);

    this.saveMaternalOutcome();
  }

  showLoading = () => {    
    this.setState({animating: true});
  }

  hideLoading = () => {
      this.setState({animating: false});
  } 

  state = {
    dystocia_labour_obstruction: null,
    dystocia_first_stage_labour: null,
    dystocia_prolonged_second_stage: null,
    haemorrhage_placenta_praevia: null,
    haemorrhage_accreta_increta_percreta_placenta: null,
    haemorrhage_abruptio_placenta: null,
    haemorrhage_ruptured_uterus: null,
    haemorrhage_intrapartum: null,
    haemorrhage_postpartum: null,
    infection_puerperal_endometritis: null,
    infection_puerperal_sepsis: null,
    infection_wound: null,
    infection_systemic_septicaemia: null,
    hypertension_severe: null,
    hypertension_pre_eclampsia: null,
    hypertension_eclampsia: null,
    other_embolic_disease: null,
    otherPotentiallyLifeThreateningCondition: null,
    conditions_during_hospital_stay: null,
    cardiovascular_dysfunction: null,
    respiratory_dysfunction: null,
    renal_dysfunction: null,
    coagulation_dysfunction: null,
    hepatic_dysfunction: null,
    neurologic_dysfunction: null,
    uterine_dysfunction: null,
    dysfunction_identified: null,
    nmm_haemorrhage_oxytocin_treatment_PPH: null,
    nmm_haemorrhage_misoprostol_treatment_PPH: null,
    nmm_haemorrhage_ergotamine_treatment_PPH: null,
    nmm_haemorrhage_other_uterotonic_treatment_PPH: null,
    nmm_haemorrhage_artery_ligation_or_embolization: null,
    nmm_haemorrhage_balloon_or_condom_tamponade: null,
    nmm_haemorrhage_repair_cervical_laceration: null,
    nmm_haemorrhage_repair_uterine_rupture: null,
    nmm_haemorrhage_b_lynch_suture: null,
    nmm_haemorrhage_hysterectomy: null,
    nmm_infection_therapeutic_antibiotics: null,
    nmm_hypertension_magnesium_sulphate_as_anticonvulsant: null,
    nmm_hypertension_other_anticonvulsant: null,
    nmm_other_removal_retained_products: null,
    nmm_other_manual_removal_placenta: null,
    nmm_other_blood_transfusion: null,
    nmm_other_laparotomy: null,
    nmm_other_admission_ICU: null,
    intervention_identified: null,
    referred_higher_complexity_hospital: null,
    status_hospital_discharge: null,
    discharge_transfer_death_date: null,
    dateDischargeError: null,
  }

  componentDidMount(){
    this.showLoading();

    woman_id = this.props.route.woman_id;
    this.state.admission_id = this.props.route.admission_id;
    this.state.modified_by_user_id = this.props.route.user_id;
    this.state.userBoldId = this.props.route.user_id;

    if(this.props.route.admission_id != undefined && this.props.route.admission_id > 0)
    {
      //console.log('********  Admission id: ' + this.props.route.admission_id);
      MaternalOutcomesRepository.getMaternalOutcomeByAdmissionId(this.props.route.admission_id)
      .then((data) => {
        if(data != undefined){
          this.setState(data);
        }
        //console.log("******** DATA: " + JSON.stringify(data));
        //console.log("********  getMaternalOutcomeByAdmissionId: " + JSON.stringify(data));
        this.hideLoading();
      });
    }
    else{
      this.hideLoading();
    }
  }

  saveMaternalOutcome() {
    if(this.state.admission_id == undefined){
      this.refs.toast.show('Erro - sem admission_id', 500);
    }else{
      if(this.state.id) {
        MaternalOutcomesRepository.updateMaternalOutcome(this.state)
          .then((data) => {
            //console.log("MATERNAL OUTCOME UPDATE " + JSON.stringify(data));
            this.refs.toast.show('Updated', 500);
          });
      }
      else {           
        MaternalOutcomesRepository.insertMaternalOutcome(this.state)
        .then((data) => {
          //console.log("MATERNAL OUTCOME INSERT " + JSON.stringify(data));
          this.setState(data);
          this.props.route.maternal_outcome_id = data.id;
          this.refs.toast.show('Saved', 500);
        }); 
      }
    }
  }
 


  render() {
    let choicesConditions = ['No','Yes, within 24h of hospital stay','Yes, after 24h of hospital stay'];
    return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView>
          <Card>
            <CardSection>
              <FormLabel>Was any of the following conditions identified during hospital stay? </FormLabel></CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Dystocia'
                buttons={[
                  {
                    label:'Labour obstruction', 
                    selectedIndex: parseInt(this.state.dystocia_labour_obstruction), 
                    onPress:(i) => { this.updateIndex('dystocia_labour_obstruction', i);} 
                  },
                  {
                    label:'Prolonged first stage of labour', 
                    selectedIndex: parseInt(this.state.dystocia_first_stage_labour), 
                    onPress:(i) => { this.updateIndex('dystocia_first_stage_labour', i);} 
                  },
                  {
                    label:'Prolonged second stage',
                    selectedIndex: parseInt(this.state.dystocia_prolonged_second_stage), 
                    onPress:(i) => { this.updateIndex('dystocia_prolonged_second_stage', i);} 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Haemorrhage'
                buttons={[
                  {
                    label:'Placenta praevia', 
                    selectedIndex: parseInt(this.state.haemorrhage_placenta_praevia), 
                    onPress:(i) => { this.updateIndex('haemorrhage_placenta_praevia', i) } 
                  },
                  {
                    label:'Accreta/increta/percreta placenta', 
                    selectedIndex: parseInt(this.state.haemorrhage_accreta_increta_percreta_placenta), 
                    onPress:(i) => { this.updateIndex('haemorrhage_accreta_increta_percreta_placenta', i) } 
                  },
                  {
                    label:'Abruptio placenta',
                    selectedIndex: parseInt(this.state.haemorrhage_abruptio_placenta), 
                    onPress:(i) => { this.updateIndex('haemorrhage_abruptio_placenta', i) } 
                  },
                  {
                    label:'Ruptured uterus',
                    selectedIndex: parseInt(this.state.haemorrhage_ruptured_uterus), 
                    onPress:(i) => { this.updateIndex('haemorrhage_ruptured_uterus', i) } 
                  },
                  {
                    label:'Intrapartum haemorrhage',
                    selectedIndex: parseInt(this.state.haemorrhage_intrapartum), 
                    onPress:(i) => { this.updateIndex('haemorrhage_intrapartum', i) } 
                  },
                  {
                    label:'Postpartum haemorrhage',
                    selectedIndex: parseInt(this.state.haemorrhage_postpartum), 
                    onPress:(i) => { this.updateIndex('haemorrhage_postpartum', i) } 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Infection'
                buttons={[
                  {
                    label:'Puerperal endometritis', 
                    selectedIndex: parseInt(this.state.infection_puerperal_endometritis), 
                    onPress:(i) => { this.updateIndex('infection_puerperal_endometritis', i) } 
                  },
                  {
                    label:'Puerperal sepsis', 
                    selectedIndex: parseInt(this.state.infection_puerperal_sepsis), 
                    onPress:(i) => { this.updateIndex('infection_puerperal_sepsis', i) } 
                  },
                  {
                    label:'Wound infection', 
                    selectedIndex: parseInt(this.state.infection_wound), 
                    onPress:(i) => { this.updateIndex('infection_wound', i) } 
                  },
                  {
                    label:'Systemic infection / septicaemia', 
                    selectedIndex: parseInt(this.state.infection_systemic_septicaemia), 
                    onPress:(i) => { this.updateIndex('infection_systemic_septicaemia', i) } 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Hypertension'
                buttons={[
                  {
                    label:'Severe hypertension', 
                    selectedIndex: parseInt(this.state.hypertension_severe), 
                    onPress:(i) => { this.updateIndex('hypertension_severe', i) } 
                  },
                  {
                    label:'Pre-eclampsia (excludes eclampsia)', 
                    selectedIndex: parseInt(this.state.hypertension_pre_eclampsia), 
                    onPress:(i) => { this.updateIndex('hypertension_pre_eclampsia', i) } 
                  },
                  {
                    label:'Eclampsia', 
                    selectedIndex: parseInt(this.state.hypertension_eclampsia), 
                    onPress:(i) => { this.updateIndex('hypertension_eclampsia', i) } 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Other maternal complications'
                buttons={[
                  {
                    label:'Embolic disease', 
                    selectedIndex: parseInt(this.state.other_embolic_disease), 
                    onPress:(i) => { this.updateIndex('other_embolic_disease', i) } 
                  },
                  {
                    label:'Other potentially life-threatening condition', 
                    selectedIndex: parseInt(this.state.otherPotentiallyLifeThreateningCondition), 
                    onPress:(i) => { this.updateIndex('otherPotentiallyLifeThreateningCondition', i) } 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Was any of the following conditions identified?'
                choices={choicesConditions}
                buttons={[
                  {
                    label:'Cardiovascular dysfunction', 
                    selectedIndex: parseInt(this.state.cardiovascular_dysfunction), 
                    onPress:(i) => { this.updateIndex('cardiovascular_dysfunction', i) }
                  },
                  {
                    label:'Respiratory dysfunction', 
                    selectedIndex: parseInt(this.state.respiratory_dysfunction), 
                    onPress:(i) => { this.updateIndex('respiratory_dysfunction', i) }
                  },
                  {
                    label:'Renal dysfunction', 
                    selectedIndex: parseInt(this.state.renal_dysfunction), 
                    onPress:(i) => { this.updateIndex('renal_dysfunction', i) }
                  },
                  {
                    label:'Coagulation dysfunction', 
                    selectedIndex: parseInt(this.state.coagulation_dysfunction), 
                    onPress:(i) => { this.updateIndex('coagulation_dysfunction', i) }
                  },
                  {
                    label:'Hepatic dysfunction', 
                    selectedIndex: parseInt(this.state.hepatic_dysfunction), 
                    onPress:(i) => { this.updateIndex('hepatic_dysfunction', i) }
                  },
                  {
                    label:'Neurologic dysfunction', 
                    selectedIndex: parseInt(this.state.neurologic_dysfunction), 
                    onPress:(i) => { this.updateIndex('neurologic_dysfunction', i) }
                  },
                  {
                    label:'Uterine dysfunction', 
                    selectedIndex: parseInt(this.state.uterine_dysfunction), 
                    onPress:(i) => { this.updateIndex('uterine_dysfunction', i) }
                  },
                ]}
              />
            </CardSection>
            <CardSection><FormLabel>Please specify whether the women used any of the following interventions</FormLabel></CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Haemorrhage'
                buttons={[
                  {
                    label:'Oxytocin for treatment of PPH', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_oxytocin_treatment_PPH), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_oxytocin_treatment_PPH', i) } 
                  },
                  {
                    label:'Misoprostol for treatment of PPH', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_misoprostol_treatment_PPH), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_misoprostol_treatment_PPH', i) } 
                  },
                  {
                    label:'Ergotamine for treatment of PPH', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_ergotamine_treatment_PPH), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_ergotamine_treatment_PPH', i) } 
                  },
                  {
                    label:'Other uterotonic for treatment of PPH', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_other_uterotonic_treatment_PPH), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_other_uterotonic_treatment_PPH', i) } 
                  },
                  {
                    label:'Artery ligation or embolization', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_artery_ligation_or_embolization), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_artery_ligation_or_embolization', i) } 
                  },
                  {
                    label:'Balloon or condom tamponade', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_balloon_or_condom_tamponade), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_balloon_or_condom_tamponade', i) } 
                  },
                  {
                    label:'Repair of cervical laceration', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_repair_cervical_laceration), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_repair_cervical_laceration', i) } 
                  },
                  {
                    label:'Repair of uterine rupture', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_repair_uterine_rupture), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_repair_uterine_rupture', i) } 
                  },
                  {
                    label:'B-lynch suture', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_b_lynch_suture), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_b_lynch_suture', i) } 
                  },
                  {
                    label:'Hysterectomy', 
                    selectedIndex: parseInt(this.state.nmm_haemorrhage_hysterectomy), 
                    onPress:(i) => { this.updateIndex('nmm_haemorrhage_hysterectomy', i) } 
                  },

                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Infection'
                buttons={[
                  {
                    label:'Therapeutic antibiotics (excludes prophylaxis)', 
                    selectedIndex: parseInt(this.state.nmm_infection_therapeutic_antibiotics), 
                    onPress:(i) => { this.updateIndex('nmm_infection_therapeutic_antibiotics', i) } 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Hypertension'
                buttons={[
                  {
                    label:'Magnesium sulphate as anticonvulsant', 
                    selectedIndex: parseInt(this.state.nmm_hypertension_magnesium_sulphate_as_anticonvulsant), 
                    onPress:(i) => { this.updateIndex('nmm_hypertension_magnesium_sulphate_as_anticonvulsant', i) } 
                  },
                  {
                    label:'Other anticonvulsant for eclampsia', 
                    selectedIndex: parseInt(this.state.nmm_hypertension_other_anticonvulsant), 
                    onPress:(i) => { this.updateIndex('nmm_hypertension_other_anticonvulsant', i) } 
                  },
                ]}
              />
            </CardSection>
            <CardSection>
              <MultipleButtonGroupSection
                title='Other interventions'
                buttons={[
                  {
                    label:'Removal of retained products', 
                    selectedIndex: parseInt(this.state.nmm_other_removal_retained_products), 
                    onPress:(i) => { this.updateIndex('nmm_other_removal_retained_products', i) } 
                  },
                  {
                    label:'Manual removal of placenta', 
                    selectedIndex: parseInt(this.state.nmm_other_manual_removal_placenta), 
                    onPress:(i) => { this.updateIndex('nmm_other_manual_removal_placenta', i) } 
                  },
                  {
                    label:'Blood transfusion', 
                    selectedIndex: parseInt(this.state.nmm_other_blood_transfusion), 
                    onPress:(i) => { this.updateIndex('nmm_other_blood_transfusion', i) } 
                  },
                  {
                    label:'Laparotomy', 
                    selectedIndex: parseInt(this.state.nmm_other_laparotomy), 
                    onPress:(i) => { this.updateIndex('nmm_other_laparotomy', i) } 
                  },
                  {
                    label:'Admission to Intensive Care Unit', 
                    selectedIndex: parseInt(this.state.nmm_other_admission_ICU), 
                    onPress:(i) => { this.updateIndex('nmm_other_admission_ICU', i) } 
                  }
                ]}
              />
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Specify whether the woman was referred to any higher complexity hospital'
                selectedIndex={parseInt(this.state.referred_higher_complexity_hospital)} 
                onPress={ (i) => { this.updateIndex('referred_higher_complexity_hospital', i) } }
                vertical= {false}
                buttons={[
                  'No','Yes'
                ]}
              />
            </CardSection>
            <CardSection>
              <ButtonGroupSection 
                label='Maternal status at hospital discharge'
                selectedIndex={parseInt(this.state.status_hospital_discharge)} 
                onPress={ (i) => { this.updateIndex('status_hospital_discharge', i) } }
                vertical= {false}
                buttons={[
                  'Alive','Dead'
                ]}
              />
            </CardSection>
            <CardSection>
              <View style={{flex:1}}>
              <Text
                style={styles.label}>
                Date of maternal discharge, transfer or death
              </Text>
              <TextInputMask
                  type={'datetime'}
                  ref = { ref => this._date_discharge = ref }
                  options={{ format: 'DD-MM-YYYY' }}
                  style={{ width: '80%', 
                    fontSize: 16,
                    marginLeft: 5
                  }}
                  placeholder="dd-MM-yyyy"
                  value= {this.state.discharge_transfer_death_date}
                  underlineColorAndroid='#BBBBBB'
                  keyboardType="numeric"
                  onChangeText={ date_discharge => {
                    if(date_discharge.length == 10){
                      if(!this._date_discharge.isValid()){
                        this.setState({'dateDischargeError':'Please, provide a valid date.'});
                      }else{
                        this.setState({'dateDischargeError': null});
                      }
                    }else{
                          this.setState({'dateDischargeError':'Please, provide a valid date.'});
                    }
                    this.setState({discharge_transfer_death_date: date_discharge})
                  }} 
                  onEndEditing={this.saveMaternalOutcome.bind(this) }
                  errorMessage={this.state.dateDischargeError}
              />
              </View>
            </CardSection>
          </Card>
        </ScrollView>
        <Loader animating = { this.state.animating } />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  },
  label: {
    fontSize: 16, 
    fontWeight: 'bold',
    color: '#86939e',
    marginLeft: 10
  }
});



export default MaternalOutcome;
