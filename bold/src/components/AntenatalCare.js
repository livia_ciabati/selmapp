import React, { Component } from 'react';
import { Button, Card, CardSection, ButtonGroupSection, InputMasked  } from './common';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import { FormLabel, ButtonGroup } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import AntenatalCareRepository from '../repositories/antenatalCareRepository';
import diseaseRepository from '../repositories/diseaseRepository';

class AntenatalCare extends Component {

  constructor(props) {
    super(props)
    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(stateVariable, selectedIndex, type) {
      if(type == 'anc')
      {
        this.state.antenatalCare[stateVariable] = selectedIndex;
        var newState = Object.assign({}, this.state.antenatalCare, {});
        this.setState({antenatalCare : newState});
        this.saveAntenatalCare();
      }
      else{
        this.state.disease[stateVariable] = selectedIndex;
        var newState = Object.assign({}, this.state.disease, {});
        this.setState({disease : newState});
        this.saveDisease();
      }

  }

  componentDidMount(){
    if(this.props.route.preg_id != undefined && this.props.route.preg_id > 0){
        //console.log("ANTENATALCARE ROUTE " + this.props.route.preg_id);
        this.state.antenatalCare.pregnancy_id = this.props.route.preg_id;
        this.state.disease.pregnancy_id = this.props.route.preg_id;
    }

    if(this.props.route.anc_id != undefined && this.props.route.anc_id > 0)
    {
      this.state.id = this.props.route.anc_id;     
      AntenatalCareRepository.getantenatalCareById(this.props.route.anc_id)
      .then((data) => {
        //console.log("ANTENATAL DATA: " + JSON.stringify(data))        
        this.setState({antenatalCare: data});
        this.props.route.anc_id = data.id;
      })
    }
    
    if(this.props.route.disea_id != undefined && this.props.route.disea_id > 0)
    {
      this.state.id = this.props.route.disea_id;     
      diseaseRepository.getDiseasesById(this.props.route.disea_id)
      .then((data) => {
        //console.log("DISEASE DATA: " + JSON.stringify(data))        
        this.setState({disease: data});
        this.props.route.disea_id = data.id;
      })
    }
  }

	componentDidAppear() {    
		//console.log("ANTENATAL componentDidAppear");
  }
  
	componentDidDisappear() {    
		//console.log("ANTENATAL componentDidDisappear");
  }
  
  saveAntenatalCare() {
    this.state.antenatalCare.enabled = 1;
    this.state.antenatalCare.modified_by_user_id = this.props.route.user_id;
    this.state.antenatalCare.userBoldId = this.props.route.user_id;
    this.state.antenatalCare.pregnancy_id = this.props.route.preg_id;
    
    if(this.props.route.anc_id != undefined && this.props.route.anc_id > 0) {
      this.state.antenatalCare.id = this.props.route.anc_id;
    }
          
    AntenatalCareRepository.updateantenatalCare(this.state.antenatalCare)
    .then((data) => {
      this.refs.toast.show('Updated', 500);
    });
  }

  saveDisease() {
    this.state.disease.enabled = 1;
    this.state.disease.modified_by_user_id = this.props.route.user_id;
    this.state.disease.userBoldId = this.props.route.user_id;
    this.state.disease.pregnancy_id = this.props.route.preg_id;
    
    if(this.props.route.disea_id != undefined && this.props.route.disea_id > 0) {
        this.state.disease.id = this.props.route.disea_id;
    }
          
    diseaseRepository.updatedisease(this.state.disease)
    .then((data) => {
      this.refs.toast.show('Updated', 500);
    });
  }

  state = {
      antenatalCare: {
        id: "",
        weight: "",
        height: "",
        placenta_praevia: "",
        accreta_increta_percreta: "",
        abruptio_placentae: "",
        other_obstetric_haemorrhage: "",
        pre_eclampsia: "",
        eclampsia: "",
        pyelonephritis: "",
        malaria: "",
        preterm_rupture_membranes: "",
        anaemia: "",
        gestational_diabetes: "",
        other_pregnancy_complications: "",
        pregnancy_id: "",
        createdAt: "",
        updatedAt: "",
        userBoldId: ""},
    disease: {        
        id: "",
        chronic_hypertension: "",
        diabetes_mellitus: "",
        hiv: "",
        aids_hiv: "",
        chronic_anaemia: "",
        obesity: "",
        heart_disease: "",
        lung_disease: "",
        renal_disease: "",
        other_chronic_disease: "",
        n_antenatal_care: "",
        neurologic_disease: "",
        liver_disease: "",
        cancer: "",
        pregnancy_id: "",
        createdAt: "",
        updatedAt: "",
        userBoldId: "",  
    }
  }


  render() {
    return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView>
        <View style={{flex:1}}>
            <Text style={styles.labelStyle}>Complications in current pregnancy</Text>
                 <CardSection>                     
                    <InputMasked
                        maskType={'only-numbers'}
                        placeholder=""
                        label="Current weight (kg)"
                        value={ this.state.antenatalCare.weight == undefined ? "" : `${this.state.antenatalCare.weight}` }
                        underlineColorAndroid='#BBBBBB'
                        keyboardType="numeric"
                        onChangeText={ weight => {
                            this.setState({'antenatalCare': { ...this.state.antenatalCare, "weight" :`${weight}`}})
                        }}
                        minValue={40}
                        maxValue={200}
                        onEndEditing={this.saveAntenatalCare.bind(this) }
                        errorMessage={this.state.antenatalCare.weightError}
                      />
                </CardSection>
                 <CardSection>                                          
                    <InputMasked
                        maskType={'only-numbers'}
                        placeholder=""
                        label="Height (cm)"
                        value={ this.state.antenatalCare.height == undefined ? "" : `${this.state.antenatalCare.height}` }
                        underlineColorAndroid='#BBBBBB'
                        keyboardType="numeric"
                        onChangeText={ height => {
                            this.setState({'antenatalCare': { ...this.state.antenatalCare, "height" :`${height}`}})
                        }}
                        minValue={120}
                        maxValue={220}
                        onEndEditing={this.saveAntenatalCare.bind(this) }
                        errorMessage={this.state.antenatalCare.heightError}
                      />
                </CardSection>
                 {/* <CardSection>                                          
                    <InputMasked
                        maskType={'only-numbers'}
                        placeholder=""
                        label="Number of antenatal care contacts"
                        value={ this.state.disease.n_antenatal_care == undefined ? "" : `${this.state.disease.n_antenatal_care}` }
                        underlineColorAndroid='#BBBBBB'
                        keyboardType="numeric"
                        onChangeText={ n_antenatal_care => {
                            this.setState({'antenatalCare': { ...this.state.disease, "n_antenatal_care" :`${n_antenatal_care}`}})
                        }}
                        minValue={0}
                        maxValue={40}
                        onEndEditing={this.saveDisease.bind(this) }
                        errorMessage={this.state.disease.n_antenatal_care}
                      />
                </CardSection> */}
                
                 <CardSection>
                    <ButtonGroupSection 
                        label="Placenta praevia"
                        onPress={ (i) => { this.updateIndex('placenta_praevia', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.placenta_praevia)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Accreta/increta/percreta placenta"
                        onPress={ (i) => { this.updateIndex('accreta_increta_percreta', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.accreta_increta_percreta)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Abruptio placentae"
                        onPress={ (i) => { this.updateIndex('abruptio_placentae', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.abruptio_placentae)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Other obstetric haemorrhage"
                        onPress={ (i) => { this.updateIndex('other_obstetric_haemorrhage', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.other_obstetric_haemorrhage)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Pre-eclampsia (excludes eclampsia)"
                        onPress={ (i) => { this.updateIndex('pre_eclampsia', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.pre_eclampsia)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Eclampsia"
                        onPress={ (i) => { this.updateIndex('eclampsia', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.eclampsia)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Pyelonephritis"
                        onPress={ (i) => { this.updateIndex('pyelonephritis', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.pyelonephritis)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Malaria"
                        onPress={ (i) => { this.updateIndex('malaria', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.malaria)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Preterm rupture of membranes"
                        onPress={ (i) => { this.updateIndex('preterm_rupture_membranes', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.preterm_rupture_membranes)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Anaemia (Hct ≤ 26% or Hb ≤ 9g/dL)"
                        onPress={ (i) => { this.updateIndex('anaemia', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.anaemia)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Gestational diabetes"
                        onPress={ (i) => { this.updateIndex('gestational_diabetes', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.gestational_diabetes)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                 <CardSection>
                    <ButtonGroupSection 
                        label="Other pregnancy complications"
                        onPress={ (i) => { this.updateIndex('other_pregnancy_complications', i, "anc") } }
                        selectedIndex={parseInt(this.state.antenatalCare.other_pregnancy_complications)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
        </View>
        <View style={{flex:1}}>
            <Text style={styles.labelStyle}>Pre-pregnancy health conditions</Text>
                <CardSection>
                <ButtonGroupSection 
                    label="Chronic Hypertension"
                    onPress={ (i) => { this.updateIndex('chronic_hypertension', i) } }
                    selectedIndex={parseInt(this.state.disease.chronic_hypertension)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Diabetes Mellitus"
                    onPress={ (i) => { this.updateIndex('diabetes_mellitus', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.diabetes_mellitus)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="HIV +"
                    onPress={ (i) => { this.updateIndex('hiv', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.hiv)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="AIDS / HIV wasting syndrome"
                    onPress={ (i) => { this.updateIndex('aids_hiv', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.aids_hiv)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Chronic Anaemia (e.g. sickle cell anaemia)"
                    onPress={ (i) => { this.updateIndex('chronic_anaemia', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.chronic_anaemia)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Obesity"
                    onPress={ (i) => { this.updateIndex('obesity', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.obesity)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Heart disease"
                    onPress={ (i) => { this.updateIndex('heart_disease', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.heart_disease)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Lung disease"
                    onPress={ (i) => { this.updateIndex('lung_disease', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.lung_disease)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Renal disease"
                    onPress={ (i) => { this.updateIndex('renal_disease', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.renal_disease)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Neurologic disease"
                    onPress={ (i) => { this.updateIndex('neurologic_disease', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.neurologic_disease)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Liver disease"
                    onPress={ (i) => { this.updateIndex('liver_disease', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.liver_disease)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Other chronic disease"
                    onPress={ (i) => { this.updateIndex('other_chronic_disease', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.other_chronic_disease)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
                <CardSection>
                <ButtonGroupSection 
                    label="Cancer"
                    onPress={ (i) => { this.updateIndex('cancer', i, "disease") } }
                    selectedIndex={parseInt(this.state.disease.cancer)} 
                    buttons={[
                        'No',
                        'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            </View>         
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  },
  labelStyle: {
    fontSize: 16, 
    fontWeight: 'bold',
    color: '#86939e',	
      padding: 10 
  }
});

export default AntenatalCare;