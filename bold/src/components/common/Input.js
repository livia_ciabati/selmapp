import React from 'react';
import { TextInput, View, Text } from 'react-native';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'

const Input = ({ label, underlineColorAndroid, value, onChangeText, onEndEditing, placeholder, secureTextEntry, autoCapitalize,errorMessage, keyboardType }) => {
    const { inputStyle, labelStyle, containerStyle } = styles;

    return (
        <View>  
            <FormLabel style={labelStyle}>{label}</FormLabel>
            <FormInput 
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                style={inputStyle}
                underlineColorAndroid={underlineColorAndroid}
                value={value}
                onChangeText={onChangeText}
                onEndEditing={onEndEditing}
                autoCapitalize={autoCapitalize}
                keyboardType={keyboardType}  
                testID={label}
                accessibilityLabel={label}
                />
                {
                    errorMessage &&
                    <FormValidationMessage>
                      {errorMessage}
                    </FormValidationMessage>
                }
        </View>

    );
};

const styles = {
    inputStyle: {
      color: '#000',
      paddingRight: 5,
      paddingLeft: 5,
      fontSize: 18,
      lineHeight: 23,
      flex: 2
    },
    labelStyle: {
      fontSize: 18,
      paddingLeft: 20,
      flex: 1
    },
    containerStyle: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center'
    }
  };

export default { Input };

