import React from 'react';
import { Text, View } from 'react-native';

const FormLabel = (props) => {
    const { labelStyle } = styles;
    return (
        <Text style={labelStyle}>{props.children}</Text>
    );
};


const styles = {
  labelStyle: {
    fontSize: 16, 
    fontWeight: 'bold',
    color: '#86939e',	
      padding: 10 
  }
};

export { FormLabel };
