import React from 'react';
import { View, Text } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';


class DateTimeInputMask extends React.Component {

    constructor(props) {
      super(props)
      this.state = { reference: '' };
    
      ////console.log("PROPS" + JSON.stringify(props));
    }
  
    updateValue(e) {
      this.setState({ reference: e.target.value });
    }
  
    checkIsValid() {
        ////console.log("REFERENCE " + this.state.reference);
        valid = this.state.reference.isValid();
        if(valid){
            this.setState({ errorMessage :'Please, provide a valid date.'});
        }
        else
        {
            this.setState({ errorMessage : null});      
        }
    }


    render(){
        return (
            <View style={{flex:1}}>
                <Text style={ styles.labelStyle }>
                    {this.props.label}
                </Text>
                <TextInputMask
                    type={'datetime'}
                    options={this.props.options}                        
                    style={ styles.inputStyle }
                    placeholder={this.props.placeholder}
                    value = {this.props.value}
                    underlineColorAndroid='#BBBBBB'
                    keyboardType="numeric"
                    onChangeText={ this.props.onChangeText } 
                    onEndEditing={ this.props.onEndEditing }
                    errorMessage={ this.state.errorMessage }
                    testID={this.props.label}
                    accessibilityLabel={this.props.label}
                /> 
            </View>
        );
    }
}

const styles = {
    labelStyle: {
      fontSize: 16, 
      fontWeight: 'bold',
      color: '#86939e',
      marginLeft: 10
    },
    inputStyle: { 
        width: '80%', 
        fontSize: 16,
        marginLeft: 5
    }
  };

  export { DateTimeInputMask };