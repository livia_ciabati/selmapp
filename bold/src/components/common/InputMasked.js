import React from 'react';
import { View } from 'react-native';
import { Input } from 'react-native-elements'



class InputMasked extends React.Component {

    constructor(props) {
      super(props)
      this.state = { reference: '' };
    
      state = { errorMessage: this.props.errorMessage }
      //console.log("PROPS " + JSON.stringify(this.props.onChangeText));
    }
    // const InputMasked = ({ label, underlineColorAndroid, value, onChangeText, 
    //     onEndEditing, placeholder, secureTextEntry, autoCapitalize,errorMessage, 
    //     keyboardType, maskType, maskOptions, maskField, maskRef }) => {
    //     const { inputStyle, labelStyle, containerStyle } = styles;

    isValid(value){
        ////console.log("MINIMUN " + JSON.stringify(this.props));
        if(value < parseInt(this.props.minValue)){
            this.setState({errorMessage: this.props.label + " can not be less then " + this.props.minValue});
            ////console.log("MINIMUN " + JSON.stringify(this.props));
        }
        else if(value > parseInt(this.props.maxValue)){
            this.setState({ errorMessage : this.props.label + " can not be more then " + this.props.maxValue });
        }
        else {            
            this.setState({ errorMessage : null });
        }
    }

    render() {
        return (
            <View style={{flex:1}}>           
                <Input 
                    placeholder={this.props.placeholder}
                    label={this.props.label}
                    underlineColorAndroid='transparent'
                    value={this.props.value}
                    returnKeyType = { "next" }
                    onChangeText={(value) => { this.props.onChangeText(value); this.isValid(value); } }
                    autoCapitalize={this.props.autoCapitalize}
                    keyboardType={this.props.keyboardType}  
                    type={ this.props.maskType }
                    options={ this.props.maskOptions }
                    secureTextEntry={this.props.secureTextEntry}
                    autoCorrect={false}
                    style={this.props.inputStyle}
                    onEndEditing={ this.props.onEndEditing }
                    minValue={this.props.minValue}
                    maxValue={this.props.maxValue}
                    errorMessage={this.state.errorMessage}
                    testID={this.props.label}
                    accessibilityLabel={this.props.label}
                />
            </View>
        );
    }
};

const styles = {
    inputStyle: {
      color: '#000',
      paddingRight: 5,
      paddingLeft: 20,
      fontSize: 14,
      lineHeight: 23,   
      flex: 1
      //width: '100%'
    },
    labelStyle: {
      fontSize: 18,
      paddingLeft: 20,
      flex: 1
    },
    containerStyle: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center'
    }
  };

export { InputMasked };

