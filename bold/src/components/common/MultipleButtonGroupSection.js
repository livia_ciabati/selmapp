import React from 'react';
import { View } from 'react-native'
import { ButtonGroup } from 'react-native-elements'
import { FormLabel } from './FormLabel';

const MultipleButtonGroupSection = ({ title, buttons, choices = ['No','Yes']  }) => {
    const { buttonGroupContainerHorizontal, buttonGroupButton, labelStyle } = styles;
    count = 0;
    buttonsList = buttons.map( button => { 
        count = count + 1;
        return (
        <View key={count} style={{flex:1}}>
            <FormLabel>{button.label}</FormLabel>
            <ButtonGroup
                selectedIndex={button.selectedIndex} 
                onPress={button.onPress}
                buttons={choices}
                containerStyle={ buttonGroupContainerHorizontal }
                buttonStyle={buttonGroupButton}
                testID={button.label}
                accessibilityLabel={button.label}
            />
        </View> );
    });
    return (
        <View style={{flex:1}}>
            <FormLabel>{title}</FormLabel>
            { buttonsList }
        </View>
    );
};

const styles = {
  buttonGroupContainerHorizontal: {
    flexDirection: 'row',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  },
  labelStyle: {
    fontSize: 16, 
    fontWeight: 'bold',
    color: '#86939e',	
      padding: 10 
  }
};

export { MultipleButtonGroupSection };
