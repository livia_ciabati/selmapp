// Import libraries for making a component
import React, { Component } from 'react';
import { Dimensions, FlatList, Text, View } from 'react-native';
import { Overlay } from 'react-native-elements';


class OverlayForms extends Component {

	constructor(props) {
        super(props)	
        
		//Navigation.events().bindComponent(this);
	}

	state = {
		formLabel: 'Woman in Ward', searchInput:'', womenList :[], numAlerts: 0
	};

    render() {
        return(
            <Overlay
                isVisible={this.state.isVisible}
                windowBackgroundColor="rgba(255, 255, 255, .5)"
                overlayBackgroundColor="red"
                width="auto"
                height="auto"
                >
                <Text>Hello from Overlay!</Text>
            </Overlay>
        )
    }

}

//export default OverlayForms;
