// Import libraries for making a component
import React from 'react';
import { Dimensions, FlatList, Text, View } from 'react-native';
import { Button, Badge  } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { diffTimeNowInMinutes } from "../../repositories/BaseRepository";

const DiagnosisBadge = ({diagnosis}) => {

    statusColor = ["success","primary", "warning", "error"]
    diagnosis = diagnosis.filter(obj => {
        return obj.value == 1
    });
    //console.log(diagnosis);

    return (	
        <FlatList
            data={ diagnosis }
            numColumns={2}
            keyExtractor={item => item.diagnosis_id != null ? item.diagnosis_id.toString() : "0" }
            renderItem={({ item }) => {
                return (
                    <View >
                        <Badge value={item.label} status={this.statusColor[item.risk_level]} />
                    </View>
                )
            }}
        />

    );
}

const deviceWidth = Dimensions.get('window').width;
const styles = {
    item: {
        width: 100,
        flexGrow: 1
      },
};
//Make the component avaibale to other parts of the app
export default DiagnosisBadge;

