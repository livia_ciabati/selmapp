import React from 'react';
import { View } from 'react-native'
import { Text, ButtonGroup } from 'react-native-elements'

const ButtonGroupSection = ({ label, onPress, selectedIndex, buttons, vertical=true  }) => {
    const {buttonGroupContainerVertical, buttonGroupContainerHorizontal, buttonGroupButton, labelStyle } = styles;
    return (
        <View style={{flex:1}}>
            <Text style={labelStyle}>{label}</Text>
            <ButtonGroup
                selectedIndex={selectedIndex} 
                onPress={onPress}
                buttons={buttons}
                containerStyle={ vertical ? buttonGroupContainerVertical : buttonGroupContainerHorizontal }
                buttonStyle={ styles.buttonStyle }	
                textStyle={ styles.buttonTextStyle }										
                selectedButtonStyle={ styles.selectedButton }
                selectedTextStyle={ styles.selectedTextStyle }
                testID={label}
                accessibilityLabel={label}
            />
        </View>
    );
};

const styles = {
  buttonGroupContainerVertical: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupContainerHorizontal: {
    flexDirection: 'row',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  },
	buttonStyle: {
		width: 'auto', 
		height: 'auto', 
		padding: 10, 
		borderWidth: 1, 
		borderColor: "#FFFFFF",
	},
	buttonTextStyle: {
		textAlign: 'center',
	},
	selectedButton: {
		backgroundColor: '#2096F3',
	},
	selectedTextStyle: {
		textAlign: 'center',
		color: '#FFF',
	},
	labelStyle: {
	  fontSize: 16, 
	  fontWeight: 'bold',
	  color: '#86939e',	
    padding: 10,
		width: 'auto'
	}
};

export { ButtonGroupSection };
