import React from 'react';
import { ActivityIndicator, Dimensions } from 'react-native';

const Loader = ({animating}) => {
    const {activityIndicator} = styles;

    let deviceHeight = 0;
    if(animating){
        //backgroundColor = '#F5FCFF88';
        deviceHeight = Dimensions.get('window').height;
    }
    
    return (
        <ActivityIndicator 
            size="large"  
            animating = {animating}
            color = '#bc2b78'
            style = {[activityIndicator,  { height: deviceHeight }]} />
    );
};

const styles = {
    activityIndicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#F5FCFF88'
    }
};

export { Loader };
