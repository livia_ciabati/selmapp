// Import libraries for making a component
import React from 'react';
import { Dimensions, FlatList, Text, View } from 'react-native';
import { Button  } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { diffTimeNowInMinutes, getCurrentTimeToDatabase } from "../../repositories/BaseRepository";

//Make a component
const AlertsItem = ({alert, markAlertAsSnozzed, markAlertAsRead}) => {
    const { textStyle, viewStyle, container, buttonArea, button } = styles;
    let time = parseInt(diffTimeNowInMinutes(alert.createdAt));
    const stringTimeAgo = time < 60 ? "minutes ago" : parseInt(time/60) == 1 ? "hour ago" : "hours ago";

    time = time < 60 ? time : parseInt(time/60);

    return (
        <View style={ container } >
            <View style={ styles.titleArea } >
                <Icon style={ styles.icon } name={ alert.nameImage == undefined || alert.nameImage == '' ? 'exclamation' : alert.nameImage } size={25} />
                <Text style={ styles.titleStyle }>{ alert.title }</Text>
            </View>
            <View style={ styles.messageArea }  >
                <Text style={ styles.textStyle }>{ alert.text }</Text>        
                <Text style={ styles.dateTimeStyle }>{ time } { stringTimeAgo }</Text>        
            </View>
            { alert.show_button == undefined  ?
                <View style={ buttonArea }>
                    <Button buttonStyle={ button } type="outline" onPress={ () => markAlertAsSnozzed(alert.admission_alerts_id, alert.alert_id) } title='Snoozze' />
                    <Button buttonStyle={ button } type="outline" onPress={ () => markAlertAsRead(alert.admission_alerts_id, alert.alert_id) } title='Read and agreed' />
                </View>
                : 
                <View></View>
            }
        </View>
    );
};


const AlertsList = ({alert, markAlertAsSnozzed, markAlertAsRead}) => {
    if(alert.length == 0)
    {
        alert[0] = {
            "nameImage": "check-circle",
            title: "No alerts at this time",
            text: "",
            createdAt: getCurrentTimeToDatabase(),
            show_button : false
        }
    }
    return (	
        <FlatList
            data={ alert }
            keyExtractor={item => item.admission_alerts_id != null ? item.admission_alerts_id.toString() : "0" }
            renderItem={({ item }) => {
                return (
                    <AlertsItem alert={item} markAlertAsSnozzed={markAlertAsSnozzed} markAlertAsRead={markAlertAsRead}  />
                )
            }}
        />

    );
}

const deviceWidth = Dimensions.get('window').width;
const styles = {
    container: {
        flex: 1,
        padding: 10,
        marginLeft:10,
        marginRight:10,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    buttonArea: {
        flexDirection: 'row'
    },
    titleArea: {
        flexDirection: 'row',
        marginLeft:10,
        marginRight:10
    },
    messageArea: {
        flexDirection: 'column',
        marginLeft:10,
        marginRight:10,
        marginBottom:10
    },
    titleStyle: {
        fontSize: 30
    },
    textStyle: {
        fontSize: 18
    },
    dateTimeStyle: {
        fontSize: 10,
        textAlign: "right"
    },
    button: {
        marginLeft: 10,
        width: (deviceWidth-70)/2
    },
    icon: {
        marginRight:10
    }
};
//Make the component avaibale to other parts of the app
export default AlertsList;
