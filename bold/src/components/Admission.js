import React, { Component } from 'react';
import { Button, Card, CardSection, ButtonGroupSection, InputMasked  } from './common';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import { FormLabel, ButtonGroup } from 'react-native-elements'
import Toast from 'react-native-easy-toast'
import AdmissionRepository from '../repositories/AdmissionRepository';
import PregnancyRepository from '../repositories/PregnancyRepository'
import { Input } from 'react-native-elements';

class Admission extends Component {

  constructor(props) {
    super(props)
    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(stateVariable, selectedIndex) {
    this.state[stateVariable] = parseInt(selectedIndex);
    var newState = Object.assign({}, this.state, {});
    this.setState(newState);

    this.saveAdmission();
  }

  componentDidMount(){
    ////console.log("ADMISSION componentDidDisappear");
    if(this.props.route.adm_id != undefined && this.props.route.adm_id > 0 &&
      this.props.route.preg_id != undefined && this.props.route.preg_id > 0 &&
      this.props.route.woman_id != undefined && this.props.route.woman_id > 0)
    {
      this.state.id = this.props.route.adm_id;
      PregnancyRepository.getPregnancyById(this.props.route.preg_id)
      .then((data) => {
        if(data.estimate_gestational_age >= 34)
        {
          this.setState({gestational_age_less_34_week: 0});
        }
        else {
          this.setState({gestational_age_less_34_week: 1});
        }
      });      
    }

    if(this.props.route.adm_id != undefined && this.props.route.adm_id > 0)
    {
      AdmissionRepository.getAdmissionById(this.props.route.adm_id)
      .then((data) => {
        ////console.log("ADMISSION DATA: " + JSON.stringify(data))        
        this.setState(data);
        this.props.route.adm_id = data.id;
      })
    }
  }

	componentDidAppear() {    
		//console.log("Admission componentDidAppear");
  }
  
	componentDidDisappear() {    
		//console.log("Admission componentDidDisappear");
  }
  
  saveAdmission() {
    this.state.enabled = 1;
    this.state.modified_by_user_id = this.props.route.user_id;
    this.state.userBoldId = this.props.route.user_id;
    this.state.admission_status = 1;

    //console.log("NEW STATE " + JSON.stringify(this.state))
    
    if(this.props.route.adm_id != undefined && this.props.route.adm_id > 0) {
      this.state.id = this.props.route.adm_id;
    }
    
    if(this.props.route.preg_id != undefined && this.props.route.preg_id > 0) {
      this.state.pregnancy_id = this.props.route.preg_id;
    }
    
    AdmissionRepository.updateAdmission(this.state)
    .then((data) => {
      this.refs.toast.show('Updated', 500);
    });
  }

  state = {
    mode_onset_labour:null,
    hours_labour_before_admission:null,
    fetal_movements_last_2_hours:null,
    first_stage_labour:null,
    fetal_death:null,
    advanced_first_stage_labour:null,
    multiple_pregnancy:null,
    gestational_age_less_34_week:null,
    elective_c_section:null,
    pre_labour_c_section:null,
    emergency_c_section_or_laparotomy:null,
    attemped_labour_induction:null,
    false_labour:null,
    non_emancipated_minors_without_guardian:null,
    unable_give_consent_or_health_problem:null,
    elegible:null,            
    symphysis_fundal_height:null,
    abdominal_circumference:null,
    pregnancy_id:null,
  }


  render() {
    return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView>
          <Card>

            <CardSection>
                <ButtonGroupSection
                    label="Mode of onset of labour"
                    selectedIndex={ parseInt(this.state.mode_onset_labour)} 
                    onPress={ (i) => { this.updateIndex('mode_onset_labour', i) } }
                    buttons={[
                      'Spontaneous',
                      'Induced'
                    ]}
                    vertical={false}
                    //0= spontaneous 1= induced
                />
            </CardSection>
            {
              (this.state.mode_onset_labour == 0) ? 
              <CardSection>
                  <InputMasked
                      maskType={'only-numbers'}
                      maskRef = { ref => this._hours_labour_before_admission = ref }
                      label = "If spontaneous onset of labour, specify duration of labour before admission in hours"
                      placeholder= ""
                      value={this.state.hours_labour_before_admission}
                      underlineColorAndroid='#BBBBBB'
                      keyboardType="numeric"
                      onChangeText={ hours_labour_before_admission => {
                        if(hours_labour_before_admission.length < 1 || hours_labour_before_admission.length > 2){
                            this.setState({'hours_labour_before_admissionError':'Please, provide a valid number.'});
                          }else{
                            this.setState({'hours_labour_before_admissionError': null});
                          } 
                          this.setState({'hours_labour_before_admission': hours_labour_before_admission})
                        }
                      }
                      errorMessage={this.state.hours_labour_before_admissionError}
                      onEndEditing={this.saveAdmission.bind(this) }
                  />
              </CardSection> : null
            }
            <CardSection>
                <ButtonGroupSection
                    label="Fetal movements in the last 2 hours, as self reported"
                    selectedIndex={ parseInt(this.state.fetal_movements_last_2_hours) } 
                    onPress={ (i) => { this.updateIndex('fetal_movements_last_2_hours', i) } }
                    buttons={[
                      'No changes/increase',
                      'Reduced',
                      'Absent'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="First stage of labour"
                    selectedIndex={ parseInt(this.state.first_stage_labour)} 
                    onPress={ (i) => { this.updateIndex('first_stage_labour', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Fetal death"
                    selectedIndex={ parseInt(this.state.fetal_death)} 
                    onPress={ (i) => { this.updateIndex('fetal_death', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Advanced first stage of labour (cervical dilation ≥7cm)"
                    selectedIndex={ parseInt(this.state.advanced_first_stage_labour)} 
                    onPress={ (i) => { this.updateIndex('advanced_first_stage_labour', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Multiple pregnancy"
                    selectedIndex={ parseInt(this.state.multiple_pregnancy) } 
                    onPress={ (i) => { this.updateIndex('multiple_pregnancy', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            {/* <CardSection>
                <ButtonGroupSection 
                    label="Gestational age less than 34 weeks"
                    selectedIndex={ parseInt(this.state.gestational_age_less_34_week) } 
                    onPress={ (i) => { this.updateIndex('gestational_age_less_34_week', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection> */}
            <CardSection>
                <ButtonGroupSection 
                    label="Elective C-section"
                    selectedIndex={ parseInt(this.state.elective_c_section)} 
                    onPress={ (i) => { this.updateIndex('elective_c_section', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Pre-labour C-section"
                    selectedIndex={ parseInt(this.state.pre_labour_c_section)} 
                    onPress={ (i) => { this.updateIndex('pre_labour_c_section', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Indication for emergency C-Section or laparotomy on admission"
                    // no banco a variavel esta com '-' no c-section
                    selectedIndex={ parseInt(this.state.emergency_c_section_or_laparotomy) } 
                    onPress={ (i) => { this.updateIndex('emergency_c_section_or_laparotomy', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Attempted induction of labour, but no labour achieved"
                    selectedIndex={ parseInt(this.state.attemped_labour_induction) } 
                    onPress={ (i) => { this.updateIndex('attemped_labour_induction', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="False labour"
                    selectedIndex={ parseInt(this.state.false_labour) } 
                    onPress={ (i) => { this.updateIndex('false_labour', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Non-emancipated minors without a guardian"
                    selectedIndex={parseInt(this.state.non_emancipated_minors_without_guardian)} 
                    onPress={ (i) => { this.updateIndex('non_emancipated_minors_without_guardian', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            <CardSection>
                <ButtonGroupSection 
                    label="Woman is not capable of giving consent due to labour distress or any health problem"
                    selectedIndex={ parseInt(this.state.unable_give_consent_or_health_problem) } 
                    onPress={ (i) => { this.updateIndex('unable_give_consent_or_health_problem', i) } }
                    buttons={[
                      'No',
                      'Yes'
                    ]}
                    vertical={false}
                />
            </CardSection>
            {/* <CardSection>
                <InputMasked
                    maskType={'only-numbers'}
                    maskRef = { ref => this._symphysis_fundal_height = ref }
                    label = "Symphysis-fundal height (cm)"
                    placeholder= ""
                    value={this.state.symphysis_fundal_height}
                    underlineColorAndroid='#BBBBBB'
                    keyboardType="numeric"
                    onChangeText={ symphysis_fundal_height => {
                      if(symphysis_fundal_height.length < 1 || symphysis_fundal_height.length > 2){
                          this.setState({'symphysis_fundal_heightError':'Please, provide a valid number.'});
                        }else{
                          this.setState({'symphysis_fundal_heightError': null});
                        } 
                        this.setState({'symphysis_fundal_height': symphysis_fundal_height})
                      }
                    }
                    errorMessage={this.state.symphysis_fundal_heightError}
                    onEndEditing={this.saveAdmission.bind(this) }
                />
            </CardSection> */}
          </Card>
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  }
});



export default Admission;