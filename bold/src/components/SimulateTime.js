import React, { Component } from 'react';
import { backToTheFuture } from './../repositories/functions';
import { Navigation } from 'react-native-navigation';
import { closeSidebar } from '../navigation';
import { StyleSheet, View, Alert } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Loader } from './common/Loader';

class SimulateTime extends Component {
    constructor(props){
        super(props)
		Navigation.events().bindComponent(this);
    }

    state = { animating: false }

    componentDidMount(){

    }

	showLoading = () => {    
		this.setState({animating: true});
	}

	hideLoading = () => {
		this.setState({animating: false});
    } 
    
    callSimulateTime(time){
		this.showLoading();	
        backToTheFuture(time)
        .then(()=> {
            this.hideLoading();	
            Alert.alert(
                'Time updated',
                time + " minutes has passed",
                [
                  {					
                    text: 'Okey', onPress: () => { 
                     }
                  }
                ]
              );
        })
    }

		navigationButtonPressed({buttonId}) {
			if (buttonId === 'cancelBtn') {
				Navigation.dismissModal(this.props.componentId);
			} else if (buttonId === 'saveBtn') {
				alert('saveBtn');
			}else if(buttonId == 'sidebarButton') {
				closeSidebar();
			}
		}
	
    list = [
        {
          title: 'Add 15 minutes',
          icon: 'add-alarm',
          onPress: () => { this.callSimulateTime(15);}
        },
        {
          title: 'Add 30 minutes',
          icon: 'add-alarm',
          onPress: () => { this.callSimulateTime(30);}
        },
        {
          title: 'Add 1 hour',
          icon: 'add-alarm',
          onPress: () => { this.callSimulateTime(60);}
        },
        {
          title: 'Add 2 hour',
          icon: 'add-alarm',
          onPress: () => { this.callSimulateTime(120);}
        },
        {
          title: 'Add 4 hour',
          icon: 'add-alarm',
          onPress: () => { this.callSimulateTime(240);}
        },
      ]

    render(){
        return (
            <View style={styles.container} >
				<View>
				{
					this.list.map((item) => (
					<ListItem
						key={item.title}
						title={item.title}
						leftIcon={{name: item.icon}}
						style={styles.containerList}
						onPress={item.onPress}
					/>
					))
				}
				</View>
				<Loader animating = { this.state.animating } />
			</View>
        )
    }
}


const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF'
	},	
	containerAvatar: {
		flex: 3,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF',
		alignItems:'center'
	},	
	containerList: {
		height: 60,
		justifyContent: 'center',
		backgroundColor: '#F5FCFF',
		alignItems:'center'
	}
});


export default SimulateTime;