import React, { Component } from 'react';
import { Button, Card, CardSection } from './common';
import { StyleSheet,Image, ScrollView, View, Alert, Dimensions } from 'react-native';
import { womanListLayout, tutorial } from '../navigation';
import { database } from '../repositories/DatabaseManager';
import CreateDatabaseRepository from '../repositories/CreateDatabaseRepository';
import { Loader } from './common/Loader';
import { Input } from 'react-native-elements'

let SQLite = require('react-native-sqlite-storage')
let CryptoJS = require("crypto-js");

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 16);
const imageWidth = dimensions.width;
const secretKey = 'B01D_S3LM4';
let db;

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.showLoading = this.showLoading.bind(this);
        this.hideLoading = this.hideLoading.bind(this);
    }

    state = { 
        username: '', 
        password: '', 
        usernameError: null , 
        passwordError: null,
        animating: false
    };

    componentDidMount(){
        this.showLoading();
        database.open().then((db) => 
        {
            CreateDatabaseRepository.createIfNotExistDatabase(db)
            .then(() => {
                CreateDatabaseRepository.loadDataToDatabase();
                CreateDatabaseRepository.listTables().then((numTabelas) => {
                    this.hideLoading();
                });
            });
        }); 

    }

    showLoading = () => {    
        this.setState({animating: true});
    }

    hideLoading = () => {
        this.setState({animating: false});
    } 
    
    login = async () => {   
        ///descomentar as 2 proximas linhas
        //womanListLayout(15);
        //tutorial(15);    
        //return;

        this.showLoading();
        this.setState({usernameError:null, passwordError:null})

        const requestInfo = {
            method: 'POST',
            body: JSON.stringify({username: this.state.username, password: this.state.password}),
            headers: new Headers({
                'Content-type':'application/json'
            })
        };
        //console.log(requestInfo);

        if(this.state.username == '' && this.state.password == ''){           
            this.setState({usernameError:'Username can not be blank', passwordError:'Password can not be blank'})
            this.hideLoading();
            return
        }
        else if(this.state.username == ''){            
            this.setState({usernameError:'Username can not be blank'})
            this.hideLoading();
            return
        }
        else if (this.state.password == ''){            
            this.setState({passwordError:'Password can not be blank'})
            this.hideLoading();
            return
        }

                
        var secretKeyHash = CryptoJS.SHA256(this.state.password.toString()).toString(CryptoJS.enc.Hex);//.slice(0,32);
        //secretKeyHash = CryptoJS.enc.Utf8.parse(secretKeyHash).toString();
        //var ciphertext = CryptoJS.AES.encrypt(this.state.password.toString(), secretKey).toString();
        // console.log("before " + this.state.username + " " +  secretKeyHash + " " + this.state.password);
        // console.log(CryptoJS.SHA256("admin".toString()).toString(CryptoJS.enc.Hex));

        ///check on database        
        database.getDatabase().then(db => { 
            db.transaction(
			(tx) => {
                tx.executeSql('Select id, username, password, email, verificationToken from UserBold where username=? and password=?', 
                [this.state.username, secretKeyHash], 
                (tx, results) => {
                    ////console.warn(tx);
                    //console.log("num results " + results.rows.length);
                        var len = results.rows.length;
                        if(len > 0){
                            let id = parseInt(results.rows.item(0).id);
                            //console.log("*************************************  BANCO: " + JSON.stringify( results.rows.item(0) ));
                            this.hideLoading();  
                            womanListLayout(id);            
                        }
                        else{
                            /// if the user is not in the database, check if there is internet connection
                    
                            //if there is internet connection, look for the user on api
                            fetch("http://200.144.254.4:3333/api/UserBolds/login", requestInfo)
                            .then(response => {
                                if(response.ok){
                                    this.hideLoading();
                                    return response.text();
                                }
                                else{
                                    this.hideLoading();
                                    throw new Error("Username or password was not found on the server.")
                                }
                                //console.log(response);
                            })
                            .then (token => { 
                                //console.log("passou aqui " + this.state.username +" "+ secretKeyHash +" "+ token)
                                db.transaction(
                                    (tx) => {                                        
                                        tx.executeSql('Insert into UserBold (id, username, password, verificationToken, email, createdAt, updatedAt)'+
                                            ' values (?, ?, ?, ?, ?, ?, ?) ', [JSON.parse(token).userId, this.state.username, secretKeyHash, JSON.parse(token).id, 'replace for email', 
                                                JSON.parse(token).created, JSON.parse(token).created], 
                                            (tx, results) => {
                                                //console.log("fez insert " + results);
                                    });
                                },
                                (tx, err) => {
                                    this.hideLoading();
                                    //console.log("warn insert " + tx);
                                });
                               //console.warn("entrou por api");                       
                                
                                if(token != null) {         
                                    tutorial(JSON.parse(token).userId);            
                                    //womanListLayout(JSON.parse(token).userId);  
                                }                               
                            })
                            .catch(error => { 
                                Alert.alert(
                                    'Error',
                                    error.message,
                                    [
                                        {
                                            text: 'OK', 
                                            onPress: () => {
                                                this.hideLoading();
                                                //console.log('Ask me later pressed')}
                                            }
                                        }
                                    ],
                                    { cancelable: false }
                                )
                            });
                        }
				    },
					(tx, err) => {
						//console.warn(tx);
					}
				)
            });      
        });
    }

    render() {
        return (  
            <View style={styles.container} >
                <ScrollView>
                    <Card style={ styles.container }>
                        <CardSection style={ styles.container }>
                            <Image source ={require('../../img/Logo.png')} 
                                    style={ styles.image }/>
                        </CardSection>
                        <CardSection>
                            <Input 
                                testID="username"
                                accessibilityLabel={'username'}
                                placeholder="username"
                                label="Username"
                                underlineColorAndroid='transparent'
                                value={this.state.username}
                                returnKeyType = { "next" }
                                onChangeText={username => this.setState({ username })} 
                                autoCapitalize = 'none'
                                errorMessage={this.state.usernameError}
                            />
                        </CardSection>
                        <CardSection>
                            <Input
                                testID="password"
                                accessibilityLabel={'password'}
                                secureTextEntry
                                placeholder="password"
                                label="Password"
                                underlineColorAndroid='transparent'
                                value={this.state.password}
                                onChangeText={password => this.setState({ password })} 
                                errorMessage={this.state.passwordError}
                            />
                        </CardSection>

                        <CardSection>
                            <Button onPress={this.login.bind(this)} 
                                testID="btLogin" 
                                accessibilityLabel="btLogin"
                            >
                                Log in
                            </Button>
                        </CardSection>
                    </Card> 
                </ScrollView>
                <Loader animating = {this.state.animating} />
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },  
    item: {
      flex: 1,
      overflow: 'hidden',
      alignItems: 'center',
      backgroundColor: 'orange',
      position: 'relative',
      margin: 10
    },  
    image: {
        width: imageWidth,
        resizeMode: 'contain'
    },
    activityIndicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
  })

export default LoginForm;
