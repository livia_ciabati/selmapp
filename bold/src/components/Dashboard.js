import React, { Component } from 'react';
import { StyleSheet,Text, ScrollView, View, FlatList, BackHandler, TouchableOpacity } from "react-native";
import { Navigation } from 'react-native-navigation';
import { closeSidebar } from '../navigation';
import { Button, Card, CardSection, Input } from './common';
import { Icon, Divider } from 'react-native-elements';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import WomanRepository     from '../repositories/WomanRepository';
import PregnancyRepository from '../repositories/PregnancyRepository';
import AdmissionRepository from '../repositories/AdmissionRepository';
import DashboardRepository from '../repositories/DashboardRepository';

class Dashboard extends Component {

  constructor(props) {
    super(props)

    this.state.reload= this.props.reload;
    this.loadDashboardData = this.loadDashboardData.bind(this);    
  }

  loadDashboardData(adm_id)
  {    
    DashboardRepository.getDashboardByNameAndAdmId('maternal_dashboard', adm_id).then(data => {
      var len = data.length;
      var d = [];
      ////console.log("************************* DATA *********************************" + JSON.stringify(data))
      for (let i = 0; i < len; i++) {
        let row = data[i];

        if(row.name_question == 'systolic_blood_pressure'){
          let combination_id = row.id;
          let systolic = (row.answer == null ? "?" : row.answer.replace("mmHg","")); 
          let diastolic = '';
          let j =0;
          while(data[j].name_question != 'diastolic_blood_pressure'){
            j++;
          }
          if(j<len){
            diastolic = (data[j].answer == null ? "?" : data[j].answer.replace("mmHg","")); 
            combination_id = combination_id + "," + data[j].id;
          }
          let pressureFormated = systolic + "/" +  diastolic;
          let questionFormated = "Blood Pressure (mmHg)";
          d.push({id: combination_id,
                  admission_id: row.admission_id,
                  label_question: questionFormated,
                  name_question: row.name_question,
                  answer: pressureFormated,
                  createdAt: (row.createdAt!=null ? 
                    row.createdAt : "--/--/----")});

        } else {
          if(row.name_question == 'number_uterine_contractions'){
            let combination_id = row.id;
            let number = (row.answer == null ? "?" : row.answer);
            let duration = '';
            let j =0;
            while(data[j].name_question != 'duration_uterine_contractions'){
              j++;
            }
            if(j<len){
              duration = (data[j].answer == null ? "?" : data[j].answer); 
              combination_id = combination_id + "," + data[j].id;
            }
            let contractionFormated = number + "/" +  duration;
            let questionFormated = "Number/Duration of uterine contractions (10min)";
            d.push({id: combination_id,
                    admission_id: row.admission_id,
                    label_question: questionFormated,
                    name_question: row.name_question,
                    answer: contractionFormated,
                    createdAt: (row.createdAt!=null ? 
                      row.createdAt : "--/--/----")});

          } else {
            if(row.name_question != 'diastolic_blood_pressure' && row.name_question != 'duration_uterine_contractions'){
              d.push({id: row.id,
                    admission_id: row.admission_id,
                    label_question: row.label_question,
                    name_question: row.name_question,
                    answer: (row.answer!=null ? row.answer : "?"),
                    createdAt: (row.createdAt!=null ? 
                      row.createdAt : "--/--/----")});  
            }
          }  
        }
      }
      this.setState({ maternal_data: d });     
    });

    //fetal data 
    DashboardRepository.getDashboardByNameAndAdmId('fetal_dashboard', adm_id).then(data => {
      this.setState({ fetal_data: data }); 
    });


    //Complementary data 
    DashboardRepository.getDashboardByNameAndAdmId('first_assessment', adm_id).then(data => {
      this.setState({ other_data: data }); 
    });

    // //admission data 
    // DashboardRepository.getDashboardByNameAndAdmId('first_assessment', adm_id).then(data => {
    //   this.setState({ admission_data: data }); 
    // });
    
  }

  state = {
    maternal_data: [{id: "", admission_id: "" ,label_question: "",name_question: "", answer: "", createdAt: ""}],
    fetal_data: [{id: "", admission_id: "" ,label_question: "",name_question: "", answer: "", createdAt: ""}],
    other_data: [{id: "", admission_id: "" ,label_question: "",name_question: "", answer: "", createdAt: ""}],
    admission_data: [],
    admission: {id_wom:"",age: "", estimate_gestacional_age: "", id_admission: "", g: "", p: "", a: ""},
    admission:{
      admission_status:"",userBoldId:"",elective_c_section:"",gestational_age_less_34_week:"",hours_labour_before_admission:"",createdAt:"",multiple_pregnancy:"",false_labour:"",
      elegible:"",symphysis_fundal_height:"",fetal_death:"",advanced_first_stage_labour:"",unable_give_consent_or_health_problem:"",first_stage_labour:"",fetal_movements_last_2_hours:"",mode_onset_labour:"",
      id:"",pre_labour_c_section:"",emergency_c_section_or_laparotomy:"",attemped_labour_induction:"",non_emancipated_minors_without_guardian:"",abdominal_circumference:"",updatedAt:"",pregnancy_id:""},
    pregnancy:{
      userBoldId:"",createdAt:"",
      woman_id:"",expected_delivery_date:"",estimate_gestional_age_method:"",estimate_gestional_age:"",referred:"",id:"",outcome_last_childbirth:"",parity:"",num_previous_abort:"",updatedAt:"",
      childbirth_last_delivery:"",gravidity:"",n_previous_spontaneous_abort:"",n_previous_induced_abort:"",num_previous_c_section:"",previous_uterine_surgery:"",date_of_last_mensturation:"",n_previous_stillbirths:""},
    woman: {id: "", name: "",enabled:"",modified_by_user_id:"",userBoldId:"",birthday:"",age:"",createdAt:"",gainful_occupation:"",marital_status:"",education_level:""}

  }
  
  showLoading = () => {    
    this.setState({animating: true});
  }

  hideLoading = () => {
      this.setState({animating: false});
  } 

  navigationButtonPressed({buttonId}) {
    if (buttonId === 'cancelBtn') {
      Navigation.dismissModal(this.props.componentId);
    } else if (buttonId === 'saveBtn') {
      alert('saveBtn');
    }else if(buttonId == 'sidebarButton') {
      closeSidebar();
    }
  }

  componentDidMount(route, navigator) 
  {
    ////console.log("COMPONENTE DID MOUNT - STATE " + JSON.stringify(this.state.reload));
      if(this.props.route.adm_id != undefined){
        AdmissionRepository.getAdmissionById(this.props.route.adm_id)
          .then((adm) => {
            this.setState({admission: adm});
            //Verificar area de tags da admissao
            //this.setState({ admission_data: adm }); 
            PregnancyRepository.getPregnancyById(adm.pregnancy_id)
            .then((preg) => {
              this.setState({pregnancy : preg});
              WomanRepository.getWomanById(preg.woman_id)
              .then((wom) => {
                this.setState({woman : wom});
              });
            });
          });
          
        this.loadDashboardData(this.props.route.adm_id);
      }
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
    ////console.log("COMPONENT WILL UNMOUNT WRAPPED ELEMENT" + JSON.stringify(this.props));
  }

  componentDidAppear(){
    ////console.log("************************************************************ DASHBOARD apear: ")
    if(this.props.route.adm_id != undefined){
      this.loadDashboardData(this.props.route.adm_id);
    }
  }

  backHandler = () => {
    return false;
  }

  getAnswersQuestion(id) {
    Navigation.showModal({
      stack: {
        children: [{
          component: {
            name: 'AssessmentQuestion',
            passProps: {
              text: 'stack with one child',
              adm_id: this.props.route.adm_id,
              question_id: id,              
              onUnmount: () => console.log('MODAL DISMISS')
            },
            options: {
              topBar: {
                title: {
                  text: 'Question'
                }
              }
            }
          }
        }]
      }
  });
  }


  render() {
    return (
      <View style={styles.container}>
        {/* <View style={styles.containerLabel}>
          <Text style={styles.informationText}>
            Name: {this.state.woman.name}  
          </Text>
          <Text style={styles.informationText}>
            Age: {this.state.woman.age} G{this.state.pregnancy.gravidity} P{this.state.pregnancy.parity} A{this.state.pregnancy.num_previous_abort}
          </Text>
        </View> */}
        <View>
          <FlatList
            data={this.state.admission_data}
            keyExtractor={item => item.id.toString()}
            numColumns={1}
            renderItem={
              ({ item }) => {
                  return (
                    <View style={(styles.warnningAdmissionView)}>
                      <Text style={styles.warnningAdmissionText}>
                        {item.label_question}
                      </Text>
                    </View> 
                  )  
              }
            }
          />
        </View>
        <View style={styles.containerInfo}>
          <ScrollView>
            <View style={styles.maternalDataView}>
						  <FontAwesome5 name={"female"} size={25} style={{textAlign:'center', color:'#000' }} />
              <Text style={styles.iconDashboard}>
                {"Maternal Data"}
              </Text>
              <Divider style={{ backgroundColor: '#bfbfbf' }} />
              <FlatList
                data={this.state.maternal_data}
                keyExtractor={item => item.id.toString()}
                numColumns={2}
                renderItem={
                  ({ item }) => {
                      return (
                        <View style={(!item.answer.includes( "?" ) ? styles.item : styles.itemNoInformation)}>
                          <TouchableOpacity style={styles.name}  /*onPress={() => this.getAnswersQuestion(item.id)}*/ >
                            <Text style={styles.questionFormat}>
                              {item.label_question}
                            </Text>
                            <Text style={styles.answerFormat}>
                              {item.answer}
                            </Text>
                            <Text style={styles.dateLastAnswerFormat}>
                              {item.createdAt}
                            </Text>
                          </TouchableOpacity>
                        </View> 
                      )  
                  }
                }
              />
            </View>
            <View style={styles.fetalDataView}>
						  <FontAwesome5 name={"baby"} size={25} style={{textAlign:'center', color:'#000' }} />
              <Text style={styles.iconDashboard}>
                {"Fetal Data"}
              </Text>
              <Divider style={{ backgroundColor: '#bfbfbf' }} />
              <FlatList
                data={this.state.fetal_data}
                keyExtractor={item => item.id.toString()}
                numColumns={2}
                renderItem={
                  ({ item }) => {
                      return (
                        <View style={(item.answer!="?" ? styles.item : styles.itemNoInformation)}>
                          <TouchableOpacity style={styles.name}  /*onPress={() => this.getAnswersQuestion(item.id)}*/ >
                            <Text style={styles.questionFormat}>
                              {item.label_question}
                            </Text>
                            <Text style={styles.answerFormat}>
                              {item.answer}
                            </Text>
                            <Text style={styles.dateLastAnswerFormat}>
                              {item.createdAt}
                            </Text>
                          </TouchableOpacity>
                        </View> 
                      )  
                  }
                }
              />
            </View>
            <View style={styles.otherDataView}>
						  <FontAwesome5 name={"info"} size={25} style={{textAlign:'center', color:'#000' }} />
              <Text style={styles.iconDashboard}>
                {"Complementary Data"}
              </Text>
              <Divider style={{ backgroundColor: '#bfbfbf' }} />
              <FlatList
                data={this.state.other_data}
                keyExtractor={item => item.id.toString()}
                numColumns={3}
                renderItem={
                  ({ item }) => {
                      return (
                        <View style={(item.answer!="?" ? styles.item : styles.itemNoInformation)}>
                          <TouchableOpacity style={styles.name}  /*onPress={() => this.getAnswersQuestion(item.id)}*/ >
                            <Text style={styles.questionFormat}>
                              {item.label_question}
                            </Text>
                            <Text style={styles.answerFormat}>
                              {item.answer}
                            </Text>
                            <Text style={styles.dateLastAnswerFormat}>
                              {item.createdAt}
                            </Text>
                          </TouchableOpacity>
                        </View> 
                      )  
                  }
                }
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
    },
  item: {
    // alignItems: "flex-start",
    backgroundColor: "#00CC00",
    flexGrow: 1,
    margin: 4,
    padding: 5,
    flexBasis: 0,
  },
  itemNoInformation: {
    backgroundColor: "#bfbfbf",
    flexGrow: 1,
    margin: 4,
    padding: 5,
    flexBasis: 0,
  },
  questionFormat: {
    fontSize: 7,
    fontWeight: 'normal',
    textAlign: 'center',
  },
  answerFormat: {
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  dateLastAnswerFormat: {
    fontSize: 7,
    fontWeight: 'normal',
    textAlign: 'center',
  },
  text: {
    color: "#333333"
  },
  informationText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'left',
  },
  containerLabel: {
    backgroundColor: "#f2f2f2",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  containerInfo: {
    backgroundColor: "#f2f2f2",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },

  maternalDataView: {
    backgroundColor: "#e7e7e7",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },

  fetalDataView: {
    backgroundColor: "#e7e7e7",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },

  admissionDataView: {
    backgroundColor: "#e7e7e7",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },

  otherDataView: {
    backgroundColor: "#e7e7e7",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },

  warnningAdmissionView: {
    backgroundColor: "#f08000",
    // flexGrow: 1,
    margin: 4,
    padding: 2,
    // flexBasis: 1,
    flex: 9,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },

  warnningAdmissionText: {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  iconDashboard: {
    fontSize: 11,
    fontWeight: 'bold',
    textAlign: 'center',
  }


});


export default Dashboard;
