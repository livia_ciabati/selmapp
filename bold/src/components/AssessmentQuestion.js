import React, { Component } from 'react';
import { FormLabel, Input } from 'react-native-elements'
import { StyleSheet,Text,View,FlatList, Alert, Dimensions, BackHandler } from 'react-native';
import Toast from 'react-native-easy-toast'

import { ButtonGroupSection, InputMasked  } from './common';
import { Navigation } from 'react-native-navigation';
import { closeSidebar } from '../navigation';
import assessmentOptionsRepository from '../repositories/assessmentOptionsRepository';
import assessmentResultRepository from '../repositories/assessmentResultRepository';
import AssessmentQuestionRepository from '../repositories/assessmentQuestionsRepository';
import { getCurrentTimeToDatabase } from '../repositories/BaseRepository';
import assessmentAnswerInfoRepository from '../repositories/assessmentAnswerInfoRepository';

import { Card, CardSection } from './common';
import { Loader } from './common/Loader';
import { DateTimeInputMask }  from './common/DateTimeInputMask';


class AssessmentQuestion extends Component {

	constructor(props) {
		super(props)

		this.state = {
			questionList: [], formLabel: '', admissionId: null,
			assessmentAnswerInfo: { 
				id: undefined, begin_fill_form: "", 
				end_fill_form: "", assessment_form_id: "" 
			}
		}
		
		this.navigationEventListener = Navigation.events().bindComponent(this);
		this.updateIndex = this.updateIndex.bind(this);
		this.checkIfFormIsCompleted = this.checkIfFormIsCompleted.bind(this);
	}
	
	showLoading = () => {    
		this.setState({animating: true});
	}

	hideLoading = () => {
		this.setState({animating: false});
	} 

	/************************* COMPONENTS LIVE CYCLE *****************************/
	componentDidMount()
	{
		this.showLoading();
		////console.log("PROPS" + JSON.stringify(this.props));
		if(this.props.adm_id != undefined && this.props.adm_id > 0 && this.props.question_id != undefined){
			AssessmentQuestionRepository.getAssessmentsQuestionById(this.props.question_id)
			.then((question) => {
				////console.log("QUESTION: " + JSON.stringify(question));
				this.setQuestionList(question);			
				this.hideLoading();
			});
		}
		BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
		
		////console.log("NAVIGATION UNMOUNTED;")
		if (this.navigationEventListener) {
			this.navigationEventListener.remove();
		}
		return false;
	}
			
	
	/************************* NAVIGATION HANDLERS *****************************/
	
	// navigationButtonPressed({ backButton }) {
	// 	//console.log("NAVIGATION BUTTON PRESSED;")
	// 	//this.checkIfFormIsCompleted();
	// }

	onBackPress = () => {
		//return this.checkIfFormIsCompleted();
	}

	onNavigatorEvent(event) {
		//console.log("NAVIGATION BUTTON PRESSED 1;")
		if (event.id === 'backPress') {
			//console.log("NAVIGATION BUTTON PRESSED 2;")
		}
	}

	navigationButtonPressed({buttonId}) {
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId == 'sidebarButton') {
			closeSidebar();
		}else if(buttonId == 'backButton') {
			this.checkIfFormIsCompleted();
		}
	}


	/************************* CUSTOM FUNCTIONS *****************************/
	setQuestionList(data){
		this.setState({	questionList: data.questionList });		
		if(data.questionList != undefined && data.questionList.length > 0)
		{
			this.setState({ formId : data.questionList[0].formId });
		}
		this.hideLoading();
	}
	
	//Confirme if all questions was answered
		//If completed, update time of end form
		//Else confirm is want to leave the form
	checkIfFormIsCompleted()
	{
		var completedForm = this.state.question.every((element, index, array) => {
			////console.log("ELEMENTO: " + JSON.stringify(element));
			element['answer'] != undefined;

		});
		
		if(!completedForm)
		{
			Alert.alert(
				'Incompleted assessment',
				"There are questions not answered. This can affect the evaluations and support decision. Are you sure you want to finish?",
				[
					{					
						text: 'No', onPress: () => { console.log('Ask me later pressed'); }
					},
					{
						text: 'Yes', 
						onPress: () => { 
							assessmentAnswerInfoRepository.updateAssessmentTimeEndFillAnswerInfo(this.state.assessmentAnswerInfo.id);
							AssessmentScheduleRepository.setAssessmentScheduleAsFilledIn(this.state.scheduled_id)
							console.log("TESTE 1");
							//Navigation.pop('WomanList');
							Navigation.pop(this.props.componentId);
						}
					}
				],
				{ cancelable: false }
			);
			return true;
		}
		else
		{
			assessmentAnswerInfoRepository.updateAssessmentTimeEndFillAnswerInfo(this.state.assessmentAnswerInfo.id);
			return false;		
		}
	}

	//Update the active button on custom form
	updateIndex(selectedIndex, questionId) {
		//adjustment to copy json
		var indexQuestion = this.state.questionList.map(function (e) { return e.questionId; }).indexOf(questionId);
		
		var itens = Object.assign([], this.state.questionList, []);

		itens[indexQuestion].selectedOptionId = itens[indexQuestion].options[selectedIndex].optionId;
		itens[indexQuestion].answer           = itens[indexQuestion].options[selectedIndex].label_reply;
		itens[indexQuestion].selectedIndex    = selectedIndex;
		
		this.setState({ questionList: itens });
		this.saveAssessmentResult(itens[indexQuestion].answer, itens[indexQuestion].questionId, itens[indexQuestion].selectedOptionId);
	}


	onChangeTextWithValidation(value, min, max)
	{
		if(value>max){

		}
		else if (value<min){
		}

	}

	onEndEditing(answer, question_id)
	{
		var indexQuestion = this.state.questionList.map(function (e) { return e.questionId; }).indexOf(question_id);
			
		var itens = Object.assign([], this.state.questionList, []);

		itens[indexQuestion].selectedOptionId = null;
		itens[indexQuestion].answer           = answer;
		itens[indexQuestion].selectedIndex    = null;
		
		this.setState({ questionList: itens });

		this.saveAssessmentResult(answer, question_id, null);
	}
	
	saveAssessmentResult(answer, question_id, assessment_options_id)
	{	      
		var assessmentResult = {
			answer: answer,                 
			admission_id: this.props.adm_id,            
			assessment_questions_id: question_id, 
			assessment_options_id: assessment_options_id,         
			forms_id: this.state.formId,                
			userBoldId: this.props.user_id,
			assessment_answer_info_id: this.assessment_answer_info_id 
		} 
		 
		if(this.state.assessmentAnswerInfo.id == undefined) 
		{
			assessmentAnswerInfo = {  
				begin_fill_form: getCurrentTimeToDatabase(), 
				end_fill_form: "", 
				assessment_form_id: this.state.formId,
				admission_id: this.props.adm_id
			}

			assessmentAnswerInfoRepository.insertassessmentAnswerInfo(assessmentAnswerInfo)
			.then((data) => {
				console.log("APOS SALVAR O INFO " + JSON.stringify(data))
				this.setState({assessmentAnswerInfo: data});
				assessmentResult.assessment_answer_info_id = data.id;
				this.callInsertAssessmentResult(assessmentResult);
			});
		}
		else{
			assessmentResult.assessment_answer_info_id = this.state.assessmentAnswerInfo.id;
			console.log("ASSESSMENT RESULT " + JSON.stringify(assessmentResult))
			this.callInsertAssessmentResult(assessmentResult);
		}
	}

	callInsertAssessmentResult(assessmentResult){
		assessmentResultRepository.insertassessmentResult(assessmentResult)
		.then((data) => {			
			this.refs.toast.show('Saved', 500);
		});
	}



	render() {
		const { questionList } = this.state
		return (
			<View style={styles.container}>
		        <Toast ref="toast" position='top'/>		                
					<FlatList
						style = {{ width: dimensions.width }}
						data={questionList}
						keyExtractor={item => item.questionId.toString()}
						renderItem={({ item }) => {
							////console.log("ITEM " + (item.minimum_value) + " " + item.maximum_value)
							////console.log("ITEM " + JSON.stringify(item))
							return (							
								<CardSection style={{flex:1}}>
									{
										(item.options != null && item.options[0].optionId != null) ?
											<ButtonGroupSection
												label={item.label_question}
												id={item.questionId}
												onPress={(selectedIndex) => this.updateIndex(selectedIndex, item.questionId)}
												selectedIndex={item.selectedIndex}
												buttons={item.options.map((option) => { return (<Text> {option.label_reply} </Text>) })}
												containerStyle={{ height: null }}
												vertical={ (item.options.length <= 4 ? false : true) } 
											/>
											: (parseInt(item.type) <= 2 ) ?
											<InputMasked
												keyboardType="numeric"
												label={item.label_question}
												id={item.questionId} 
												onChangeText={(id) => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												minValue={item.minimum_value}
												maxValue={item.maximum_value}
											/>
											: (parseInt(item.type) == 4) ?
											<DateTimeInputMask
												label={item.label_question}
												id={item.questionId} 
												options={{ format: 'DD/MM/YYYY HH:mm' }}	
												placeholder="dd-MM-yyyy hh:mm"
												value={ item.answer == undefined ? "" : `${item.answer}` }
												onChangeText={() => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												errorMessage={""}
												/>
											: (parseInt(item.type) == 5) ?
											<DateTimeInputMask
												label={item.label_question}
												id={item.questionId} 
												options={{ format: 'DD-MM-YYYY' }}		
												placeholder="dd-MM-yyyy"
												value={ item.answer == undefined ? "" : `${item.answer}` }
												onChangeText={() => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												errorMessage={""}
												/>
											: (parseInt(item.type) == 6) ? 
											<DateTimeInputMask
												label={item.label_question}
												id={item.questionId} 
												options={{ format: 'HH:mm' }}	
												placeholder="HH:mm"		
												value={ item.answer == undefined ? "" : `${item.answer}` }
												onChangeText={() => {}}
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												errorMessage={""}
												/>													
											:
											<InputMasked
												label={item.label_question}
												id={item.questionId} 
												onEndEditing={(value) => this.onEndEditing(value.nativeEvent.text, item.questionId) }
												minValue={item.minimum_value}
												maxValue={item.maximum_value}
											/>

									}
								</CardSection>
							)
						}}
					/>	
				<Loader animating = { this.state.animating } />
			</View>
		);
	}
}


const dimensions = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#F5FCFF',
		alignSelf: 'stretch'
	},
	welcome: {
		fontSize: 20,
		textAlign: 'center',
		margin: 10,
	},
	instructions: {
		textAlign: 'center',
		color: '#333333',
		marginBottom: 5,
	},
	datetimeStyle: {
		width: '80%', 
		fontSize: 16,
		marginLeft: 5
	}
});

export default AssessmentQuestion;
