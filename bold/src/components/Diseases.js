import React, { Component } from 'react';
import { Button, Card, CardSection, ButtonGroupSection, InputMasked  } from './common';
import { ScrollView, View, Text, Image, StyleSheet } from 'react-native';
import { FormLabel, ButtonGroup } from 'react-native-elements';
import Toast from 'react-native-easy-toast';
import DiseasesRepository from '../repositories/diseaseRepository';

class Diseases extends Component {

  constructor(props) {
    super(props)
    this.updateIndex = this.updateIndex.bind(this);
  }

  updateIndex(stateVariable, selectedIndex) {
    this.state[stateVariable] = selectedIndex;
    var newState = Object.assign({}, this.state, {});
    this.setState(newState);

    this.saveDiseases();
  }

  componentDidMount(){
    //console.log("Diseases componentDidDisappear");
    if(this.props.route.disea_id != undefined && this.props.route.disea_id > 0 )
    {
      this.state.id = this.props.route.disea_id;   
      DiseasesRepository.getDiseasesById(this.props.route.disea_id)
      .then((data) => {
        //console.log("Diseases DATA: " + JSON.stringify(data))        
        this.setState(data);
        this.props.route.disea_id = data.id;
      })
    }
  }

	componentDidAppear() {    
		//console.log("Diseases componentDidAppear");
  }
  
	componentDidDisappear() {    
		//console.log("Diseases componentDidDisappear");
  }
  
  saveDiseases() {
    this.state.enabled = 1;
    this.state.modified_by_user_id = this.props.route.user_id;
    this.state.userBoldId = this.props.route.user_id;

    ////console.log("NEW STATE " + JSON.stringify(this.state))
    
    if(this.props.route.disea_id != undefined && this.props.route.disea_id > 0) {
      this.state.id = this.props.route.disea_id;
    }
    
    if(this.props.route.preg_id != undefined && this.props.route.preg_id > 0) {
      this.state.pregnancy_id = this.props.route.preg_id;
    }
    
    DiseasesRepository.updateDiseases(this.state)
    .then((data) => {
      this.refs.toast.show('Updated', 500);
    });
  }

  state = {    
  }


  render() {
    return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView>
        </ScrollView>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  }
});

export default Diseases;