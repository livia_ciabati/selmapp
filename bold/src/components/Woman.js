import React, { Component } from 'react';
import { Button, Card, CardSection, ButtonGroupSection, InputMasked } from './common';
import { ScrollView, View, StyleSheet, ActivityIndicator } from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import Toast from 'react-native-easy-toast'
import moment from 'moment';
import { Loader } from './common/Loader';
import { DateTimeInputMask }  from './common/DateTimeInputMask';
import { Input, Text } from 'react-native-elements';
import { closeSidebar } from '../navigation';

import { calculateAge } from '../repositories/BaseRepository'
import WomanRepository from '../repositories/WomanRepository';
import AdmissionRepository from '../repositories/AdmissionRepository';
import PregnancyRepository from '../repositories/PregnancyRepository';
import antenatalCareRepository from '../repositories/antenatalCareRepository';
import diseaseRepository from '../repositories/diseaseRepository';

class Woman extends Component {

  constructor(props) {
    super(props)
    this.updateIndex = this.updateIndex.bind(this);

    this.props.woman_id = this.props.route.woman_id;
    //console.log("WOMAN PROPS " + JSON.stringify(props));
    this.state = {  
      name : "",
      animating: false,
      backgroundColor:'#F5FCFF88',
      height: 0 
    }
  }
  
  showLoading = () => {    
    this.setState({animating: true});
  }

  hideLoading = () => {
      this.setState({animating: false});
  } 
	
	navigationButtonPressed({buttonId}) {
		if (buttonId === 'cancelBtn') {
			Navigation.dismissModal(this.props.componentId);
		} else if (buttonId === 'saveBtn') {
			alert('saveBtn');
		}else if(buttonId === 'sidebarButton') {
			closeSidebar();
		}else if(buttonId === 'backButton'){
			this.checkIfFormIsCompleted();
		}
	}

  updateIndex(stateVariable, selectedIndex) {
    //computed property name
    this.state[stateVariable] = selectedIndex;
    var newState = Object.assign({}, this.state, {});
     
    this.setState(newState);

    this.saveWoman();
  }

  componentDidMount(){
    this.showLoading();
    if(this.props.route.woman_id != undefined && this.props.route.woman_id > 0)
    {
      WomanRepository.getWomanById(this.props.route.woman_id)
      .then((data) => {
        //console.log(JSON.stringify(data));
        this.setState(data);
        this.hideLoading();
      });
    }
    else{
      this.hideLoading();
    }
  }

  saveWoman() {
    this.state.enabled = 1;
    this.state.modified_by_user_id = this.props.route.user_id;
    this.state.userBoldId = this.props.route.user_id;
    
    if(this.props.route.woman_id) 
    {
      this.state.id = this.props.route.woman_id;
      WomanRepository.updateWoman(this.state)
        .then((data) => {
          this.refs.toast.show('Updated', 500);
        });
    }
    else 
    {       
      WomanRepository.insertWoman(this.state)
      .then((data) => 
      {
        this.setState(data);
        this.props.route.woman_id = data.id;

        pregnancy = { gravidity: 1, parity: 0, num_previous_abort: 0, n_previous_induced_abort: 0, 
            n_previous_spontaneous_abort: 0, num_previous_c_section: 0, previous_uterine_surgery: null, 
            n_previous_stillbirths: null, outcome_last_childbirth: null, childbirth_last_delivery: null, referred: null, 
            estimate_gestional_age: null, estimate_gestional_age_method: null, date_of_last_mensturation: null, 
            expected_delivery_date: null, userBoldId: this.state.userBoldId, 
            woman_id : data.id };

        PregnancyRepository.insertPregnancy(pregnancy)
        .then((data) => 
        {
          this.props.route.preg_id = data.id;

          admission = { 
            mode_onset_labour: null, hours_labour_before_admission: null, fetal_movements_last_2_hours: null, 
              first_stage_labour: null, fetal_death: null, advanced_first_stage_labour: null, multiple_pregnancy: null, 
              gestational_age_less_34_week: null, elective_c_section: null, pre_labour_c_section: null, 
              emergency_c_section_or_laparotomy: null, attemped_labour_induction: null, 
              false_labour: null, non_emancipated_minors_without_guardian: null, unable_give_consent_or_health_problem: null, 
              elegible: null, symphysis_fundal_height: null, abdominal_circumference: null, 
              userBoldId: this.state.userBoldId, admission_status: null, pregnancy_id: data.id }
          AdmissionRepository.insertAdmission(admission).then((data) => {
            this.props.route.adm_id = data.id;
          });

          antenatalCare = {
            weight: "", height: "", placenta_praevia: "", accreta_increta_percreta: "", abruptio_placentae: "", other_obstetric_haemorrhage: "",
            pre_eclampsia: "", eclampsia: "", pyelonephritis: "", malaria: "", preterm_rupture_membranes: "", anaemia: "",
            gestational_diabetes: "", other_pregnancy_complications: "", pregnancy_id: data.id, userBoldId: this.state.userBoldId 
          }

          antenatalCareRepository.insertantenatalCare(antenatalCare).then((data) => {
            this.props.route.anc_id = data.id;
          });

          disease = {   
            chronic_hypertension: "", diabetes_mellitus: "", hiv: "", aids_hiv: "",
            chronic_anaemia: "", obesity: "", heart_disease: "", lung_disease: "",
            renal_disease: "",other_chronic_disease: "", n_antenatal_care: "", neurologic_disease: "",
            liver_disease: "", cancer: "",pregnancy_id: data.id,userBoldId: this.state.userBoldId  
          }
          
          diseaseRepository.insertdisease(disease).then((data) => {
            this.props.route.disea_id = data.id;
          })

          this.refs.toast.show('Saved', 500);
        });
      }); 
    }
  }
  
  render() {
		return (
      <View style={styles.container}>
        <Toast ref="toast" position='top'/>
        <ScrollView>
          <Card>
                <CardSection>
                    <Input
                        label="Name"
                        value={this.state.name }
                        onChangeText={ name => this.setState({ name : name })} 
                        onEndEditing={this.saveWoman.bind(this) }                       
                    />                    
                </CardSection>                    
                <CardSection>  
                  <DateTimeInputMask 
                    label="Birthday"
                    value={ this.state.birthday == undefined ? "" : `${this.state.birthday}` }
                    options={{ format: 'DD-MM-YYYY' }}		
                    placeholder="dd-MM-yyyy"
                    onChangeText={ birthday => {
                      if(birthday.length == 10){
                          this.setState({'dateBirthdayError': null});
                      } else {
                        this.setState({'dateBirthdayError':'Please, provide a valid date.'});
                      }
                      this.setState({'birthday': birthday});
                      if(birthday.length == 10){
                        age = calculateAge(birthday); 
                        this.state.age = age;
                        this.setState({'age': age});
                      }
                    }} 
                    onEndEditing={ (value) => {
                      if(value.nativeEvent.text.length == 10){
                        age = calculateAge(value.nativeEvent.text); 
                        this.state.age = age;
                        this.setState({'age': age});
                      }
                      this.saveWoman(); 
                    }}
                    errorMessage={this.state.dateBirthdayError}
                    />
                </CardSection>

                <CardSection>
                    <InputMasked
                        maskType={'only-numbers'}
                        placeholder=""
                        label="Age"
                        value={ this.state.age == undefined ? "" : `${this.state.age}` }
                        keyboardType="numeric"
                        onChangeText={ age => { this.setState({'age': `${age}`}); } }
                        onEndEditing={this.saveWoman.bind(this) }                            
                        minValue={10}
                        maxValue={55}
                        errorMessage={this.state.ageError}
                      />
                </CardSection>
                <CardSection>
                    <ButtonGroupSection 
                        label="Marital Status" 
                        onPress={ (i) => { this.updateIndex('marital_status', i) } }
                        selectedIndex={parseInt(this.state.marital_status)} 
                        buttons={[
                          'Married / Cohabitating',
                          'Single / Separated / Divorced / Widowed'
                        ]}
                        vertical={false}
                       
                    />
                </CardSection>
                <CardSection>
                    <ButtonGroupSection 
                        label="Gainful Occupation"
                        onPress={ (i) => { this.updateIndex('gainful_occupation', i) } }
                        selectedIndex={parseInt(this.state.gainful_occupation)} 
                        buttons={[
                          'No',
                          'Yes'
                        ]}
                        vertical={false}
                    />
                </CardSection>
                <CardSection>
                    <ButtonGroupSection 
                        label="Education Level"
                        onPress={ (i) => { this.updateIndex('education_level', i) } }
                        selectedIndex={parseInt(this.state.education_level)} 
                        buttons={[
                          'No education',
                          'Pre-primary education',
                          'Incomplete primary education',
                          'Complete primary education',
                          'Incomplete secondary education',
                          'Complete secondary education',
                          'Incomplete post-secondary/tertiary education',
                          'Complete post-secondary/tertiary or higher education',
                          'Other (e.g. Quranic / Nomadic education only)'
                        ]}
                    />
                </CardSection>            
          </Card>
        </ScrollView>
      
        <Loader animating = { this.state.animating } />
      </View>

    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#F5FCFF'
  },
  tabBar: {
    flexDirection: 'row',
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    padding: 16,
  },
  tabIcon: {
    flex: 1,
    alignItems: 'flex-end',
    padding: 16,
  },
  buttonGroupContainer: {
    flexDirection: 'column',
    flex:1,
    height: null
  },
  buttonGroupButton: {
    width: 'auto',
    height: 'auto',
    padding: 10,
    borderWidth: 1,
    borderColor: "#FFFFFF"
  },
  label: {
    fontSize: 16, 
    fontWeight: 'bold',
    color: '#86939e',
    marginLeft: 10
  }
});

export default Woman;
