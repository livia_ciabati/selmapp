import React, { Component } from 'react';
import {BackHandler} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';
import assessmentAnswerInfoRepository from '../repositories/assessmentAnswerInfoRepository';
import  { getCurrentTimeToDatabase, formatDateToTimelinefromDatabase } from '../repositories/BaseRepository';

class TimelineAssessment extends Component{

    constructor(){
        super()
        this.state = {timeline : [  ]};
        ////console.log("BEFORE UPDATE TIMELINE: " + JSON.stringify(this.state.timeline));
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
        //console.log("COMPONENT WILL UNMOUNT WRAPPED ELEMENT" + JSON.stringify(this.props));
    }
    
    backHandler = () => {
        return false;
    }

    componentDidMount(){
        assessmentAnswerInfoRepository.getassessmentAnswerInfoByAdmissionId(this.props.route.adm_id)
        .then((data) => 
        {   
            var info = [];
            for (let i = 0; i < data.length; i++) {
                info.push({time: formatDateToTimelinefromDatabase(data[i].begin_fill_form), title: data[i].label_form, description: ""});
            }     
            
            this.setState({timeline: info})
        })
    }

    render(){
        return(
            <Timeline
                data={this.state.timeline}
            />
        )
    }
}

export default TimelineAssessment;