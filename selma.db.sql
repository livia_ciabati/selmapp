" BEGIN TRANSACTION;                                                                 "+
" CREATE TABLE IF NOT EXISTS `woman` (                                               "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`enabled`	TEXT,                                                                "+
" 	`modified_by_user_id`	TEXT,                                                    "+
" 	`birthday`	TEXT,                                                                "+
" 	`age`	TEXT,                                                                    "+
" 	`marital_status`	TEXT,                                                        "+
" 	`education_level`	TEXT,                                                        "+
" 	`gainful_occupation`	TEXT,                                                    "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `questionsOptions` (                                    "+
" 	`assessment_questions_id`	TEXT,                                                "+
" 	`assessment_options_id`	TEXT,                                                    "+
" 	`id`	TEXT                                                                     "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `questionsForms` (                                      "+
" 	`assessment_questions_id`	TEXT,                                                "+
" 	`assessments_forms_id`	TEXT,                                                    "+
" 	`order_question_number`	TEXT,                                                    "+
" 	`id`	TEXT                                                                     "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `pregnancy` (                                           "+
" 	`id`	TEXT,                                                                    "+
" 	`gravidity`	TEXT,                                                                "+
" 	`parity`	TEXT,                                                                "+
" 	`num_previous_abort`	TEXT,                                                    "+
" 	`n_previous_induced_abort`	TEXT,                                                "+
" 	`n_previous_spontaneous_abort`	TEXT,                                            "+
" 	`num_previous_c_section`	TEXT,                                                "+
" 	`previous_uterine_surgery`	TEXT,                                                "+
" 	`n_previous_stillbirths`	TEXT,                                                "+
" 	`outcome_last_childbirth`	TEXT,                                                "+
" 	`childbirth_last_delivery`	TEXT,                                                "+
" 	`referred`	TEXT,                                                                "+
" 	`estimate_gestional_age`	TEXT,                                                "+
" 	`estimate_gestional_age_method`	TEXT,                                            "+
" 	`date_of_last_mensturation`	TEXT,                                                "+
" 	`expected_delivery_date`	TEXT,                                                "+
" 	`woman_id`	TEXT,                                                                "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `neonatalOutcomes` (                                    "+
" 	`id`	TEXT,                                                                    "+
" 	`final_mode_delivery`	TEXT,                                                    "+
" 	`fetal_presentation_delivery`	TEXT,                                            "+
" 	`infant_sex`	TEXT,                                                            "+
" 	`birth_weight`	TEXT,                                                            "+
" 	`condition_vital_status`	TEXT,                                                "+
" 	`condition_apgar_5min`	TEXT,                                                    "+
" 	`morbidity_admission_ICU`	TEXT,                                                "+
" 	`morbidity_intubation`	TEXT,                                                    "+
" 	`morbidity_nasal_CPAP`	TEXT,                                                    "+
" 	`morbidity_surfactant_administration`	TEXT,                                    "+
" 	`morbidity_cardio_pulmonary_resuscitation`	TEXT,                                "+
" 	`morbidity__use_vasoactive_drug`	TEXT,                                        "+
" 	`morbidity_use_anticonvulsants`	TEXT,                                            "+
" 	`morbidity_use_phototerapy_first_24hours`	TEXT,                                "+
" 	`morbidity_any_blood_products`	TEXT,                                            "+
" 	`morbidity_use_steroids`	TEXT,                                                "+
" 	`morbidity_therapeutic_intravenous_antibiotics`	TEXT,                            "+
" 	`morbidity_any_surgery`	TEXT,                                                    "+
" 	`morbidity_any_severe_malformation`	TEXT,                                        "+
" 	`status_hospital_discharge`	TEXT,                                                "+
" 	`discharge_transfer_death_date`	TEXT,                                            "+
" 	`discharge_transfer_death_hour`	TEXT,                                            "+
" 	`admission_id`	TEXT,                                                            "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `maternalOutcomes` (                                    "+
" 	`id`	TEXT,                                                                    "+
" 	`dystocia_labour_obstruction`	TEXT,                                            "+
" 	`dystocia_first_stage_labour`	TEXT,                                            "+
" 	`dystocia_prolonged_second_stage`	TEXT,                                        "+
" 	`haemorrhage_placenta_praevia`	TEXT,                                            "+
" 	`haemorrhage_accreta_increta_percreta_placenta`	TEXT,                            "+
" 	`haemorrhage_abruptio_placenta`	TEXT,                                            "+
" 	`haemorrhage_ruptured_uterus`	TEXT,                                            "+
" 	`haemorrhage_intrapartum`	TEXT,                                                "+
" 	`haemorrhage_postpartum`	TEXT,                                                "+
" 	`infection_puerperal_endometritis`	TEXT,                                        "+
" 	`infection_puerperal_sepsis`	TEXT,                                            "+
" 	`infection_wound`	TEXT,                                                        "+
" 	`infection_systemic_septicaemia`	TEXT,                                        "+
" 	`hypertension_severe`	TEXT,                                                    "+
" 	`hypertension_pre_eclampsia`	TEXT,                                            "+
" 	`hypertension_eclampsia`	TEXT,                                                "+
" 	`other_embolic_disease`	TEXT,                                                    "+
" 	`otherPotentiallyLifeThreateningCondition`	TEXT,                                "+
" 	`conditions_during_hospital_stay`	TEXT,                                        "+
" 	`cardiovascular_dysfunction`	TEXT,                                            "+
" 	`respiratory_dysfunction`	TEXT,                                                "+
" 	`renal_dysfunction`	TEXT,                                                        "+
" 	`coagulation_dysfunction`	TEXT,                                                "+
" 	`hepatic_dysfunction`	TEXT,                                                    "+
" 	`neurologic_dysfunction`	TEXT,                                                "+
" 	`uterine_dysfunction`	TEXT,                                                    "+
" 	`dysfunction_identified`	TEXT,                                                "+
" 	`nmm_haemorrhage_oxytocin_treatment_PPH`	TEXT,                                "+
" 	`nmm_haemorrhage_misoprostol_treatment_PPH`	TEXT,                                "+
" 	`nmm_haemorrhage_ergotamine_treatment_PPH`	TEXT,                                "+
" 	`nmm_haemorrhage_other_uterotonic_treatment_PPH`	TEXT,                        "+
" 	`nmm_haemorrhage_artery_ligation_or_embolization`	TEXT,                        "+
" 	`nmm_haemorrhage_balloon_or_condom_tamponade`	TEXT,                            "+
" 	`nmm_haemorrhage_repair_cervical_laceration`	TEXT,                            "+
" 	`nmm_haemorrhage_repair_uterine_rupture`	TEXT,                                "+
" 	`nmm_haemorrhage_b_lynch_suture`	TEXT,                                        "+
" 	`nmm_haemorrhage_hysterectomy`	TEXT,                                            "+
" 	`nmm_infection_therapeutic_antibiotics`	TEXT,                                    "+
" 	`nmm_hypertension_magnesium_sulphate_as_anticonvulsant`	TEXT,                    "+
" 	`nmm_hypertension_other_anticonvulsant`	TEXT,                                    "+
" 	`nmm_other_removal_retained_products`	TEXT,                                    "+
" 	`nmm_other_manual_removal_placenta`	TEXT,                                        "+
" 	`nmm_other_blood_transfusion`	TEXT,                                            "+
" 	`nmm_other_laparotomy`	TEXT,                                                    "+
" 	`nmm_other_admission_ICU`	TEXT,                                                "+
" 	`intervention_identified`	TEXT,                                                "+
" 	`referred_higher_complexity_hospital`	TEXT,                                    "+
" 	`status_hospital_discharge`	TEXT,                                                "+
" 	`discharge_transfer_death_date`	TEXT,                                            "+
" 	`admission_id`	TEXT,                                                            "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `interventionParameters` (                              "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`type`	TEXT,                                                                    "+
" 	`assessment_questions_id`	TEXT                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `hospital` (                                            "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`site`	TEXT,                                                                    "+
" 	`telephone`	TEXT,                                                                "+
" 	`address`	TEXT,                                                                "+
" 	`county`	TEXT,                                                                "+
" 	`city`	TEXT,                                                                    "+
" 	`state`	TEXT,                                                                    "+
" 	`country`	TEXT,                                                                "+
" 	`zip_code`	TEXT,                                                                "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`number`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `disease` (                                             "+
" 	`id`	TEXT,                                                                    "+
" 	`chronic_hypertension`	TEXT,                                                    "+
" 	`diabetes_mellitus`	TEXT,                                                        "+
" 	`hiv`	TEXT,                                                                    "+
" 	`aids_hiv`	TEXT,                                                                "+
" 	`chronic_anaemia`	TEXT,                                                        "+
" 	`obesity`	TEXT,                                                                "+
" 	`heart_disease`	TEXT,                                                            "+
" 	`lung_disease`	TEXT,                                                            "+
" 	`renal_disease`	TEXT,                                                            "+
" 	`other_chronic_disease`	TEXT,                                                    "+
" 	`n_antenatal_care`	TEXT,                                                        "+
" 	`neurologic_disease`	TEXT,                                                    "+
" 	`liver_disease`	TEXT,                                                            "+
" 	`cancer`	TEXT,                                                                "+
" 	`pregnancy_id`	TEXT,                                                            "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `curves` (                                              "+
" 	`0`	TEXT,                                                                        "+
" 	`1`	TEXT,                                                                        "+
" 	`2`	TEXT,                                                                        "+
" 	`3`	TEXT,                                                                        "+
" 	`4`	TEXT,                                                                        "+
" 	`5`	TEXT,                                                                        "+
" 	`6`	TEXT,                                                                        "+
" 	`7`	TEXT,                                                                        "+
" 	`8`	TEXT,                                                                        "+
" 	`9`	TEXT,                                                                        "+
" 	`10`	TEXT,                                                                    "+
" 	`11`	TEXT,                                                                    "+
" 	`12`	TEXT,                                                                    "+
" 	`13`	TEXT,                                                                    "+
" 	`14`	TEXT,                                                                    "+
" 	`15`	TEXT,                                                                    "+
" 	`16`	TEXT,                                                                    "+
" 	`17`	TEXT,                                                                    "+
" 	`18`	TEXT,                                                                    "+
" 	`19`	TEXT,                                                                    "+
" 	`20`	TEXT,                                                                    "+
" 	`21`	TEXT,                                                                    "+
" 	`22`	TEXT,                                                                    "+
" 	`23`	TEXT,                                                                    "+
" 	`24`	TEXT,                                                                    "+
" 	`25`	TEXT,                                                                    "+
" 	`26`	TEXT,                                                                    "+
" 	`27`	TEXT,                                                                    "+
" 	`28`	TEXT,                                                                    "+
" 	`29`	TEXT,                                                                    "+
" 	`30`	TEXT,                                                                    "+
" 	`group`	TEXT,                                                                    "+
" 	`intervention`	TEXT,                                                            "+
" 	`curve`	TEXT,                                                                    "+
" 	`start`	TEXT                                                                     "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `bold_initial_data` (                                   "+
" 	`id`	TEXT,                                                                    "+
" 	`augmentation_labour`	TEXT,                                                    "+
" 	`education_level`	TEXT,                                                        "+
" 	`axillary_temperature`	TEXT,                                                    "+
" 	`c18_temperature_c`	TEXT,                                                        "+
" 	`cervical_dilatation_cm`	TEXT,                                                "+
" 	`c18_cervical_dilatation`	TEXT,                                                "+
" 	`fetal_heart_rate_bpm`	TEXT,                                                    "+
" 	`c18_fetal_heart_rate`	TEXT,                                                    "+
" 	`maternal_heart_rate`	TEXT,                                                    "+
" 	`c18_maternal_heart_rate`	TEXT,                                                "+
" 	`num_antenatal_care_visits`	TEXT,                                                "+
" 	`systolic_blood_pressure`	TEXT,                                                "+
" 	`c18_syst_blood_pressure`	TEXT,                                                "+
" 	`caesarean_section_cs`	TEXT,                                                    "+
" 	`outcome`	TEXT,                                                                "+
" 	`robson`	TEXT                                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `assessmentsForms` (                                    "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`label_form`	TEXT,                                                            "+
" 	`version`	TEXT,                                                                "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT                                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `assessmentResult` (                                    "+
" 	`id`	TEXT,                                                                    "+
" 	`answer`	TEXT,                                                                "+
" 	`admission_id`	TEXT,                                                            "+
" 	`assessment_questions_id`	TEXT,                                                "+
" 	`assessment_options_id`	TEXT,                                                    "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`forms_id`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `assessmentQuestions` (                                 "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`label_question`	TEXT,                                                        "+
" 	`type`	TEXT,                                                                    "+
" 	`minimum_value`	TEXT,                                                            "+
" 	`maximum_value`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `assessmentOptions` (                                   "+
" 	`id`	TEXT,                                                                    "+
" 	`value`	TEXT,                                                                    "+
" 	`label_reply`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `antenatalCare` (                                       "+
" 	`id`	TEXT,                                                                    "+
" 	`weight`	TEXT,                                                                "+
" 	`height`	TEXT,                                                                "+
" 	`placenta_praevia`	TEXT,                                                        "+
" 	`accreta_increta_percreta`	TEXT,                                                "+
" 	`abruptio_placentae`	TEXT,                                                    "+
" 	`other_obstetric_haemorrhage`	TEXT,                                            "+
" 	`pre_eclampsia`	TEXT,                                                            "+
" 	`eclampsia`	TEXT,                                                                "+
" 	`pyelonephritis`	TEXT,                                                        "+
" 	`malaria`	TEXT,                                                                "+
" 	`preterm_rupture_membranes`	TEXT,                                                "+
" 	`anaemia`	TEXT,                                                                "+
" 	`gestational_diabetes`	TEXT,                                                    "+
" 	`other_pregnancy_complications`	TEXT,                                            "+
" 	`pregnancy_id`	TEXT,                                                            "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `alert` (                                               "+
" 	`id`	TEXT,                                                                    "+
" 	`title`	TEXT,                                                                    "+
" 	`text`	TEXT,                                                                    "+
" 	`link`	TEXT                                                                     "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `admissionAlerts` (                                     "+
" 	`id`	TEXT,                                                                    "+
" 	`read`	TEXT,                                                                    "+
" 	`alert_id`	TEXT,                                                                "+
" 	`admission_id`	TEXT,                                                            "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT                                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `admission` (                                           "+
" 	`id`	TEXT,                                                                    "+
" 	`mode_onset_labour`	TEXT,                                                        "+
" 	`hours_labour_before_admission`	TEXT,                                            "+
" 	`fetal_movements_last_2_hours`	TEXT,                                            "+
" 	`first_stage_labour`	TEXT,                                                    "+
" 	`fetal_death`	TEXT,                                                            "+
" 	`advanced_first_stage_labour`	TEXT,                                            "+
" 	`multiple_pregnancy`	TEXT,                                                    "+
" 	`gestational_age_less_34_week`	TEXT,                                            "+
" 	`elective_c_section`	TEXT,                                                    "+
" 	`pre_labour_c_section`	TEXT,                                                    "+
" 	`emergency_c-section_or_laparotomy`	TEXT,                                        "+
" 	`attemped_labour_induction`	TEXT,                                                "+
" 	`false_labour`	TEXT,                                                            "+
" 	`non_emancipated_minors_without_guardian`	TEXT,                                "+
" 	`unable_give_consent_or_health_problem`	TEXT,                                    "+
" 	`elegible`	TEXT,                                                                "+
" 	`symphysis_fundal_height`	TEXT,                                                "+
" 	`abdominal_circumference`	TEXT,                                                "+
" 	`pregnancy_id`	TEXT,                                                            "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT,                                                                "+
" 	`userBoldId`	TEXT,                                                            "+
" 	`admission_status`	TEXT                                                         "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `UserBold` (                                            "+
" 	`id`	TEXT,                                                                    "+
" 	`realm`	TEXT,                                                                    "+
" 	`username`	TEXT,                                                                "+
" 	`password`	TEXT,                                                                "+
" 	`email`	TEXT,                                                                    "+
" 	`emailVerified`	TEXT,                                                            "+
" 	`verificationToken`	TEXT,                                                        "+
" 	`createdAt`	TEXT,                                                                "+
" 	`updatedAt`	TEXT                                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `Rules` (                                               "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`priority`	TEXT,                                                                "+
" 	`on`	TEXT,                                                                    "+
" 	`condition`	TEXT,                                                                "+
" 	`consequence`	TEXT                                                             "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `RoleMapping` (                                         "+
" 	`id`	TEXT,                                                                    "+
" 	`principalType`	TEXT,                                                            "+
" 	`principalId`	TEXT,                                                            "+
" 	`roleId`	TEXT                                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `Role` (                                                "+
" 	`id`	TEXT,                                                                    "+
" 	`name`	TEXT,                                                                    "+
" 	`description`	TEXT,                                                            "+
" 	`created`	TEXT,                                                                "+
" 	`modified`	TEXT                                                                 "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `AssessmentSchedule` (                                  "+
" 	`id`	TEXT,                                                                    "+
" 	`schedule_time `	TEXT,                                                            "+
" 	`assessments_forms_id`	TEXT                                                     "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `AccessToken` (                                         "+
" 	`id`	TEXT,                                                                    "+
" 	`ttl`	TEXT,                                                                    "+
" 	`scopes`	TEXT,                                                                "+
" 	`created`	TEXT,                                                                "+
" 	`userId`	TEXT,                                                                "+
" 	`token`	TEXT                                                                     "+
" );                                                                                 "+
" CREATE TABLE IF NOT EXISTS `ACL` (                                                 "+
" 	`id`	TEXT,                                                                    "+
" 	`model`	TEXT,                                                                    "+
" 	`property`	TEXT,                                                                "+
" 	`accessType`	TEXT,                                                            "+
" 	`permission`	TEXT,                                                            "+
" 	`principalType`	TEXT,                                                            "+
" 	`principalId`	TEXT                                                             "+
" );                                                                                 "+
" COMMIT;                                                                            ";
