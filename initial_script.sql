
INSERT INTO `assessmentsForms` (`id`, `name`, `label_form`, `version`, `createdAt`, `updatedAt`) VALUES
(1, 'first_assessment',    'First Assessment', '1', '2018-04-16 19:00:37', '2018-04-16 19:00:37'),
(2, 'intrapartum_data', 'Intrapartum Data', '1', '2018-04-16 19:01:12', '2018-04-16 19:01:12');

INSERT INTO `assessmentQuestions` (`id`, `name`, `label_question`, `type`, `minimum_value`, `maximum_value`) VALUES
(183, 'History_Prolonged_Labour', 'Antecedent history of prolonged labour / obstructed labour', 2, 12, 123);


INSERT INTO `questionsForms` (`assessment_questions_id`, `assessments_forms_id`, `order_question_number`) VALUES
(23, 12, NULL);


